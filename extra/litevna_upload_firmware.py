#!/usr/bin/env python3

import argparse
from serial import Serial
from sys import exit
import struct

CMD_INDICATE = b"\x0d"
CMD_READ_1 = b"\x10"
CMD_READ_4 = b"\x12"
CMD_WRITE_1 = b"\x20"
CMD_WRITE_4 = b"\x22"
CMD_WRITE_FIFO = b"\x28"

CONST_INDICATE = b"2"
CONST_RESET = b"\x5e"
CONST_DEVICE_LITEVNA = b"\x02"
CONST_HARDWARE_DFU = b"\x00"
CONST_PROTOCOL_VERSION = b"\x01"
CONST_FIRMWARE_DFU = b"\xff"
CONST_FIRMWARE_BASE_ADDR = 0x08000000
CONST_FIRMWARE_V2_BASE_ADDR = CONST_FIRMWARE_BASE_ADDR + 0x00004000

REG_WRITE_ADDRESS = b"\xe0"
REG_FLASH_FIFO = b"\xe4"
REG_RESET = b"\xef"
REG_DEVICE = b"\xf0"
REG_PROTOCOL = b"\xf1"
REG_HARDWARE = b"\xf2"
REG_FIRMWARE_DFU = b"\xf3"
REG_FIRMWARE_BOOTLOADER = b"\xf4"

FIFO_CHUNK = 128

parser = argparse.ArgumentParser(prog=__file__,
                                 description="LiteVNA firmware upload")
parser.add_argument("--port", help="serial port")
parser.add_argument("--firmware", help="firmware file")
args = parser.parse_args()

with open(args.firmware, "rb") as f, Serial(args.port, baudrate=115200, timeout=3) as ser:
    ser.write(CMD_INDICATE)
    if ser.read(1) != CONST_INDICATE:
        exit("unexpected response")
    ser.write(CMD_READ_1 + REG_DEVICE)
    if ser.read(1) != CONST_DEVICE_LITEVNA:
        exit("device is not LiteVNA")
    ser.write(CMD_READ_1 + REG_PROTOCOL)
    if ser.read(1) != CONST_PROTOCOL_VERSION:
        exit("protocol version mismatch")
    ser.write(CMD_READ_1 + REG_HARDWARE)
    if ser.read(1) != CONST_HARDWARE_DFU:
        exit("hardware version mismatch")
    ser.write(CMD_READ_1 + REG_FIRMWARE_DFU)
    if ser.read(1) != CONST_FIRMWARE_DFU:
        exit("dfu version mismatch")
    ser.write(CMD_READ_1 + REG_FIRMWARE_BOOTLOADER)
    print("bootloader version: ", struct.unpack("B", ser.read(1))[0])
    flash_pos = CONST_FIRMWARE_V2_BASE_ADDR
    ser.write(CMD_WRITE_4 + REG_WRITE_ADDRESS + struct.pack("<I", flash_pos))
    while chunk := f.read(FIFO_CHUNK):
        print(f"\rProgramming address: 0x{flash_pos:08x}", end="")
        ser.write(CMD_WRITE_FIFO + REG_FLASH_FIFO + struct.pack("B", len(chunk)) + chunk)
        ser.write(CMD_INDICATE)
        if ser.read(1) != CONST_INDICATE:
            exit(f"\nunexpected response at flash position {flash_pos}")
        flash_pos += len(chunk)
    print("")
    ser.write(CMD_WRITE_1 + REG_RESET + CONST_RESET)
