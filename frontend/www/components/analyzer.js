const AnalyzerDummy = {
    props: ["choices", "attributes", "disabled"],
    template: `<div class="shadow mb-3 p-3 g-1 bg-body rounded container">
  <div class="g-2 row align-items-center">
    <div class="input-group col-12 col-md">
      <span class="input-group-text">Power level</span>
      <select v-model.number="attributes.power" class="form-select" :disabled="disabled">
        <option selected disabled value="">Power level</option>
        <option v-for="name in choices.power.choices">{{name}}</option>
      </select>
      <span class="input-group-text">\\(\\left[\\text{dBm}\\right]\\)</span>
    </div>
    <div class="col-12 col-md">
      <input v-model="attributes.smoothing" type="checkbox" id="analyzer_dummy_smoothing" class="m-1"
             :disabled="disabled">
      <label for="analyzer_dummy_smoothing">Smoothing</label>
      <input v-model="attributes.fail_on_constructor" type="checkbox" id="analyzer_dummy_fail_on_constructor" class="m-1" :disabled="disabled">
      <label for="analyzer_dummy_fail_on_constructor">Fail constructor</label>
      <input v-model="attributes.fail_on_interrupt" type="checkbox" id="analyzer_dummy_fail_on_interrupt" class="m-1" :disabled="disabled">
      <label for="analyzer_dummy_fail_on_interrupt">Fail interrupt</label>
      <input v-model="attributes.fail_on_nop" type="checkbox" id="analyzer_dummy_fail_on_nop" class="m-1" :disabled="disabled">
      <label for="analyzer_dummy_fail_on_nop">Fail NoP</label>
      <input v-model="attributes.fail_on_s_part" type="checkbox" id="analyzer_dummy_fail_on_s_part" class="m-1" :disabled="disabled">
      <label for="analyzer_dummy_fail_on_s_part">Fail S part</label>
      <input v-model="attributes.fail_on_f_left" type="checkbox" id="analyzer_dummy_fail_on_f_left" class="m-1" :disabled="disabled">
      <label for="analyzer_dummy_fail_on_f_left">Fail f left</label>
      <input v-model="attributes.fail_on_f_right" type="checkbox" id="analyzer_dummy_fail_on_f_right" class="m-1" :disabled="disabled">
      <label for="analyzer_dummy_fail_on_f_right">Fail f right</label>
      <input v-model="attributes.fail_on_f_center" type="checkbox" id="analyzer_dummy_fail_on_f_center" class="m-1" :disabled="disabled">
      <label for="analyzer_dummy_fail_on_f_center">Fail f center</label>
      <input v-model="attributes.fail_on_f_span" type="checkbox" id="analyzer_dummy_fail_on_f_span" class="m-1" :disabled="disabled">
      <label for="analyzer_dummy_fail_on_f_span">Fail f span</label>
      <input v-model="attributes.fail_on_measure" type="checkbox" id="analyzer_dummy_fail_on_measure" class="m-1" :disabled="disabled">
      <label for="analyzer_dummy_fail_on_measure">Fail measure</label>
    </div>
  </div>
</div>
`
};

const AnalyzerLiteVNA = {
    props: ["choices", "attributes", "disabled"],
    template: `<div class="shadow mb-3 p-3 g-1 bg-body rounded container">
  <div v-if="attributes.averaging > 5" class="alert alert-danger" role="alert">
    <strong>Warning:</strong> averaging value {{attributes.averaging}} may result in long measurement time
  </div>
  <div class="g-2 row align-items-center">
    <div class="col">
      <label for="litevna_power_low" class="form-label">LF power: {{attributes.power_low}}</label>
      <input v-model.number="attributes.power_low" type="range" class="form-range" id="litevna_power_low"
         :disabled="disabled" data-bs-toggle="tooltip" data-bs-placement="bottom" :title="attributes.power_low"
         :min="choices.power_low.min" :max="choices.power_low.max" :step="choices.power_low.step">
    </div>
    <div class="col">
      <label for="litevna_power_high" class="form-label">HF power: {{attributes.power_high}}</label>
      <input v-model.number="attributes.power_high" type="range" class="form-range" id="litevna_power_high"
         :disabled="disabled" data-bs-toggle="tooltip" data-bs-placement="bottom" :title="attributes.power_high"
         :min="choices.power_high.min" :max="choices.power_high.max" :step="choices.power_high.step">
    </div>
    <div class="col">
      <label for="litevna_averaging" class="form-label">Averaging: {{attributes.averaging}}</label>
      <input v-model.number="attributes.averaging" type="range" class="form-range" id="litevna_averaging"
         :disabled="disabled" data-bs-toggle="tooltip" data-bs-placement="bottom" :title="attributes.averaging"
         :min="choices.averaging.min" :max="choices.averaging.max" :step="choices.averaging.step">
    </div>
  </div>
  <div class="g-2 row align-items-center">
    <div class="col-12 col-md-6 col-lg">
      <div class="input-group" v-if="attributes.device_variant != undefined">
        <span class="input-group-text">Device variant</span>
        <input v-model="attributes.device_variant" type="number" class="form-control" readonly>
      </div>
    </div>
    <div class="col-12 col-md-6 col-lg">
      <div class="input-group col-12 col-md-6 col-lg" v-if="attributes.protocol_version != undefined">
        <span class="input-group-text">Protocol version</span>
        <input v-model="attributes.protocol_version" type="number" class="form-control" readonly>
      </div>
    </div>
    <div class="col-12 col-md-6 col-lg">
      <div class="input-group col-12 col-md-6 col-lg" v-if="attributes.hardware_revision != undefined">
        <span class="input-group-text">Hardware revision</span>
        <input v-model="attributes.hardware_revision" type="number" class="form-control" readonly>
      </div>
    </div>
    <div class="col-12 col-md-6 col-lg">
      <div class="input-group col-12 col-md-6 col-lg" v-if="attributes.firmware_version != undefined">
        <span class="input-group-text">Firmware version</span>
        <input :value="attributes.firmware_version[0] + '.' + attributes.firmware_version[1]" type="text"
               class="form-control" readonly>
      </div>
    </div>
  </div>
</div>
`
};

const AnalyzerAgilent87XX = {
    props: ["choices", "attributes", "disabled"],
    template: `<div class="shadow mb-3 p-3 g-1 bg-body rounded container">
  <div v-if="attributes.averaging > 5 || attributes.ifbw <= 100" class="alert alert-danger" role="alert">
    <div v-if="attributes.averaging > 5">
      <strong>Warning:</strong> averaging value {{attributes.averaging}} may result in long measurement time
    </div>
    <div v-if="attributes.ifbw <= 100">
      <strong>Warning:</strong> integration bandwidth value {{attributes.ifbw}} may result in long measurement time
    </div>
  </div>
  <div class="g-2 row align-items-center">
    <div class="col-12 col-md-6 col-lg-6 col-xl">
      <div class="input-group">
        <span class="input-group-text">IFBW</span>
        <select v-model.number="attributes.ifbw" class="form-select" :disabled="disabled">
          <option selected disabled value="">Integration bandwidth</option>
          <option v-for="name in choices.ifbw.choices">{{name}}</option>
        </select>
      </div>
    </div>
    <div class="col-12 col-md-6 col-lg-6 col-xl">
      <label for="agilent87xx_power" class="form-label">Power: {{attributes.power}}</label>
      <input v-model.number="attributes.power" type="range" class="form-range" id="agilent87xx_power"
         :disabled="disabled" data-bs-toggle="tooltip" data-bs-placement="bottom" :title="attributes.power"
         :min="choices.power.min" :max="choices.power.max" :step="choices.power.step">
    </div>
    <div class="col-12 col-md-6 col-lg col-xl">
      <label for="agilent87xx_averaging" class="form-label">Averaging: {{attributes.averaging}}</label>
      <input v-model.number="attributes.averaging" type="range" class="form-range" id="agilent87xx_averaging"
         :disabled="disabled" data-bs-toggle="tooltip" data-bs-placement="bottom" :title="attributes.averaging"
         :min="choices.averaging.min" :max="choices.averaging.max" :step="choices.averaging.step">
    </div>
    <div class="col-12 col-md-6 col-lg col-xl">
      <label for="agilent87xx_smoothing" class="form-label">Smoothing: {{attributes.smoothing >= 0.05 ? attributes.smoothing : 'off'}}</label>
      <input v-model.number="attributes.smoothing" type="range" class="form-range" id="agilent87xx_smoothing"
         :disabled="disabled" data-bs-toggle="tooltip" data-bs-placement="bottom" :title="attributes.smoothing"
         :min="choices.smoothing.min" :max="choices.smoothing.max" :step="choices.smoothing.step">
    </div>
    <div class="col-12 col-md-2 col-lg-1">
      <input v-model="attributes.beep" type="checkbox" id="agilent87xx_beep" class="m-1"
             :disabled="disabled">
      <label for="agilent87xx_beep">Beep</label>
    </div>
    <div class="col-12 col-md-2 col-lg-2 col-xl-1">
      <input v-model="attributes.display" type="checkbox" id="agilent87xx_display" class="m-1"
             :disabled="disabled">
      <label for="agilent87xx_beep">Display</label>
    </div>
  </div>
  <div class="g-2 row align-items-center">
    <div class="col-12 col-md-6 col-lg">
      <div class="input-group" v-if="attributes.identity != undefined">
        <span class="input-group-text">Device identity</span>
        <input v-model="attributes.identity" type="text" class="form-control" readonly>
      </div>
    </div>
    <div class="col-12 col-md-6 col-lg">
      <div class="input-group" v-if="attributes.serial != undefined">
        <span class="input-group-text">Device serial</span>
        <input v-model="attributes.serial" type="text" class="form-control" readonly>
      </div>
    </div>
  </div>
</div>
`
};

const AnalyzerAgilent506X = {
    props: ["choices", "attributes", "disabled"],
    template: `<div class="shadow mb-3 p-3 g-1 bg-body rounded container">
  <div v-if="attributes.averaging > 5 || attributes.ifbw <= 100" class="alert alert-danger" role="alert">
    <div v-if="attributes.averaging > 5">
      <strong>Warning:</strong> averaging value {{attributes.averaging}} may result in long measurement time
    </div>
    <div v-if="attributes.ifbw <= 100">
      <strong>Warning:</strong> integration bandwidth value {{attributes.ifbw}} may result in long measurement time
    </div>
  </div>
  <div class="g-2 row align-items-center">
    <div class="col-12 col-md-6 col-lg-6 col-xl">
      <div class="input-group">
        <span class="input-group-text">IFBW</span>
        <select v-model.number="attributes.ifbw" class="form-select" :disabled="disabled">
          <option selected disabled value="">Integration bandwidth</option>
          <option v-for="name in choices.ifbw.choices">{{name}}</option>
        </select>
      </div>
    </div>
    <div class="col-12 col-md-6 col-lg-6 col-xl">
      <label for="agilent87xx_power" class="form-label">Power: {{attributes.power}}</label>
      <input v-model.number="attributes.power" type="range" class="form-range" id="agilent87xx_power"
         :disabled="disabled" data-bs-toggle="tooltip" data-bs-placement="bottom" :title="attributes.power"
         :min="choices.power.min" :max="choices.power.max" :step="choices.power.step">
    </div>
    <div class="col-12 col-md-6 col-lg col-xl">
      <label for="agilent87xx_averaging" class="form-label">Averaging: {{attributes.averaging}}</label>
      <input v-model.number="attributes.averaging" type="range" class="form-range" id="agilent87xx_averaging"
         :disabled="disabled" data-bs-toggle="tooltip" data-bs-placement="bottom" :title="attributes.averaging"
         :min="choices.averaging.min" :max="choices.averaging.max" :step="choices.averaging.step">
    </div>
    <div class="col-12 col-md-6 col-lg col-xl">
      <label for="agilent87xx_smoothing" class="form-label">Smoothing: {{attributes.smoothing >= 0.05 ? attributes.smoothing : 'off'}}</label>
      <input v-model.number="attributes.smoothing" type="range" class="form-range" id="agilent87xx_smoothing"
         :disabled="disabled" data-bs-toggle="tooltip" data-bs-placement="bottom" :title="attributes.smoothing"
         :min="choices.smoothing.min" :max="choices.smoothing.max" :step="choices.smoothing.step">
    </div>
    <div class="col-12 col-md-2 col-lg-1">
      <input v-model="attributes.beep" type="checkbox" id="agilent87xx_beep" class="m-1"
             :disabled="disabled">
      <label for="agilent87xx_beep">Beep</label>
    </div>
  </div>
  <div class="g-2 row align-items-center">
    <div class="col-lg">
      <div class="input-group" v-if="attributes.identity != undefined">
        <span class="input-group-text">Device identity</span>
        <input v-model="attributes.identity" type="text" class="form-control" readonly>
      </div>
    </div>
  </div>
</div>
`
};

export { AnalyzerDummy, AnalyzerLiteVNA, AnalyzerAgilent87XX, AnalyzerAgilent506X };
