const MotorDummy = {
    props: ["choices", "attributes", "disabled"],
    template: `<div class="shadow mb-3 p-3 bg-body rounded container">
  <div class="g-2 row">
    <div class="col">
      <label for="motor_dummy_current" class="form-label">Current limit: {{attributes.current_limit}}</label>
      <input v-model.number="attributes.current_limit" type="range" class="form-range" id="motor_dummy_current"
	     data-bs-toggle="tooltip" data-bs-placement="bottom" :title="attributes.current_limit" :disabled="disabled"
	     :min="choices.current_limit.min" :max="choices.current_limit.max" :step="choices.current_limit.step">
    </div>
    <div class="col">
      <label for="motor_dummy_g" class="form-label">Gravity: {{attributes.g}}</label>
      <input v-model.number="attributes.g" type="range" class="form-range" id="motor_dummy_g" :disabled="disabled"
	     data-bs-toggle="tooltip" data-bs-placement="bottom" :title="attributes.g" :min="choices.g.min"
	     :max="choices.g.max" :step="choices.g.step">
    </div>
  </div>
  <div class="g-2 row">
    <div class="col">
      <input v-model="attributes.fail_on_constructor" type="checkbox" id="motor_dummy_fail_on_constructor" class="m-1" :disabled="disabled">
      <label for="motor_dummy_fail_on_constructor">Fail constructor</label>
      <input v-model="attributes.fail_on_interrupt" type="checkbox" id="motor_dummy_fail_on_interrupt" class="m-1" :disabled="disabled">
      <label for="motor_dummy_fail_on_interrupt">Fail interrupt</label>
      <input v-model="attributes.fail_on_set_origin" type="checkbox" id="motor_dummy_fail_on_set_origin" class="m-1" :disabled="disabled">
      <label for="motor_dummy_fail_on_set_origin">Fail origin</label>
      <input v-model="attributes.fail_on_move_to" type="checkbox" id="motor_dummy_fail_on_move_to" class="m-1" :disabled="disabled">
      <label for="motor_dummy_fail_on_move_to">Fail move to</label>
      <input v-model="attributes.fail_on_move_by" type="checkbox" id="motor_dummy_fail_on_move_by" class="m-1" :disabled="disabled">
      <label for="motor_dummy_fail_on_move_by">Fail move by</label>
      <input v-model="attributes.fail_on_position" type="checkbox" id="motor_dummy_fail_on_position" class="m-1" :disabled="disabled">
      <label for="motor_dummy_fail_on_position">Fail position</label>
      <input v-model="attributes.fail_on_stop" type="checkbox" id="motor_dummy_fail_on_stop" class="m-1" :disabled="disabled">
      <label for="motor_dummy_fail_on_stop">Fail stop</label>
      <input v-model="attributes.fail_on_reverse" type="checkbox" id="motor_dummy_fail_on_reverse" class="m-1" :disabled="disabled">
      <label for="motor_dummy_fail_on_reverse">Fail reverse</label>
      <input v-model="attributes.fail_on_steppers" type="checkbox" id="motor_dummy_fail_on_steppers" class="m-1" :disabled="disabled">
      <label for="motor_dummy_fail_on_steppers">Fail steppers</label>
      <input v-model="attributes.fail_on_speed" type="checkbox" id="motor_dummy_fail_on_speed" class="m-1" :disabled="disabled">
      <label for="motor_dummy_fail_on_speed">Fail speed</label>
      <input v-model="attributes.fail_on_return_speed" type="checkbox" id="motor_dummy_fail_on_return_speed" class="m-1" :disabled="disabled">
      <label for="motor_dummy_fail_on_return_speed">Fail return speed</label>
      <input v-model="attributes.fail_on_acceleration" type="checkbox" id="motor_dummy_fail_on_acceleration" class="m-1" :disabled="disabled">
      <label for="motor_dummy_fail_on_acceleration">Fail acceleration</label>
      <input v-model="attributes.fail_on_deceleration" type="checkbox" id="motor_dummy_fail_on_deceleration" class="m-1" :disabled="disabled">
      <label for="motor_dummy_fail_on_deceleration">Fail deceleration</label>
    </div>
  </div>
</div>`
};

const MotorUIRobot = {
    props: ["choices", "attributes", "disabled"],
    template: `<div class="shadow mb-3 p-3 bg-body rounded container">
  <div class="g-2 mb-2 row align-items-center">
    <div class="input-group col-12 col-md">
      <span class="input-group-text">New baudrate</span>
      <select v-model.number="attributes.new_baudrate" class="form-select" data-bs-toggle="tooltip"
              :disabled="disabled" data-bs-placement="bottom" title="Reconfigure motor to new baudrate">
	<option selected disabled value="">Baudrate</option>
	<option v-for="name in choices.new_baudrate.choices">{{name}}</option>
      </select>
    </div>
    <div class="col-12 col-lg-2">
      <input v-model="attributes.reconfigure" type="checkbox" id="motor_uirobot_reconfigure" class="m-1"
             :disabled="disabled">
      <label for="motor_uirobot_reconfigure" data-bs-toggle="tooltip" data-bs-placement="bottom"
             title="Reconfigure motor to new baudrate">Reconfigure</label>
    </div>
  </div>
  <div class="g-2 mb-2 row">
    <div class="input-group col-12 col-lg">
	    <span class="input-group-text">Current limit</span>
	    <input type="number" v-model.number="attributes.current" class="form-control" data-bs-toggle="tooltip"
		    :disabled="disabled" data-bs-placement="bottom" title="Motor current limit"
                    :min="choices.current.min" :max="choices.current.max" :step="choices.current.step">
	    <span class="input-group-text">\\(\\left[\\text{A}\\right]\\)</span>
    </div>
    <div class="input-group col-12 col-lg">
	    <span class="input-group-text">Current reduction</span>
	    <input type="number" v-model.number="attributes.auto_current" class="form-control" data-bs-toggle="tooltip"
		    :readonly="disabled" data-bs-placement="bottom" title="Motor automatic current reduction"
		    :min="choices.auto_current.min" :max="choices.auto_current.max" :step="choices.auto_current.step">
	    <span class="input-group-text">\\(\\left[\\%\\right]\\)</span>
    </div>
    <div class="input-group col-12 col-lg">
	    <span class="input-group-text">Micro steps</span>
	    <select v-model.number="attributes.microstepping" class="form-select" data-bs-toggle="tooltip"
		    :disabled="disabled" data-bs-placement="bottom" title="Motor step divisor">
		    <option selected disabled value="">Division</option>
		    <option v-for="name in choices.microstepping.choices">{{name}}</option>
	    </select>
    </div>
    <div class="g-2 mb-2 row">
      <div class="input-group col-12 col-lg">
	<span class="input-group-text">Start speed</span>
	<input type="number" v-model.number="attributes.max_start" class="form-control" data-bs-toggle="tooltip"
		:readonly="disabled" data-bs-placement="bottom" title="Immediately applied speed when starting moving"
		:min="choices.max_start.min" :max="choices.max_start.max" :step="choices.max_start.step">
	<span class="input-group-text">\\(\\left[\\frac{\\text{pulse}}{\\text{s}}\\right]\\)</span>
      </div>
      <div class="input-group col-12 col-lg">
	<span class="input-group-text">Stop speed</span>
	<input type="number" v-model.number="attributes.max_stop" class="form-control" data-bs-toggle="tooltip"
		:readonly="disabled" data-bs-placement="bottom" title="Speed at which motor immediately stops"
		:min="choices.max_stop.min" :max="choices.max_stop.max" :step="choices.max_stop.step">
	<span class="input-group-text">\\(\\left[\\frac{\\text{pulse}}{\\text{s}}\\right]\\)</span>
      </div>
      <div class="input-group col-12 col-lg" v-if="attributes.backlash">
	<span class="input-group-text">Backlash</span>
	<input type="number" v-model.number="attributes.backlash" class="form-control" data-bs-toggle="tooltip"
	       :readonly="disabled" data-bs-placement="bottom" title="Motor backlash compensation"
               :min="choices.backlash.min" :max="choices.backlash.max" :step="choices.backlash.step">
	<span class="input-group-text">\\(\\left[\\frac{\\text{pulse}}{\\text{s}}\\right]\\)</span>
      </div>
    </div>
  </div>
  <div class="g-2 mb-2 row">
    <div class="input-group col-12 col-md" v-if="attributes.max_current != undefined">
      <span class="input-group-text">Maximum current</span>
      <input v-model="attributes.max_current" type="number" class="form-control" readonly>
      <span class="input-group-text">\\(\\left[\\text{A}\\right]\\)</span>
    </div>
    <div class="input-group col-12 col-md" v-if="attributes.fw_version != undefined">
      <span class="input-group-text">Firmware version</span>
      <input v-model="attributes.fw_version" type="number" class="form-control" readonly>
    </div>
  </div>
  <div class="g-2 mb-2 row align-items-center">
    <div class="col-12 col-md" v-if="attributes.q_enc != undefined">
      <input v-model="attributes.q_enc" type="checkbox" id="motor_uirobot_quadrature_enc" class="m-1" readonly
             disabled>
      <label for="motor_uirobot_quadrature_enc" data-bs-toggle="tooltip" data-bs-placement="bottom"
             title="Internal quadrature encoding">Quadrature encoding</label>
    </div>
    <div class="col-12 col-md" v-if="attributes.closed_loop != undefined">
      <input v-model="attributes.closed_loop" type="checkbox" id="motor_uirobot_closed_loop" class="m-1" readonly
             disabled>
      <label for="motor_uirobot_closed_loop" data-bs-toggle="tooltip" data-bs-placement="bottom"
             title="Motor closed loop">Closed loop</label>
    </div>
    <div class="col-12 col-md" v-if="attributes.adv_motion != undefined">
      <input v-model="attributes.adv_motion" type="checkbox" id="motor_uirobot_adv_motion" class="m-1" readonly
             disabled>
      <label for="motor_uirobot_adv_motion" data-bs-toggle="tooltip" data-bs-placement="bottom"
             title="Motor support for acceleration/deceleration">Advanced motion</label>
    </div>
    <div class="col-12 col-md" v-if="attributes.sensors != undefined">
      <input v-model="attributes.sensors[0]" type="checkbox" class="m-1" readonly disabled
             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Sensor 1">
      <input v-model="attributes.sensors[1]" type="checkbox" class="m-1" readonly disabled
             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Sensor 2">
      <input v-model="attributes.sensors[2]" type="checkbox" class="m-1" readonly disabled
             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Sensor 3">
      <input v-model="attributes.sensors[3]" type="checkbox" class="m-1" readonly disabled
             data-bs-toggle="tooltip" data-bs-placement="bottom" title="Sensor 4" id="motor_uirobot_sensor">
      <label for="motor_uirobot_sensor" data-bs-toggle="tooltip" data-bs-placement="bottom"
             title="Motor sensors">Motor sensors</label>
    </div>
  </div>
</div>`
};

export { MotorDummy, MotorUIRobot };
