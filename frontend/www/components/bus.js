const BusDummy = {
    props: ["choices", "attributes", "disabled"],
    template: `<div class="shadow mb-3 p-3 bg-body rounded container">
  <div class="g-2 row">
    <div class="input-group col-12 col-md">
      <span class="input-group-text">Device</span>
      <select v-model="attributes.device" class="form-select" :disabled="disabled">
        <option selected disabled value="">Device</option>
        <option v-for="name in choices.device.choices">{{name}}</option>
      </select>
    </div>
    <div class="input-group col-12 col-md">
      <span class="input-group-text">Baudrate</span>
      <select v-model.number="attributes.baudrate" class="form-select" :disabled="disabled">
        <option selected disabled value="">Baudrate</option>
        <option v-for="name in choices.baudrate.choices">{{name}}</option>
      </select>
    </div>
  </div>
  <div class="g-2 row align-items-center">
    <div class="col">
      <label for="bus_dummy_delay" class="form-label">Delay: {{attributes.delay}}</label>
      <input v-model.number="attributes.delay" type="range" class="form-range" id="bus_dummy_delay"
         :disabled="disabled" data-bs-toggle="tooltip" data-bs-placement="bottom" :title="attributes.delay"
         :min="choices.delay.min" :max="choices.delay.max" :step="choices.delay.step">
    </div>
    <div class="col">
      <input v-model="attributes.fail_on_constructor" type="checkbox" id="bus_dummy_fail_on_constructor" class="m-1"
             :disabled="disabled">
      <label for="bus_dummy_fail_on_constructor">Fail constructor</label>
      <input v-model="attributes.timeout_on_constructor" type="checkbox" id="bus_dummy_timeout_on_constructor" class="m-1"
             :disabled="disabled">
      <label for="bus_dummy_timeout_on_constructor">Timeout constructor</label>
      <input v-model="attributes.fail_on_send" type="checkbox" id="bus_dummy_fail_on_send" class="m-1"
             :disabled="disabled">
      <label for="bus_dummy_fail_on_send">Fail send</label>
      <input v-model="attributes.fail_on_rcv" type="checkbox" id="bus_dummy_fail_on_rcv" class="m-1"
             :disabled="disabled">
      <label for="bus_dummy_fail_on_rcv">Fail receive</label>
      <input v-model="attributes.fail_on_check_interrupt" type="checkbox" id="bus_dummy_fail_on_check_interrupt" class="m-1"
             :disabled="disabled">
      <label for="bus_dummy_fail_on_check_interrupt">Fail on interrupt</label>
    </div>
  </div>
</div>`
};

const BusSerial = {
    props: ["choices", "attributes", "disabled"],
    template: `<div class="shadow mb-3 p-3 bg-body rounded container">
  <div class="g-2 mb-2 row">
    <div class="input-group col-6 col-md">
      <span class="input-group-text">Device</span>
      <select v-model="attributes.device" class="form-select" :disabled="disabled">
        <option selected disabled value="">Device</option>
        <option v-for="name in choices.device.choices">{{name}}</option>
      </select>
    </div>
    <div class="input-group col-6 col-md">
      <span class="input-group-text">Baudrate</span>
      <select v-model.number="attributes.baudrate" class="form-select" :disabled="disabled">
        <option selected disabled value="">Baudrate</option>
        <option v-for="name in choices.baudrate.choices">{{name}}</option>
      </select>
    </div>
  </div>
  <div class="g-2 mb-2 row">
    <div class="col-12 col-md-6 col-lg">
      <div class="input-group">
        <span class="input-group-text">Char size</span>
        <select v-model.number="attributes.char_size" class="form-select" :disabled="disabled">
          <option selected disabled value="">Char size</option>
          <option v-for="name in choices.char_size.choices">{{name}}</option>
        </select>
      </div>
    </div>
    <div class="col-12 col-md-6 col-lg">
      <div class="input-group">
        <span class="input-group-text">Flow control</span>
        <select v-model.number="attributes.flowcontrol" class="form-select" :disabled="disabled">
          <option selected disabled value="">Flow control</option>
          <option v-for="name in choices.flowcontrol.choices">{{name}}</option>
        </select>
      </div>
    </div>
    <div class="col-12 col-md-6 col-lg">
      <div class="input-group">
        <span class="input-group-text">Parity</span>
        <select v-model.number="attributes.parity" class="form-select" :disabled="disabled">
          <option selected disabled value="">Parity</option>
          <option v-for="name in choices.parity.choices">{{name}}</option>
        </select>
      </div>
    </div>
    <div class="col-12 col-md-6 col-lg">
      <div class="input-group">
        <span class="input-group-text">Stop bits</span>
        <select v-model.number="attributes.stop_bits" class="form-select" :disabled="disabled">
          <option selected disabled value="">Stop bits</option>
          <option v-for="name in choices.stop_bits.choices">{{name}}</option>
        </select>
      </div>
    </div>
  </div>
</div>`
};

const BusGPIB = {
    props: ["choices", "attributes", "disabled"],
    template: `<div class="shadow mb-3 p-3 bg-body rounded container">
  <div class="g-2 mb-2 row align-items-center">
    <div class="input-group col-6 col-md">
      <span class="input-group-text">Device</span>
      <select v-model="attributes.device" class="form-select" :disabled="disabled">
        <option selected disabled value="">Device</option>
        <option v-for="name in choices.device.choices">{{name}}</option>
      </select>
    </div>
    <div class="input-group col-6 col-md">
      <span class="input-group-text">Address</span>
        <input type="number" v-model.number="attributes.address" class="form-control" data-bs-toggle="tooltip"
		    :disabled="disabled" data-bs-placement="bottom" title="Device address"
                    :min="choices.address.min" :max="choices.address.max" :step="choices.address.step">
    </div>
  </div>
</div>`
};

export { BusDummy, BusSerial, BusGPIB };
