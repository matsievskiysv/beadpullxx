import { connect_websocket, debounce } from "/static/common.js";
import { createApp } from "/libs/vue.esm-browser.js";

createApp({
	data() {
		return {
			selected: false,
			connected: false,
			measuring: false,
			error_message: "",
			position: 1,
			position_actual: 1,
			position_multiplier: 1.0,
			speed: 1,
			speed_actual: 1,
			speed_multiplier: 1,
			acceleration: 1,
			acceleration_actual: 1,
			acceleration_multiplier: 1,
			position_valid: false,
			position_invalid: false,
			speed_valid: false,
			speed_invalid: false,
			acceleration_valid: false,
			acceleration_invalid: false,
		};
	},
	created() {
		connect_websocket(
			(_, ws) => {
				ws.send(JSON.stringify({ subscribe: ["motor"] }));
				this.connected = true;
			},
			(event, _) => {
				const message = JSON.parse(event.data);
				if ((message.event == "connect") && (message.target == "motor")) {
					this.selected = true;
				}
				if ((message.event == "disconnect") && (message.target == "motor")) {
					this.selected = false;
				}
				if (message.event == "update") {
					switch (message.target) {
						case "position_multiplier":
							this.position_multiplier = message.value;
							break;
						case "speed_multiplier":
							this.speed_multiplier = message.value;
							break;
						case "acceleration_multiplier":
							this.acceleration_multiplier = message.value;
							break;
					}
				}
				if ((message.event == "notification") && (message.target == "field_measure")) {
					switch (message.value) {
						case "start":
							this.measuring = true;
							break;
						default:
							this.measuring = false;
							break;
					}
				}
			},
			() => {
				this.connected = false;
			}
		);
	},
	updated() {
		if (window.MathJax != undefined)
			window.MathJax.typeset();
	},
	watch: {
		connected(new_val, old_val) {
			if (new_val == old_val)
				return;
			if (new_val) {
				fetch("/api/motor").then(res => this.selected = res.ok);
			} else {
				this.selected = false;
			}
		},
		selected(new_val, old_val) {
			if (new_val == old_val)
				return;
			if (new_val) {
				this.get_values();
				fetch("/api/measuring_field").then(res => {
					if (res.ok)
						res.json().then(data => this.measuring = data);
				});
			} else {
				this.measuring = false;
			}
		},
		position() {
			this.update_position();
		},
		position_actual() {
			this.update_position();
		},
		speed() {
			this.update_speed();
		},
		speed_actual() {
			this.update_speed();
		},
		acceleration() {
			this.update_acceleration();
		},
		acceleration_actual() {
			this.update_acceleration();
		},
		position_multiplier(new_val, old_val) {
			if (new_val == old_val)
				return;
			this.position_valid = false;
			this.position_invalid = false;
			if ((typeof (new_val) != "number") ||
				(new_val <= 0) ||
				(new_val == Infinity)) {
				this.position_invalid = true;
			} else {
				debounce(() => {
					if (this.position_invalid)
						return;
					const query = new URLSearchParams({ value: this.position_multiplier });
					fetch("/api/position_multiplier?" + query.toString(), { method: "POST" })
						.then(res => {
							if (res.ok) {
								this.position_valid = true;
							} else {
								res.json().then(message => this.error_message = message);
							}
						});
				}, "position_multiplier");
			}
		},
		speed_multiplier(new_val, old_val) {
			if (new_val == old_val)
				return;
			this.speed_valid = false;
			this.speed_invalid = false;
			if ((typeof (new_val) != "number") ||
				(new_val <= 0) ||
				(new_val == Infinity)) {
				this.speed_invalid = true;
			} else {
				debounce(() => {
					if (this.speed_invalid)
						return;
					const query = new URLSearchParams({ value: this.speed_multiplier });
					fetch("/api/speed_multiplier?" + query.toString(), { method: "POST" })
						.then(res => {
							if (res.ok) {
								this.speed_valid = true;
							} else {
								res.json().then(message => this.error_message = message);
							}
						});
				}, "speed_multiplier");
			}
		},
		acceleration_multiplier(new_val, old_val) {
			if (new_val == old_val)
				return;
			this.acceleration_valid = false;
			this.acceleration_invalid = false;
			if ((typeof (new_val) != "number") ||
				(new_val <= 0) ||
				(new_val == Infinity)) {
				this.acceleration_invalid = true;
			} else {
				debounce(() => {
					if (this.acceleration_invalid)
						return;
					const query = new URLSearchParams({ value: this.acceleration_multiplier });
					fetch("/api/acceleration_multiplier?" + query.toString(), { method: "POST" })
						.then(res => {
							if (res.ok) {
								this.acceleration_valid = true;
							} else {
								res.json().then(message => this.error_message = message);
							}
						});
				}, "acceleration_multiplier");
			}
		},
	},
	methods: {
		get_values() {
			fetch("/api/position_multiplier")
				.then(res => {
					if (res.ok) {
						res.json().then(value => this.position_multiplier = value);
					}
				});
			fetch("/api/speed_multiplier")
				.then(res => {
					if (res.ok) {
						res.json().then(value => this.speed_multiplier = value);
					}
				});
			fetch("/api/acceleration_multiplier")
				.then(res => {
					if (res.ok) {
						res.json().then(value => this.acceleration_multiplier = value);
					}
				});
		},
		update_position() {
			if ((this.position != 0) && (this.position_actual != 0))
				this.position_multiplier = this.position / this.position_actual;
		},
		update_speed() {
			if ((this.speed != 0) && (this.speed_actual != 0))
				this.speed_multiplier = this.speed / this.speed_actual;
		},
		update_acceleration() {
			if ((this.acceleration != 0) && (this.acceleration_actual != 0))
				this.acceleration_multiplier = this.acceleration / this.acceleration_actual;
		},
	},
}).mount("#app_calibration_motor");
