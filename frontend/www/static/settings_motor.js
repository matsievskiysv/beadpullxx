import { connect_websocket } from "/static/common.js";
import { createApp } from "/libs/vue.esm-browser.js";
import { BusDummy, BusSerial, BusGPIB } from "/components/bus.js";
import { MotorDummy, MotorUIRobot } from "/components/motor.js";

createApp({
	data() {
		return {
			selected: false,
			connected: false,
			measuring: false,
			error_message: "",
			motor_list: [],
			bus_list: [],
			motor_name: "",
			bus_name: "",
			motor_attributes: {},
			motor_choices: {},
			bus_attributes: {},
			bus_choices: {},
		};
	},
	created() {
		connect_websocket(
			(_, ws) => {
				ws.send(JSON.stringify({ subscribe: ["motor"] }));
				this.connected = true;
			},
			(event, _) => {
				const message = JSON.parse(event.data);
				if ((message.event == "connect") && (message.target == "motor")) {
					this.selected = true;
					this.motor_name = message.data.motor;
					this.bus_name = message.data.bus;
				}
				if ((message.event == "disconnect") && (message.target == "motor")) {
					this.selected = false;
				}
				if ((message.event == "notification") && (message.target == "field_measure")) {
					switch (message.value) {
						case "start":
							this.measuring = true;
							break;
						default:
							this.measuring = false;
							break;
					}
				}
			},
			() => {
				this.connected = false;
			}
		);
	},
	updated() {
		if (window.MathJax != undefined)
			window.MathJax.typeset();
	},
	watch: {
		connected(new_val, old_val) {
			if (new_val == old_val)
				return;
			if (new_val) {
				fetch("/api/motor_list")
					.then((res) => res.json())
					.then((data) => {
						this.motor_list = data;
						fetch("/api/motor")
							.then(res => {
								if (res.ok)
									res.json().then(motor => {
										this.motor_name = motor;
										this.selected = true;
									});
							});
					});
			} else {
				this.measuring = false;
				this.selected = false;
			}
		},
		selected(new_val, old_val) {
			if (new_val == old_val)
				return;
			if (new_val) {
				fetch("/api/measuring_field").then(res => {
					if (res.ok)
						res.json().then(data => this.measuring = data);
				});
			} else {
				this.measuring = false;
			}
		},
		motor_name(name, _) {
			this.motor_choices = {};
			this.motor_attributes = {};
			this.bus_choices = {};
			this.bus_attributes = {};
			this.bus_name = "";
			if (name == "")
				return;
			const query = new URLSearchParams({ motor: name });
			fetch("/api/motor_bus_list?" + query.toString())
				.then(res => {
					if (res.ok) {
						res.json().then(bus_list => {
							this.bus_list = bus_list;
							fetch("/api/motor_bus")
								.then(res => {
									if (res.ok) {
										res.json().then(bus_name => {
											if (this.bus_list.find(name => name == bus_name) != undefined)
												this.bus_name = bus_name;
										});
									}
								});
						});
					} else {
						res.json().then(message => this.error_message = message);
					}
				});
			fetch("/api/motor_attribute_choices?" + query.toString())
				.then(res => {
					if (res.ok) {
						res.json().then(motor_choice_values => {
							for (const [key, value] of Object.entries(motor_choice_values)) {
								try {
									this.motor_attributes[key] = value.value;
								} catch (_) {
								}
							}
							this.motor_choices = motor_choice_values;
							fetch("/api/motor_attributes")
								.then(res => {
									if (res.ok) {
										res.json().then(motor_attributes => this.motor_attributes = motor_attributes);
									}
								});
						});
					}
				});
		},
		bus_name(name, _) {
			this.bus_choices = {};
			this.bus_attributes = {};
			const query = new URLSearchParams({ bus: name });
			fetch("/api/bus_attribute_choices?" + query.toString())
				.then(res => {
					if (res.ok)
						res.json().then(choices => {
							for (const [key, value] of Object.entries(choices)) {
								try {
									this.bus_attributes[key] = value.value;
								} catch (_) {
								}
							}
							this.bus_choices = choices;
						});
				});
			fetch("/api/motor_bus_attributes")
				.then(res => {
					if (res.ok)
						res.json().then(attrs => this.bus_attributes = attrs);
				});
		}
	},
	methods: {
		select(_) {
			this.error_message = "";
			const headers = new Headers({ "Content-Type": "application/json" });
			const query = new URLSearchParams({ motor: this.motor_name, bus: this.bus_name });
			fetch("/api/motor?" + query.toString(), {
				method: "POST",
				headers: headers,
				body: JSON.stringify({
					motor_attributes: this.motor_attributes,
					bus_attributes: this.bus_attributes,
				}),
			}).then(res => {
				if (!res.ok)
					res.json().then(message => this.error_message = message);
				else
					fetch("/api/motor_attributes")
						.then(res => {
							if (res.ok) {
								res.json().then(motor_attributes => this.motor_attributes = motor_attributes);
							}
						});
			});
		},
		deselect(_) {
			this.error_message = "";
			fetch("/api/motor", { method: "DELETE" });
		},
	},
	components: {
		BusDummy,
		BusSerial,
		BusGPIB,
		MotorDummy,
		MotorUIRobot,
	},
}).mount("#app_settings_motor");
