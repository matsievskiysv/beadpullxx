import { connect_websocket, debounce } from "/static/common.js";
import { createApp } from "/libs/vue.esm-browser.js";

createApp({
	data() {
		return {
			selected: false,
			connected: false,
			measuring: false,
			error_message: "",
			capabilities: {},
			speed: 10,
			return_speed: 10,
			acceleration: 5,
			deceleration: 5,
			speed_valid: false,
			return_speed_valid: false,
			acceleration_valid: false,
			deceleration_valid: false,
			speed_invalid: false,
			return_speed_invalid: false,
			acceleration_invalid: false,
			deceleration_invalid: false,
			position: 0,
			step_by: 10,
			steppers: false,
			position_autoupdate: true,
			reverse: false,
		};
	},
	created() {
		connect_websocket(
			(_, ws) => {
				ws.send(JSON.stringify({ subscribe: ["motor"] }));
				this.connected = true;
			},
			(event, _) => {
				const message = JSON.parse(event.data);
				if ((message.event == "connect") && (message.target == "motor")) {
					this.selected = true;
				}
				if ((message.event == "disconnect") && (message.target == "motor")) {
					this.selected = false;
				}
				if ((message.event == "notification") && (message.target == "field_measure")) {
					switch (message.value) {
						case "start":
							this.measuring = true;
							break;
						default:
							this.measuring = false;
							break;
					}
				}
				if (message.event == "update") {
					switch (message.target) {
						case "position":
							if (this.position_autoupdate)
								this.position = message.value / 1000;
							break;
						case "speed":
							this.speed = message.value / 1000;
							break;
						case "return_speed":
							this.return_speed = message.value / 1000;
							break;
						case "acceleration":
							this.acceleration = message.value / 1000;
							break;
						case "deceleration":
							this.deceleration = message.value / 1000;
							break;
						case "steppers":
							this.steppers = message.value;
							break;
						case "reverse":
							this.reverse = message.value;
							break;
					}
				}
			},
			() => {
				this.connected = false;
			});
	},
	updated() {
		if (window.MathJax != undefined)
			window.MathJax.typeset();
	},
	watch: {
		connected(new_val, old_val) {
			if (new_val == old_val)
				return;
			if (new_val) {
				fetch("/api/motor").then(res => this.selected = res.ok);
			} else {
				this.selected = false;
			}
		},
		selected(new_val, old_val) {
			if (new_val == old_val)
				return;
			if (new_val) {
				this.get_capabilities();
				this.get_values();
				fetch("/api/position")
					.then(res => {
						if (res.ok) {
							res.json().then(value => this.position = value / 1000);
						}
					});
				fetch("/api/measuring_field").then(res => {
					if (res.ok)
						res.json().then(data => this.measuring = data);
				});
			} else {
				this.measuring = false;
				this.capabilities = {};
			}
		},
		steppers(value, _) {
			const query = new URLSearchParams({ value: value });
			fetch("/api/steppers?" + query.toString(), { method: "POST" })
				.then(res => {
					if (!res.ok)
						res.json().then(message => this.error_message = message);
				});
		},
		reverse(value, _) {
			const query = new URLSearchParams({ value: value });
			fetch("/api/reverse?" + query.toString(), { method: "POST" })
				.then(res => {
					if (!res.ok)
						res.json().then(message => this.error_message = message);
				});
		},
		speed(new_val, old_val) {
			if (new_val == old_val)
				return;
			this.speed_valid = false;
			this.speed_invalid = false;
			if ((typeof(new_val) != "number") || (new_val < 0)) {
				this.speed_invalid = true;
			} else {
				debounce(() => {
					if (this.speed_invalid)
						return;
					const query = new URLSearchParams({
						value: Math.round(this.speed * 1000),
					});
					fetch("/api/speed?" + query.toString(), { method: "POST" }).then(
						res => {
							if (res.ok)
								this.speed_valid = true;
							else
								res.json().then(data => this.error_message = data);
						});
				}, "speed");
			}
		},
		return_speed(new_val, old_val) {
			if (new_val == old_val)
				return;
			this.return_speed_valid = false;
			this.return_speed_invalid = false;
			if ((typeof(new_val) != "number") || (new_val < 0)) {
				this.return_speed_invalid = true;
			} else {
				debounce(() => {
					if (this.return_speed_invalid)
						return;
					const query = new URLSearchParams({
						value: Math.round(this.return_speed * 1000),
					});
					fetch("/api/return_speed?" + query.toString(), { method: "POST" }).then(
						res => {
							if (res.ok)
								this.return_speed_valid = true;
							else
								res.json().then(data => this.error_message = data);
						});
				}, "return_speed");
			}
		},
		acceleration(new_val, old_val) {
			if (new_val == old_val)
				return;
			this.acceleration_valid = false;
			this.acceleration_invalid = false;
			if ((typeof(new_val) != "number") || (new_val < 0)) {
				this.acceleration_invalid = true;
			} else {
				debounce(() => {
					if (this.acceleration_invalid)
						return;
					const query = new URLSearchParams({
						value: Math.round(this.acceleration * 1000),
					});
					fetch("/api/acceleration?" + query.toString(), { method: "POST" }).then(
						res => {
							if (res.ok)
								this.acceleration_valid = true;
							else
								res.json().then(data => this.error_message = data);
						});
				}, "acceleration");
			}
		},
		deceleration(new_val, old_val) {
			if (new_val == old_val)
				return;
			this.deceleration_valid = false;
			this.deceleration_invalid = false;
			if ((typeof(new_val) != "number") || (new_val < 0)) {
				this.deceleration_invalid = true;
			} else {
				debounce(() => {
					if (this.deceleration_invalid)
						return;
					const query = new URLSearchParams({
						value: Math.round(this.deceleration * 1000),
					});
					fetch("/api/deceleration?" + query.toString(), { method: "POST" }).then(
						res => {
							if (res.ok)
								this.deceleration_valid = true;
							else
								res.json().then(data => this.error_message = data);
						});
				}, "deceleration");
			}
		},
	},
	methods: {
		get_capabilities() {
			fetch("/api/motor_capabilities")
				.then(res => {
					if (res.ok)
						res.json().then(caps => {
							this.capabilities = caps;
						});
				});
		},
		get_values() {
			fetch("/api/speed")
				.then(res => {
					if (res.ok) {
						res.json().then(value => this.speed = value / 1000);
					}
				});
			fetch("/api/return_speed")
				.then(res => {
					if (res.ok) {
						res.json().then(value => this.return_speed = value / 1000);
					}
				});
			fetch("/api/acceleration")
				.then(res => {
					if (res.ok) {
						res.json().then(value => this.acceleration = value / 1000);
					}
				});
			fetch("/api/deceleration")
				.then(res => {
					if (res.ok) {
						res.json().then(value => this.deceleration = value / 1000);
					}
				});
			fetch("/api/steppers")
				.then(res => {
					if (res.ok) {
						res.json().then(value => this.steppers = value);
					}
				});
			fetch("/api/reverse")
				.then(res => {
					if (res.ok) {
						res.json().then(value => this.reverse = value);
					}
				});
			if (this.position_autoupdate) {
				fetch("/api/position")
					.then(res => {
						if (res.ok) {
							res.json().then(value => this.position = value / 1000);
						}
					});
			}
		},
		origin_set() {
			this.error_message = "";
			fetch("/api/set_origin", { method: "POST" })
				.then(res => {
					if (!res.ok) {
						res.json().then(message => this.error_message = message);
					}
				});
		},
		move(position, return_speed) {
			this.error_message = "";
			const query = new URLSearchParams({
				value: Math.round(position * 1000),
				return_speed: return_speed
			});
			fetch("/api/move_to?" + query.toString(), { method: "POST" })
				.then(res => {
					if (!res.ok) {
						res.json().then(message => this.error_message = message);
					}
				});
		},
		do_step_by(step) {
			this.error_message = "";
			const query = new URLSearchParams({
				value: Math.round(step * 1000),
				return_speed: false
			});
			fetch("/api/move_by?" + query.toString(), { method: "POST" })
				.then(res => {
					if (!res.ok) {
						res.json().then(message => this.error_message = message);
					}
				});
		},
		step_forward() {
			this.do_step_by(this.step_by);
		},
		step_backward() {
			this.do_step_by(-this.step_by);
		},
		update_position() {
			this.error_message = "";
			fetch("/api/position")
				.then(res => {
					if (res.ok) {
						res.json().then(value => this.position = value / 1000);
					} else {
						res.json().then(message => this.error_message = message);
					}
				});
		},
		stop() {
			this.error_message = "";
			fetch("/api/stop", { method: "POST" })
				.then(res => {
					if (res.ok) {
						res.json().then(value => this.position = value / 1000);
					} else {
						res.json().then(message => this.error_message = message);
					}
				});
		},
	},
}).mount("#app_control_motor");
