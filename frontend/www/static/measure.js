import { connect_websocket, export2csv } from "/static/common.js";
import { createApp } from "/libs/vue.esm-browser.js";

createApp({
	data() {
		return {
			selected: false,
			selected_motor: false,
			selected_analyzer: false,
			connected: false,
			measuring: false,
			measuring_scattering: false,
			measuring_field: false,
			error_message: "",
			field: "electric",
			material: "metal",
			method: "indirect",
			coeff: 1,
			Ql: 1000,
			start: 0,
			stop: 100,
			nop: 11,
			reflection: false,
			phase: false,
			measurements: {},
			normalized: false,
		};
	},
	created() {
		connect_websocket(
			(_, ws) => {
				ws.send(JSON.stringify({ subscribe: ["analyzer", "motor", "analyzer"] }));
				this.connected = true;
			},
			(event, _) => {
				const message = JSON.parse(event.data);
				if ((message.event == "connect") || (message.event == "disconnect")) {
					switch (message.target) {
						case "motor":
							this.selected_motor = message.event == "connect";
							break;
						case "analyzer":
							this.selected_analyzer = message.event == "connect";
							break;
					}
				}
				if (message.event == "update") {
					switch (message.target) {
						case "s_part":
							this.reflection = message.value[0] == message.value[1];
							break;
						case "field_measure":
							fetch("/api/field_measurements").then(res => {
								if (res.ok)
									res.json().then(data => {
										this.measurements = data;
									});
							});
							break;
					}
				}
				if (message.event == "notification") {
					switch (message.target) {
						case "scattering_measure":
							switch (message.value) {
								case "start":
									this.measuring_scattering = true;
									break;
								case "complete":
								case "interrupt":
								case "error":
									this.measuring_scattering = false;
									break;
							}
							break;
						case "field_measure":
							switch (message.value) {
								case "start":
									this.measuring_field = true;
									break;
								case "complete":
									fetch("/api/field_measurements").then(res => {
										if (res.ok)
											res.json().then(data => {
												this.measurements = data;
											});
									});
									this.measuring_field = false;
									break;
								case "interrupt":
									this.measuring_field = false;
									this.error_message = "Measure interrupted";
									break;
								case "timeout":
									this.measuring_field = false;
									this.error_message = "Measure timeout";
									break;
								case "error":
									this.measuring_field = false;
									this.error_message = message.message;
									break;
							}
							break;
					}
				}
			},
			() => {
				this.connected = false;
			}
		);
	},
	updated() {
		if (window.MathJax != undefined)
			window.MathJax.typeset();
		this.plot();
	},
	watch: {
		connected(new_val, old_val) {
			if (new_val == old_val)
				return;
			if (new_val) {
				fetch("/api/motor").then(res => this.selected_motor = res.ok);
				fetch("/api/analyzer").then(res => this.selected_analyzer = res.ok);
			} else {
				this.selected = false;
			}
		},
		selected_motor(new_val, old_val) {
			if (new_val == old_val)
				return;
			if (new_val) {
				fetch("/api/measuring_field").then(res => {
					if (res.ok)
						res.json().then(data => this.measuring_field = data);
				});
			} else {
				this.measuring_field = false;
			}
		},
		selected_analyzer(new_val, old_val) {
			if (new_val == old_val)
				return;
			if (new_val) {
				fetch("/api/measuring_scattering").then(res => {
					if (res.ok)
						res.json().then(data => this.measuring_scattering = data);
				});
				fetch("/api/measuring_field").then(res => {
					if (res.ok)
						res.json().then(data => this.measuring_field = data);
				});
				fetch("/api/s_part").then(res => {
					if (res.ok)
						res.json().then(s => this.reflection = s[0] == s[1]);
				});
				fetch("/api/field_measurements").then(res => {
					if (res.ok)
						res.json().then(data => this.measurements = data);
				});
			} else {
				this.measuring_scattering = false;
				this.measuring_field = false;
			}
		},
		measuring_scattering(value, _) {
			this.measuring = value || this.measuring_field;
		},
		measuring_field(value, _) {
			this.measuring = value || this.measuring_scattering;
		},
		field(value, _) {
			if ((value != "H" && this.material == "magnetic") ||
				(value == "H" && this.material == "dielectric"))
				this.material = "metal";
		},
		method(value, _) {
			if (value != "indirect")
				this.phase = false;
		},
		reflection(value, _) {
			if (!value)
				this.phase = false;
		},
		measurements(new_val, old_val) {
			if (new_val == old_val)
				return;
			this.plot();
		},
	},
	methods: {
		do_measurement() {
			this.error_message = "";
			const query = new URLSearchParams({
				field: this.field,
				phase: this.phase,
				material: this.material,
				method: this.method,
				coeff: this.coeff,
				Ql: this.Ql,
				start: this.start * 1000,
				stop: this.stop * 1000,
				nop: this.nop,
			});
			fetch("/api/measure_field?" + query.toString(), { method: "POST" }).then(
				res => {
					if (!res.ok) {
						res.json().then(data => this.error_message = data);
						this.measure = false;
					}
				});
		},
		interrupt_measure() {
			fetch("/api/interrupt_measure_field", { method: "POST" }).then(res => {
				if (!res.ok)
					res.json().then(message => this.error_message = message);
			});
		},
		move(position) {
			this.error_message = "";
			const query = new URLSearchParams({
				value: Math.round(position * 1000),
				return_speed: true,
			});
			fetch("/api/move_to?" + query.toString(), { method: "POST" })
				.then(res => {
					if (!res.ok) {
						res.json().then(message => this.error_message = message);
					}
				});
		},
		swap() {
			const start = this.start;
			this.start = this.stop;
			this.stop = start;
		},
		download_data() {
			const file_content = export2csv(this.measurements, [
				`Field: ${this.field}`,
				`Field phase: ${this.phase}`,
				`Material: ${this.material}`,
				`Method: ${this.method}`,
				`Koeff: ${this.coeff}`,
				`Ql: ${this.Ql}`,
				`S type: ${this.reflection? "reflection" : "transmission"}`,
			]);
			const element = document.createElement('a');
			element.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(file_content));
			element.setAttribute("download", "field.csv");
			element.style.display = "none";
			document.body.appendChild(element);
			element.click();
			document.body.removeChild(element);
		},
		plot() {
			const rect_layout = {
				showlegend: false,
				margin: { t: 40, r: 40, b: 40, l: 40 },
			};

			if ((this.measurements.x != undefined) && (this.measurements.x.length > 0) &&
				(this.measurements.field != undefined) && (this.measurements.field.length > 0) &&
				(this.measurements.field_norm != undefined) && (this.measurements.field_norm.length > 0)) {
				const x = this.measurements.x.map(it => it / 1000);
				const field = this.normalized ? this.measurements.field_norm : this.measurements.field;
				window.Plotly.newPlot("field_plot", [
					{
						type: "scatter",
						x: x,
						y: field,
					}
				], rect_layout, { responsive: true });
			} else {
				document.getElementById("field_plot").textContent = "";
			}
		}
	},
}).mount("#app_measure");
