import { connect_websocket } from "/static/common.js";
import { createApp } from "/libs/vue.esm-browser.js";
import { BusDummy, BusSerial, BusGPIB } from "/components/bus.js";
import { AnalyzerDummy, AnalyzerLiteVNA, AnalyzerAgilent87XX, AnalyzerAgilent506X } from "/components/analyzer.js";

createApp({
	data() {
		return {
			selected: false,
			connected: false,
			measuring: false,
			measuring_scattering: false,
			measuring_field: false,
			error_message: "",
			analyzer_list: [],
			bus_list: [],
			analyzer_name: "",
			bus_name: "",
			analyzer_attributes: {},
			analyzer_choices: {},
			bus_attributes: {},
			bus_choices: {},
		};
	},
	created() {
		connect_websocket(
			(_, ws) => {
				ws.send(JSON.stringify({ subscribe: ["analyzer", "measure"] }));
				this.connected = true;
			},
			(event, _) => {
				const message = JSON.parse(event.data);
				if ((message.event == "connect") && (message.target == "analyzer")) {
					this.selected = true;
					this.analyzer_name = message.data.analyzer;
					this.bus_name = message.data.bus;
				}
				if ((message.event == "disconnect") && (message.target == "analyzer")) {
					this.selected = false;
				}
				if (message.event == "notification") {
					switch (message.target) {
						case "scattering_measure":
							switch (message.value) {
								case "start":
									this.measuring_scattering = true;
									break;
								default:
									this.measuring_scattering = false;
									break;
							}
							break;
						case "field_measure":
							switch (message.value) {
								case "start":
									this.measuring_field = true;
									break;
								default:
									this.measuring_field = false;
									break;
							}
							break;
					}
				}
			},
			() => {
				this.connected = false;
			}
		);
	},
	updated() {
		if (window.MathJax != undefined)
			window.MathJax.typeset();
	},
	watch: {
		connected(new_val, old_val) {
			if (new_val == old_val)
				return;
			if (new_val) {
				fetch("/api/analyzer_list")
					.then((res) => res.json())
					.then((data) => {
						this.analyzer_list = data;
						fetch("/api/analyzer")
							.then(res => {
								if (res.ok)
									res.json().then(motor => {
										this.analyzer_name = motor;
										this.selected = true;
									});
							});
					});
			} else {
				this.selected = false;
			}
		},
		selected(new_val, old_val) {
			if (new_val == old_val)
				return;
			if (new_val) {
				fetch("/api/measuring_scattering").then(res => {
					if (res.ok)
						res.json().then(data => this.measuring_scattering = data);
				});
				fetch("/api/measuring_field").then(res => {
					if (res.ok)
						res.json().then(data => this.measuring_field = data);
				});
			} else {
				this.measuring_scattering = false;
				this.measuring_field = false;
			}
		},
		measuring_scattering(value, _) {
			this.measuring = value || this.measuring_field;
		},
		measuring_field(value, _) {
			this.measuring = value || this.measuring_scattering;
		},
		analyzer_name(name, _) {
			this.analyzer_choices = {};
			this.analyzer_attributes = {};
			this.bus_choices = {};
			this.bus_attributes = {};
			this.bus_name = "";
			if (name == "")
				return;
			const query = new URLSearchParams({ analyzer: name });
			fetch("/api/analyzer_bus_list?" + query.toString())
				.then(res => {
					if (res.ok) {
						res.json().then(bus_list => {
							this.bus_list = bus_list;
							fetch("/api/analyzer_bus")
								.then(res => {
									if (res.ok) {
										res.json().then(bus_name => {
											if (this.bus_list.find(name => name == bus_name) != undefined)
												this.bus_name = bus_name;
										});
									}
								});
						});
					} else {
						res.json().then(message => this.error_message = message);
					}
				});
			fetch("/api/analyzer_attribute_choices?" + query.toString())
				.then(res => {
					if (res.ok) {
						res.json().then(analyzer_choice_values => {
							for (const [key, value] of Object.entries(analyzer_choice_values)) {
								try {
									this.analyzer_attributes[key] = value.value;
								} catch (_) {
								}
							}
							this.analyzer_choices = analyzer_choice_values;
							fetch("/api/analyzer_attributes")
								.then(res => {
									if (res.ok) {
										res.json().then(analyzer_attributes => this.analyzer_attributes = analyzer_attributes);
									}
								});
						});
					}
				});
		},
		bus_name(name, _) {
			this.bus_choices = {};
			this.bus_attributes = {};
			const query = new URLSearchParams({ bus: name });
			fetch("/api/bus_attribute_choices?" + query.toString())
				.then(res => {
					if (res.ok)
						res.json().then(choices => {
							for (const [key, value] of Object.entries(choices)) {
								try {
									this.bus_attributes[key] = value.value;
								} catch (_) {
								}
							}
							this.bus_choices = choices;
						});
				});
			fetch("/api/analyzer_bus_attributes")
				.then(res => {
					if (res.ok)
						res.json().then(attrs => this.bus_attributes = attrs);
				});
		}
	},
	methods: {
		select(_) {
			this.error_message = "";
			const headers = new Headers({ "Content-Type": "application/json" });
			const query = new URLSearchParams({ analyzer: this.analyzer_name, bus: this.bus_name });
			fetch("/api/analyzer?" + query.toString(), {
				method: "POST",
				headers: headers,
				body: JSON.stringify({
					analyzer_attributes: this.analyzer_attributes,
					bus_attributes: this.bus_attributes,
				}),
			}).then(res => {
				if (!res.ok)
					res.json().then(message => this.error_message = message);
				else
					fetch("/api/analyzer_attributes")
						.then(res => {
							if (res.ok) {
								res.json().then(analyzer_attributes => this.analyzer_attributes = analyzer_attributes);
							}
						});
			});
		},
		deselect(_) {
			this.error_message = "";
			fetch("/api/analyzer", { method: "DELETE" });
		},
	},
	components: {
		BusDummy,
		BusSerial,
		BusGPIB,
		AnalyzerDummy,
		AnalyzerLiteVNA,
		AnalyzerAgilent87XX,
		AnalyzerAgilent506X,
	},
}).mount("#app_settings_analyzer");
