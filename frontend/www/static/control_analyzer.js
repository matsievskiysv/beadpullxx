import { connect_websocket, debounce, export2csv } from "/static/common.js";
import { createApp } from "/libs/vue.esm-browser.js";

createApp({
	data() {
		return {
			selected: false,
			connected: false,
			measuring: false,
			measuring_scattering: false,
			measuring_field: false,
			error_message: "",
			f_left: 0,
			f_right: 0,
			f_center: 0,
			f_span: 0,
			f_left_valid: false,
			f_right_valid: false,
			f_center_valid: false,
			f_span_valid: false,
			f_left_invalid: false,
			f_right_invalid: false,
			f_center_invalid: false,
			f_span_invalid: false,
			f_mult: 1000000,
			continuous_measure: false,
			f_style: "range",
			measurement: "mag_lin",
			measurements: {},
			measurements_names: [
				{ name: "\\(\\Re{\\left\\{S\\right\\}}\\)", value: "re" },
				{ name: "\\(\\Im{\\left\\{S\\right\\}}\\)", value: "im" },
				{ name: "\\(Mag\\)", value: "mag_lin" },
				{ name: "\\(log{Mag}\\)", value: "mag_log" },
				{ name: "\\(\\varphi\\)", value: "phase" },
				{ name: "Smith", value: "smith" },
			],
			mult_list: [
				{ name: "\\(\\left[\\text{kHz}\\right]\\)", value: 1000 },
				{ name: "\\(\\left[\\text{MHz}\\right]\\)", value: 1000000 },
				{ name: "\\(\\left[\\text{GHz}\\right]\\)", value: 1000000000 },
			],
			nop: 0,
			nops: [],
			s_part: "",
			s_parts: [],
		};
	},
	created() {
		connect_websocket(
			(_, ws) => {
				ws.send(JSON.stringify({ subscribe: ["analyzer"] }));
				this.connected = true;
			},
			(event, _) => {
				const message = JSON.parse(event.data);
				if ((message.event == "connect") && (message.target == "analyzer")) {
					this.selected = true;
				}
				if ((message.event == "disconnect") && (message.target == "analyzer")) {
					this.selected = false;
				}
				if (message.event == "update") {
					switch (message.target) {
						case "f":
							switch (this.f_style) {
								case "range":
									this.f_left = message.value.left / this.f_mult;
									this.f_right = message.value.right / this.f_mult;
									break;
								case "span":
									this.f_center = message.value.center / this.f_mult;
									this.f_span = message.value.span / this.f_mult;
									break;
							}
							break;
						case "number_of_points":
							this.nop = message.value;
							break;
						case "s_part":
							this.s_part = JSON.stringify(message.value);
							break;
					}
				}
				if (message.event == "notification") {
					switch (message.target) {
						case "scattering_measure":
							switch (message.value) {
								case "start":
									this.measuring_scattering = true;
									break;
								case "complete":
									fetch("/api/scattering_measurements").then(res => {
										if (res.ok)
											res.json().then(data => {
												this.measurements = data;
												if (this.continuous_measure)
													this.do_measurement();
											});
									});
									this.measuring_scattering = false;
									break;
								case "interrupt":
									this.error_message = "Measure interrupted";
									this.measuring_scattering = false;
									this.continuous_measure = false;
									break;
								case "error":
									this.error_message = "Measureing error";
									this.measuring_scattering = false;
									this.continuous_measure = false;
									break;
							}
							break;
						case "field_measure":
							switch (message.value) {
								case "start":
									this.measuring_field = true;
									break;
								case "complete":
								case "interrupt":
								case "error":
									this.measuring_field = false;
									break;
							}
							break;
					}
				}
			},
			() => {
				this.connected = false;
			}
		);
	},
	updated() {
		if (window.MathJax != undefined)
			window.MathJax.typeset();
	},
	watch: {
		connected(new_val, old_val) {
			if (new_val == old_val)
				return;
			if (new_val) {
				fetch("/api/analyzer").then(res => this.selected = res.ok);
			} else {
				this.selected = false;
			}
		},
		selected(new_val, old_val) {
			if (new_val == old_val)
				return;
			if (new_val) {
				this.get_values();
				fetch("/api/measuring_scattering").then(res => {
					if (res.ok)
						res.json().then(data => this.measuring_scattering = data);
				});
				fetch("/api/measuring_field").then(res => {
					if (res.ok)
						res.json().then(data => this.measuring_field = data);
				});
			} else {
				this.measuring_scattering = false;
				this.measuring_field = false;
				this.capabilities = {};
			}
		},
		measuring_scattering(value, _) {
			this.measuring = value || this.measuring_field;
		},
		measuring_field(value, _) {
			this.measuring = value || this.measuring_scattering;
		},
		f_style(new_val, old_val) {
			if (new_val == old_val)
				return;
			switch (new_val) {
				case "range": {
					this.f_left = Math.max(this.f_center - this.f_span / 2, 0);
					this.f_right = Math.max(this.f_center + this.f_span / 2, this.f_left);
					break;
				}
				case "span": {
					this.f_span = this.f_right - this.f_left;
					this.f_center = (this.f_left + this.f_right) / 2;
					break;
				}
			}
		},
		f_left(new_val, old_val) {
			if (new_val == old_val)
				return;
			this.f_left_valid = false;
			this.f_left_invalid = false;
			if ((typeof(new_val) != "number") || (new_val > this.f_right) || (new_val < 0)) {
				this.f_left_invalid = true;
			} else {
				debounce(() => {
					if (this.f_left_invalid)
						return;
					const query = new URLSearchParams({
						value: Math.round(this.f_left * this.f_mult),
					});
					fetch("/api/f_left?" + query.toString(), { method: "POST" }).then(
						res => {
							if (res.ok)
								this.f_left_valid = true;
							else
								res.json().then(data => this.error_message = data);
						});
				}, "f_left");
			}
		},
		f_right(new_val, old_val) {
			if (new_val == old_val)
				return;
			this.f_right_valid = false;
			this.f_right_invalid = false;
			if ((typeof(new_val) != "number") || (new_val < this.f_left) || (new_val < 0)) {
				this.f_right_invalid = true;
			} else {
				debounce(() => {
					if (this.f_right_invalid)
						return;
					const query = new URLSearchParams({
						value: Math.round(this.f_right * this.f_mult),
					});
					fetch("/api/f_right?" + query.toString(), { method: "POST" }).then(
						res => {
							if (res.ok)
								this.f_right_valid = true;
							else
								res.json().then(data => this.error_message = data);
						});
				}, "f_right");
			}
		},
		f_center(new_val, old_val) {
			if (new_val == old_val)
				return;
			this.f_center_valid = false;
			this.f_center_invalid = false;
			if ((typeof(new_val) != "number") || (new_val - this.f_span / 2 < 0)) {
				this.f_center_invalid = true;
			} else {
				debounce(() => {
					if (this.f_center_invalid)
						return;
					const query = new URLSearchParams({
						value: Math.round(this.f_center * this.f_mult),
					});
					fetch("/api/f_center?" + query.toString(), { method: "POST" }).then(
						res => {
							if (res.ok)
								this.f_center_valid = true;
							else
								res.json().then(data => this.error_message = data);
						});
				}, "center");
			}
		},
		f_span(new_val, old_val) {
			if (new_val == old_val)
				return;
			this.f_span_valid = false;
			this.f_span_invalid = false;
			if ((typeof(new_val) != "number") || (this.f_center - new_val / 2 < 0)) {
				this.f_span_invalid = true;
			} else {
				debounce(() => {
					if (this.f_span_invalid)
						return;
					const query = new URLSearchParams({
						value: Math.round(this.f_span * this.f_mult),
					});
					fetch("/api/f_span?" + query.toString(), { method: "POST" }).then(
						res => {
							if (res.ok)
								this.f_span_valid = true;
							else
								res.json().then(data => this.error_message = data);
						});
				}, "span");
			}
		},
		f_mult(new_val, old_val) {
			if (new_val == old_val)
				return;
			this.query_f();
		},
		nop(new_val, old_val) {
			if (new_val == old_val)
				return;
			const query = new URLSearchParams({ value: new_val });
			fetch("/api/nop?" + query.toString(), { method: "POST" }).then(res => {
				if (res.ok)
					fetch("/api/nop").then(res => {
						if (res.ok)
							res.json().then(data => this.nop = data);
						else
							res.json().then(data => this.error_message = data);
					});
				else
					res.json().then(data => this.error_message = data);
			});
		},
		s_part(new_val, old_val) {
			if (new_val == old_val)
				return;
			const indices = JSON.parse(new_val);
			const query = new URLSearchParams({ from: indices[1], to: indices[0] });
			fetch("/api/s_part?" + query.toString(), { method: "POST" }).then(res => {
				if (res.ok)
					fetch("/api/s_part").then(res => {
						if (res.ok)
							res.json().then(data => this.s_part = JSON.stringify(data));
						else
							res.json().then(data => this.error_message = data);
					});
				else
					res.json().then(data => this.error_message = data);
			});
		},
		measurement(new_val, old_val) {
			if (new_val == old_val)
				return;
			this.plot();
		},
		measurements(new_val, old_val) {
			if (new_val == old_val)
				return;
			this.plot();
		},
		continuous_measure(new_val, old_val) {
			if (new_val == old_val)
				return;
			if (new_val)
				this.do_measurement();
		},
		measure(value, _) {
			if (value) {
				this.do_measurement();
			} else {
				if (this.measure_handle != null) {
					clearTimeout(this.measure_handle);
					this.measure_handle = null;
				}
			}
		},
	},
	methods: {
		do_measurement() {
			this.error_message = "";
			fetch("/api/measure_scattering", { method: "POST" }).then(
				res => {
					if (!res.ok) {
						res.json().then(data => this.error_message = data);
						this.continuous_measure = false;
					}
				});
		},
		query_f() {
			fetch("/api/f_all").then(res => {
				if (res.ok)
					res.json().then(data => {
						this.f_left = data.left / this.f_mult;
						this.f_right = data.right / this.f_mult;
						this.f_center = data.center / this.f_mult;
						this.f_span = data.span / this.f_mult;
					});
			});
		},
		interrupt_measure() {
			this.continuous_measure = false;
			fetch("/api/interrupt_measure_scattering", { method: "POST" }).then(res => {
				if (!res.ok)
					res.json().then(message => this.error_message = message);
			});
		},
		get_values() {
			this.query_f();
			fetch("/api/nop_values").then(res => {
				if (res.ok)
					res.json().then(data => this.nops = data);
			});
			fetch("/api/nop").then(res => {
				if (res.ok)
					res.json().then(data => this.nop = data);
			});
			fetch("/api/s_parts").then(res => {
				if (res.ok)
					res.json().then(data => {
						this.s_parts = [];
						Array.from(data).forEach(it => this.s_parts.push({
							value: JSON.stringify(it),
							name: `S${it[0]}${it[1]}`
						}));
					});
			});
			fetch("/api/s_part").then(res => {
				if (res.ok)
					res.json().then(data => this.s_part = JSON.stringify(data));
			});
			fetch("/api/scattering_measurements").then(res => {
				if (res.ok)
					res.json().then(data => this.measurements = data);
			});
		},
		download_data() {
			const indices = JSON.parse(this.s_part);
			const file_content = export2csv(this.measurements, [
				`S${indices[1]}${indices[0]}`,
			]);
			const element = document.createElement('a');
			element.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(file_content));
			element.setAttribute("download", "scattering.csv");
			element.style.display = "none";
			document.body.appendChild(element);
			element.click();
			document.body.removeChild(element);
		},
		plot() {
			const rect_layout = {
				showlegend: false,
				margin: { t: 40, r: 40, b: 40, l: 40 },
			};

			switch (this.measurement) {
				case "re":
					if ((this.measurements.f != undefined) && (this.measurements.s_re != undefined)
						&& (this.measurements.f.length > 0) && (this.measurements.s_re.length > 0)) {
						window.Plotly.newPlot("analyzer_plot", [
							{
								type: "scatter",
								x: this.measurements.f,
								y: this.measurements.s_re,
							}
						], rect_layout, { responsive: true });
					} else {
						document.getElementById("analyzer_plot").textContent = "";
					}
					break;
				case "im":
					if ((this.measurements.f != undefined) && (this.measurements.s_im != undefined)
						&& (this.measurements.f.length > 0) && (this.measurements.s_im.length > 0)) {
						window.Plotly.newPlot("analyzer_plot", [
							{
								type: "scatter",
								x: this.measurements.f,
								y: this.measurements.s_im,
							}
						], rect_layout, { responsive: true });
					} else {
						document.getElementById("analyzer_plot").textContent = "";
					}
					break;
				case "mag_lin":
					if ((this.measurements.f != undefined) && (this.measurements.s_mag != undefined)
						&& (this.measurements.f.length > 0) && (this.measurements.s_mag.length > 0)) {
						window.Plotly.newPlot("analyzer_plot", [
							{
								type: "scatter",
								x: this.measurements.f,
								y: this.measurements.s_mag,
							}
						], rect_layout, { responsive: true });
					} else {
						document.getElementById("analyzer_plot").textContent = "";
					}
					break;
				case "mag_log":
					if ((this.measurements.f != undefined) && (this.measurements.s_mag_log != undefined)
						&& (this.measurements.f.length > 0) && (this.measurements.s_mag_log.length > 0)) {
						window.Plotly.newPlot("analyzer_plot", [
							{
								type: "scatter",
								x: this.measurements.f,
								y: this.measurements.s_mag_log,
							}
						], rect_layout, { responsive: true });
					} else {
						document.getElementById("analyzer_plot").textContent = "";
					}
					break;
				case "phase":
					if ((this.measurements.f != undefined) && (this.measurements.phase != undefined)
						&& (this.measurements.f.length > 0) && (this.measurements.phase.length > 0)) {
						window.Plotly.newPlot("analyzer_plot", [
							{
								type: "scatter",
								x: this.measurements.f,
								y: this.measurements.phase,
							}
						], rect_layout, { responsive: true });
					} else {
						document.getElementById("analyzer_plot").textContent = "";
					}
					break;
				case "smith":
					if ((this.measurements.zin_re != undefined) && (this.measurements.zin_im != undefined)
						&& (this.measurements.zin_re.length > 0) && (this.measurements.zin_im.length > 0)) {
						window.Plotly.newPlot("analyzer_plot", [
							{
								type: "scattersmith",
								real: this.measurements.zin_re,
								imag: this.measurements.zin_im,
							}
						], {
							showlegend: false,
							margin: { t: 20, r: 20, b: 20, l: 20 },
						}, { responsive: true });
					} else {
						document.getElementById("analyzer_plot").textContent = "";
					}
					break;
				default:
					document.getElementById("analyzer_plot").textContent = "";
			}
		}
	},
}).mount("#app_control_analyzer");
