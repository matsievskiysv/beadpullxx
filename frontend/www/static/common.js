function connect_websocket(onopen_fun, onmessage_fun, onclose_fun, time_from = 500, time_to = 5000) {
	if (connect_websocket.url == undefined) {
		connect_websocket.url = new URL(document.baseURI);
		if (connect_websocket.url.protocol == "http:")
			connect_websocket.url.protocol = "ws:";
		else
			connect_websocket.url.protocol = "wss:";
		connect_websocket.url.pathname = "/";
		connect_websocket.onopen_list = [];
		connect_websocket.onmessage_list = [];
		connect_websocket.onclose_list = [];
	}
	if (onopen_fun != null)
		connect_websocket.onopen_list.push(onopen_fun);
	if (onmessage_fun != null)
		connect_websocket.onmessage_list.push(onmessage_fun);
	if (onclose_fun != null)
		connect_websocket.onclose_list.push(onclose_fun);
	if (connect_websocket.connection == undefined) {
		connect_websocket.connected = false;
		connect_websocket.connection = new WebSocket(connect_websocket.url);
		// FIXME: when time_from = 250, `onclose` hook of stopped socket may be executed
		// after `onopen` hook of connected socket
		connect_websocket.connection.onopen = event =>
			connect_websocket.onopen_list.forEach(fun => {
				connect_websocket.connected = true;
				fun(event, connect_websocket.connection);
			});
		connect_websocket.connection.onmessage = event =>
			connect_websocket.onmessage_list.forEach(fun => {
				fun(event, connect_websocket.connection);
			});
		connect_websocket.connection.onclose = event => {
			connect_websocket.onclose_list.forEach(fun => fun(event, connect_websocket.connection));
			connect_websocket.connection = undefined;
			connect_websocket(onopen_fun = null, onmessage_fun = null, onclose_fun = null,
				Math.min(time_from * 1.2, time_to));
		};
		setTimeout(() => {
			if ((connect_websocket.connection !== undefined) &&
				(connect_websocket.connection.readyState == WebSocket.CONNECTING)) {
				connect_websocket.connection.close();
			}
		}, time_from);
	} else {
		if (connect_websocket.connected) {
			onopen_fun(null, connect_websocket.connection);
		}

	}
}

function debounce(cb, id, delay = 500) {
	if (typeof (debounce[id]) == "number")
		clearTimeout(debounce[id]);
	debounce[id] = setTimeout(cb, delay);
}

function export2csv(data, comments = [], data_separator=",", comment_char="#", line_terminator="\n") {
	let lines = comments.map(el => `${comment_char} ${el}`);
	const headers = Object.keys(data);
	if (headers.length == 0)
		return lines.join(line_terminator);
	lines.push(headers.join(data_separator));
	const columns = headers.map(column => data[column]); // doing this instead of Object.values to ensure order
	for (let i = 0; i < columns[0].length; i++) {
		lines.push(columns.map(column => column[i]).join(data_separator));
	}
	return lines.join(line_terminator);
}

export { connect_websocket, debounce, export2csv };
