import { connect_websocket } from "/static/common.js";
import { createApp } from "/libs/vue.esm-browser.js";

createApp({
	data() {
		return {
			selected: false,
			connected: false,
			measuring: false,
			measuring_scattering: false,
			measuring_field: false,
			error_message: "",
		};
	},
	created() {
		connect_websocket(
			(_, ws) => {
				ws.send(JSON.stringify({ subscribe: ["analyzer"] }));
				this.connected = true;
			},
			(event, _) => {
				const message = JSON.parse(event.data);
				if ((message.event == "connect") && (message.target == "analyzer")) {
					this.selected = true;
				}
				if ((message.event == "disconnect") && (message.target == "analyzer")) {
					this.selected = false;
				}
				if (message.event == "notification") {
					switch (message.target) {
						case "scattering_measure":
							switch (message.value) {
								case "start":
									this.measuring_scattering = true;
									break;
								default:
									this.measuring_scattering = false;
									break;
							}
							break;
						case "field_measure":
							switch (message.value) {
								case "start":
									this.measuring_field = true;
									break;
								default:
									this.measuring_field = false;
									break;
							}
							break;
					}
				}
			},
			() => {
				this.connected = false;
			}
		);
		fetch("/api/analyzer").then(res => this.connected = res.ok);
	},
	updated() {
		if (window.MathJax != undefined)
			window.MathJax.typeset();
	},
	watch: {
		connected(new_val, old_val) {
			if (new_val == old_val)
				return;
			if (new_val) {
				fetch("/api/analyzer").then(res => this.selected = res.ok);
			} else {
				this.selected = false;
			}
		},
		selected(new_val, old_val) {
			if (new_val == old_val)
				return;
			if (new_val) {
				fetch("/api/measuring_scattering").then(res => {
					if (res.ok)
						res.json().then(data => this.measuring_scattering = data);
				});
				fetch("/api/measuring_field").then(res => {
					if (res.ok)
						res.json().then(data => this.measuring_field = data);
				});
			} else {
				this.measuring_scattering = false;
				this.measuring_field = false;
			}
		},
		measuring_scattering(value, _) {
			this.measuring = value || this.measuring_field;
		},
		measuring_field(value, _) {
			this.measuring = value || this.measuring_scattering;
		},
	}
}).mount("#app_calibration_analyzer");
