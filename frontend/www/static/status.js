import { connect_websocket } from "/static/common.js";
import { createApp } from "/libs/vue.esm-browser.js";

createApp({
	data() {
		return {
			connected: false,
			measuring_field: false,
			current_point: 0,
			total_points: 100,
		};
	},
	created() {
		connect_websocket(
			(_, ws) => {
				ws.send(JSON.stringify({ subscribe: ["measure"] }));
				this.connected = true;
			},
			(event, _) => {
				const message = JSON.parse(event.data);
				if ((message.event == "notification") && (message.target == "field_measure")) {
					switch (message.value) {
						case "start":
							this.measuring_field = true;
							break;
						default:
							this.measuring_field = false;
							break;
					}
				}
				if ((message.event == "update") && (message.target == "field_measure")) {
					this.current_point = message.value.point;
					this.total_points = message.value.total;
				}
			},
			() => {
				this.connected = false;
			}
		);
	},
	watch: {
		connected(new_val, old_val) {
			if (new_val == old_val)
				return;
			if (new_val) {
				fetch("/api/measuring_field").then(res => {
					if (res.ok)
						res.json().then(data => this.measuring_field = data);
				});
				fetch("/api/measuring_scattering").then(res => {
					if (res.ok)
						res.json().then(data => this.measuring_scattering = data);
				});
			} else {
				this.measuring_field = false;
			}
		}
	},
	methods: {
		interrupt_field_measure() {
			fetch("/api/interrupt_measure_field", { method: "POST" });
		},
	}
}).mount("#app_connection");
