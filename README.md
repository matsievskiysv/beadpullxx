# Description

Headless server for controlling RF field test bench via bead pulling method.

![scheme](./extra/scheme/stand.png)

Supported motor drivers:

* UIMotor UIM241 (using serial bus)

Supported analyzer drivers:

* [LiteVNA](https://www.zeenko.tech/litevna) (using serial bus)
* Agilent 506x (using GPIB bus)
* Agilent 87xx (using GPIB bus)

# Installation

Program was tested under [Debian](https://www.debian.org/) operation system.

## Clone project

Clone project to build folder.

```bash
git clone --recursive https://gitlab.com/matsievskiysv/beadpullxx.git
```

## Install dependencies

### Common

Common dependencies

```bash
sudo apt install build-essential cmake cmake-curses-gui libdocopt-dev libssl-dev libyaml-cpp-dev libboost-filesystem1.81-dev libboost-log1.81-dev libboost-json1.81-dev npm ruby-sass pkg-config
```

### Serial bus

Serial bus dependencies

```bash
sudo apt install libserial-dev
```

### GPIB bus

In order to use [*Linux-GPIB*](https://linux-gpib.sourceforge.io/) drivers, one must compile them.

**Note:** Drivers are build for current *Linux* kernel. So, after kernel update one needs to recompile GPIB drivers.

#### Getting sources

The following additional programs are needed:
```bash
sudo apt install -y git git-svn
```

Getting the latest sources:
```bash
git svn clone -r HEAD https://svn.code.sf.net/p/linux-gpib/code/trunk linux-gpib-code && cd linux-gpib-code
```

#### Compiling drivers

For driver compilation you will need Linux sources:
```bash
sudo apt install linux-source
```

```bash
cd linux-gpib-kernel
```

Issue following command to start compilation:

```bash
make
```

And then:

```bash
make install
```

That's it for driver compilation. Return one folder back.

```bash
cd ..
```

### Compiling userspace utilities

```bash
cd linux-gpib-user
```

Since we use source from project repo, we need to prepare [autotools](https://www.gnu.org/software/automake/) installation scripts:

```bash
apt install autotools-dev
./bootstrap
./configure --sysconfdir=/etc
make
make install
```

### Configuring GPIB device

Download GPIB board firmware form <https://linux-gpib.sourceforge.io/firmware/>.

Later in this document `agilent_82350a` device will be used.
Firmware archive is assumed to be located in `~/Downloads/` folder.

Configure your board in `/etc/gpib.conf`. Consult [configuration manual](https://linux-gpib.sourceforge.io/doc_html/configuration-gpib-conf.html).
Configuration will look something like this:
```ini
interface {
        minor = 0       /* board index, minor = 0 uses /dev/gpib0, minor = 1 uses /dev/gpib1, etc. */
        board_type = "agilent_82350b"   /* type of interface board being used */
        name = "violet" /* optional name, allows you to get a board descriptor using ibfind() */
        pad = 0 /* primary address of interface             */
        sad = 0 /* secondary address of interface           */
        timeout = T3s   /* timeout for commands */

        eos = 0x0a      /* EOS Byte, 0xa is newline and 0xd is carriage return
*/
        set-reos = yes  /* Terminate read if EOS */
        set-bin = no    /* Compare EOS 8-bit */
        set-xeos = no   /* Assert EOI whenever EOS byte is sent */
        set-eot = yes   /* Assert EOI with last byte on writes */

/* settings for boards that lack plug-n-play capability */
        base = 0        /* Base io ADDRESS                  */
        irq  = 0        /* Interrupt request level */
        dma  = 0        /* DMA channel (zero disables)      */
}

device {
        minor = 0       /* minor number for interface board this device is connected to */
        name = "analyzer"       /* device mnemonic */
        pad = 7 /* The Primary Address */
        sad = 0 /* Secondary Address */
        timeout = T3s

        eos = 0xa       /* EOS Byte */
        set-reos = no /* Terminate read if EOS */
        set-bin = no /* Compare EOS 8-bit */
}
```

Upload board firmware, if necessary:
```bash
sudo gpib_config --init-data ~/Downloads/gpib_firmware-2008-08-10/hp_82350a/agilent_82350a.bin
```

### Configure udev rules

Some boards require firmware upload on every start.
Also, superuser privileges are required for communication with device.
In order to remove these problems, lets add udev rule for device.

Firstly, lets create new group, which will be able to use GPIB devices:
```bash
addgroup gpib
```

Copy your device's firmware to `/usr/local/sbin`.
Command will look something like this:
```bash
cp Downloads/gpib_firmware-2008-08-10/hp_82350a/agilent_82350a.bin /usr/local/sbin/
```
Then, create a file `/usr/local/sbin/load_agilent` with contents:
```bash
#!/bin/bash

gpib_config --init-data /usr/local/sbin/agilent_82350a.bin
```

And allow its execution:
```bash
chmod 755 /usr/local/sbin/load_agilent
```

Add the following line to the `/etc/udev/rules.d/99-gpib.rules` file:
```udev
KERNEL=="gpib0", SUBSYSTEM=="gpib_common", GROUP="gpib", MODE="0660", RUN+="/usr/local/sbin/load_agilent"
```

Now device will be available for users in group `gpib`, its firmware will be automatically loaded.

## Select drivers

Enter `ccmake` configuration tool

```bash
ccmake -S . -B build
```

Press <kbd>c</kbd> to initially configure project. After that, device drivers may be selected from the list.
Variable prefixes are

* `ANALYZER`: analyzer drivers
* `MOTOR`: motor drivers
* `BUS`: bus drivers

After selecting drivers, press <kbd>c</kbd> to apply the selection.

## Build release version

Issue the command

```bash
npm install
npm run build
```

## Build testing version

Testing version requires extra packages

```bash
sudo apt install valgrind
```

Issue the command

```bash
npm install
npm run build_debug
```

To run tests, issue the command

```bash
npm run build_debug
npm run test
```

## Build documentation

Building documentation requires extra packages

```bash
sudo apt install doxygen graphviz
```

Issue the command

```bash
npm run build_doc
```

HTTP endpoints are documented in the \ref endpoint section of the Doxygen documentation.

## Generate test coverage report

Generating test coverage report requires extra packages

```bash
sudo apt install gcovr
```

Enable `TEST_COVERAGE_REPORT` option via `ccmake` and build testing version of the program.
Then, to generate test coverage report, issue the command

```bash
npm run test_coverage
```

# Run program

To start the program

* Generate certificate

   ```bash
   ./extra/create_cert.sh
   ```
* Adjust program configuration in `config.yaml` file, if needed
* Issue the command

   ```bash
   npm run start
   ```
