#pragma once

#include <config.h>
#ifdef BUS_SERIAL

/** @file serial.hxx
 * @brief bus::Serial bus implementation.
 */

#include "bus.hxx"

#include <libserial/SerialPort.h>
#include <thread>

namespace bus
{

/**
 * @brief Serial Bus implementation.
 */
class Serial : public Bus
{
      private:
	std::unique_ptr<LibSerial::SerialPort> port;		       ///< Serial port.
	std::jthread			       thr;		       ///< Interrupt thread.
	bool				       data_available = false; ///< Interrupt flag.

      public:
	static constexpr const char *NAME = "serial"; ///< Bus name.
	/**
	 * @brief Bus attribute choices.
	 *
	 * @return Bus attribute choices.
	 */
	static const util::attribute_t &attribute_choices(void);

	/**
	 * @brief Serial constructor.
	 *
	 * @param[in] timeout Communication timeout value.
	 * @param[in] notifier Message ready notifier.
	 * @param[in] attributes Bus attributes.
	 */
	Serial(util::timeout_t &&timeout, util::notifier_t notifier, util::attribute_t &&attributes);

	/**
	 * @brief Do not allow Serial copying.
	 */
	Serial(Serial &) = delete;

	/**
	 * @brief Do not allow Serial moving.
	 */
	Serial(Serial &&) = delete;

	/**
	 * @brief Serial destructor.
	 */
	~Serial();

	const util::string_t &name(void) noexcept;
	void		      send(const util::msg_t &msg);
	void		      rcv(util::msg_t &msg, std::size_t count);
	void		      flush_input(void);
	void		      flush_output(void);
	void		      flush(void);
	bool		      interrupted(void) const noexcept;
	void		      check_interrupt(util::msg_t &msg, std::size_t count);
};

} // namespace bus

#endif // BUS_SERIAL
