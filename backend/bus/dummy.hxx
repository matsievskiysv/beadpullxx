#pragma once

#include <config.h>
#ifdef BUS_DUMMY

/** @file dummy.hxx
 * @brief bus::Dummy bus::Bus implementation for testing purposes.
 */

#include "bus.hxx"

namespace bus
{

/**
 * @brief Dummy Bus implementation for testing purposes.
 */
class Dummy : public Bus
{
      private:
	bool intpted		     = false; ///< Interrupted flag.
	bool fail_on_constructor     = false; ///< Fail on Dummy::Dummy(). Set via attributes.
	bool timeout_on_constructor  = false; ///< Timeout on Dummy::Dummy(). Set via attributes.
	bool fail_on_send	     = false; ///< Fail on send(). Set via attributes.
	bool fail_on_rcv	     = false; ///< Fail on rcv(). Set via attributes.
	bool fail_on_check_interrupt = false; ///< Fail on check_interrupt(). Set via attributes.

      public:
	static constexpr const char *NAME = "dummy"; ///< Bus name.
	/**
	 * @brief Bus attribute choices.
	 *
	 * @return Bus attribute choices.
	 */
	static const util::attribute_t &attribute_choices(void);

	/**
	 * @brief Bus constructor.
	 *
	 * @param[in] timeout Communication timeout value.
	 * @param[in] notifier Message ready notifier.
	 * @param[in] attributes Bus attributes.
	 */
	Dummy(util::timeout_t &&timeout, util::notifier_t notifier, util::attribute_t &&attributes);

	/**
	 * @brief Do not allow Dummy copying.
	 */
	Dummy(Dummy &) = delete;

	/**
	 * @brief Do not allow Dummy moving.
	 */
	Dummy(Dummy &&) = delete;

	/**
	 * @brief Bus destructor.
	 */
	~Dummy();

	const util::string_t &name(void) noexcept;
	void		      send(const util::msg_t &msg);
	void		      rcv(util::msg_t &msg, std::size_t count);
	bool		      interrupted(void) const noexcept;
	void		      check_interrupt(util::msg_t &msg, std::size_t count);
};

} // namespace bus

#endif // BUS_DUMMY
