#include <config.h>
#ifdef BUS_GPIB

/** @file gpib.cxx
 * @brief @ref bus::GPIB @ref bus::Bus implementation.
 */

// GCOVR_EXCL_START do not test hardware driver

#include "gpib.hxx"

#include <boost/filesystem.hpp>
#include <boost/log/trivial.hpp>
#include <boost/regex.hpp>
#include <gpib/ib.h>
#include <sstream>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <types.hxx>

namespace fs = boost::filesystem;
namespace re = boost;

namespace bus
{

/**
 * @brief GPIB bus error.
 */
enum GPIBError {
	EDVR = 0, ///< A system call has failed. ibcnt/ibcntl will be set to the value of errno.
	ECIC = 1, ///< Your interface board needs to be controller-in-charge, but is not.
	ENOL =
	    2, ///< You have attempted to write data or command bytes, but there are no listeners currently addressed.
	EADR = 3, ///< The interface board has failed to address itself properly before starting an io operation.
	EARG = 4, ///< One or more arguments to the function call were invalid.
	ESAC = 5, ///< The interface board needs to be system controller, but is not.
	EABO = 6, ///< A read or write of data bytes has been aborted, possibly due to a timeout or reception of a
		  ///< device clear command.
	ENEB =
	    7, ///< The GPIB interface board does not exist, its driver is not loaded, or it is not configured properly.
	EDMA = 8,  ///< Not used (DMA error), included for compatibility purposes.
	EOIP = 10, ///< Function call can not proceed due to an asynchronous IO operation (ibrda(), ibwrta(), or
		   ///< ibcmda()) in progress.
	ECAP = 11, ///< Incapable of executing function call, due the GPIB board lacking the capability, or the
		   ///< capability being disabled in software.
	EFSO = 12, ///< File system error. ibcnt/ibcntl will be set to the value of errno.
	EBUS = 14, ///< An attempt to write command bytes to the bus has timed out.
	ESTB = 15, ///< One or more serial poll status bytes have been lost. This can occur due to too many status bytes
		   ///< accumulating (through automatic serial polling) without being read.
	ESRQ = 16, ///< The serial poll request service line is stuck on. This can occur if a physical device on the bus
		   ///< requests service, but its GPIB address has not been opened (via ibdev() for example) by any
		   ///< process. Thus the automatic serial polling routines are unaware of the device's existence and
		   ///< will never serial poll it.
	ETAB = 20, ///< This error can be returned by ibevent(), FindLstn(), or FindRQS(). See their descriptions for
		   ///< more information.
};

/**
 * @brief GPIB device status.
 */
enum class GPIBDeviceStatus {
	ERROR,
	TIMEOUT,
	SERVICE_REQUIRED,
	COMPLETE,
	OTHER,
};

/**
 * @brief GPIB bus error.
 */
enum GPIBStatus {
	DCAS = (1 << 0), ///< DCAS is set when a board receives the device clear command (that is, the SDC or DCL
			 ///< command byte). It is cleared on the next 'traditional' or 'multidevice' function
			 ///< call following ibwait() (with DCAS set in the wait mask), or following a read or
			 ///< write (ibrd(), ibwrt(), Receive(), etc.). The DCAS and DTAS bits will only be set if
			 ///< the event queue is disabled. The event queue may be disabled with ibconfig().

	DTAS = (1 << 1),   ///< DTAS is set when a board has received a device trigger command (that is, the GET
			   ///< command byte). It is cleared on the next 'traditional' or 'multidevice' function call
			   ///< following ibwait() (with DTAS in the wait mask). The DCAS and DTAS bits will only be
			   ///< set if the event queue is disabled. The event queue may be disabled with ibconfig().
	LACS = (1 << 2),   ///< Board is currently addressed as a listener (IEEE listener state machine is in LACS
			   ///< or LADS).
	TACS = (1 << 3),   ///< Board is currently addressed as talker (IEEE talker state machine is in TACS or
			   ///< TADS).
	ATN  = (1 << 4),   ///< The ATN line is asserted.
	CIC  = (1 << 5),   ///< Board is controller-in-charge, so it is able to set the ATN line.
	REM  = (1 << 6),   ///< Board is in 'remote' state.
	LOK  = (1 << 7),   ///< Board is in 'lockout' state.
	CMPL = (1 << 8),   ///< I/O operation is complete. Useful for determining when an asynchronous I/O operation
			   ///< (ibrda(), ibwrta(), etc) has completed.
	EVENT = (1 << 9),  ///< One or more clear, trigger, or interface clear events have been received, and are
			   ///< available in the event queue (see ibevent()). The EVENT bit will only be set if the
			   ///< event queue is enabled. The event queue may be enabled with ibconfig().
	SPOLL = (1 << 10), ///< If this bit is enabled (see ibconfig()), it is set when the board is serial polled.
			   ///< The SPOLL bit is cleared when the board requests service (see ibrsv()) or you call
			   ///< ibwait() on the board with SPOLL in the wait mask.
	RQS = (1 << 11),   ///< RQS indicates that the device has requested service, and one or more status bytes are
			   ///< available for reading with ibrsp(). RQS will only be set if you have automatic serial
			   ///< polling enabled (see ibconfig()).
	SRQI = (1 << 12),  ///< SRQI indicates that a device connected to the board is asserting the SRQ line. It is
			   ///< only set if the board is the controller-in-charge. If automatic serial polling is
			   ///< enabled (see ibconfig()), SRQI will generally be cleared, since when a device
			   ///< requests service it will be automatically polled and then unassert SRQ.
	END = (1 << 13),   ///< END is set if the last io operation ended with the EOI line asserted, and may be set
			   ///< on reception of the end-of-string character. The IbcEndBitIsNormal option of
			   ///< ibconfig() can be used to configure whether or not END should be set on reception of
			   ///< the eos character.
	TIMO = (1 << 14),  ///< TIMO indicates that the last io operation or ibwait() timed out.
	ERR  = (1 << 15),  ///< ERR is set if the last 'traditional' or 'multidevice' function call failed. The
			   ///< global variable iberr will be set indicate the cause of the error.
};

/**
 * @brief GPIB timeout value.
 */
enum GPIBTimeout {
	TNONE  = 0,  ///< Never timeout
	T10us  = 1,  ///< 10 microseconds
	T30us  = 2,  ///< 30 microseconds
	T100us = 3,  ///< 100 microseconds
	T300us = 4,  ///< 300 microseconds
	T1ms   = 5,  ///< 1 millisecond
	T3ms   = 6,  ///< 3 milliseconds
	T10ms  = 7,  ///< 10 milliseconds
	T30ms  = 8,  ///< 30 milliseconds
	T100ms = 9,  ///< 100 milliseconds
	T300ms = 10, ///< 300 milliseconds
	T1s    = 11, ///< 1 second
	T3s    = 12, ///< 3 seconds
	T10s   = 13, ///< 10 seconds
	T30s   = 14, ///< 30 seconds
	T100s  = 15, ///< 100 seconds
	T300s  = 16, ///< 300 seconds
	T1000s = 17, ///< 1000 seconds
};

/**
 * @brief GPIB EOS style.
 */
enum GPIBEOS {
	REOS = 0x400, ///< Enable termination of reads when eos character is received.
	XEOS = 0x800, ///< Assert the EOI line whenever the eos character is sent during writes.
	BIN =
	    0x1000, ///< Match eos character using all 8 bits (instead of only looking at the 7 least significant bits).
};

/**
 * @brief Convert timeout value to discrete internal level.
 *
 * @param timeout Continuous timeout value.
 * @return Discrete timeout value.
 */
static GPIBTimeout
timeout_clamp(util::timeout_t timeout)
{
	if (timeout <= std::chrono::microseconds(10))
		return GPIBTimeout::T10us;
	else if (timeout <= std::chrono::microseconds(30))
		return GPIBTimeout::T30us;
	else if (timeout <= std::chrono::microseconds(100))
		return GPIBTimeout::T100us;
	else if (timeout <= std::chrono::microseconds(300))
		return GPIBTimeout::T300us;
	else if (timeout <= std::chrono::milliseconds(1))
		return GPIBTimeout::T1ms;
	else if (timeout <= std::chrono::milliseconds(3))
		return GPIBTimeout::T3ms;
	else if (timeout <= std::chrono::milliseconds(10))
		return GPIBTimeout::T10ms;
	else if (timeout <= std::chrono::milliseconds(30))
		return GPIBTimeout::T30ms;
	else if (timeout <= std::chrono::milliseconds(100))
		return GPIBTimeout::T100ms;
	else if (timeout <= std::chrono::milliseconds(300))
		return GPIBTimeout::T300ms;
	else if (timeout <= std::chrono::seconds(1))
		return GPIBTimeout::T1s;
	else if (timeout <= std::chrono::seconds(3))
		return GPIBTimeout::T3s;
	else if (timeout <= std::chrono::seconds(10))
		return GPIBTimeout::T10s;
	else if (timeout <= std::chrono::seconds(30))
		return GPIBTimeout::T30s;
	else if (timeout <= std::chrono::seconds(100))
		return GPIBTimeout::T100s;
	else if (timeout <= std::chrono::seconds(300))
		return GPIBTimeout::T300s;
	else
		return GPIBTimeout::T1000s;
}

/**
 * @brief Parse device status.
 *
 * @param status Device status register.
 * @return Device status.
 */
static GPIBDeviceStatus
device_status(std::uint16_t status)
{
	if (status & GPIBStatus::ERR)
		return GPIBDeviceStatus::ERROR;
	else if (status & GPIBStatus::TIMO)
		return GPIBDeviceStatus::TIMEOUT;
	else if (status & GPIBStatus::RQS)
		return GPIBDeviceStatus::SERVICE_REQUIRED;
	else if (status & GPIBStatus::CMPL)
		return GPIBDeviceStatus::COMPLETE;
	else
		return GPIBDeviceStatus::OTHER;
}

util::attribute_t _attribute_choices = {{"template", util::string_t(GPIB::NAME)},
					{"device", {{"choices", nullptr}, {"value", nullptr}}},
					{"address", {{"min", 0}, {"max", 30}, {"step", 1}, {"value", 1}}}};

const util::attribute_t &
GPIB::attribute_choices(void)
{
	boost::json::array device_choices;
	const re::regex	   pattern(BUS_GPIB_PATTERN);
	re::cmatch	   match;
	for (auto entry : fs::directory_iterator("/dev")) {
		if (re::regex_match(entry.path().filename().c_str(), match, pattern)) {
			device_choices.push_back(util::string_t(entry.path().c_str()));
		}
	}
	std::sort(device_choices.begin(), device_choices.end(), [](const auto left, const auto right) {
		auto lefts  = left.as_string();
		auto rights = right.as_string();
		if (lefts.size() == rights.size())
			return left.as_string() < right.as_string();
		else
			return lefts.size() < rights.size();
	});

	_attribute_choices.as_object()["device"].as_object()["choices"] = device_choices;
	_attribute_choices.as_object()["device"].as_object()["value"] =
	    device_choices.size() > 0 ? device_choices.front() : "";
	return _attribute_choices;
}

GPIB::GPIB(util::timeout_t &&timeout, util::notifier_t notifier, util::attribute_t &&attributes)
    : Bus(std::move(timeout), notifier, std::move(attributes))
{
	BOOST_LOG_TRIVIAL(trace) << "[GPIB] open port";
	BOOST_LOG_TRIVIAL(trace) << "[GPIB] attributes " << attributes;
	status_byte_mask   = attributes.as_object()["status_byte_mask"].as_int64();
	auto	    device = attributes.as_object()["device"].as_string();
	struct stat dev_stat;
	if (stat(device.c_str(), &dev_stat)) {
		BOOST_LOG_TRIVIAL(warning) << "[GPIB] cannot stat file";
		throw std::runtime_error("cannot stat file");
	}
	if (!S_ISCHR(dev_stat.st_mode)) {
		BOOST_LOG_TRIVIAL(warning) << "[GPIB] not a device";
		throw std::runtime_error("not a device");
	}
	auto minor   = minor(dev_stat.st_rdev);
	auto address = attributes.as_object()["address"].as_int64();
	auto send_eoi =
	    attributes.as_object()["send_eoi"].is_bool() ? attributes.as_object()["send_eoi"].as_bool() : true;
	auto eos_reos =
	    attributes.as_object()["eos_reos"].is_bool() ? attributes.as_object()["eos_reos"].as_bool() : false;
	auto eos_xeos =
	    attributes.as_object()["eos_xeos"].is_bool() ? attributes.as_object()["eos_xeos"].as_bool() : false;
	auto eos_bin =
	    attributes.as_object()["eos_bin"].is_bool() ? attributes.as_object()["eos_bin"].as_bool() : false;
	BOOST_LOG_TRIVIAL(debug) << "[GPIB] send_eoi: " << send_eoi << " ;eos_reos: " << eos_reos
				 << " ;eos_xeos: " << eos_xeos << " ;eos_bin: " << eos_bin;
	auto timeout_clamped = timeout_clamp(timeout);
	switch (timeout_clamped) {
	case GPIBTimeout::TNONE:
		BOOST_LOG_TRIVIAL(debug) << "[GPIB] timeout: none";
		break;
	case GPIBTimeout::T10us:
		BOOST_LOG_TRIVIAL(debug) << "[GPIB] timeout: 10us";
		break;
	case GPIBTimeout::T30us:
		BOOST_LOG_TRIVIAL(debug) << "[GPIB] timeout: 30us";
		break;
	case GPIBTimeout::T100us:
		BOOST_LOG_TRIVIAL(debug) << "[GPIB] timeout: 100us";
		break;
	case GPIBTimeout::T300us:
		BOOST_LOG_TRIVIAL(debug) << "[GPIB] timeout: 300us";
		break;
	case GPIBTimeout::T1ms:
		BOOST_LOG_TRIVIAL(debug) << "[GPIB] timeout: 1ms";
		break;
	case GPIBTimeout::T3ms:
		BOOST_LOG_TRIVIAL(debug) << "[GPIB] timeout: 3ms";
		break;
	case GPIBTimeout::T10ms:
		BOOST_LOG_TRIVIAL(debug) << "[GPIB] timeout: 10ms";
		break;
	case GPIBTimeout::T30ms:
		BOOST_LOG_TRIVIAL(debug) << "[GPIB] timeout: 30ms";
		break;
	case GPIBTimeout::T100ms:
		BOOST_LOG_TRIVIAL(debug) << "[GPIB] timeout: 100ms";
		break;
	case GPIBTimeout::T300ms:
		BOOST_LOG_TRIVIAL(debug) << "[GPIB] timeout: 300ms";
		break;
	case GPIBTimeout::T1s:
		BOOST_LOG_TRIVIAL(debug) << "[GPIB] timeout: 1s";
		break;
	case GPIBTimeout::T3s:
		BOOST_LOG_TRIVIAL(debug) << "[GPIB] timeout: 3s";
		break;
	case GPIBTimeout::T10s:
		BOOST_LOG_TRIVIAL(debug) << "[GPIB] timeout: 10s";
		break;
	case GPIBTimeout::T30s:
		BOOST_LOG_TRIVIAL(debug) << "[GPIB] timeout: 30s";
		break;
	case GPIBTimeout::T100s:
		BOOST_LOG_TRIVIAL(debug) << "[GPIB] timeout: 100s";
		break;
	case GPIBTimeout::T300s:
		BOOST_LOG_TRIVIAL(debug) << "[GPIB] timeout: 300s";
		break;
	case GPIBTimeout::T1000s:
		BOOST_LOG_TRIVIAL(debug) << "[GPIB] timeout: 1000s";
		break;
	}
	short found_listener;
	if ((handle = ibdev(minor, address, NO_SAD, timeout_clamped, send_eoi,
			    (eos_reos ? GPIBEOS::REOS : 0) | (eos_xeos ? GPIBEOS::XEOS : 0) |
				(eos_bin ? GPIBEOS::BIN : 0))) < 0) {
		BOOST_LOG_TRIVIAL(warning) << "[GPIB] cannot open device";
		throw std::runtime_error("cannot open device");
	}
	if ((device_status(ibln(minor, address, 0, &found_listener)) == GPIBDeviceStatus::ERROR) ||
	    (found_listener == 0)) {
		ibonl(handle, 0);
		BOOST_LOG_TRIVIAL(warning) << "[GPIB] Cannot open device at address " << address;
		util::msg_t addresses;
		for (std::uint8_t addr = 0; addr <= 30; ++addr) {
			BOOST_LOG_TRIVIAL(trace) << "[GPIB] probe device at address: " << int(addr);
			if ((device_status(ibln(minor, addr, NO_SAD, &found_listener)) != GPIBDeviceStatus::ERROR) &&
			    found_listener) {
				BOOST_LOG_TRIVIAL(debug) << "[GPIB] found device at address " << addr;
				addresses.push_back(addr);
			}
		}
		std::stringstream addresses_str;
		if (addresses.size() > 0) {
			addresses_str << "cannot open device at address " << address << ". Found devices at "
				      << addresses;
			BOOST_LOG_TRIVIAL(warning) << "[GPIB] Found devices at " << addresses;
			throw std::runtime_error(addresses_str.str());
		} else {
			BOOST_LOG_TRIVIAL(warning) << "[GPIB] Not found devices on the bus";
			addresses_str << "cannot open device at address " << address
				      << ". Not found any devices on the bus";
			throw std::runtime_error(addresses_str.str());
		}
	}
	if (device_status(ibclr(handle)) == GPIBDeviceStatus::ERROR) {
		BOOST_LOG_TRIVIAL(warning) << "[GPIB] cannot clear device";
		throw std::runtime_error("cannot clear device");
	}
}

GPIB::~GPIB()
{
	ibonl(handle, 0);
	BOOST_LOG_TRIVIAL(trace) << "[GPIB] close port";
}

const util::string_t _name = util::string_t(GPIB::NAME);

const util::string_t &
GPIB::name(void) noexcept
{
	return _name;
}

void
GPIB::send(const util::msg_t &message)
{
	BOOST_LOG_TRIVIAL(trace) << "[GPIB] send " << std::hex << std::showbase << std::nouppercase << message;
	switch (device_status(ibwrt(handle, message.data(), message.size()))) {
	case GPIBDeviceStatus::ERROR:
		BOOST_LOG_TRIVIAL(trace) << "[GPIB] error writing to device";
		throw std::runtime_error("error writing to device");
		break;
	case GPIBDeviceStatus::TIMEOUT:
		BOOST_LOG_TRIVIAL(trace) << "[GPIB] timeout writing to device";
		throw std::runtime_error("timeout writing to device");
		break;
	default:
		break;
	}
	if (ThreadIbcnt() != int(message.size())) {
		BOOST_LOG_TRIVIAL(trace) << "[GPIB] inconsistent write size";
		throw std::runtime_error("inconsistent write size");
	}
}

void
GPIB::rcv(util::msg_t &message, std::size_t count)
{
	message.clear();
	message.resize(count, 0);
	switch (device_status(ibrd(handle, message.data(), count))) {
	case GPIBDeviceStatus::ERROR:
		BOOST_LOG_TRIVIAL(trace) << "[GPIB] error reading device";
		throw std::runtime_error("error reading device");
		break;
	case GPIBDeviceStatus::TIMEOUT:
		BOOST_LOG_TRIVIAL(trace) << "[GPIB] timeout reading device";
		throw std::runtime_error("timeout reading device");
		break;
	default:
		break;
	}
	message.resize(ThreadIbcnt());
	BOOST_LOG_TRIVIAL(trace) << "[GPIB] receive (" << message.size() << ") " << std::hex << std::showbase
				 << std::nouppercase << message;
}

bool
GPIB::interrupted(void) const noexcept
{
	char event = 0;
	switch (device_status(ibrsp(handle, &event))) {
	case GPIBDeviceStatus::ERROR:
		BOOST_LOG_TRIVIAL(warning) << "[GPIB] error polling device";
		return false;
	case GPIBDeviceStatus::TIMEOUT:
		BOOST_LOG_TRIVIAL(warning) << "[GPIB] timeout polling device";
		return false;
	default:
		return event & status_byte_mask;
	}
}

void
GPIB::check_interrupt(util::msg_t &message, std::size_t)
{
	char event = 0;
	switch (device_status(ibrsp(handle, &event))) {
	case GPIBDeviceStatus::ERROR:
		BOOST_LOG_TRIVIAL(warning) << "[GPIB] error polling device";
		throw std::runtime_error("error polling device");
	case GPIBDeviceStatus::TIMEOUT:
		BOOST_LOG_TRIVIAL(warning) << "[GPIB] timeout polling device";
		throw std::runtime_error("timeout polling device");
	default:
		message.push_back(event);
		return;
	}
}

} // namespace bus

// GCOVR_EXCL_STOP
#endif // BUS_GPIB
