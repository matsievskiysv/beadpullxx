/** @file list.hxx
 * @brief @ref bus::Bus list.
 */

#pragma once

#include "bus.hxx"

#include <config.h>
#include <types.hxx>
#ifdef BUS_DUMMY
#include "dummy.hxx"
#endif
#ifdef BUS_SERIAL
#include "serial.hxx"
#endif
#ifdef BUS_GPIB
#include "gpib.hxx"
#endif

namespace bus
{

extern const util::name_list_t bus_list; ///< List of compiled Bus drivers.

/**
 * @brief Attribute choices for the Bus.
 *
 * @param[in] bus Bus name.
 * @return Bus attributes.
 */
const util::attribute_t &get_attribute_choices(const util::string_t &bus);

/**
 * @brief Create Bus.
 *
 * @param[in] bus Bus name.
 * @param[in] timeout Bus timeout value.
 * @param[in] notifier controller::Controller event notifier.
 * @param[in] attributes Bus attributes.
 * @return Bus pointer.
 * @throws std::runtime_error Any constructor errors.
 * @throws Timeout Communication timeout.
 */
std::shared_ptr<Bus> create_bus(const util::string_t &bus, util::timeout_t &&timeout, util::notifier_t notifier,
				util::attribute_t &&attributes);

} // namespace bus
