#include <config.h>
#ifdef BUS_DUMMY

/** @file dummy.cxx
 * @brief Dummy @ref bus::Bus implementation for testing purposes.
 */

#include "dummy.hxx"

#include <boost/log/trivial.hpp>
#include <thread>

namespace bus
{

static const util::attribute_t _attribute_choices = {
    {"template", util::string_t(Dummy::NAME)},
    {"device", {{"choices", {"/dev/ttyUSB0", "/dev/ttyUSB1", "/dev/ttyUSB2"}}, {"value", "/dev/ttyUSB0"}}},
    {"baudrate", {{"choices", {9600, 57600, 115200}}, {"value", 57600}}},
    {"delay", {{"min", 0.1}, {"max", 0.5}, {"step", 0.1}, {"value", 0.3}}},
    {"timeout_on_constructor", {{"value", false}}},
    {"fail_on_constructor", {{"value", false}}},
    {"fail_on_check_interrupt", {{"value", false}}},
    {"fail_on_send", {{"value", false}}},
    {"fail_on_rcv", {{"value", false}}}}; ///< Bus attribute choices.

const util::attribute_t &
Dummy::attribute_choices(void)
{
	return _attribute_choices;
}

Dummy::Dummy(util::timeout_t &&timeout, util::notifier_t notifier, util::attribute_t &&attributes)
    : Bus(std::move(timeout), notifier, std::move(attributes))
{
	attributes.as_object()["device"].as_string();
	attributes.as_object()["baudrate"].as_int64();
	attributes.as_object()["delay"].as_double();
	fail_on_constructor	= JSON_DEFAULT(attributes.as_object(), "fail_on_constructor", bool, false);
	timeout_on_constructor	= JSON_DEFAULT(attributes.as_object(), "timeout_on_constructor", bool, false);
	fail_on_send		= JSON_DEFAULT(attributes.as_object(), "fail_on_send", bool, false);
	fail_on_rcv		= JSON_DEFAULT(attributes.as_object(), "fail_on_rcv", bool, false);
	fail_on_check_interrupt = JSON_DEFAULT(attributes.as_object(), "fail_on_check_interrupt", bool, false);

	BOOST_LOG_TRIVIAL(trace) << "[BusDummy] fail_on_constructor: " << fail_on_constructor;
	BOOST_LOG_TRIVIAL(trace) << "[BusDummy] timeout_on_constructor: " << timeout_on_constructor;
	BOOST_LOG_TRIVIAL(trace) << "[BusDummy] fail_on_send: " << fail_on_send;
	BOOST_LOG_TRIVIAL(trace) << "[BusDummy] fail_on_rcv: " << fail_on_rcv;
	BOOST_LOG_TRIVIAL(trace) << "[BusDummy] fail_on_check_interrupt: " << fail_on_check_interrupt;
	if (fail_on_constructor)
		throw std::runtime_error("fail on demand");
	if (timeout_on_constructor)
		std::this_thread::sleep_for(10 * timeout);
	BOOST_LOG_TRIVIAL(debug) << "[BusDummy] create bus";
}

Dummy::~Dummy()
{
	BOOST_LOG_TRIVIAL(debug) << "[BusDummy] free bus";
}

static const util::string_t _name = util::string_t(Dummy::NAME); ///< Bus name.

const util::string_t &
Dummy::name(void) noexcept
{
	return _name;
}

void
Dummy::send(const util::msg_t &)
{
	BOOST_LOG_TRIVIAL(trace) << "[BusDummy] send";
	if (fail_on_send)
		throw std::runtime_error("fail on demand");
	std::this_thread::sleep_for(timeout / 2);
}

void
Dummy::rcv(util::msg_t &, std::size_t count)
{
	BOOST_LOG_TRIVIAL(trace) << "[BusDummy] rcv count: " << count;
	if (fail_on_rcv)
		throw std::runtime_error("fail on demand");
	std::this_thread::sleep_for(timeout / 2);
}

bool
Dummy::interrupted(void) const noexcept
{
	return intpted;
}

void
Dummy::check_interrupt(util::msg_t &, std::size_t)
{
	if (fail_on_check_interrupt)
		throw std::runtime_error("fail on demand");
}

} // namespace bus

#endif // BUS_DUMMY
