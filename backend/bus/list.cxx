/** @file list.cxx
 * @brief @ref bus::Bus list.
 */

#include "list.hxx"

namespace bus
{

const util::name_list_t bus_list({
#ifdef BUS_DUMMY
    util::string_t(Dummy::NAME),
#endif
#ifdef BUS_SERIAL
    util::string_t(Serial::NAME),
#endif
#ifdef BUS_GPIB
    util::string_t(GPIB::NAME),
#endif
}); ///< List of compiled Bus drivers.

const util::attribute_t &
get_attribute_choices([[maybe_unused]] const util::string_t &bus)
{
#ifdef BUS_DUMMY
	if (bus == Dummy::NAME)
		return Dummy::attribute_choices();
#endif
// GCOVR_EXCL_START do not test hardware driver
#ifdef BUS_SERIAL
	if (bus == Serial::NAME)
		return Serial::attribute_choices();
#endif
#ifdef BUS_GPIB
	if (bus == GPIB::NAME)
		return GPIB::attribute_choices();
#endif
// GCOVR_EXCL_STOP
	throw std::runtime_error("unknown bus");
}

std::shared_ptr<Bus>
create_bus([[maybe_unused]] const util::string_t &bus, [[maybe_unused]] util::timeout_t &&timeout,
	   [[maybe_unused]] util::notifier_t notifier, [[maybe_unused]] util::attribute_t &&attributes)
{
#ifdef BUS_DUMMY
	if (bus == Dummy::NAME)
		return std::shared_ptr<Bus>(new Dummy(std::move(timeout), notifier, std::move(attributes)));
#endif
// GCOVR_EXCL_START do not test hardware driver
#ifdef BUS_SERIAL
	if (bus == Serial::NAME)
		return std::shared_ptr<Bus>(new Serial(std::move(timeout), notifier, std::move(attributes)));
#endif
#ifdef BUS_GPIB
	if (bus == GPIB::NAME)
		return std::shared_ptr<Bus>(new GPIB(std::move(timeout), notifier, std::move(attributes)));
#endif
// GCOVR_EXCL_STOP
	throw std::runtime_error("unknown bus");
}

} // namespace bus
