/** @file bus.cxx
 * @brief Base bus::Bus and related classes definition.
 */

#include "bus.hxx"

namespace bus
{

static const util::attribute_t _attribute_choices = {{"template", nullptr}}; ///< Bus attribute choices.

const util::attribute_t &
Bus::attribute_choices(void)
{
	return _attribute_choices;
}

Bus::Bus(util::timeout_t &&timeout, util::notifier_t notifier, util::attribute_t &&attributes)
    : timeout(timeout), notifier(notifier), attributes(attributes)
{
}

Bus::~Bus()
{
}

const std::lock_guard<std::mutex>
Bus::get_lock(void) noexcept
{
	return std::lock_guard<std::mutex>(mutex);
}

static const util::string_t _name = util::string_t(Bus::NAME); ///< Bus name.

const util::string_t &
Bus::name(void) noexcept
{
	return _name;
}

bool
Bus::interrupted(void) const noexcept
{
	return false;
}

void
Bus::check_interrupt(util::msg_t &, std::size_t)
{
}

void
Bus::flush_input(void)
{
}
void
Bus::flush_output(void)
{
}

void
Bus::flush(void)
{
	flush_input();
	flush_output();
}

void
Bus::notify(void) const noexcept
{
	notifier->release();
}

} // namespace bus
