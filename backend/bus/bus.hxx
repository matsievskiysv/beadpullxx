/** @file bus.hxx
 * @brief Base bus::Bus and related classes declaration.
 */

#pragma once

#include <mutex>
#include <types.hxx>

/** @namespace bus
 * @brief bus::Bus driver namespace.
 */
namespace bus
{

/**
 * @brief Bus timeout exception.
 */
class Timeout : public std::runtime_error
{
      public:
	/**
	 * @brief Create exception from string.
	 *
	 * @param what_arg String.
	 */
	Timeout(const std::string &what_arg) : runtime_error(what_arg)
	{
	}

	/**
	 * @brief Create exception from string.
	 *
	 * @param what_arg String.
	 */
	Timeout(const char *what_arg) : runtime_error(what_arg)
	{
	}
};

/**
 * @brief Base Bus class.
 *
 * Used for communication with motor::Motor and analyzer::Analyzer.
 */
class Bus
{
      protected:
	util::timeout_t	 timeout;  ///< Communication timeout value.
	util::notifier_t notifier; ///< Message ready notifier.
	std::mutex	 mutex;	   ///< Bus lock.

      public:
	static constexpr const char *NAME = "bus"; ///< Bus name.
	util::attribute_t	     attributes;   ///< Selected Bus attributes.
	/**
	 * @brief Bus attribute choices.
	 *
	 * @return Bus attribute choices.
	 */
	static const util::attribute_t &attribute_choices(void);

	/**
	 * @brief Bus constructor.
	 *
	 * @param[in] timeout Communication timeout value.
	 * @param[in] notifier Message ready notifier.
	 * @param[in] attributes Bus attributes.
	 */
	Bus(util::timeout_t &&timeout, util::notifier_t notifier, util::attribute_t &&attributes);

	/**
	 * @brief Do not allow Bus copying.
	 */
	Bus(Bus &) = delete;

	/**
	 * @brief Do not allow Bus moving.
	 */
	Bus(Bus &&) = delete;

	/**
	 * @brief Bus destructor.
	 */
	virtual ~Bus();

	/**
	 * @brief Get Bus lock object.
	 *
	 * Most Bus transactions are made of multiple send() and rcv() calls.
	 * It is not efficient to lock every one of these calls.
	 * Instead, get lock for the entire function, which uses these functions.
	 *
	 * @return Lock object.
	 */
	const std::lock_guard<std::mutex> get_lock(void) noexcept;

	/**
	 * @brief Bus name.
	 *
	 * @return Bus name.
	 */
	virtual const util::string_t &name(void) noexcept;

	/**
	 * @brief Check Bus interrupt triggered.
	 *
	 * @return Interrupt triggered.
	 */
	virtual bool interrupted(void) const noexcept;

	/**
	 * @brief Check Bus interrupt message.
	 *
	 * It's up to Bus implementation to decide, how to handle message sizes exceeded \p count number of bytes.
	 *
	 * @warning Caller is responsible for locking Bus via get_lock() prior to this call.
	 * @param[in,out] msg Container for received message. \p msg length  attribute may be used by the driver
	 * implementation in addition to the \p count argument.
	 * @param[in] count Message size.
	 * @throws Timeout Bus timeout.
	 */
	virtual void check_interrupt(util::msg_t &msg, std::size_t count);

	/**
	 * @brief Send message on Bus.
	 *
	 * @warning Caller is responsible for locking Bus via get_lock() prior to this call.
	 * @throws Timeout Bus timeout.
	 * @param[in] msg Message to send.
	 */
	virtual void send(const util::msg_t &msg) = 0;

	/**
	 * @brief Receive message on Bus.
	 *
	 * It's up to Bus implementation to decide, how to handle message sizes exceeded \p count number of bytes.
	 *
	 * @warning Caller is responsible for locking Bus via get_lock() prior to this call.
	 * @throws Timeout Bus timeout.
	 * @param[in,out] msg Container for received message. \p msg length  attribute may be used by the driver
	 * implementation in addition to the \p count argument.
	 * @param[in] count Message size.
	 */
	virtual void rcv(util::msg_t &msg, std::size_t count) = 0;

	/**
	 * @brief Flush input buffer.
	 *
	 * May not be implemented.
	 *
	 * @warning Caller is responsible for locking Bus via get_lock() prior to this call.
	 * @throws Timeout Bus timeout.
	 */
	virtual void flush_input(void);

	/**
	 * @brief Flush output buffer.
	 *
	 * May not be implemented.
	 *
	 * @warning Caller is responsible for locking Bus via get_lock() prior to this call.
	 * @throws Timeout Bus timeout.
	 */
	virtual void flush_output(void);

	/**
	 * @brief Flush input and output buffers.
	 *
	 * May not be implemented.
	 *
	 * @warning Caller is responsible for locking Bus via get_lock() prior to this call.
	 * @throws Timeout Bus timeout.
	 */
	virtual void flush(void);

	/**
	 * @brief Trigger Bus notification manually.
	 *
	 * Normally should not be used. Added for virtual devices.
	 *
	 * @warning Caller is responsible for locking Bus via get_lock() prior to this call.
	 */
	virtual void notify(void) const noexcept;
};

} // namespace bus
