#include <config.h>
#ifdef BUS_SERIAL

/** @file serial.cxx
 * @brief bus::Serial bus implementation.
 */

// GCOVR_EXCL_START do not test hardware driver

#include "serial.hxx"

#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/log/trivial.hpp>
#include <boost/regex.hpp>
#include <libserial/SerialPort.h>
#include <map>
#include <thread>
#include <types.hxx>

namespace fs  = boost::filesystem;
namespace re  = boost;
namespace ser = LibSerial;

/**
 * @brief Serial baudrate map.
 */
static const std::map<int, ser::BaudRate> baudrates = {
    {50, ser::BaudRate::BAUD_50},	 {75, ser::BaudRate::BAUD_75},	       {110, ser::BaudRate::BAUD_110},
    {134, ser::BaudRate::BAUD_134},	 {150, ser::BaudRate::BAUD_150},       {200, ser::BaudRate::BAUD_200},
    {300, ser::BaudRate::BAUD_300},	 {600, ser::BaudRate::BAUD_600},       {1200, ser::BaudRate::BAUD_1200},
    {1800, ser::BaudRate::BAUD_1800},	 {2400, ser::BaudRate::BAUD_2400},     {4800, ser::BaudRate::BAUD_4800},
    {9600, ser::BaudRate::BAUD_9600},	 {19200, ser::BaudRate::BAUD_19200},   {38400, ser::BaudRate::BAUD_38400},
    {57600, ser::BaudRate::BAUD_57600},	 {115200, ser::BaudRate::BAUD_115200}, {230400, ser::BaudRate::BAUD_230400},
    {115200, ser::BaudRate::BAUD_115200}};
/**
 * @brief Serial char size map.
 */
static const std::map<int, ser::CharacterSize> char_sizes = {{5, ser::CharacterSize::CHAR_SIZE_5},
							     {6, ser::CharacterSize::CHAR_SIZE_6},
							     {7, ser::CharacterSize::CHAR_SIZE_7},
							     {8, ser::CharacterSize::CHAR_SIZE_8}};
/**
 * @brief Serial stop bits map.
 */
static const std::map<int, ser::StopBits> stop_bits = {{1, ser::StopBits::STOP_BITS_1},
						       {2, ser::StopBits::STOP_BITS_2}};
/**
 * @brief Serial parity map.
 */
static const std::map<const util::string_t, ser::Parity> parities = {
    {"even", ser::Parity::PARITY_EVEN}, {"odd", ser::Parity::PARITY_ODD}, {"none", ser::Parity::PARITY_NONE}};
/**
 * @brief Serial flow control map.
 */
static const std::map<const util::string_t, ser::FlowControl> flowcontrols = {
    {"hardware", ser::FlowControl::FLOW_CONTROL_HARDWARE},
    {"software", ser::FlowControl::FLOW_CONTROL_SOFTWARE},
    {"none", ser::FlowControl::FLOW_CONTROL_NONE}};

namespace bus
{

static util::attribute_t _attribute_choices; ///< Bus attribute choices.

const util::attribute_t &
Serial::attribute_choices(void)
{
	boost::json::array device_choices;
	const re::regex	   pattern(BUS_SERIAL_PATTERN);
	re::cmatch	   match;
	for (auto entry : fs::directory_iterator("/dev")) {
		if (re::regex_match(entry.path().filename().c_str(), match, pattern)) {
			device_choices.push_back(util::string_t(entry.path().c_str()));
		}
	}
	std::sort(device_choices.begin(), device_choices.end(), [](const auto left, const auto right) {
		auto lefts  = left.as_string();
		auto rights = right.as_string();
		if (lefts.size() == rights.size())
			return left.as_string() < right.as_string();
		else
			return lefts.size() < rights.size();
	});

	boost::json::array baudrate_choices;
	baudrate_choices.reserve(baudrates.size());
	for (const auto &[key, val] : baudrates) {
		baudrate_choices.push_back(key);
	}

	boost::json::array char_size_choices;
	char_size_choices.reserve(char_sizes.size());
	for (const auto &[key, val] : char_sizes) {
		char_size_choices.push_back(key);
	}

	boost::json::array stop_bits_choices;
	stop_bits_choices.reserve(stop_bits.size());
	for (const auto &[key, val] : stop_bits) {
		stop_bits_choices.push_back(key);
	}

	boost::json::array parity_choices;
	parity_choices.reserve(parities.size());
	for (const auto &[key, val] : parities) {
		parity_choices.push_back(key);
	}

	boost::json::array flowcontrol_choices;
	flowcontrol_choices.reserve(flowcontrols.size());
	for (const auto &[key, val] : flowcontrols) {
		flowcontrol_choices.push_back(key);
	}

	_attribute_choices = {
	    {"template", util::string_t(Serial::NAME)},
	    {"device",
	     {{"choices", device_choices}, {"value", device_choices.size() > 0 ? device_choices.front() : ""}}},
	    {"baudrate", {{"choices", baudrate_choices}, {"value", 115200}}},
	    {"char_size", {{"choices", char_size_choices}, {"value", 8}}},
	    {"flowcontrol", {{"choices", flowcontrol_choices}, {"value", "none"}}},
	    {"parity", {{"choices", parity_choices}, {"value", "none"}}},
	    {"stop_bits", {{"choices", stop_bits_choices}, {"value", 1}}}};

	return _attribute_choices;
}

Serial::Serial(util::timeout_t &&timeout, util::notifier_t notifier, util::attribute_t &&attributes)
    : Bus(std::move(timeout), notifier, std::move(attributes)),
      port(std::unique_ptr<ser::SerialPort>(
	  new ser::SerialPort(std::string(attributes.as_object()["device"].as_string()),
			      baudrates.at(attributes.as_object()["baudrate"].as_int64()),
			      char_sizes.at(attributes.as_object()["char_size"].as_int64()),
			      flowcontrols.at(attributes.as_object()["flowcontrol"].as_string()),
			      parities.at(attributes.as_object()["parity"].as_string()),
			      stop_bits.at(attributes.as_object()["stop_bits"].as_int64()))))
{
	BOOST_LOG_TRIVIAL(debug) << "[Serial] open port";
	BOOST_LOG_TRIVIAL(trace) << "[Serial] attributes " << attributes;
	BOOST_LOG_TRIVIAL(trace) << "[Serial] timeout " << timeout;
	thr = std::jthread([&mutex = this->mutex, &data_available = this->data_available, &port = this->port,
			    &notifier = this->notifier, timeout = timeout](std::stop_token stoken) {
		BOOST_LOG_TRIVIAL(debug) << "[Serial] spawn interrupt thread";
		while (!stoken.stop_requested()) {
			{
				std::lock_guard<std::mutex> lock(mutex);
				data_available = port->IsDataAvailable();
				if (data_available)
					notifier->release();
			}
			std::this_thread::sleep_for(timeout / 10);
		}
		BOOST_LOG_TRIVIAL(debug) << "[Serial] stop interrupt thread";
	});
}

Serial::~Serial()
{
	thr.request_stop();
	thr.join();

	try {
		port->Close();
	} catch (const std::exception &err) {
		BOOST_LOG_TRIVIAL(trace) << "[Serial] port exception: " << err.what();
	}
	BOOST_LOG_TRIVIAL(debug) << "[Serial] close port";
}

static util::string_t _name = util::string_t(Serial::NAME); ///< Bus name.

const util::string_t &
Serial::name(void) noexcept
{
	return _name;
}

void
Serial::send(const util::msg_t &message)
{
	BOOST_LOG_TRIVIAL(trace) << "[Serial] send " << std::hex << std::showbase << std::nouppercase << message;
	port->Write(message);
}

void
Serial::rcv(util::msg_t &message, std::size_t count)
{
	try {
		port->Read(message, count, timeout.count());
	} catch (const ser::ReadTimeout &) {
		throw bus::Timeout("read timeout");
	}
	BOOST_LOG_TRIVIAL(trace) << "[Serial] receive " << std::hex << std::showbase << std::nouppercase << message;
}

void
Serial::flush_input(void)
{
	port->FlushInputBuffer();
	BOOST_LOG_TRIVIAL(debug) << "[Serial] flush input";
}

void
Serial::flush_output(void)
{
	port->FlushOutputBuffer();
	BOOST_LOG_TRIVIAL(debug) << "[Serial] flush output";
}

void
Serial::flush(void)
{
	port->FlushIOBuffers();
	BOOST_LOG_TRIVIAL(debug) << "[Serial] flush";
}

bool
Serial::interrupted(void) const noexcept
{
	return data_available;
}

void
Serial::check_interrupt(util::msg_t &msg, std::size_t count)
{
	try {
		port->Read(msg, count, (timeout / 100).count());
	} catch (const ser::ReadTimeout &) {
		throw bus::Timeout("read timeout");
	}
	BOOST_LOG_TRIVIAL(trace) << "[Serial] interrupt receive " << std::hex << std::showbase << std::nouppercase
				 << msg;
}

} // namespace bus

// GCOVR_EXCL_STOP

#endif // BUS_SERIAL
