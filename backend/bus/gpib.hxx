#pragma once

#include <config.h>
#ifdef BUS_GPIB

/** @file gpib.hxx
 * @brief bus::GPIB bus::Bus implementation.
 */

#include "bus.hxx"

namespace bus
{

/**
 * @brief GPIB Bus implementation.
 */
class GPIB : public Bus
{
      private:
	int	     handle	      = -1;   ///< GPIB bus handle.
	std::uint8_t status_byte_mask = 0x00; ///< Status byte mask. Should be set by analyzer::Analyzer via
					      ///< analyzer::Analyzer::patch_bus_attributes() call.

      public:
	static constexpr const char *NAME = "GPIB"; ///< Bus name.
	/**
	 * @brief Bus attribute choices.
	 *
	 * @return Bus attribute choices.
	 */
	static const util::attribute_t &attribute_choices(void);

	/**
	 * @brief Bus constructor.
	 *
	 * @param[in] timeout Communication timeout value.
	 * @param[in] notifier Message ready notifier.
	 * @param[in] attributes Bus attributes.
	 */
	GPIB(util::timeout_t &&timeout, util::notifier_t notifier, util::attribute_t &&attributes);

	/**
	 * @brief Do not allow GPIB copying.
	 */
	GPIB(GPIB &) = delete;

	/**
	 * @brief Do not allow GPIB moving.
	 */
	GPIB(GPIB &&) = delete;

	~GPIB();

	const util::string_t &name(void) noexcept;
	void		      send(const util::msg_t &msg);
	void		      rcv(util::msg_t &msg, std::size_t count);
	bool		      interrupted(void) const noexcept;
	void		      check_interrupt(util::msg_t &msg, std::size_t count);
};

} // namespace bus

#endif // BUS_GPIB
