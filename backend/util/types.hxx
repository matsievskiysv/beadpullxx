/** @file types.hxx
 * @brief Global type definitions and aliases.
 */

#pragma once

#include <boost/json/value.hpp>
#include <boost/log/utility/formatting_ostream.hpp>
#include <chrono>
#include <cstdint>
#include <iostream>
#include <map>
#include <memory>
#include <semaphore>
#include <string>
#include <tuple>
#include <vector>

#define STR_HELPER(x) #x	    ///< Convert preprocessor definition to string helper.
#define STR(x)	      STR_HELPER(x) ///< Convert preprocessor definition to string.

#define JSON_DEFAULT(expr, field, type, deflt) ((expr).if_contains(field) && (expr)[field].is_ ## type() ? \
						(expr)[field].as_ ## type() : (deflt))

namespace util
{

using string_t	     = boost::json::string;			    ///< String representation.
using name_list_t    = std::vector<string_t>;			    ///< List of names. E.g. driver names list.
using msg_t	     = std::vector<std::uint8_t>;		    ///< Binary message.
using timeout_t	     = std::chrono::duration<uint32_t, std::milli>; ///< Timeout in milliseconds.
using notifier_t     = std::shared_ptr<std::binary_semaphore>;	    ///< controller::Controller event loop notifier.
using attribute_t    = boost::json::value;			    ///< Generic attribute of unknown type.
using pos_t	     = std::int32_t;	   ///< Position of the body, controlled by the motor::Motor.
using speed_t	     = std::uint32_t;	   ///< Speed of the body, controlled by the motor::Motor.
using acceleration_t = std::uint32_t;	   ///< Acceleration of the body, controlled by the motor::Motor.
using nop_t	     = std::uint32_t;	   ///< Number of points.
using nop_vals_t     = std::vector<nop_t>; ///< List of numbers of points.
using int_band_t     = std::uint32_t;	   ///< Integration band width of the analyzer::Analyzer.
using power_t	     = std::uint32_t;	   ///< Power of the analyzer::Analyzer.
using s_port_t	     = std::uint16_t; ///< Port of the analyzer::Analyzer. NOTE: cannot use std::uint8_t because it gets
				      ///< parsed wrong by the boost::lexical_cast
using s_part_t	      = std::tuple<s_port_t, s_port_t>; ///< Port connection of the analyzer::Analyzer.
using s_parts_t	      = std::vector<s_part_t>;		///< List of analyzer::Analyzer port connections.
using markers_count_t = std::uint8_t;			///< Number of analyzer::Analyzer markers.
using f_t	      = std::uint64_t;			///< Oscillation frequency.
using Q_t	      = std::uint64_t;			///< Quality factor.
using f_lim_t = std::tuple<f_t, f_t, f_t, f_t>; ///< Frequency range of the analyzer::Analyzer. Expressed as two pairs
						///< of frequency values: left, right, center, span.
using f_array_t	   = std::vector<f_t>;		///< Frequency list.
using meas_t	   = double;			///< analyzer::Analyzer measurement point.
using coeff_t	   = double;			///< analyzer::Analyzer data conversion coefficient.
using pos_array_t  = std::vector<pos_t>;	///< List of motor::Motor positions.
using meas_array_t = std::vector<meas_t>;	///< List of analyzer::Analyzer measurements.
using meas_pack_t  = boost::json::value;	///< Measurement data package for sending to client.

/**
 * @brief Convert std::string to msg_t.
 *
 * Purpose of this function is to increase readability of the code.
 *
 * @param str String.
 * @return Message.
 */
msg_t str2msg(const std::string &str);

} // namespace util

namespace std
{

/**
 * @brief Dump data value to output stream.
 *
 * @param[in,out] os Output stream.
 * @param[in] vec Data.
 * @return Stream.
 */
std::ostream &operator<<(std::ostream &os, const util::msg_t &vec);

/**
 * @brief Add string to message.
 *
 * @param[in,out] us Data message.
 * @param[in] them Data appending to message.
 * @return Data message.
 */
util::msg_t &operator+=(util::msg_t &us, const std::string &them);

/**
 * @brief Add byte to message..
 *
 * @param[in,out] us Data message.
 * @param[in] them Data appending to message.
 * @return Data message.
 */
util::msg_t &operator+=(util::msg_t &us, std::uint8_t them);

} // namespace std
