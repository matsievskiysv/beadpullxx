/** @file chunked.cxx
 * @brief Data chunk compression class implementation.
 */

#include "chunked.hxx"

#include <algorithm>

namespace util
{

Chunked::Chunked(stream_t stream, size_t chunk_size, bool compress, DataType data_type,
		 CompressionLevel compression_level)
    : chunk_size(chunk_size), input_buffer(chunk_size), output_buffer(chunk_size), input_stream(std::move(stream)),
      compress(compress)
{
	if (input_stream == nullptr)
		throw std::runtime_error("stream is null");
	if (!input_stream->good())
		throw std::runtime_error("stream is not good");
	if (compress) {
		zstream.zalloc	  = Z_NULL;
		zstream.zfree	  = Z_NULL;
		zstream.opaque	  = Z_NULL;
		zstream.data_type = static_cast<int>(data_type);
		zstream.next_in	  = 0;
		zstream.avail_in  = 0;
		auto rv		  = deflateInit(&zstream, static_cast<int>(compression_level));
		if (rv != Z_OK)
			throw std::runtime_error("Could not initialize Zlib compressor");
	}
}

Chunked::~Chunked()
{
	if (compress) {
		deflateEnd(&zstream);
	}
}

std::tuple<bool, bool>
Chunked::compress_chunk(void)
{
	if (ended)
		return std::make_tuple(false, true); // not changed, ended
	if (compress) {
		zstream.next_out  = output_buffer.data();
		zstream.avail_out = chunk_size;
		while (zstream.avail_out != 0) {
			if (zstream.avail_in == 0) {
				zstream.avail_in =
				    input_stream->readsome(reinterpret_cast<char *>(input_buffer.data()), chunk_size);
				zstream.next_in = input_buffer.data();
				input_stream->peek();
			}
			auto rv = deflate(&zstream, input_stream->eof() ? Z_FINISH : (Z_PARTIAL_FLUSH));
			if (rv == Z_STREAM_ERROR)
				throw std::runtime_error("Compression error");
			if (rv == Z_STREAM_END) {
				ended = true;
				break;
			}
		}
		output_buffer.resize(chunk_size - zstream.avail_out);
		return std::make_tuple(true, ended); // changed, maybe ended
	} else {
		size_t output_size = 0;
		while (output_size != chunk_size) {
			output_size += input_stream->readsome(
			    reinterpret_cast<char *>(input_buffer.data() + output_size), chunk_size - output_size);
			input_stream->peek();
			if (input_stream->eof())
				break;
		}
		input_buffer.resize(output_size);
		return std::make_tuple(true, input_stream->eof()); // changed, maybe ended
	}
}

const Chunked::buffer_t &
Chunked::get_chunk(void) const
{
	if (compress)
		return output_buffer;
	else
		return input_buffer;
}

} // namespace util
