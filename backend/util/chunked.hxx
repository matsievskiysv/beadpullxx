/** @file chunked.hxx
 * @brief Data chunk compression class definition.
 */

#pragma once

#include <cstdint>
#include <istream>
#include <memory>
#include <tuple>
#include <vector>

extern "C" {
#include <zlib.h>
}

namespace util
{

/**
 * @brief Class splits data stream into chunks for network transfer. Chunks may be compressed.
 */
class Chunked
{
	/**
	 * @brief Chunked data type.
	 */
	enum class DataType : int {
		BINARY = Z_BINARY, ///< Binary data.
		TEXT   = Z_TEXT	   ///< Textual data.
	};

	/**
	 * @brief Compression level.
	 */
	enum class CompressionLevel : int {
		L0		    = 0,
		L1		    = 1,
		L2		    = 2,
		L3		    = 3,
		L4		    = 4,
		L5		    = 5,
		L6		    = 6,
		L7		    = 7,
		L8		    = 8,
		L9		    = 9,
		NO_COMPRESSION	    = Z_NO_COMPRESSION,
		BEST_SPEED	    = Z_BEST_SPEED,
		BEST_COMPRESSION    = Z_BEST_COMPRESSION,
		DEFAULT_COMPRESSION = Z_DEFAULT_COMPRESSION
	};

      public:
	using buffer_t = std::vector<std::uint8_t>;		    ///< Data buffer type.
	using stream_t = std::unique_ptr<std::basic_istream<char>>; ///< Input data stream type.

      private:
	const size_t chunk_size;    ///< Data chunk size.
	buffer_t     input_buffer;  ///< Input buffers.
	buffer_t     output_buffer; ///< Output buffers.
	stream_t     input_stream;  ///< Input stream.
	z_stream_s   zstream;	    ///< ZLib stream.
	bool	     compress;	    ///< Compression flag.
	bool	     ended = false; ///< Data is ended.

      public:
	/**
	 * @brief Chunked constructor.
	 *
	 * @param[in] stream Input data stream.
	 * @param[in] chunk_size Size of data chunk.
	 * @param[in] compress Compress data flag.
	 * @param[in] data_type Input stream data type. Used by ZLib algorithm.
	 * @param[in] compression_level ZLib compression level.
	 */
	Chunked(stream_t stream, size_t chunk_size, bool compress, DataType data_type = DataType::BINARY,
		CompressionLevel compression_level = CompressionLevel::DEFAULT_COMPRESSION);
	/**
	 * @brief Destructor.
	 */
	~Chunked();
	/**
	 * @brief Compress chunk of data.
	 *
	 * @return Pair of flags <changed, ended>. Changed flag indicates that data buffer was changed by the call.
	 * Ended flag indicates that input stream ended.
	 */
	std::tuple<bool, bool> compress_chunk(void);
	/**
	 * @brief Get chunk of data.
	 *
	 * @return Result of the last @ref compress_chunk() call.
	 */
	const buffer_t &get_chunk(void) const;
};

} // namespace util
