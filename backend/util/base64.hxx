/** @file base64.hxx
 * @brief Base64 encoding functions.
 */

#pragma once

#include <string>

namespace util
{

/**
 * @brief Base64 encode data.
 *
 * @param[in] bindata Data to be encoded.
 * @return Encoded string.
 */
std::string base64_encode(const std::string &bindata);

/**
 * @brief Base64 decode data.
 *
 * @param[in] ascdata Data to be decoded.
 * @return Decoded string.
 */
std::string base64_decode(const std::string &ascdata);

} // namespace util
