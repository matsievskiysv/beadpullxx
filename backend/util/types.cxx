/** @file types.cxx
 * @brief Global type definitions and aliases.
 */

#include "types.hxx"

namespace util
{

msg_t
str2msg(const std::string &str)
{
	msg_t msg(str.begin(), str.end());
	return msg;
}

} // namespace util

namespace std
{

std::ostream &
operator<<(std::ostream &os, const util::msg_t &vec)
{
	os << "[";
	for (const auto &el : vec) {
		os << " " << int(el);
	}
	os << " ]";
	return os;
}

util::msg_t &
operator+=(util::msg_t &us, const std::string &them)
{
	us.reserve(us.size() + them.size());
	us.insert(us.end(), them.begin(), them.end());
	return us;
}

util::msg_t &
operator+=(util::msg_t &us, std::uint8_t them)
{
	us.push_back(them);
	return us;
}

} // namespace std
