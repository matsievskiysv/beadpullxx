/** @file event_queue.hxx
 * @brief Thread safe message queue data type.
 */

#pragma once

#include <boost/circular_buffer.hpp>
#include <mutex>
#include <types.hxx>

namespace util
{

/**
 * @brief Thread safe message queue data type.
 *
 * Queue is implemented as circular buffer. Message gets lost when buffer overflows.
 */
template <typename T> class EventQueue
{
      private:
	boost::circular_buffer<T> buffer;   ///< Message buffer.
	notifier_t		  notifier; ///< Optional notification object.
	std::mutex		  mutex;    ///< Object lock.

      public:
	/**
	 * @brief Constructor.
	 *
	 * @param[in] capacity Maximum number of messages in queue.
	 * @param[in] notifier Event addition notifier. May be nullptr.
	 */
	EventQueue(size_t capacity, notifier_t notifier) : buffer(capacity), notifier(notifier)
	{
	}

	/**
	 * @brief Queue is not empty.
	 *
	 * @retval true Queue has an event.
	 * @retval false Queue is empty.
	 */
	bool
	has_event(void) noexcept
	{
		std::lock_guard<std::mutex> lock(mutex);
		return !buffer.empty();
	}

	/**
	 * @brief Push event to queue.
	 *
	 * @param[in] event New event.
	 * @param[in] front Push to beginning of the queue.
	 * @retval true Lost old event pushing new one to the buffer.
	 * @retval false Didn't lose events while pushing new one to the buffer.
	 */
	bool
	push(T &&event, bool front = false) noexcept
	{
		std::lock_guard<std::mutex> lock(mutex);
		auto			    pushed_out = buffer.full();
		if (front)
			buffer.push_front(event);
		else
			buffer.push_back(event);
		if (notifier != nullptr)
			notifier->release();
		return pushed_out;
	}

	/**
	 * @brief Pop event from buffer.
	 *
	 * @return Event from the beginning of the queue.
	 * @throws std::range_error is there is no events in the queue.
	 */
	T
	consume(void)
	{
		std::lock_guard<std::mutex> lock(mutex);
		if (buffer.empty())
			throw std::range_error("Buffer is empty");
		auto item = buffer.front();
		buffer.pop_front();
		return item;
	}

	/**
	 * @brief Clear queue.
	 *
	 * Clear queue.
	 */
	void
	clear(void) noexcept
	{
		std::lock_guard<std::mutex> lock(mutex);
		buffer.clear();
	}
};

} // namespace util
