/** @file controller.cxx
 * @brief Base controller::Controller and related classes definitions.
 */

#include "controller.hxx"

#include <algorithm>
#include <boost/log/trivial.hpp>
#include <condition_variable>
#include <config.h>

namespace controller
{

Timeout::Timeout(util::timeout_t motor_communication, util::timeout_t motor_step,
		 util::timeout_t analyzer_communication, util::timeout_t analyzer_measure,
		 util::timeout_t pre_measure_delay)
    : loop_period(std::min(motor_communication, analyzer_communication)), motor_communication(motor_communication),
      motor_step(motor_step), analyzer_communication(analyzer_communication), analyzer_measure(analyzer_measure),
      pre_measure_delay(pre_measure_delay)
{
}

Controller::Controller(Timeout								      timeout,
		       std::function<void(const util::string_t &, const util::attribute_t &)> send_message)
    : notifier(std::shared_ptr<std::binary_semaphore>(new std::binary_semaphore(1))),
      motor_events(EVENT_QUEUE_SIZE, nullptr), analyzer_events(EVENT_QUEUE_SIZE, nullptr), timeout(std::move(timeout)),
      send_message(send_message)
{
	BOOST_LOG_TRIVIAL(debug) << "[Controller] motor communication timeout " << timeout.motor_communication;
	BOOST_LOG_TRIVIAL(debug) << "[Controller] analyzer communication timeout " << timeout.analyzer_communication;
	BOOST_LOG_TRIVIAL(debug) << "[Controller] motor step timeout " << timeout.motor_step;
	BOOST_LOG_TRIVIAL(debug) << "[Controller] analyzer measure timeout " << timeout.analyzer_measure;
	BOOST_LOG_TRIVIAL(debug) << "[Controller] analyzer pre measure delay " << timeout.pre_measure_delay;
}

void
Controller::interrupt_check(void) noexcept
{
	if (notifier == nullptr) {
		std::this_thread::sleep_for(timeout.loop_period);
	} else {
		notifier->try_acquire_for(timeout.loop_period);
	}
	if (motor_connected()) {
		std::lock_guard<std::mutex> lock(motor_mutex);
		if (motor->interrupted()) {
			motor->check_interrupt(motor_events);
		}
		while (motor_events.has_event()) {
			auto [event, message] = motor_events.consume();
			switch (event) {
			case motor::Event::STOPPED:
				BOOST_LOG_TRIVIAL(debug) << "[Controller] stop event";
				send_message("motor",
					     {{"event", "notification"}, {"target", "motor"}, {"value", "stop"}});
				try {
					auto pos = motor->position();
					send_message("motor",
						     {{"event", "update"}, {"target", "position"}, {"value", pos}});
				} catch (const std::runtime_error &err) {
					BOOST_LOG_TRIVIAL(warning)
					    << "[Controller] position query error: " << err.what();
				}
				break;
			case motor::Event::ERROR:
				BOOST_LOG_TRIVIAL(debug) << "[Controller] position update event";
				send_message("motor",
					     {{"event", "notification"}, {"target", "motor"}, {"value", "error"}});
				break;
			}
		}
	}
	if (analyzer_connected()) {
		std::lock_guard<std::mutex> lock(analyzer_mutex);
		if (analyzer->interrupted()) {
			analyzer->check_interrupt(analyzer_events);
		}
		while (analyzer_events.has_event()) {
			auto [event, message] = analyzer_events.consume();
			switch (event) {
			case analyzer::Event::MEASURE_COMPLETE:
				try {
					analyzer->measurements(scattering);
					BOOST_LOG_TRIVIAL(debug) << "[Controller] measure complete";
					send_message("measure", {{"event", "notification"},
								 {"target", "scattering_measure"},
								 {"value", "complete"}});
				} catch (const std::exception &err) {
					BOOST_LOG_TRIVIAL(debug)
					    << "[Controller] analyzer measurement processing error: " << err.what();
					send_message("measure", {{"event", "notification"},
								 {"target", "scattering_measure"},
								 {"value", "error"}});
				}
				break;
			case analyzer::Event::INTERRUPTED:
				BOOST_LOG_TRIVIAL(debug) << "[Controller] measure interrupt";
				send_message("measure", {{"event", "notification"},
							 {"target", "scattering_measure"},
							 {"value", "interrupt"}});
				break;
			case analyzer::Event::ERROR:
				BOOST_LOG_TRIVIAL(debug) << "[Controller] measure error";
				send_message(
				    "measure",
				    {{"event", "notification"}, {"target", "scattering_measure"}, {"value", "error"}});
				break;
			}
		}
	}
}

void
Controller::event_loop(void)
{
	do {
		if (measuring_field_flag) {
			pull_bead();
			measuring_field_flag = false;
		} else {
			interrupt_check();
		}
	} while (!exit_loop);
	exit_loop = false;
}

const util::attribute_t &
Controller::bus_attribute_choices(util::string_t bus) const
{
	return bus::get_attribute_choices(bus);
}

void
Controller::close(void) noexcept
{
	exit_loop = true;
}

} // namespace controller
