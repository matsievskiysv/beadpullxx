/** @file measure.cxx
 * @brief Measuring related controller::Controller definitions.
 */

#include "controller.hxx"

#include <algorithm>
#include <boost/log/trivial.hpp>
#include <chrono>
#include <condition_variable>
#include <config.h>
#include <thread>

namespace controller
{

bool
Controller::measuring_scattering(void)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	return timeout_function<bool>([=, analyzer = this->analyzer](std::stop_token) { return analyzer->measuring(); },
				      timeout.analyzer_communication, "analyzer check measure scattering timeout");
}

void
Controller::interrupt_measure_scattering(void)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	timeout_function<void>([=, analyzer = this->analyzer, &analyzer_events = this->analyzer_events](
				   std::stop_token) { analyzer->interrupt_measure(analyzer_events); },
			       timeout.analyzer_communication, "analyzer interrupt measure scattering timeout");
}

void
Controller::measure_scattering(void)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	timeout_function<void>([=, analyzer = this->analyzer](std::stop_token) { analyzer->measure(); },
			       timeout.analyzer_communication, "analyzer measure scattering timeout");
	send_message("measure", {{"event", "notification"}, {"target", "scattering_measure"}, {"value", "start"}});
}

const util::meas_pack_t &
Controller::scattering_measurements(void)
{
	return scattering.serialized();
}

void
Controller::measure_field(util::pos_t start, util::pos_t stop, util::nop_t number_of_points, result::FieldPart field,
			  bool phase, result::Material material, result::Method method, util::coeff_t coeff,
			  util::Q_t Ql)
{
	util::f_t	       f0;
	result::ScatteringType scattering;
	check_motor_connected();
	check_analyzer_connected();
	if (start == stop)
		throw std::runtime_error("start and stop positions must differ");
	if (number_of_points < 2)
		throw std::runtime_error("need at least two measurement points");
	switch (field) {
	case result::FieldPart::Electric: {
		if (material == result::Material::Magnetic)
			throw std::runtime_error("Cannot measure electric field with magnetic bead");
		break;
	}
	case result::FieldPart::Magnetic: {
		if (material == result::Material::Dielectric)
			throw std::runtime_error("Cannot measure magnetic field with dielectric bead");
		break;
	}
	}
	{
		std::lock_guard<std::mutex> lock_motor(motor_mutex);
		auto			    steppers_shutdown_supported = timeout_function<bool>(
			   [=, motor = this->motor](std::stop_token) { return motor->steppers_shutdown_supported(); },
			   timeout.motor_communication, "motor steppers capability query timeout");
		if (steppers_shutdown_supported) {
			auto steppers = timeout_function<bool>(
			    [=, motor = this->motor](std::stop_token) { return motor->steppers(); },
			    timeout.motor_communication, "motor steppers query timeout");
			if (!steppers)
				throw std::runtime_error("motor steppers disabled");
		}
	}
	{
		std::lock_guard<std::mutex> lock_analyzer(analyzer_mutex);
		f0 = timeout_function<util::f_t>(
		    [=, analyzer = this->analyzer](std::stop_token) { return analyzer->f_center(); },
		    timeout.motor_communication, "motor f center query timeout");
		auto s_part = timeout_function<util::s_part_t>(
		    [=, analyzer = this->analyzer](std::stop_token) { return analyzer->s_part(); },
		    timeout.motor_communication, "motor s part query timeout");
		scattering = std::get<0>(s_part) == std::get<1>(s_part) ? result::ScatteringType::Reflection
									: result::ScatteringType::Transmission;
	}
	if (phase && (method != result::Method::Indirect || scattering != result::ScatteringType::Reflection)) {
		throw std::runtime_error("Can measure phase only with indirect reflection measurements");
	}
#if 0
	motor->movement_ready();
	analyzer->measure_ready();
#endif
	this->field.setup(start, stop, number_of_points, field, phase, material, method, coeff, Ql, f0);
	measuring_field_flag = true;
}

bool
Controller::measuring_field(void)
{
	return measuring_field_flag;
}

void
Controller::pull_bead_movement(util::pos_t next_position, bool use_return_speed)
{
	check_motor_connected();
	motor->move_to(next_position, use_return_speed);
	const auto movement_start = std::chrono::steady_clock::now();
	for (;;) {
		BOOST_LOG_TRIVIAL(trace) << "[Controller] check motor timeout << "
					 << std::chrono::steady_clock::now() - movement_start;
		if ((std::chrono::steady_clock::now() - movement_start) > timeout.motor_step) {
			BOOST_LOG_TRIVIAL(warning) << "[Controller] motor timed out";
			throw std::runtime_error("motor movement timeout");
		}
		if (notifier == nullptr) {
			std::this_thread::sleep_for(timeout.motor_communication);
		} else {
			notifier->try_acquire_for(timeout.motor_communication);
		}
		if (motor->interrupted()) {
			motor->check_interrupt(motor_events);
		}
		while (motor_events.has_event()) {
			const auto &[event, message] = motor_events.consume();
			util::pos_t pos;
			switch (event) {
			case motor::Event::STOPPED:
				BOOST_LOG_TRIVIAL(debug) << "[Controller] stop event";
				pos = motor->position();
				send_message("motor", {{"event", "update"}, {"target", "position"}, {"value", pos}});
				return;
			case motor::Event::ERROR:
				BOOST_LOG_TRIVIAL(debug) << "[Controller] error event";
				throw std::runtime_error(message.c_str());
			}
		}
	};
}

void
Controller::pull_bead_measurement(void)
{
	check_analyzer_connected();
	analyzer->measure();

	const auto measure_start = std::chrono::steady_clock::now();
	for (;;) {
		BOOST_LOG_TRIVIAL(trace) << "[Controller] check analyzer timeout << "
					 << std::chrono::steady_clock::now() - measure_start;
		if (std::chrono::steady_clock::now() - measure_start > timeout.analyzer_measure) {
			BOOST_LOG_TRIVIAL(debug) << "[Controller] analyzer timed out";
			send_message("measure",
				     {{"event", "notification"}, {"target", "field_measure"}, {"value", "timeout"}});
			analyzer->interrupt_measure(analyzer_events);
			throw std::runtime_error("analyzer measure timeout");
		}
		if (notifier == nullptr) {
			std::this_thread::sleep_for(timeout.analyzer_communication);
		} else {
			notifier->try_acquire_for(timeout.analyzer_communication);
		}
		if (analyzer->interrupted()) {
			analyzer->check_interrupt(analyzer_events);
		}
		while (analyzer_events.has_event()) {
			const auto &[event, message] = analyzer_events.consume();
			switch (event) {
			case analyzer::Event::MEASURE_COMPLETE:
				BOOST_LOG_TRIVIAL(debug) << "[Controller] measure complete";
				analyzer->measurements(scattering);
				send_message("measure", {{"event", "notification"},
							 {"target", "scattering_measure"},
							 {"value", "complete"}});
				return;
			case analyzer::Event::INTERRUPTED:
				BOOST_LOG_TRIVIAL(debug) << "[Controller] measure interrupt";
				send_message(
				    "measure",
				    {{"event", "notification"}, {"target", "field_measure"}, {"value", "interrupt"}});
				throw std::runtime_error(message.c_str());
			case analyzer::Event::ERROR:
				BOOST_LOG_TRIVIAL(debug) << "[Controller] measure error";
				send_message("measure", {{"event", "notification"},
							 {"target", "field_measure"},
							 {"value", "error"},
							 {"message", message}});
				throw std::runtime_error(message.c_str());
			}
		}
	}
}

void
Controller::pull_bead(void) noexcept
{
	BOOST_LOG_TRIVIAL(debug) << "[Controller] pulling bead";
	auto nop = field.point_count();
	send_message("measure", {{"event", "notification"}, {"target", "field_measure"}, {"value", "start"}});
	send_message("measure", {{"event", "update"},
				 {"target", "field_measure"},
				 {"value",
				  {
				      {"point", 0},
				      {"total", nop},
				  }}});
	std::lock_guard<std::mutex> motor_lock(motor_mutex);
	std::lock_guard<std::mutex> analyzer_lock(analyzer_mutex);
	for (util::nop_t point = 0; point < nop; point++) {
		BOOST_LOG_TRIVIAL(info) << "[Controller] measure point " << point + 1 << "/" << nop;
		auto next_position = field.next_point();
		BOOST_LOG_TRIVIAL(debug) << "[Controller] measure position: " << next_position;
		if (!measuring_field_flag) {
			send_message("measure",
				     {{"event", "notification"}, {"target", "field_measure"}, {"value", "interrupt"}});
			return;
		}
		try {
			BOOST_LOG_TRIVIAL(trace) << "[Controller] motor move";
			for (size_t attempt = 1; attempt <= MEASURE_COMM_RETRY; attempt++) {
				try {
					pull_bead_movement(next_position, point == 0);
					break;
				} catch (const std::exception &err) {
					BOOST_LOG_TRIVIAL(warning) << "[Controller] motor move failed: " << err.what();
					BOOST_LOG_TRIVIAL(debug)
					    << "[Controller] motor move attempt " << attempt << "/" << MEASURE_COMM_RETRY;
					if (attempt == MEASURE_COMM_RETRY)
						std::rethrow_exception(std::current_exception());
					std::this_thread::sleep_for(timeout.motor_communication);
				}
			}

			BOOST_LOG_TRIVIAL(trace) << "[Controller] pre measure delay";
			std::this_thread::sleep_for(timeout.pre_measure_delay);

			BOOST_LOG_TRIVIAL(trace) << "[Controller] start measure";
			for (size_t attempt = 1; attempt <= MEASURE_COMM_RETRY; attempt++) {
				try {
					pull_bead_measurement();
					break;
				} catch (const std::exception &err) {
					BOOST_LOG_TRIVIAL(warning)
					    << "[Controller] analyzer measure failed: " << err.what();
					BOOST_LOG_TRIVIAL(debug) << "[Controller] analyzer measure attempt " << attempt
								 << "/" << MEASURE_COMM_RETRY;
					if (attempt == MEASURE_COMM_RETRY)
						std::rethrow_exception(std::current_exception());
					std::this_thread::sleep_for(timeout.analyzer_communication);
				}
			}

			field.add_point(scattering);
			send_message("measure", {{"event", "update"},
						 {"target", "field_measure"},
						 {"value",
						  {
						      {"point", point + 1},
						      {"total", nop},
						  }}});
		} catch (const std::exception &err) {
			send_message("measure", {{"event", "notification"},
						 {"target", "field_measure"},
						 {"value", "error"},
						 {"message", err.what()}});
			return;
		}
	}
	send_message("measure", {{"event", "notification"}, {"target", "field_measure"}, {"value", "complete"}});
}

void
Controller::interrupt_measure_field(void)
{
	measuring_field_flag = false;
	stop();
	interrupt_measure_scattering();
}

const util::meas_pack_t &
Controller::field_measurements(void)
{
	return field.serialized();
}

} // namespace controller
