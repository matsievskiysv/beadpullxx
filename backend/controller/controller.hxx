/** @file controller.hxx
 * @brief Base controller::Controller and related classes.
 */

#pragma once

#include <analyzer/list.hxx>
#include <atomic>
#include <event_queue.hxx>
#include <future>
#include <motor/list.hxx>
#include <mutex>
#include <result/field.hxx>
#include <result/scattering.hxx>
#include <thread>
#include <types.hxx>

namespace json = boost::json;

/** @namespace controller
 * @brief controller::Controller namespace.
 */
namespace controller
{

/**
 * @brief Time and timeout data class.
 */
class Timeout
{
      public:
	util::timeout_t loop_period;		///< Event loop period.
	util::timeout_t motor_communication;	///< motor::Motor function evaluation timeout.
	util::timeout_t motor_step;		///< motor::Motor step timeout.
	util::timeout_t analyzer_communication; ///< analyzer::Analyzer function evaluation timeout.
	util::timeout_t analyzer_measure;	///< analyzer::Analyzer measurement timeout.
	util::timeout_t pre_measure_delay;	///< analyzer::Analyzer pre-measure delay. Wait for bead-pulling wire
						///< oscillations to settle.
	/**
	 * @brief Controller explicit constructor.
	 *
	 * @param motor_communication motor::Motor function evaluation timeout.
	 * @param motor_step motor::Motor step timeout.
	 * @param analyzer_communication analyzer::Analyzer function evaluation timeout.
	 * @param analyzer_measure analyzer::Analyzer measurement timeout.
	 * @param pre_measure_delay analyzer::Analyzer pre-measure delay. Wait for bead-pulling wire
	 */
	Timeout(util::timeout_t motor_communication, util::timeout_t motor_step, util::timeout_t analyzer_communication,
		util::timeout_t analyzer_measure, util::timeout_t pre_measure_delay);
};

/**
 * @brief Device controller.
 *
 * Controller contains the main logic of the program. It binds motor::Motor and analyzer::Analyzer drivers together,
 * relays the web server commands.
 *
 * Function wrapper @ref timeout_function is used for making function calls in separate threads with timeout.
 * Evaluation result is returned to the caller and broadcasted to all clients via @ref send_message.
 */
class Controller
{
      private:
	util::notifier_t
		   notifier; ///< Event loop notifier. Used to trigger event loop cycle in case of bus::Bus interrupts.
	std::mutex motor_mutex;					       ///< motor::Motor lock.
	std::mutex analyzer_mutex;				       ///< analyzer::Analyzer lock.
	std::shared_ptr<motor::Motor>		   motor    = nullptr; ///< motor::Motor handle.
	std::shared_ptr<analyzer::Analyzer>	   analyzer = nullptr; ///< analyzer::Analyzer handle.
	util::EventQueue<motor::motor_event>	   motor_events;       ///< motor::Motor event list.
	util::EventQueue<analyzer::analyzer_event> analyzer_events;    ///< analyzer::Analyzer event list.
	result::Scattering			   scattering;	       ///< Latest scattering measurement results.
	result::Field				   field;	       ///< Latest field measurement results.
	std::atomic_bool			   measuring_field_flag = false; ///< Field measuring flag.
	std::atomic_bool			   exit_loop		= false; ///< Exit event loop flag.
	const Timeout				   timeout;			 ///< Timeout container.

	/**
	 * @brief Send message server callback.
	 *
	 * This function is supplied by the server and used to pass message to WebSocket sinks.
	 */
	std::function<void(const util::string_t &, const util::attribute_t &)> send_message = nullptr;

	/**
	 * @brief Check motor::Motor is connected.
	 *
	 * @throws std::runtime_error motor::Motor is not connected.
	 */
	void check_motor_connected(void) const;

	/**
	 * @brief Check analyzer::Analyzer is connected.
	 *
	 * @throws std::runtime_error analyzer::Analyzer is not connected.
	 */
	void check_analyzer_connected(void) const;

	/**
	 * @brief Check motor::Motor is connected.
	 *
	 * @throws std::runtime_error motor::Motor is not connected.
	 */
	void interrupt_check(void) noexcept;

	/**
	 * @brief Do a bead pull measurement, movement part.
	 *
	 * Called as part of pull_bead().
	 *
	 * @param[in] next_position: Position to move to.
	 * @param[in] use_return_speed: Use return speed for movement.
	 * @throws std::runtime_error: motor::Motor communication error.
	 * @throws std::runtime_error: motor::Motor movement timeout.
	 */
	void pull_bead_movement(util::pos_t next_position, bool use_return_speed);

	/**
	 * @brief Do a bead pull measurement, measurement part.
	 */
	void pull_bead_measurement(void);

	/**
	 * @brief Do a bead pull measurement.
	 */
	void pull_bead(void) noexcept;

	/**
	 * @brief Call function in separate thread with timeout.
	 *
	 * @param func Function to call.
	 * @param timeout Call timeout.
	 * @param timeout_message Message to use with timeout exception.
	 * @return Function evaluation result.
	 * @throws std::runtime_error Function evaluation timeout.
	 * @throws std::runtime_error Function exceptions are propagated to the caller.
	 */
	template <typename T>
	T
	timeout_function(std::function<T(std::stop_token)> func, util::timeout_t timeout,
			 const std::string &timeout_message) const
	{
		std::packaged_task<T(std::stop_token)> task(func);
		auto				       result = task.get_future();
		std::jthread			       thr(std::move(task));

		if (result.wait_for(timeout) == std::future_status::ready) {
			return result.get();
		} else {
			thr.request_stop();
			thr.detach();
			throw std::runtime_error(timeout_message);
		}
	}

      public:
	/**
	 * @brief Controller constructor.
	 *
	 * @param timeout Controller timeouts.
	 * @param send_message Send message function. Used to broadcast messages to WebSocket.
	 */
	Controller(Timeout								  timeout,
		   std::function<void(const util::string_t &, const util::attribute_t &)> send_message);
	/**
	 * @brief Controller do not allow copying.
	 */
	Controller(const Controller &) = delete;
	/**
	 * @brief Controller do not allow moving.
	 */
	Controller(const Controller &&) = delete;

	/**
	 * @brief Event loop function.
	 */
	void event_loop(void);

	/**
	 * @brief Get motor::Motor attribute choices.
	 *
	 * @param name motor::Motor name.
	 * @return Attribute choices.
	 */
	const util::attribute_t &motor_attribute_choices(util::string_t name) const;
	/**
	 * @brief Get bus::Bus attribute choices.
	 *
	 * @param name bus::Bus name.
	 * @return Attribute choices.
	 */
	const util::attribute_t &bus_attribute_choices(util::string_t name) const;
	/**
	 * @brief Get analyzer::Analyzer attribute choices.
	 *
	 * @param name analyzer::Analyzer name.
	 * @return Attribute choices.
	 */
	const util::attribute_t &analyzer_attribute_choices(util::string_t name) const;
	/**
	 * @brief Get selected motor::Motor attributes.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @return Selected attributes.
	 */
	const util::attribute_t &motor_attributes(void);
	/**
	 * @brief Get selected motor::Motor bus::Bus attributes.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @return Selected attributes.
	 */
	const util::attribute_t &motor_bus_attributes(void);
	/**
	 * @brief Get selected analyzer::Analyzer bus::Bus attributes.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @return Selected attributes.
	 */
	const util::attribute_t &analyzer_attributes(void);
	/**
	 * @brief Get selected analyzer::Analyzer bus::Bus attributes.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @return Selected attributes.
	 */
	const util::attribute_t &analyzer_bus_attributes(void);

	/**
	 * @brief Get available motor::Motor list.
	 *
	 * @return motor::Motor list.
	 */
	const util::name_list_t &motor_list(void) const noexcept;

	/**
	 * @brief Get motor::Motor supported bus::Bus list.
	 *
	 * @return motor::Motor list.
	 */
	const util::name_list_t &motor_bus_list(util::string_t name) const;
	/**
	 * @brief Get connected motor::Motor name.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @return motor::Motor name.
	 */
	const util::string_t motor_name(void);
	/**
	 * @brief Get connected motor::Motor bus::Bus name.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @return bus::Bus name.
	 */
	const util::string_t motor_bus_name(void);
	/**
	 * @brief Get connected motor::Motor capabilities.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @return motor::Motor capabilities.
	 */
	const util::attribute_t motor_capabilities(void);
	/**
	 * @brief Connect to motor::Motor \p motor_name via bus::Bus \p bus_name.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @param motor_name motor::Motor name.
	 * @param motor_attributes motor::Motor attributes.
	 * @param bus_name bus::Bus name.
	 * @param bus_attributes bus::Bus attributes.
	 */
	void set_motor(util::string_t motor_name, util::attribute_t motor_attributes, util::string_t bus_name,
		       util::attribute_t bus_attributes);
	/**
	 * @brief Check motor::Motor connected.
	 *
	 * @return Connected status.
	 */
	bool motor_connected(void) const noexcept;
	/**
	 * @brief Disconnect motor::Motor.
	 */
	void motor_disconnect(void);
	/**
	 * @brief Enable/disable motor::Motor steppers.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @throws std::runtime_error motor::Motor does not support steppers disabling.
	 * @param value Enable value.
	 */
	void steppers(bool value);
	/**
	 * @brief Get motor::Motor steppers enable status.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @throws std::runtime_error motor::Motor does not support steppers disabling.
	 * @return Enable value.
	 */
	bool steppers(void);
	/**
	 * @brief Move motor::Motor to position \p position.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @param position New position.
	 * @param fast_return Use return speed value for movement.
	 */
	void move_to(util::pos_t position, bool fast_return);
	/**
	 * @brief Move motor::Motor by step \p step.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @param step Step value.
	 * @param fast_return Use return speed value for movement.
	 */
	void move_by(util::pos_t step, bool fast_return);
	/**
	 * @brief Use current position as origin point.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 */
	void set_origin(void);
	/**
	 * @brief Return motor::Motor to origin point.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 */
	void return_origin(void);
	/**
	 * @brief Stop motor::Motor.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 */
	void stop(void);
	/**
	 * @brief Get current motor::Motor position.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @return Current motor::Motor position.
	 */
	util::pos_t position(void);
	/**
	 * @brief Set forward motor::Motor speed.
	 *
	 * Forward speed is used for measurements.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @throws std::runtime_error motor::Motor does not support speed change.
	 * @param speed New motor::Motor forward speed value.
	 */
	void speed(util::speed_t speed);
	/**
	 * @brief Get forward motor::Motor speed.
	 *
	 * Forward speed is used for measurements.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @throws std::runtime_error motor::Motor does not support speed change.
	 * @return motor::Motor forward speed.
	 */
	util::speed_t speed(void);
	/**
	 * @brief Set return motor::Motor speed.
	 *
	 * Return speed is used for fast return to origin.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @throws std::runtime_error motor::Motor does not support speed change.
	 * @param speed New motor::Motor return speed value.
	 */
	void return_speed(util::speed_t speed);
	/**
	 * @brief Get return motor::Motor speed.
	 *
	 * Return speed is used for measurements.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @throws std::runtime_error motor::Motor does not support speed change.
	 * @return motor::Motor return speed.
	 */
	util::speed_t return_speed(void);
	/**
	 * @brief Set motor::Motor acceleration.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @throws std::runtime_error motor::Motor does not support acceleration change.
	 * @param acceleration New motor::Motor return acceleration value.
	 */
	void acceleration(util::acceleration_t acceleration);
	/**
	 * @brief Get motor::Motor acceleration.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @throws std::runtime_error motor::Motor does not support acceleration change.
	 * @return motor::Motor return acceleration.
	 */
	util::acceleration_t acceleration(void);
	/**
	 * @brief Set motor::Motor deceleration.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @throws std::runtime_error motor::Motor does not support deceleration change.
	 * @param deceleration New motor::Motor return deceleration value.
	 */
	void deceleration(util::acceleration_t deceleration);
	/**
	 * @brief Get motor::Motor deceleration.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @throws std::runtime_error motor::Motor does not support deceleration change.
	 * @return motor::Motor return deceleration.
	 */
	util::acceleration_t deceleration(void);
	/**
	 * @brief Reverse motor::Motor movement direction.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @param value Reverse motor::Motor movement deceleration.
	 */
	void reverse(bool value);
	/**
	 * @brief Get motor::Motor movement direction.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @return motor::Motor direction reverse status.
	 */
	bool reverse(void);
	/**
	 * @brief Set motor::Motor position multiplier.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @param value motor::Motor position multiplier.
	 */
	void position_multiplier(double value);
	/**
	 * @brief Get motor::Motor position multiplier.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @return motor::Motor position multiplier.
	 */
	double position_multiplier(void);
	/**
	 * @brief Set motor::Motor speed multiplier.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @param value motor::Motor speed multiplier.
	 */
	void speed_multiplier(double value);
	/**
	 * @brief Get motor::Motor speed multiplier.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @return motor::Motor speed multiplier.
	 */
	double speed_multiplier(void);
	/**
	 * @brief Set motor::Motor acceleration multiplier.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @param value motor::Motor acceleration multiplier.
	 */
	void acceleration_multiplier(double value);
	/**
	 * @brief Get motor::Motor acceleration multiplier.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @return motor::Motor acceleration multiplier.
	 */
	double acceleration_multiplier(void);

	/**
	 * @brief Get available analyzer::Analyzer list.
	 *
	 * @return analyzer::Analyzer list.
	 */
	const util::name_list_t &analyzer_list(void) const noexcept;
	/**
	 * @brief Get analyzer::Analyzer supported bus::Bus list.
	 *
	 * @return analyzer::Analyzer list.
	 */
	const util::name_list_t &analyzer_bus_list(util::string_t) const;
	/**
	 * @brief Get connected analyzer::Analyzer name.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @return analyzer::Analyzer name.
	 */
	const util::string_t analyzer_name(void);
	/**
	 * @brief Get connected analyzer::Analyzer bus::Bus name.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @return bus::Bus name.
	 */
	const util::string_t analyzer_bus_name(void);
	/**
	 * @brief Get connected analyzer::Analyzer capabilities.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @return analyzer::Analyzer capabilities.
	 */
	const util::attribute_t analyzer_capabilities(void);
	/**
	 * @brief Connect to analyzer::Analyzer \p analyzer_name via bus::Bus \p bus_name.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @param analyzer_name analyzer::Analyzer name.
	 * @param analyzer_attributes analyzer::Analyzer attributes.
	 * @param bus_name bus::Bus name.
	 * @param bus_attributes bus::Bus attributes.
	 */
	void set_analyzer(util::string_t analyzer_name, util::attribute_t analyzer_attributes, util::string_t bus_name,
			  util::attribute_t bus_attributes);
	/**
	 * @brief Check analyzer::Analyzer connected.
	 *
	 * @return Connected status.
	 */
	bool analyzer_connected(void) const noexcept;
	/**
	 * @brief Disconnect motor::Motor.
	 */
	void analyzer_disconnect(void);
	/**
	 * @brief Get analyzer::Analyzer number of points possible values.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @return analyzer::Analyzer number of points possible values.
	 */
	util::nop_vals_t number_of_points_values(void);
	/**
	 * @brief Get analyzer::Analyzer selected number of points.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @return analyzer::Analyzer selected number of points.
	 */
	util::nop_t number_of_points(void);
	/**
	 * @brief Set analyzer::Analyzer number of points.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @param nop New analyzer::Analyzer number of points.
	 */
	void number_of_points(util::nop_t nop);
	/**
	 * @brief Get analyzer::Analyzer available S matrix parts.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @return analyzer::Analyzer available S matrix parts list.
	 */
	util::s_parts_t s_parts(void);
	/**
	 * @brief Set analyzer::Analyzer S matrix part.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @param from analyzer::Analyzer source port index.
	 * @param to analyzer::Analyzer destination port index.
	 */
	void s_part(util::s_port_t from, util::s_port_t to);
	/**
	 * @brief Get analyzer::Analyzer S matrix part.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @return analyzer::Analyzer S matrix part.
	 */
	util::s_part_t s_part(void);
	/**
	 * @brief Get analyzer::Analyzer left frequency.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @return analyzer::Analyzer left frequency.
	 */
	util::f_t f_left(void);
	/**
	 * @brief Set analyzer::Analyzer left frequency.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @param left analyzer::Analyzer left frequency.
	 */
	void f_left(util::f_t left);
	/**
	 * @brief Get analyzer::Analyzer right frequency.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @return analyzer::Analyzer right frequency.
	 */
	util::f_t f_right(void);
	/**
	 * @brief Set analyzer::Analyzer right frequency.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @param right analyzer::Analyzer right frequency.
	 */
	void f_right(util::f_t right);
	/**
	 * @brief Get analyzer::Analyzer center frequency.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @return analyzer::Analyzer center frequency.
	 */
	util::f_t f_center(void);
	/**
	 * @brief Set analyzer::Analyzer center frequency.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @param center analyzer::Analyzer center frequency.
	 */
	void f_center(util::f_t center);
	/**
	 * @brief Get analyzer::Analyzer frequency span.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @return analyzer::Analyzer frequency span.
	 */
	util::f_t f_span(void);
	/**
	 * @brief Set analyzer::Analyzer frequency span.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @param span analyzer::Analyzer frequency span.
	 */
	void f_span(util::f_t span);
	/**
	 * @brief Get analyzer::Analyzer frequencies.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @return analyzer::Analyzer frequencies.
	 */
	util::f_lim_t f_all(void);
	/**
	 * @brief Set analyzer::Analyzer frequencies.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @param left analyzer::Analyzer left frequency.
	 * @param right analyzer::Analyzer right frequency.
	 * @param center analyzer::Analyzer center frequency.
	 * @param span analyzer::Analyzer frequency span.
	 */
	void f_all(util::f_t left, util::f_t right, util::f_t center, util::f_t span);
	/**
	 * @brief Measure scattering using analyzer::Analyzer.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 */
	void measure_scattering(void);
	/**
	 * @brief Check scattering measurement is in progress.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @return Measurement is in progress.
	 */
	bool measuring_scattering(void);
	/**
	 * @brief Interrupt scattering measurement.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 */
	void interrupt_measure_scattering(void);
	/**
	 * @brief Retrieve scattering measurements.
	 *
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @return Scattering measurement values.
	 */
	const util::meas_pack_t &scattering_measurements(void);
	/**
	 * @brief Check field measurement is in progress.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @return Measurement is in progress.
	 */
	bool measuring_field(void);
	/**
	 * @brief Interrupt field measurement.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 */
	void interrupt_measure_field(void);
	/**
	 * @brief Measure field.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @param start Start position.
	 * @param stop Stop position.
	 * @param number_of_points Number of measurement points.
	 * @param field Field part.
	 * @param phase Use S phase for measurement.
	 * @param material Bead material.
	 * @param method Measurement method.
	 * @param coeff Measure coefficient. Meaning depends on selected field part and measurement method.
	 * @param Ql Loaded frequency.
	 */
	void measure_field(util::pos_t start, util::pos_t stop, util::nop_t number_of_points, result::FieldPart field,
			   bool phase, result::Material material, result::Method method, util::coeff_t coeff,
			   util::Q_t Ql);
	/**
	 * @brief Retrieve field measurements.
	 *
	 * @throws std::runtime_error motor::Motor not connected.
	 * @throws std::runtime_error analyzer::Analyzer not connected.
	 * @return Field measurement values.
	 */
	const util::meas_pack_t &field_measurements(void);
	/**
	 * @brief Stop Controller.
	 */
	void close(void) noexcept;
};

} // namespace controller
