/** @file motor.cxx
 * @brief motor::Motor related controller::Controller definitions.
 */

#include "controller.hxx"

#include <algorithm>
#include <boost/log/trivial.hpp>
#include <condition_variable>
#include <config.h>

namespace controller
{

void
Controller::check_motor_connected(void) const
{
	if (!motor_connected())
		throw std::runtime_error("motor not connected");
}

const util::attribute_t &
Controller::motor_attribute_choices(util::string_t motor) const
{
	return motor::get_attribute_choices(motor);
}

const util::attribute_t &
Controller::motor_attributes(void)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	return motor->attributes;
}

const util::attribute_t &
Controller::motor_bus_attributes(void)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	return motor->bus_attributes();
}

const util::name_list_t &
Controller::motor_list(void) const noexcept
{
	return motor::motor_list;
}

const util::name_list_t &
Controller::motor_bus_list(util::string_t motor) const
{
	return motor::get_supported_busses(motor);
}

const util::string_t
Controller::motor_name(void)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	return motor->name();
}

const util::string_t
Controller::motor_bus_name(void)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	return motor->bus_name();
}

const util::attribute_t
Controller::motor_capabilities(void)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	return {{"steppers", motor->steppers_shutdown_supported()},
		{"speed", motor->speed_supported()},
		{"acceleration", motor->acceleration_supported()},
		{"deceleration", motor->deceleration_supported()}};
}

void
Controller::set_motor(util::string_t new_motor, util::attribute_t motor_attrs, util::string_t motor_bus,
		      util::attribute_t bus_attrs)
{
	if (motor_connected())
		throw std::runtime_error("motor already connected");

	std::lock_guard<std::mutex> lock(motor_mutex);
	motor = timeout_function<std::shared_ptr<motor::Motor>>(
	    [&, timeout = this->timeout.motor_communication, notifier = this->notifier, bus_attrs,
	     motor_attrs](std::stop_token stoken) {
		    auto bus_attributes	  = std::move(bus_attrs);
		    auto motor_attributes = std::move(motor_attrs);
		    motor::patch_bus_attributes(new_motor, bus_attributes);
		    auto bus = bus::create_bus(motor_bus, timeout / 2, notifier, std::move(bus_attributes));
		    BOOST_LOG_TRIVIAL(debug) << "[Controller] created bus";
		    if (stoken.stop_requested())
			    throw std::runtime_error("abort");
		    auto motor = motor::create_motor(new_motor, std::move(motor_attributes), bus);
		    BOOST_LOG_TRIVIAL(debug) << "[Controller] created motor";
		    if (stoken.stop_requested())
			    throw std::runtime_error("abort");
		    return motor;
	    },
	    timeout.motor_communication, "motor connection timeout");
	send_message("motor",
		     {{"event", "connect"}, {"target", "motor"}, {"data", {{"motor", new_motor}, {"bus", motor_bus}}}});
}

bool
Controller::motor_connected(void) const noexcept
{
	return motor != nullptr;
}

void
Controller::motor_disconnect(void)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	timeout_function<void>([=, &motor = this->motor](std::stop_token) { motor.reset(); },
			       timeout.motor_communication, "motor disconnect timeout");
	send_message("motor", {{"event", "disconnect"}, {"target", "motor"}});
}

void
Controller::steppers(bool enable)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	timeout_function<void>([=, motor = this->motor](std::stop_token) { motor->steppers(enable); },
			       timeout.motor_communication, "steppers set timeout");
	send_message("motor", {{"event", "update"}, {"target", "steppers"}, {"value", enable}});
}

bool
Controller::steppers(void)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	return timeout_function<bool>([motor = this->motor](std::stop_token) { return motor->steppers(); },
				      timeout.motor_communication, "steppers set timeout");
}

void
Controller::move_to(util::pos_t position, bool return_speed)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	timeout_function<void>([=, motor = this->motor](std::stop_token) { motor->move_to(position, return_speed); },
			       timeout.motor_communication, "move to timeout");
}

void
Controller::move_by(util::pos_t position, bool return_speed)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	timeout_function<void>([=, motor = this->motor](std::stop_token) { motor->move_by(position, return_speed); },
			       timeout.motor_communication, "move by timeout");
}

void
Controller::set_origin(void)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	timeout_function<void>([=, motor = this->motor](std::stop_token) { motor->set_origin(); },
			       timeout.motor_communication, "set origin timeout");
	send_message("motor", {{"event", "update"}, {"target", "position"}, {"value", 0}});
}

void
Controller::stop(void)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	timeout_function<void>([=, motor = this->motor,
				&motor_events = this->motor_events](std::stop_token) { motor->stop(motor_events); },
			       timeout.motor_communication, "stop timeout");
}

util::pos_t
Controller::position(void)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	auto			    position =
	    timeout_function<util::pos_t>([motor = this->motor](std::stop_token) { return motor->position(); },
					  timeout.motor_communication, "get position timeout");
	send_message("motor", {{"event", "update"}, {"target", "position"}, {"value", position}});
	return position;
}

void
Controller::speed(util::speed_t speed)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	timeout_function<void>([=, motor = this->motor](std::stop_token) { motor->speed(speed); },
			       timeout.motor_communication, "speed set timeout");
	send_message("motor", {{"event", "update"}, {"target", "speed"}, {"value", speed}});
}

util::speed_t
Controller::speed(void)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	return timeout_function<util::speed_t>([=, motor = this->motor](std::stop_token) { return motor->speed(); },
					       timeout.motor_communication, "get speed timeout");
}

void
Controller::return_speed(util::speed_t speed)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	timeout_function<void>([=, motor = this->motor](std::stop_token) { motor->return_speed(speed); },
			       timeout.motor_communication, "return speed set timeout");
	send_message("motor", {{"event", "update"}, {"target", "return_speed"}, {"value", speed}});
}

util::speed_t
Controller::return_speed(void)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	return timeout_function<util::speed_t>(
	    [=, motor = this->motor](std::stop_token) { return motor->return_speed(); }, timeout.motor_communication,
	    "get return speed timeout");
}

void
Controller::acceleration(util::acceleration_t acceleration)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	timeout_function<void>([=, motor = this->motor](std::stop_token) { motor->acceleration(acceleration); },
			       timeout.motor_communication, "acceleration set timeout");
	send_message("motor", {{"event", "update"}, {"target", "acceleration"}, {"value", acceleration}});
}

util::acceleration_t
Controller::acceleration(void)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	return timeout_function<util::acceleration_t>(
	    [=, motor = this->motor](std::stop_token) { return motor->acceleration(); }, timeout.motor_communication,
	    "get acceleration timeout");
}

void
Controller::deceleration(util::acceleration_t deceleration)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	timeout_function<void>([=, motor = this->motor](std::stop_token) { motor->deceleration(deceleration); },
			       timeout.motor_communication, "deceleration set timeout");
	send_message("motor", {{"event", "update"}, {"target", "deceleration"}, {"value", deceleration}});
}

util::acceleration_t
Controller::deceleration(void)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	return timeout_function<util::acceleration_t>(
	    [=, motor = this->motor](std::stop_token) { return motor->deceleration(); }, timeout.motor_communication,
	    "get deceleration timeout");
}

void
Controller::reverse(bool val)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	timeout_function<void>([=, motor = this->motor](std::stop_token) { return motor->reverse(val); },
			       timeout.motor_communication, "reverse set timeout");
	send_message("motor", {{"event", "update"}, {"target", "reverse"}, {"value", val}});
}

bool
Controller::reverse(void)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	return timeout_function<bool>([=, motor = this->motor](std::stop_token) { return motor->reverse(); },
				      timeout.motor_communication, "get reverse timeout");
}

void
Controller::position_multiplier(double val)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	timeout_function<void>([=, motor = this->motor](std::stop_token) { motor->position_multiplier = val; },
			       timeout.motor_communication, "position multiplier set timeout");
	send_message("motor", {{"event", "update"}, {"target", "position_multiplier"}, {"value", val}});
}

double
Controller::position_multiplier(void)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	return timeout_function<double>(
	    [=, motor = this->motor](std::stop_token) { return motor->position_multiplier; },
	    timeout.motor_communication, "get position multiplier timeout");
}

void
Controller::speed_multiplier(double val)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	timeout_function<void>([=, motor = this->motor](std::stop_token) { motor->speed_multiplier = val; },
			       timeout.motor_communication, "speed multiplier set timeout");
	send_message("motor", {{"event", "update"}, {"target", "speed_multiplier"}, {"value", val}});
}

double
Controller::speed_multiplier(void)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	return timeout_function<double>([=, motor = this->motor](std::stop_token) { return motor->speed_multiplier; },
					timeout.motor_communication, "get speed multiplier timeout");
}

void
Controller::acceleration_multiplier(double val)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	timeout_function<void>([=, motor = this->motor](std::stop_token) { motor->acceleration_multiplier = val; },
			       timeout.motor_communication, "acceleration multiplier set timeout");
	send_message("motor", {{"event", "update"}, {"target", "acceleration_multiplier"}, {"value", val}});
}

double
Controller::acceleration_multiplier(void)
{
	check_motor_connected();
	std::lock_guard<std::mutex> lock(motor_mutex);
	return timeout_function<double>(
	    [=, motor = this->motor](std::stop_token) { return motor->acceleration_multiplier; },
	    timeout.motor_communication, "get acceleration multiplier timeout");
}

} // namespace controller
