/** @file analyzer.cxx
 * @brief analyzer::Analyzer related controller::Controller.
 */

#include "controller.hxx"

#include <algorithm>
#include <boost/log/trivial.hpp>
#include <condition_variable>
#include <config.h>

namespace controller
{

void
Controller::check_analyzer_connected(void) const
{
	if (!analyzer_connected())
		throw std::runtime_error("analyzer not connected");
}

const util::attribute_t &
Controller::analyzer_attribute_choices(util::string_t analyzer) const
{
	return analyzer::get_attribute_choices(analyzer);
}

const util::attribute_t &
Controller::analyzer_attributes(void)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	return analyzer->attributes;
}

const util::attribute_t &
Controller::analyzer_bus_attributes(void)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	return analyzer->bus_attributes();
}

const util::name_list_t &
Controller::analyzer_list(void) const noexcept
{
	return analyzer::analyzer_list;
}

const util::name_list_t &
Controller::analyzer_bus_list(util::string_t analyzer) const
{
	return analyzer::get_supported_busses(analyzer);
}

const util::string_t
Controller::analyzer_name(void)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	return analyzer->name();
}

const util::string_t
Controller::analyzer_bus_name(void)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	return analyzer->bus_name();
}

const util::attribute_t
Controller::analyzer_capabilities(void)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	return util::attribute_t();
}

void
Controller::set_analyzer(util::string_t new_analyzer, util::attribute_t analyzer_attrs, util::string_t analyzer_bus,
			 util::attribute_t bus_attrs)
{
	if (analyzer_connected())
		throw std::runtime_error("analyzer already connected");

	std::lock_guard<std::mutex> lock(analyzer_mutex);
	analyzer = timeout_function<std::shared_ptr<analyzer::Analyzer>>(
	    [&, notifier = this->notifier, timeout = this->timeout.analyzer_communication, bus_attrs,
	     analyzer_attrs](std::stop_token stoken) {
		    auto bus_attributes	     = std::move(bus_attrs);
		    auto analyzer_attributes = std::move(analyzer_attrs);
		    analyzer::patch_bus_attributes(new_analyzer, bus_attributes);
		    auto bus = bus::create_bus(analyzer_bus, timeout / 2, notifier, std::move(bus_attributes));
		    if (stoken.stop_requested())
			    throw std::runtime_error("abort");
		    auto analyzer = analyzer::create_analyzer(new_analyzer, std::move(analyzer_attributes), bus);
		    if (stoken.stop_requested())
			    throw std::runtime_error("abort");
		    return analyzer;
	    },
	    timeout.analyzer_communication, "analyzer connection timeout");
	send_message("analyzer", {{"event", "connect"},
				  {"target", "analyzer"},
				  {"data", {{"analyzer", new_analyzer}, {"bus", analyzer_bus}}}});
}

bool
Controller::analyzer_connected(void) const noexcept
{
	return analyzer != nullptr;
}

void
Controller::analyzer_disconnect(void)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	timeout_function<void>([=, &analyzer = this->analyzer](std::stop_token) { analyzer.reset(); },
			       timeout.analyzer_communication, "analyzer disconnect timeout");
	send_message("analyzer", {{"event", "disconnect"}, {"target", "analyzer"}});
}

util::nop_vals_t
Controller::number_of_points_values(void)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	return timeout_function<util::nop_vals_t>(
	    [=, analyzer = this->analyzer](std::stop_token) { return analyzer->number_of_points_values(); },
	    timeout.analyzer_communication, "analyzer number of points query timeout");
}

util::nop_t
Controller::number_of_points(void)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	return timeout_function<util::nop_t>(
	    [=, analyzer = this->analyzer](std::stop_token) { return analyzer->number_of_points(); },
	    timeout.analyzer_communication, "analyzer number of points query timeout");
}

void
Controller::number_of_points(util::nop_t nop)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	timeout_function<void>([=, analyzer = this->analyzer](std::stop_token) { analyzer->number_of_points(nop); },
			       timeout.analyzer_communication, "analyzer number of points set timeout");
	send_message("analyzer", {{"event", "update"}, {"target", "number_of_points"}, {"value", nop}});
	auto value = timeout_function<util::f_lim_t>(
	    [=, analyzer = this->analyzer](std::stop_token) { return analyzer->f_all(); },
	    timeout.analyzer_communication, "analyzer frequency query timeout");
	send_message("analyzer", {{"event", "update"},
				  {"target", "f"},
				  {"value",
				   {{"left", std::get<0>(value)},
				    {"right", std::get<1>(value)},
				    {"center", std::get<2>(value)},
				    {"span", std::get<3>(value)}}}});
}

util::s_parts_t
Controller::s_parts(void)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	return timeout_function<util::s_parts_t>(
	    [=, analyzer = this->analyzer](std::stop_token) { return analyzer->s_parts(); },
	    timeout.analyzer_communication, "analyzer S parts query timeout");
}

void
Controller::s_part(util::s_port_t from, util::s_port_t to)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	timeout_function<void>([=, analyzer = this->analyzer](std::stop_token) { analyzer->s_part(from, to); },
			       timeout.analyzer_communication, "analyzer S part set timeout");
	send_message("analyzer", {{"event", "update"}, {"target", "s_part"}, {"value", {to, from}}});
}

util::s_part_t
Controller::s_part(void)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	return timeout_function<util::s_part_t>(
	    [=, analyzer = this->analyzer](std::stop_token) { return analyzer->s_part(); },
	    timeout.analyzer_communication, "analyzer S part query timeout");
}

util::f_t
Controller::f_left(void)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	return timeout_function<util::f_t>(
	    [=, analyzer = this->analyzer](std::stop_token) { return analyzer->f_left(); },
	    timeout.analyzer_communication, "analyzer left frequency point query timeout");
}

void
Controller::f_left(util::f_t f)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	auto			    value = timeout_function<util::f_lim_t>(
		   [=, analyzer = this->analyzer](std::stop_token) { return analyzer->f_left(f); },
		   timeout.analyzer_communication, "analyzer left frequency point set timeout");
	send_message("analyzer", {{"event", "update"},
				  {"target", "f"},
				  {"value",
				   {{"left", std::get<0>(value)},
				    {"right", std::get<1>(value)},
				    {"center", std::get<2>(value)},
				    {"span", std::get<3>(value)}}}});
}

util::f_t
Controller::f_right(void)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	return timeout_function<util::f_t>(
	    [=, analyzer = this->analyzer](std::stop_token) { return analyzer->f_right(); },
	    timeout.analyzer_communication, "analyzer right frequency point query timeout");
}

void
Controller::f_right(util::f_t f)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	auto			    value = timeout_function<util::f_lim_t>(
		   [=, analyzer = this->analyzer](std::stop_token) { return analyzer->f_right(f); },
		   timeout.analyzer_communication, "analyzer right frequency point set timeout");
	send_message("analyzer", {{"event", "update"},
				  {"target", "f"},
				  {"value",
				   {{"left", std::get<0>(value)},
				    {"right", std::get<1>(value)},
				    {"center", std::get<2>(value)},
				    {"span", std::get<3>(value)}}}});
}

util::f_t
Controller::f_center(void)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	return timeout_function<util::f_t>(
	    [=, analyzer = this->analyzer](std::stop_token) { return analyzer->f_center(); },
	    timeout.analyzer_communication, "analyzer center frequency point query timeout");
}

void
Controller::f_center(util::f_t f)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	auto			    value = timeout_function<util::f_lim_t>(
		   [=, analyzer = this->analyzer](std::stop_token) { return analyzer->f_center(f); },
		   timeout.analyzer_communication, "analyzer center frequency point set timeout");
	send_message("analyzer", {{"event", "update"},
				  {"target", "f"},
				  {"value",
				   {{"left", std::get<0>(value)},
				    {"right", std::get<1>(value)},
				    {"center", std::get<2>(value)},
				    {"span", std::get<3>(value)}}}});
}

util::f_t
Controller::f_span(void)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	return timeout_function<util::f_t>(
	    [=, analyzer = this->analyzer](std::stop_token) { return analyzer->f_span(); },
	    timeout.analyzer_communication, "analyzer frequency span query timeout");
}

void
Controller::f_span(util::f_t f)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	auto			    value = timeout_function<util::f_lim_t>(
		   [=, analyzer = this->analyzer](std::stop_token) { return analyzer->f_span(f); },
		   timeout.analyzer_communication, "analyzer frequency span set timeout");
	send_message("analyzer", {{"event", "update"},
				  {"target", "f"},
				  {"value",
				   {{"left", std::get<0>(value)},
				    {"right", std::get<1>(value)},
				    {"center", std::get<2>(value)},
				    {"span", std::get<3>(value)}}}});
}

util::f_lim_t
Controller::f_all(void)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	return timeout_function<util::f_lim_t>(
	    [=, analyzer = this->analyzer](std::stop_token) { return analyzer->f_all(); },
	    timeout.analyzer_communication, "analyzer frequency query timeout");
}

void
Controller::f_all(util::f_t fl, util::f_t fr, util::f_t fc, util::f_t fs)
{
	check_analyzer_connected();
	std::lock_guard<std::mutex> lock(analyzer_mutex);
	auto			    value = timeout_function<util::f_lim_t>(
		   [=, analyzer = this->analyzer](std::stop_token) { return analyzer->f_all(fl, fr, fc, fs); },
		   timeout.analyzer_communication, "analyzer frequency span set timeout");
	send_message("analyzer", {{"event", "update"},
				  {"target", "f"},
				  {"value",
				   {{"left", std::get<0>(value)},
				    {"right", std::get<1>(value)},
				    {"center", std::get<2>(value)},
				    {"span", std::get<3>(value)}}}});
}

} // namespace controller
