find_package(OpenMP REQUIRED)

file(GLOB sources "*.cxx")
target_sources(${PROJECT_NAME}_lib PRIVATE ${sources})
target_compile_options(${PROJECT_NAME}_lib PRIVATE "${OpenMP_CXX_FLAGS}")
target_include_directories(${PROJECT_NAME}_lib PRIVATE "${OpenMP_CXX_INCLUDE_DIR}")
target_link_libraries(${PROJECT_NAME}_lib PRIVATE "${OpenMP_CXX_LIBRARIES}")
