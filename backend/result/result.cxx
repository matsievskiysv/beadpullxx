/** @file result.cxx
 * @brief Base result::Result definitions.
 */

#include "result.hxx"

namespace result
{

const std::lock_guard<std::mutex>
Result::get_lock(void)
{
	return std::lock_guard<std::mutex>(mutex);
}

util::meas_pack_t &
Result::serialized(void)
{
	std::lock_guard<std::mutex> lock(mutex);
	if (!is_serialized) {
		serialize();
		is_serialized = true;
	}
	return serialization_cache;
}

} // namespace result
