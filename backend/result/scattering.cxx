/** @file scattering.cxx
 * @brief result::Scattering definitions.
 */

#include "scattering.hxx"

#include <boost/json/value_from.hpp>
#include <boost/log/trivial.hpp>
#include <cmath>
#include <complex>

namespace json = boost::json;

namespace result
{

Scattering::Scattering()
{
}

util::f_array_t &
Scattering::get_f_buffer(void)
{
	return f_points;
}

util::meas_array_t &
Scattering::get_s_re_buffer(void)
{
	return s_re_points;
}

util::meas_array_t &
Scattering::get_s_im_buffer(void)
{
	return s_im_points;
}

const util::f_array_t &
Scattering::get_f_view(void) const
{
	return f_points;
}

const util::meas_array_t &
Scattering::get_s_re_view(void) const
{
	return s_re_points;
}

const util::meas_array_t &
Scattering::get_s_im_view(void) const
{
	return s_im_points;
}

const util::meas_array_t &
Scattering::get_s_mag_view(void) const
{
	return s_mag_points;
}

const util::meas_array_t &
Scattering::get_s_mag_log_view(void) const
{
	return s_mag_log_points;
}

const util::meas_array_t &
Scattering::get_phase_view(void) const
{
	return phase_points;
}

const util::meas_array_t &
Scattering::get_zin_re_view(void) const
{
	return zin_re_points;
}

const util::meas_array_t &
Scattering::get_zin_im_view(void) const
{
	return zin_im_points;
}

void
Scattering::process(void)
{
	is_serialized = false;
	auto size     = f_points.size();
	if ((s_re_points.size() != size) || (s_im_points.size() != size))
		throw std::runtime_error("data lengths do not match");

	s_mag_points.resize(size, 0);
	s_mag_log_points.resize(size, 0);
	phase_points.resize(size, 0);
	if (scattering_type == ScatteringType::Reflection) {
		zin_re_points.resize(size, 0);
		zin_im_points.resize(size, 0);
	} else {
		zin_re_points.clear();
		zin_im_points.clear();
	}

#pragma omp parallel for
	for (size_t idx = 0; idx < size; ++idx) {
		s_mag_points.at(idx) = std::sqrt(std::pow(s_re_points.at(idx), 2) + std::pow(s_im_points.at(idx), 2));
		s_mag_log_points.at(idx) = 10 * std::log(s_mag_points.at(idx));
		phase_points.at(idx) = std::atan2(s_im_points.at(idx), s_re_points.at(idx)) * 180 / std::numbers::pi;
		if (scattering_type == ScatteringType::Reflection) {
			// $$Z_{in}=\frac{1+S_{11}}{1-S_{11}}$$
			using namespace std::complex_literals;
			std::complex<double> s11 = s_re_points.at(idx) + s_im_points.at(idx) * 1i;
			auto		     zin = (1.0 + s11) / (1.0 - s11);
			zin_re_points.at(idx)	 = zin.real();
			zin_im_points.at(idx)	 = zin.imag();
		}
	}
}

void
Scattering::serialize(void)
{
	serialization_cache = {
	    {"f", json::value_from(f_points)},
	    {"s_re", json::value_from(s_re_points)},
	    {"s_im", json::value_from(s_im_points)},
	    {"s_mag", json::value_from(s_mag_points)},
	    {"s_mag_log", json::value_from(s_mag_log_points)},
	    {"phase", json::value_from(phase_points)},
	    {"zin_re", json::value_from(zin_re_points)},
	    {"zin_im", json::value_from(zin_im_points)},
	};
}

} // namespace result
