/** @file scattering.hxx
 * @brief result::Scattering definitions.
 */

#pragma once

#include "result.hxx"

namespace result
{

/**
 * @brief Scattering measurement.
 */
enum class ScatteringType {
	Reflection,   ///< Reflection mode.
	Transmission, ///< Transmission mode.
};

/**
 * @brief Scattering measurement result.
 */
class Scattering : public Result
{
      private:
	util::f_array_t f_points = {}; ///< Frequency points.
	// TODO: use complex array //
	util::meas_array_t s_re_points	    = {}; ///< Scattering points real part.
	util::meas_array_t s_im_points	    = {}; ///< Scattering points imaginary part.
	util::meas_array_t s_mag_points	    = {}; ///< Scattering magnitude points.
	util::meas_array_t s_mag_log_points = {}; ///< Scattering magnitude points in log scale.
	util::meas_array_t phase_points	    = {}; ///< Scattering phase.
	util::meas_array_t zin_re_points    = {}; ///< Input impedance real part.
	util::meas_array_t zin_im_points    = {}; ///< Input impedance imaginary part.

      protected:
	/**
	 * @brief Serialize measurement.
	 */
	void serialize(void);

      public:
	ScatteringType scattering_type = ScatteringType::Reflection; ///< Scattering type.

	/**
	 * @brief Scattering constructor.
	 */
	Scattering();

	/**
	 * @brief Get frequency points container.
	 *
	 * After measurement, analyzer::Analyzer sets frequency and scattering containers in Scattering object.
	 *
	 * @warning Caller is responsible for locking Scattering via get_lock() prior to this call.
	 * @return Frequency container.
	 */
	util::f_array_t &get_f_buffer(void);
	/**
	 * @brief Get scattering points real part container.
	 *
	 * After measurement, analyzer::Analyzer sets frequency and scattering containers in Scattering object.
	 *
	 * @warning Caller is responsible for locking Scattering via get_lock() prior to this call.
	 * @return Scattering real part container.
	 */
	util::meas_array_t &get_s_re_buffer(void);
	/**
	 * @brief Get scattering points imaginary part container.
	 *
	 * After measurement, analyzer::Analyzer sets frequency and scattering containers in Scattering object.
	 *
	 * @warning Caller is responsible for locking Scattering via get_lock() prior to this call.
	 * @return Scattering imaginary part container.
	 */
	util::meas_array_t &get_s_im_buffer(void);
	/**
	 * @brief Get frequency points.
	 *
	 * After measurement, analyzer::Analyzer sets frequency and scattering containers in Scattering object.
	 *
	 * @return Frequency points.
	 */
	const util::f_array_t &get_f_view(void) const;
	/**
	 * @brief Get scattering real part points.
	 *
	 * After measurement, analyzer::Analyzer sets frequency and scattering containers in Scattering object.
	 *
	 * @return Scattering real part.
	 */
	const util::meas_array_t &get_s_re_view(void) const;
	/**
	 * @brief Get scattering imaginary part points.
	 *
	 * After measurement, analyzer::Analyzer sets frequency and scattering containers in Scattering object.
	 *
	 * @return Scattering imaginary part.
	 */
	const util::meas_array_t &get_s_im_view(void) const;
	/**
	 * @brief Get scattering magnitude points.
	 *
	 * After measurement, analyzer::Analyzer sets frequency and scattering containers in Scattering object.
	 *
	 * @return Scattering magnitude.
	 */
	const util::meas_array_t &get_s_mag_view(void) const;
	/**
	 * @brief Get scattering magnitude log scale points.
	 *
	 * After measurement, analyzer::Analyzer sets frequency and scattering containers in Scattering object.
	 *
	 * @return Scattering magnitude log scale.
	 */
	const util::meas_array_t &get_s_mag_log_view(void) const;
	/**
	 * @brief Get scattering phase.
	 *
	 * After measurement, analyzer::Analyzer sets frequency and scattering containers in Scattering object.
	 *
	 * @return Scattering phase.
	 */
	const util::meas_array_t &get_phase_view(void) const;
	/**
	 * @brief Get input impedance real part points.
	 *
	 * After measurement, analyzer::Analyzer sets frequency and scattering containers in Scattering object.
	 *
	 * @return Input impedance real part.
	 */
	const util::meas_array_t &get_zin_re_view(void) const;
	/**
	 * @brief Get input impedance imaginary part points.
	 *
	 * After measurement, analyzer::Analyzer sets frequency and scattering containers in Scattering object.
	 *
	 * @return Input impedance imaginary part.
	 */
	const util::meas_array_t &get_zin_im_view(void) const;

	/**
	 * @brief Process data points.
	 *
	 * After setting data containers, this function converts scattering.
	 *
	 * @warning Caller is responsible for locking Scattering via get_lock() prior to this call.
	 */
	void process(void);
};

} // namespace result
