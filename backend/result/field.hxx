/** @file field.hxx
 * @brief result::Field definitions.
 */

#pragma once

#include "result.hxx"
#include "scattering.hxx"

namespace result
{

/**
 * @brief Bead material.
 */
enum class Material {
	Metal,	    ///< Metal bead.
	Dielectric, ///< Dielectric bead.
	Magnetic,   ///< Magnetic bead.
};
/**
 * @brief Measuring field part.
 */
enum class FieldPart {
	Electric, ///< Electric field.
	Magnetic, ///< Magnetic field.
};
/**
 * @brief Measurement method.
 */
enum class Method {
	Direct,	  ///< Direct measurement.
	Indirect, ///< Indirect measurement.
};

/**
 * @brief Field measurement result.
 */
class Field : public Result
{
      private:
	util::pos_array_t  pos_points;	     ///< Measurement coordinates.
	util::pos_t	   start;	     ///< Start position.
	util::pos_t	   stop;	     ///< Stop position.
	util::nop_t	   current_point;    ///< Current measurement position.
	util::nop_t	   number_of_points; ///< Number of measurement points.
	util::meas_array_t meas_re    = {};  ///< Raw measurement data real part.
	util::meas_array_t meas_im    = {};  ///< Raw measurement data imaginary part.
	util::meas_array_t field      = {};  ///< Calculated field data.
	util::meas_array_t field_norm = {};  ///< Calculated normalized field data.
	FieldPart	   field_part;	     ///< Measuring field part.
	bool		   phase;	     ///< Using S phase for measurement.
	Material	   material;	     ///< Bead material.
	Method		   method;	     ///< Measuring method.
	util::coeff_t	   coeff;	     ///< Measuring coefficient.
	util::Q_t	   Ql;		     ///< Loaded quality factor.
	util::f_t	   f0;		     ///< Central frequency.
	ScatteringType	   scattering_type;  ///< Scattering type.

	/**
	 * @brief Process data points.
	 *
	 * After setting data containers, this function converts scattering.
	 *
	 * @warning Caller is responsible for locking Scattering via get_lock() prior to this call.
	 */
	void process(void);

      protected:
	/**
	 * @brief Serialize measurement.
	 */
	void serialize(void);

      public:
	/**
	 * @brief Field constructor.
	 */
	Field();

	/**
	 * @brief Prepare measurement.
	 *
	 * @param start Start position.
	 * @param stop Stop position.
	 * @param number_of_points Number of points.
	 * @param field Field part.
	 * @param phase Use S phase for measurement.
	 * @param material Bead material.
	 * @param method Measurement method.
	 * @param coeff Measurement coefficient.
	 * @param Ql Loaded quality factor.
	 * @param f0 Central frequency.
	 */
	void setup(util::pos_t start, util::pos_t stop, util::nop_t number_of_points, result::FieldPart field,
		   bool phase, result::Material material, result::Method method, util::coeff_t coeff, util::Q_t Ql,
		   util::f_t f0);
	/**
	 * @brief Get number of measurement points.
	 *
	 * @return Number of measurement points.
	 */
	util::nop_t point_count(void);
	/**
	 * @brief Get next measurement point position.
	 *
	 * @return Next measurement point position.
	 */
	util::pos_t next_point(void);
	/**
	 * @brief Set measurement point data.
	 *
	 * @param point Measurement point data.
	 */
	void add_point(const Scattering &point);
};

} // namespace result
