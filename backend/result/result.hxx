/** @file result.hxx
 * @brief Base result::Result definitions.
 */

#pragma once

#include <mutex>
#include <types.hxx>

/** @namespace result
 * @brief result::Result namespace.
 */
namespace result
{

/**
 * @brief Measurement result base class.
 */
class Result
{
      protected:
	std::mutex	  mutex;				       ///< Access lock
	bool		  is_serialized	      = false;		       ///< Serialized flag. Used for lazy calculation.
	util::meas_pack_t serialization_cache = boost::json::object(); ///< Serialization cache.

	/**
	 * @brief Serialize measurement.
	 */
	virtual void serialize(void) = 0;

      public:
	/**
	 * @brief Get Result lock object.
	 *
	 * @return Lock object.
	 */
	const std::lock_guard<std::mutex> get_lock(void);
	/**
	 * @brief Get serialized measurement.
	 *
	 * @return Serialized measurement.
	 */
	util::meas_pack_t &serialized(void);
};

} // namespace result
