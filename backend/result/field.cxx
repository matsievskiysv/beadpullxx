/** @file field.cxx
 * @brief result::Field definitions.
 */

#include "field.hxx"

#include <boost/json/value_from.hpp>
#include <boost/log/trivial.hpp>
#include <cmath>
#include <complex>
#include <numbers>

namespace json = boost::json;

namespace result
{

/**
 * @brief Reduction data.
 *
 * This struct is used for min and max data search via OMP.
 */
struct Compare {
	util::meas_t val;   ///< Value.
	size_t	     index; ///< Value index.
};
#pragma omp declare reduction(minindex : struct Compare : omp_out = omp_in.val < omp_out.val ? omp_in : omp_out)       \
    initializer(omp_priv = {-1, 0})
#pragma omp declare reduction(maxindex : struct Compare : omp_out = omp_in.val > omp_out.val ? omp_in : omp_out)       \
    initializer(omp_priv = {-1, 0})

Field::Field()
{
}

void
Field::serialize(void)
{
	// TODO: use slice view [0:current_point] to construct json string //
	serialization_cache = {{"x", json::value_from(pos_points)},
			       {"field", json::value_from(field)},
			       {"field_norm", json::value_from(field_norm)}};
}

void
Field::setup(util::pos_t start, util::pos_t stop, util::nop_t number_of_points, result::FieldPart field, bool phase,
	     result::Material material, result::Method method, util::coeff_t coeff, util::Q_t Ql, util::f_t f0)
{
	std::lock_guard<std::mutex> lock(mutex);
	this->start	       = start;
	this->stop	       = stop;
	this->number_of_points = number_of_points;
	field_part	       = field;
	this->phase	       = phase;
	this->material	       = material;
	this->method	       = method;
	this->coeff	       = coeff;
	this->Ql	       = Ql;
	this->f0	       = f0;
	pos_points.resize(number_of_points);
	meas_re.resize(number_of_points);
	meas_im.resize(number_of_points);
	this->field.resize(number_of_points);
	field_norm.resize(number_of_points);
	current_point	 = 0;
	util::pos_t step = (stop - start) / (static_cast<util::pos_t>(number_of_points) - 1);
	BOOST_LOG_TRIVIAL(trace) << "[Field] stop: " << stop << " start:" << start << " nop: " << number_of_points
				 << " step: " << step;
#pragma omp parallel for
	for (size_t idx = 0; idx < number_of_points; ++idx) {
		pos_points.at(idx)  = start + step * idx;
		meas_re.at(idx)	    = 0;
		meas_im.at(idx)	    = 0;
		this->field.at(idx) = 0;
		field_norm.at(idx)  = 0;
	}
}

util::nop_t
Field::point_count(void)
{
	std::lock_guard<std::mutex> lock(mutex);
	return number_of_points;
}

util::pos_t
Field::next_point(void)
{
	std::lock_guard<std::mutex> lock(mutex);
	if (current_point == number_of_points)
		throw std::runtime_error("no more points");
	return pos_points[current_point];
}

void
Field::add_point(const Scattering &result)
{
	if (current_point == number_of_points)
		throw std::runtime_error("no more points");
	std::lock_guard<std::mutex> lock(mutex);
	is_serialized	= false;
	scattering_type = result.scattering_type;
	switch (method) {
	case Method::Direct: {
		auto	f_view = result.get_f_view();
		auto	s_view = result.get_s_mag_view();
		Compare f_idx  = {s_view[0], 0};
		switch (scattering_type) {
		case ScatteringType::Reflection: {
#pragma omp parallel for reduction(minindex : f_idx)
			for (size_t idx = 0; idx < s_view.size(); idx++) {
				if (f_idx.val == -1) {
					f_idx.index = idx;
					f_idx.val   = s_view[idx];
				} else if (s_view[idx] < f_idx.val) {
					f_idx.index = idx;
					f_idx.val   = s_view[idx];
				}
			}
			break;
		}
		case ScatteringType::Transmission: {
#pragma omp parallel for reduction(maxindex : f_idx)
			for (size_t idx = 0; idx < s_view.size(); idx++) {
				if (f_idx.val == -1) {
					f_idx.index = idx;
					f_idx.val   = s_view[idx];
				} else if (s_view[idx] > f_idx.val) {
					f_idx.index = idx;
					f_idx.val   = s_view[idx];
				}
			}
			break;
		}
		}
		BOOST_LOG_TRIVIAL(trace) << "[Field] point: " << current_point << " f_res: " << f_view[f_idx.index];
		meas_re[current_point] = f_view[f_idx.index];
		break;
	}
	case Method::Indirect: {
		switch (scattering_type) {
		case ScatteringType::Reflection: {
			if (phase) {
				auto phase     = result.get_phase_view();
				auto phase_val = phase[phase.size() / 2];
				BOOST_LOG_TRIVIAL(trace)
				    << "[Field] point: " << current_point << " phase: " << phase_val;
				meas_re[current_point] = phase_val;
			} else {
				auto s_re     = result.get_s_re_view();
				auto s_im     = result.get_s_im_view();
				auto s_re_val = s_re[s_re.size() / 2];
				auto s_im_val = s_im[s_re.size() / 2];
				BOOST_LOG_TRIVIAL(trace) << "[Field] point: " << current_point << " s_re: " << s_re_val
							 << " s_im: " << s_im_val;
				meas_re[current_point] = s_re_val;
				meas_im[current_point] = s_im_val;
			}
			break;
		}
		case ScatteringType::Transmission: {
			auto phase     = result.get_phase_view();
			auto phase_val = phase[phase.size() / 2];
			BOOST_LOG_TRIVIAL(trace) << "[Field] point: " << current_point << " phase: " << phase_val;
			meas_re[current_point] = phase_val;
			break;
		}
		}
		break;
	}
	}

	current_point++;
	process();
}

void
Field::process(void)
{
	switch (method) {
	case Method::Direct: {
		auto denom = std::sqrt(2 * std::numbers::pi * coeff * f0 * f0);
		auto f_ref = meas_re.at(0);
		auto mult  = field_part == FieldPart::Magnetic ? -1 : 1;
#pragma omp parallel for
		for (size_t idx = 0; idx < current_point; ++idx)
			field.at(idx) = mult * std::sqrt(meas_re.at(idx) - f_ref) / denom;
		break;
	}
	case Method::Indirect: {
		switch (scattering_type) {
		case ScatteringType::Reflection: {
			if (phase) {
				auto phi0 = meas_re.at(0);
#pragma omp parallel for
				for (size_t idx = 0; idx < current_point; ++idx)
					field.at(idx) = (meas_re.at(idx) - phi0) / 2;
			} else {
				auto denom = std::sqrt(2 * std::numbers::pi * coeff * f0 * f0);
				auto mult  = field_part == FieldPart::Magnetic ? -1 : 1;
				auto s0	   = std::complex<util::meas_t>(meas_re.at(0), meas_im.at(0));
#pragma omp parallel for
				for (size_t idx = 0; idx < current_point; ++idx) {
					auto s	      = std::complex<util::meas_t>(meas_re.at(idx), meas_im.at(idx));
					field.at(idx) = mult * std::abs(s - s0) / denom;
				}
			}
			break;
		}
		case ScatteringType::Transmission: {
			auto denom = std::sqrt(4 * std::numbers::pi * coeff * f0 * Ql);
			auto mult  = field_part == FieldPart::Magnetic ? -1 : 1;
			auto phi0  = meas_re.at(0);
#pragma omp parallel for
			for (size_t idx = 0; idx < current_point; ++idx) {
				field.at(idx) = mult * std::abs(meas_re.at(idx) - phi0) / denom;
			}
			break;
		}
		}
		break;
	}
	}
	auto field_max = field.at(0);
#pragma omp parallel for reduction(max : field_max)
	for (size_t idx = 0; idx < current_point; idx++)
		field_max = std::max(field_max, std::abs(field.at(idx)));
	field_max = field_max == 0 ? 1 : field_max;
#pragma omp parallel for
	for (size_t idx = 0; idx < current_point; ++idx)
		field_norm.at(idx) = field.at(idx) / field_max;
}

} // namespace result
