/** @file measure.cxx
 * @brief HTTP measurement related endpoints.
 */

#include "measure.hxx"

#include <boost/json.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/log/trivial.hpp>
#include <result/field.hxx>
#include <server.hxx>
#include <types.hxx>

namespace json = boost::json;

/** @namespace endpoint
 * ## Measure endpoints
 */
namespace endpoint
{

/** @namespace endpoint
 * ### Measure scattering
 *
 * Measure scattering.
 *
 * **Method**: `POST`
 *
 * **Path**: `/measure_scattering`
 *
 * **Function**: post_measure_scattering()
 */
void
post_measure_scattering(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		ctrl.measure_scattering();
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Measuring scattering
 *
 * Measuring scattering.
 *
 * **Method**: `GET`
 *
 * **Path**: `/measuring_scattering`
 *
 * **Function**: get_measuring_scattering()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: Measuring scattering indication.
 */
void
get_measuring_scattering(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			 const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto message = json::value(ctrl.measuring_scattering());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(message));
	});
}

/** @namespace endpoint
 * ### Interrupt scattering measure
 *
 * Interrupt scattering measure.
 *
 * **Method**: `POST`
 *
 * **Path**: `/interrupt_measure_scattering`
 *
 * **Function**: post_interrupt_measure_scattering()
 */
void
post_interrupt_measure_scattering(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
				  const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		ctrl.interrupt_measure_scattering();
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Get scattering measurement data
 *
 * Get scattering measurement data.
 *
 * **Method**: `GET`
 *
 * **Path**: `/scattering_measurements`
 *
 * **Function**: get_scattering_measurements()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: Scattering measurement data.
 */
void
get_scattering_measurements(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			    const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto data =
		    util::Chunked::stream_t(new std::stringstream(json::serialize(ctrl.scattering_measurements())));
		bool compress = req->getHeader("accept-encoding").find("deflate") != std::string::npos;
		auto chunked_string =
		    std::shared_ptr<util::Chunked>(new util::Chunked(std::move(data), SEND_CHUNK_SIZE, compress));
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		if (compress)
			res->writeHeader("Content-Encoding", "deflate");
		res->onWritable([=](auto) { return send_chunked(res, chunked_string); });
		res->onAborted([]() {});
		send_chunked(res, chunked_string);
	});
}

/** @namespace endpoint
 * ### Measuring field
 *
 * Measuring field.
 *
 * **Method**: `GET`
 *
 * **Path**: `/measuring_field`
 *
 * **Function**: get_measuring_field()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: Measuring field indication.
 */
void
get_measuring_field(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		    const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto value = json::value_from(ctrl.measuring_field());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(value));
	});
}

/** @namespace endpoint
 * ### Measure field
 *
 * Measure field.
 *
 * **Method**: `POST`
 *
 * **Path**: `/measure_field`
 *
 * **Function**: post_measure_field()
 *
 * **Arguments**:
 * - `start`: start position.
 * - `stop`: stop position.
 * - `nop`: number of measurement points.
 * - `field`: field type. Possible values: `electric`, `magnetic`.
 * - `phase`: use field phase flag. Possible values: `true`, `false`.
 * - `material`: material. Possible values: `metal`, `dielectric`, `magnetic`.
 * - `method`: measure method. Possible values: `direct`, `indirect`.
 * - `coeff`: measure coefficient.
 * - `Ql`: loaded quality factor.
 */
void
post_measure_field(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		   const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("start");
	CHECK_ARG("stop");
	CHECK_ARG("nop");
	CHECK_ARG("field");
	CHECK_ARG("phase");
	CHECK_ARG("material");
	CHECK_ARG("method");
	CHECK_ARG("coeff");
	CHECK_ARG("Ql");
	auto start    = req->getQuery("start");
	auto stop     = req->getQuery("stop");
	auto nop      = req->getQuery("nop");
	auto field    = req->getQuery("field");
	auto phase    = req->getQuery("phase");
	auto material = req->getQuery("material");
	auto method   = req->getQuery("method");
	auto coeff    = req->getQuery("coeff");
	auto Ql	      = req->getQuery("Ql");
	CHECK_VAL(field, field != "electric" && field != "magnetic");
	CHECK_VAL(phase, phase != "true" && phase != "false");
	CHECK_VAL(material, material != "metal" && material != "dielectric" && material != "magnetic");
	CHECK_VAL(method, method != "direct" && method != "indirect");
	CATCH_ERROR({
		ctrl.measure_field(
		    boost::lexical_cast<util::pos_t>(start), boost::lexical_cast<util::pos_t>(stop),
		    boost::lexical_cast<util::nop_t>(nop),
		    field == "electric" ? result::FieldPart::Electric : result::FieldPart::Magnetic, phase == "true",
		    material == "metal"
			? result::Material::Metal
			: (material == "dielectric" ? result::Material::Dielectric : result::Material::Magnetic),
		    method == "direct" ? result::Method::Direct : result::Method::Indirect,
		    boost::lexical_cast<util::coeff_t>(coeff), boost::lexical_cast<util::Q_t>(Ql));
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Interrupt field measure
 *
 * Interrupt field measure.
 *
 * **Method**: `POST`
 *
 * **Path**: `/interrupt_measure_field`
 *
 * **Function**: post_interrupt_measure_field()
 */
void
post_interrupt_measure_field(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			     const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		ctrl.interrupt_measure_field();
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Get field measurement data
 *
 * Get field measurement data.
 *
 * **Method**: `GET`
 *
 * **Path**: `/field_measurements`
 *
 * **Function**: get_field_measurements()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: Field measurement data.
 */
void
get_field_measurements(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		       const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto data = util::Chunked::stream_t(new std::stringstream(json::serialize(ctrl.field_measurements())));
		bool compress = req->getHeader("accept-encoding").find("deflate") != std::string::npos;
		auto chunked_string =
		    std::shared_ptr<util::Chunked>(new util::Chunked(std::move(data), SEND_CHUNK_SIZE, compress));
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		if (compress)
			res->writeHeader("Content-Encoding", "deflate");
		res->onWritable([=](auto) { return send_chunked(res, chunked_string); });
		res->onAborted([]() {});
		send_chunked(res, chunked_string);
	});
}

} // namespace endpoint
