/** @file endpoints.hxx
 * @brief HTTP endpoints.
 */

#pragma once

#include <App.h>
#include <controller/controller.hxx>
#include <string>

namespace endpoint
{

/**
 * @brief Get bus attribute choices.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_bus_attribute_choices(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			       const std::string &auth);

} // namespace endpoint
