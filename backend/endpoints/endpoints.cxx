/** @file endpoints.cxx
 * @brief HTTP endpoints.
 */

#include "endpoints.hxx"

#include <boost/json.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/log/trivial.hpp>
#include <server.hxx>
#include <types.hxx>

namespace json = boost::json;

/** @namespace endpoint
 * @brief HTTP endpoint namespace.
 *
 * ## Other endpoints
 */
namespace endpoint
{

/** @namespace endpoint
 * ### Bus attribute choices
 *
 * Get bus::Bus attribute choices.
 *
 * **Method**: `GET`
 *
 * **Path**: `/bus_attribute_choices`
 *
 * **Function**: get_bus_attribute_choices()
 *
 * **Arguments**:
 * - `bus`: bus::Bus name.
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: bus::Bus attribute choices dictionary.
 */
void
get_bus_attribute_choices(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			  const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("bus");
	auto bus_name = req->getQuery("bus");
	CATCH_ERROR({
		auto attrs = ctrl.bus_attribute_choices(json::string(bus_name));
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(attrs));
	});
}

} // namespace endpoint
