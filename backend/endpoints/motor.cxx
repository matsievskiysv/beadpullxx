/** @file motor.cxx
 * @brief HTTP motor::Motor related endpoints.
 */

#include "motor.hxx"

#include <boost/json.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/log/trivial.hpp>
#include <server.hxx>
#include <types.hxx>

namespace json = boost::json;

/** @namespace endpoint
 * ## Motor endpoints
 */
namespace endpoint
{

/** @namespace endpoint
 * ### Motor list
 *
 * Get motor::Motor list.
 *
 * **Method**: `GET`
 *
 * **Path**: `/motor_list`
 *
 * **Function**: get_motor_list()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: motor::Motor name list.
 */
void
get_motor_list(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
	       const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	auto motor_list = json::value_from(ctrl.motor_list());
	res->writeStatus(uWS::HTTP_200_OK);
	res->writeHeader("Content-Type", "application/json");
	res->end(json::serialize(motor_list));
}

/** @namespace endpoint
 * ### Motor bus list
 *
 * Get motor::Motor bus::Bus list.
 *
 * **Method**: `GET`
 *
 * **Path**: `/motor_bus_list`
 *
 * **Function**: get_motor_bus_list()
 *
 * **Arguments**:
 * - `motor`: motor::Motor name.
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: List of the bus::Bus, supported by the motor::Motor.
 */
void
get_motor_bus_list(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		   const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("motor");
	auto motor_name = req->getQuery("motor");
	CATCH_ERROR({
		auto bus_list = json::value_from(ctrl.motor_bus_list(json::string(motor_name)));
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(bus_list));
	});
}

/** @namespace endpoint
 * ### Motor attribute choices
 *
 * Get motor::Motor bus::Bus list.
 *
 * **Method**: `GET`
 *
 * **Path**: `/motor_attribute_choices`
 *
 * **Function**: get_motor_attribute_choices()
 *
 * **Arguments**:
 * - `motor`: motor::Motor name.
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: motor::Motor attribute choices dictionary.
 */
void
get_motor_attribute_choices(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			    const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("motor");
	auto motor_name = req->getQuery("motor");
	CATCH_ERROR({
		auto attrs = ctrl.motor_attribute_choices(json::string(motor_name));
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(attrs));
	});
}

/** @namespace endpoint
 * ### Motor attributes
 *
 * Get motor::Motor selected attributes.
 *
 * **Method**: `GET`
 *
 * **Path**: `/motor_attributes`
 *
 * **Function**: get_motor_attributes()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: motor::Motor attributes dictionary.
 */
void
get_motor_attributes(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		     const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto attrs = ctrl.motor_attributes();
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(attrs));
	});
}

/** @namespace endpoint
 * ### Motor bus attributes
 *
 * Get motor::Motor bus::Bus selected attributes.
 *
 * **Method**: `GET`
 *
 * **Path**: `/motor_bus_attributes`
 *
 * **Function**: get_motor_bus_attributes()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: motor::Motor bus::Bus attributes dictionary.
 */
void
get_motor_bus_attributes(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			 const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto attrs = ctrl.motor_bus_attributes();
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(attrs));
	});
}

/** @namespace endpoint
 * ### Motor capabilities
 *
 * Get motor::Motor capabilities.
 *
 * **Method**: `GET`
 *
 * **Path**: `/motor_capabilities`
 *
 * **Function**: get_motor_capabilities()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: motor::Motor capabilities list.
 */
void
get_motor_capabilities(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		       const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto supported = json::value(ctrl.motor_capabilities());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(supported));
	});
}

/** @namespace endpoint
 * ### Select motor
 *
 * Select motor::Motor.
 *
 * **Method**: `POST`
 *
 * **Path**: `/motor`
 *
 * **Function**: post_motor()
 *
 * **Arguments**:
 * - `motor`: motor::Motor name.
 * - `bus`: bus::Bus name.
 *
 * **Body mime**: `application/json`
 *
 * **Method body**: motor::Motor and bus::Bus attributes dictionaries in form
 * `{"motor_attributes": ..., "bus_attributes": ...}`.
 * motor::Motor and bus::Bus have the same structure as get_motor_attribute_choices() and
 * get_bus_attribute_choices() dictionaries.
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: motor::Motor and bus::Bus attributes. They could be changed by the driver constructor code.
 */
void
post_motor(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("motor");
	CHECK_ARG("bus");
	CHECK_CONTENT_HEADER("application/json");
	auto			     motor_name = req->getQuery("motor");
	auto			     bus_name	= req->getQuery("bus");
	std::unique_ptr<std::string> payload(new std::string(""));
	res->onData([res, payload = std::move(payload), motor_name, bus_name, &ctrl](auto chunk, auto isLast) {
		payload->append(chunk);
		if (isLast) {
			CATCH_ERROR({
				auto message	 = json::parse(*payload);
				auto motor_attrs = message.as_object()["motor_attributes"];
				auto bus_attrs	 = message.as_object()["bus_attributes"];
				ctrl.set_motor(json::string(motor_name), motor_attrs, json::string(bus_name),
					       bus_attrs);
			});
			res->writeStatus(uWS::HTTP_200_OK);
			res->end();
		}
	});
	res->onAborted([]() {});
}

/** @namespace endpoint
 * ### Motor name
 *
 * Get connected motor::Motor name.
 *
 * **Method**: `GET`
 *
 * **Path**: `/motor`
 *
 * **Function**: get_motor()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: motor::Motor name.
 */
void
get_motor(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto motor_name = ctrl.motor_name();
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(motor_name));
	});
}

/** @namespace endpoint
 * ### Motor bus name
 *
 * Get connected motor::Motor bus::Bus name.
 *
 * **Method**: `GET`
 *
 * **Path**: `/motor_bus`
 *
 * **Function**: get_motor_bus()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: motor::Motor bus::Bus name.
 */
void
get_motor_bus(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
	      const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto bus_name = ctrl.motor_bus_name();
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(bus_name));
	});
}

/** @namespace endpoint
 * ### Disconnect motor
 *
 * Disconnect motor::Motor.
 *
 * **Method**: `DELETE`
 *
 * **Path**: `/motor`
 *
 * **Function**: del_motor()
 */
void
del_motor(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		ctrl.motor_disconnect();
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Motor speed
 *
 * Get motor::Motor speed.
 *
 * **Method**: `GET`
 *
 * **Path**: `/speed`
 *
 * **Function**: get_speed()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: motor::Motor speed.
 */
void
get_speed(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto speed = json::value(ctrl.speed());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(speed));
	});
}

/** @namespace endpoint
 * ### Set motor speed
 *
 * Set motor::Motor speed.
 *
 * **Method**: `POST`
 *
 * **Path**: `/speed`
 *
 * **Function**: post_speed()
 *
 * **Arguments**:
 * - `value`: speed value.
 */
void
post_speed(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("value");
	auto value = req->getQuery("value");
	CATCH_ERROR({
		ctrl.speed(boost::lexical_cast<util::speed_t>(value));
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Motor return speed
 *
 * Get motor::Motor return speed.
 *
 * **Method**: `GET`
 *
 * **Path**: `/return_speed`
 *
 * **Function**: get_return_speed()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: motor::Motor return speed.
 */
void
get_return_speed(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		 const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto speed = json::value(ctrl.return_speed());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(speed));
	});
}

/** @namespace endpoint
 * ### Set motor return speed
 *
 * Set motor::Motor return speed.
 *
 * **Method**: `POST`
 *
 * **Path**: `/return_speed`
 *
 * **Function**: post_return_speed()
 *
 * **Arguments**:
 * - `value`: return speed value.
 */
void
post_return_speed(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		  const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("value");
	auto value = req->getQuery("value");
	CATCH_ERROR({
		ctrl.return_speed(boost::lexical_cast<util::speed_t>(value));
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Motor acceleration
 *
 * Get motor::Motor acceleration.
 *
 * **Method**: `GET`
 *
 * **Path**: `/acceleration`
 *
 * **Function**: get_acceleration()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: motor::Motor acceleration.
 */
void
get_acceleration(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		 const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto speed = json::value(ctrl.acceleration());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(speed));
	});
}

/** @namespace endpoint
 * ### Set motor acceleration
 *
 * Set motor::Motor acceleration.
 *
 * **Method**: `POST`
 *
 * **Path**: `/acceleration`
 *
 * **Function**: post_acceleration()
 *
 * **Arguments**:
 * - `value`: acceleration value.
 */
void
post_acceleration(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		  const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("value");
	auto value = req->getQuery("value");
	CATCH_ERROR({
		ctrl.acceleration(boost::lexical_cast<util::acceleration_t>(value));
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Motor deceleration
 *
 * Get motor::Motor deceleration.
 *
 * **Method**: `GET`
 *
 * **Path**: `/deceleration`
 *
 * **Function**: get_deceleration()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: motor::Motor deceleration.
 */
void
get_deceleration(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		 const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto speed = json::value(ctrl.deceleration());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(speed));
	});
}

/** @namespace endpoint
 * ### Set motor deceleration
 *
 * Set motor::Motor deceleration.
 *
 * **Method**: `POST`
 *
 * **Path**: `/deceleration`
 *
 * **Function**: post_deceleration()
 *
 * **Arguments**:
 * - `value`: deceleration value.
 */
void
post_deceleration(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		  const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("value");
	auto value = req->getQuery("value");
	CATCH_ERROR({
		ctrl.deceleration(boost::lexical_cast<util::acceleration_t>(value));
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Motor steppers status
 *
 * Get motor::Motor steppers status.
 *
 * **Method**: `GET`
 *
 * **Path**: `/steppers`
 *
 * **Function**: get_steppers()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: motor::Motor stepper status.
 */
void
get_steppers(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto supported = json::value(ctrl.steppers());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(supported));
	});
}

/** @namespace endpoint
 * ### Set motor steppers status
 *
 * Set motor::Motor steppers status.
 *
 * **Method**: `POST`
 *
 * **Path**: `/steppers`
 *
 * **Function**: post_steppers()
 *
 * **Arguments**:
 * - `value`: steppers status.
 */
void
post_steppers(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
	      const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("value");
	auto enable = req->getQuery("value");
	CHECK_VAL(value, enable != "true" && enable != "false");
	CATCH_ERROR({
		ctrl.steppers(enable == "true");
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Motor position
 *
 * Get motor::Motor position.
 *
 * **Method**: `GET`
 *
 * **Path**: `/position`
 *
 * **Function**: get_position()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: motor::Motor position.
 */
void
get_position(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto position = json::value(ctrl.position());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(position));
	});
}

/** @namespace endpoint
 * ### Move to position
 *
 * Move to position.
 *
 * **Method**: `POST`
 *
 * **Path**: `/move_to`
 *
 * **Function**: post_move_to()
 *
 * **Arguments**:
 * - `value`: position value.
 * - `return_speed`: use return speed for movement. Possible values: `true`, `false`.
 */
void
post_move_to(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("value");
	CHECK_ARG("return_speed");
	auto value	  = req->getQuery("value");
	auto return_speed = req->getQuery("return_speed");
	CHECK_VAL(return_speed, return_speed != "true" && return_speed != "false");
	CATCH_ERROR({
		ctrl.move_to(boost::lexical_cast<util::pos_t>(value), return_speed == "true");
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Move by step
 *
 * Move by step.
 *
 * **Method**: `POST`
 *
 * **Path**: `/move_by`
 *
 * **Function**: post_move_by()
 *
 * **Arguments**:
 * - `value`: step value.
 * - `return_speed`: use return speed for movement. Possible values: `true`, `false`.
 */
void
post_move_by(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("value");
	CHECK_ARG("return_speed");
	auto value	  = req->getQuery("value");
	auto return_speed = req->getQuery("return_speed");
	CHECK_VAL(return_speed, return_speed != "true" && return_speed != "false");
	CATCH_ERROR({
		ctrl.move_by(boost::lexical_cast<util::pos_t>(value), return_speed == "true");
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Set motor origin
 *
 * Set motor::Motor origin.
 *
 * **Method**: `POST`
 *
 * **Path**: `/set_origin`
 *
 * **Function**: post_set_origin()
 */
void
post_set_origin(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		ctrl.set_origin();
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Stop motor
 *
 * Stop motor::Motor.
 *
 * **Method**: `POST`
 *
 * **Path**: `/stop`
 *
 * **Function**: post_stop()
 */
void
post_stop(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		ctrl.stop();
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Motor reverse status
 *
 * Get motor::Motor reverse status.
 *
 * **Method**: `GET`
 *
 * **Path**: `/reverse`
 *
 * **Function**: get_reverse()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: motor::Motor reverse status.
 */
void
get_reverse(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto reverse = json::value(ctrl.reverse());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(reverse));
	});
}

/** @namespace endpoint
 * ### Set motor reverse status
 *
 * Set motor::Motor reverse status.
 *
 * **Method**: `POST`
 *
 * **Path**: `/reverse`
 *
 * **Function**: post_reverse()
 *
 * **Arguments**:
 * - `value`: reverse status.
 */
void
post_reverse(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("value");
	auto reverse = req->getQuery("value");
	CHECK_VAL(reverse, reverse != "true" && reverse != "false");
	CATCH_ERROR({
		ctrl.reverse(reverse == "true");
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Motor position multiplier
 *
 * Get motor::Motor position multiplier.
 *
 * **Method**: `GET`
 *
 * **Path**: `/position_multiplier`
 *
 * **Function**: get_position_multiplier()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: motor::Motor position multiplier.
 */
void
get_position_multiplier(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto value = json::value(ctrl.position_multiplier());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(value));
	});
}

/** @namespace endpoint
 * ### Set motor position multiplier
 *
 * Set motor::Motor position multiplier.
 *
 * **Method**: `POST`
 *
 * **Path**: `/position_multiplier`
 *
 * **Function**: post_position_multiplier()
 *
 * **Arguments**:
 * - `value`: position multiplier value.
 */
void
post_position_multiplier(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			 const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("value");
	auto value = req->getQuery("value");
	CATCH_ERROR({
		ctrl.position_multiplier(boost::lexical_cast<double>(value));
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Motor speed multiplier
 *
 * Get motor::Motor speed multiplier.
 *
 * **Method**: `GET`
 *
 * **Path**: `/speed_multiplier`
 *
 * **Function**: get_speed_multiplier()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: motor::Motor speed multiplier.
 */
void
get_speed_multiplier(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		     const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto value = json::value(ctrl.speed_multiplier());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(value));
	});
}

/** @namespace endpoint
 * ### Set motor speed multiplier
 *
 * Set motor::Motor speed multiplier.
 *
 * **Method**: `POST`
 *
 * **Path**: `/speed_multiplier`
 *
 * **Function**: post_speed_multiplier()
 *
 * **Arguments**:
 * - `value`: speed multiplier value.
 */
void
post_speed_multiplier(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		      const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("value");
	auto value = req->getQuery("value");
	CATCH_ERROR({
		ctrl.speed_multiplier(boost::lexical_cast<double>(value));
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Motor acceleration multiplier
 *
 * Get motor::Motor acceleration multiplier.
 *
 * **Method**: `GET`
 *
 * **Path**: `/acceleration_multiplier`
 *
 * **Function**: get_acceleration_multiplier()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: motor::Motor acceleration multiplier.
 */
void
get_acceleration_multiplier(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			    const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto value = json::value(ctrl.acceleration_multiplier());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(value));
	});
}

/** @namespace endpoint
 * ### Set motor acceleration multiplier
 *
 * Set motor::Motor acceleration multiplier.
 *
 * **Method**: `POST`
 *
 * **Path**: `/acceleration_multiplier`
 *
 * **Function**: post_acceleration_multiplier()
 *
 * **Arguments**:
 * - `value`: acceleration multiplier value.
 */
void
post_acceleration_multiplier(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			     const std::string &auth)
{

	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("value");
	auto value = req->getQuery("value");
	CATCH_ERROR({
		ctrl.acceleration_multiplier(boost::lexical_cast<double>(value));
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

} // namespace endpoint
