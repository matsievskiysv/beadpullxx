/** @file measure.hxx
 * @brief HTTP measurement related endpoints.
 */

#pragma once

#include <App.h>
#include <controller/controller.hxx>
#include <string>

namespace endpoint
{

/**
 * @brief Interrupt scattering measure.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_interrupt_measure_scattering(uWS::HttpResponse<true> *res, uWS::HttpRequest *req,
				       controller::Controller &ctrl, const std::string &auth);
/**
 * @brief Measure scattering.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_measure_scattering(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			     const std::string &auth);
/**
 * @brief Query scattering measurement is in progress.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_measuring_scattering(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			      const std::string &auth);
/**
 * @brief Get scattering measurements.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_scattering_measurements(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
				 const std::string &auth);
/**
 * @brief Query field measuring is in progress.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_measuring_field(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			 const std::string &auth);
/**
 * @brief Measure field.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_measure_field(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			const std::string &auth);
/**
 * @brief Interrupt field measure.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_interrupt_measure_field(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
				  const std::string &auth);
/**
 * @brief Get field measurements.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_field_measurements(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			    const std::string &auth);

} // namespace endpoint
