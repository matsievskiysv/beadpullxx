/** @file analyzer.hxx
 * @brief HTTP analyzer::Analyzer related endpoints.
 */

#pragma once

#include <App.h>
#include <controller/controller.hxx>
#include <string>

namespace endpoint
{

/**
 * @brief Get analyzer list.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_analyzer_list(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		       const std::string &auth);
/**
 * @brief Get analyzer::Analyzer bus::Bus list.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_analyzer_bus_list(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			   const std::string &auth);
/**
 * @brief Get analyzer::Analyzer attribute choices.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_analyzer_attribute_choices(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
				    const std::string &auth);
/**
 * @brief Get analyzer::Analyzer attributes.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_analyzer_attributes(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			     const std::string &auth);
/**
 * @brief Get analyzer::Analyzer bus::Bus attributes.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_analyzer_bus_attributes(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
				 const std::string &auth);
/**
 * @brief Get analyzer::Analyzer capabilities.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_analyzer_capabilities(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			       const std::string &auth);
/**
 * @brief Select analyzer::Analyzer.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_analyzer(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		   const std::string &auth);
/**
 * @brief Get selected analyzer::Analyzer.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_analyzer(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		  const std::string &auth);
/**
 * @brief Get selected analyzer::Analyzer bus::Bus.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_analyzer_bus(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		      const std::string &auth);
/**
 * @brief Disconnect analyzer::Analyzer.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void del_analyzer(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		  const std::string &auth);
/**
 * @brief Get analyzer::Analyzer number of points values.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_nop_values(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		    const std::string &auth);
/**
 * @brief Get analyzer::Analyzer selected number of points.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_nop(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
	     const std::string &auth);
/**
 * @brief Select analyzer::Analyzer number of points.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_nop(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
	      const std::string &auth);
/**
 * @brief Get S matrix available parts.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_s_parts(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		 const std::string &auth);
/**
 * @brief Get S matrix selected part.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_s_part(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		const std::string &auth);
/**
 * @brief Select S matrix part.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_s_part(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		 const std::string &auth);
/**
 * @brief Get left frequency.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_f_left(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		const std::string &auth);
/**
 * @brief Set left frequency.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_f_left(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		 const std::string &auth);
/**
 * @brief Get right frequency.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_f_right(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		 const std::string &auth);
/**
 * @brief Set right frequency.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_f_right(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		  const std::string &auth);
/**
 * @brief Get center frequency.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_f_center(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		  const std::string &auth);
/**
 * @brief Set center frequency.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_f_center(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		   const std::string &auth);
/**
 * @brief Get frequency span.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_f_span(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		const std::string &auth);
/**
 * @brief Set frequency span.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_f_span(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		 const std::string &auth);
/**
 * @brief Get frequencies.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_f_all(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
	       const std::string &auth);
/**
 * @brief Set frequencies.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_f_all(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		const std::string &auth);

} // namespace endpoint
