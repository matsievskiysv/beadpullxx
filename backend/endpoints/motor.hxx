/** @file motor.hxx
 * @brief HTTP motor::Motor related endpoints.
 */

#pragma once

#include <App.h>
#include <controller/controller.hxx>
#include <string>

/** @namespace endpoint
 * ## Motor endpoints
 */
namespace endpoint
{

/**
 * @brief Get motor::Motor list.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_motor_list(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		    const std::string &auth);
/**
 * @brief Get motor::Motor bus::Bus list.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_motor_bus_list(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			const std::string &auth);
/**
 * @brief Get motor::Motor attribute choices.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_motor_attribute_choices(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
				 const std::string &auth);
/**
 * @brief Get motor::Motor attributes.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_motor_attributes(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			  const std::string &auth);
/**
 * @brief Get motor::Motor bus::Bus attributes.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_motor_bus_attributes(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			      const std::string &auth);
/**
 * @brief Get motor::Motor capabilities.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_motor_capabilities(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			    const std::string &auth);
/**
 * @brief Select motor::Motor.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_motor(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		const std::string &auth);
/**
 * @brief Get motor::Motor name.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_motor(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
	       const std::string &auth);
/**
 * @brief Get motor::Motor bus::Bus.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_motor_bus(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		   const std::string &auth);
/**
 * @brief Disconnect motor::Motor.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void del_motor(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
	       const std::string &auth);
/**
 * @brief Get speed.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_speed(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
	       const std::string &auth);
/**
 * @brief Set speed.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_speed(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		const std::string &auth);
/**
 * @brief Get return speed.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_return_speed(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		      const std::string &auth);
/**
 * @brief Set return speed.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_return_speed(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		       const std::string &auth);
/**
 * @brief Get acceleration.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_acceleration(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		      const std::string &auth);
/**
 * @brief Set acceleration.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_acceleration(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		       const std::string &auth);
/**
 * @brief Get deceleration.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_deceleration(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		      const std::string &auth);
/**
 * @brief Set deceleration.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_deceleration(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		       const std::string &auth);
/**
 * @brief Get steppers disable status.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_steppers(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		  const std::string &auth);
/**
 * @brief Set steppers disable status.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_steppers(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		   const std::string &auth);
/**
 * @brief Get position.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_position(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		  const std::string &auth);
/**
 * @brief Move to position.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_move_to(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		  const std::string &auth);
/**
 * @brief Move by step.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_move_by(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		  const std::string &auth);
/**
 * @brief Set origin.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_set_origin(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		     const std::string &auth);
/**
 * @brief Stop motor::Motor.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_stop(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
	       const std::string &auth);
/**
 * @brief Get reverse direction.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_reverse(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		 const std::string &auth);
/**
 * @brief Set reverse direction.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_reverse(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		  const std::string &auth);
/**
 * @brief Get position multiplier.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_position_multiplier(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			     const std::string &auth);
/**
 * @brief Set position multiplier.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_position_multiplier(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			      const std::string &auth);
/**
 * @brief Get speed multiplier.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_speed_multiplier(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			  const std::string &auth);
/**
 * @brief Set speed multiplier.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_speed_multiplier(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			   const std::string &auth);
/**
 * @brief Get acceleration multiplier.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void get_acceleration_multiplier(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
				 const std::string &auth);
/**
 * @brief Set acceleration multiplier.
 *
 * @param res Server response.
 * @param req Server request.
 * @param ctrl Controller.
 * @param auth Simple authentication.
 */
void post_acceleration_multiplier(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
				  const std::string &auth);

} // namespace endpoint
