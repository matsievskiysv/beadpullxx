/** @file analyzer.cxx
 * @brief HTTP analyzer::Analyzer related endpoints.
 */

#include "analyzer.hxx"

#include <boost/json.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/log/trivial.hpp>
#include <server.hxx>
#include <types.hxx>

namespace json = boost::json;

/** @namespace endpoint
 * ## Analyzer endpoints
 */
namespace endpoint
{

/** @namespace endpoint
 * ### Analyzer list
 *
 * Get analyzer::Analyzer list.
 *
 * **Method**: `GET`
 *
 * **Path**: `/analyzer_list`
 *
 * **Function**: get_analyzer_list()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: analyzer::Analyzer name list.
 */
void
get_analyzer_list(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		  const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	auto analyzer_list = json::value_from(ctrl.analyzer_list());
	res->writeStatus(uWS::HTTP_200_OK);
	res->writeHeader("Content-Type", "application/json");
	res->end(json::serialize(analyzer_list));
}

/** @namespace endpoint
 * ### Analyzer bus list
 *
 * Get analyzer::Analyzer bus::Bus list.
 *
 * **Method**: `GET`
 *
 * **Path**: `/analyzer_bus_list`
 *
 * **Function**: get_analyzer_bus_list()
 *
 * **Arguments**:
 * - `analyzer`: analyzer::Analyzer name.
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: List of the bus::Bus, supported by the analyzer::Analyzer.
 */
void
get_analyzer_bus_list(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		      const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("analyzer");
	auto analyzer_name = req->getQuery("analyzer");
	CATCH_ERROR({
		auto bus_list = json::value_from(ctrl.analyzer_bus_list(json::string(analyzer_name)));
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(bus_list));
	});
}

/** @namespace endpoint
 * ### Analyzer attribute choices
 *
 * Get analyzer::Analyzer bus::Bus list.
 *
 * **Method**: `GET`
 *
 * **Path**: `/analyzer_attribute_choices`
 *
 * **Function**: get_analyzer_attribute_choices()
 *
 * **Arguments**:
 * - `analyzer`: analyzer::Analyzer name.
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: analyzer::Analyzer attribute choices dictionary.
 */
void
get_analyzer_attribute_choices(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			       const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("analyzer");
	auto analyzer_name = req->getQuery("analyzer");
	CATCH_ERROR({
		auto attrs = ctrl.analyzer_attribute_choices(json::string(analyzer_name));
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(attrs));
	});
}

/** @namespace endpoint
 * ### Analyzer attributes
 *
 * Get analyzer::Analyzer selected attributes.
 *
 * **Method**: `GET`
 *
 * **Path**: `/analyzer_attributes`
 *
 * **Function**: get_analyzer_attributes()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: analyzer::Analyzer attributes dictionary.
 */
void
get_analyzer_attributes(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto attrs = ctrl.analyzer_attributes();
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(attrs));
	});
}

/** @namespace endpoint
 * ### Analyzer bus attributes
 *
 * Get analyzer::Analyzer bus::Bus selected attributes.
 *
 * **Method**: `GET`
 *
 * **Path**: `/analyzer_bus_attributes`
 *
 * **Function**: get_analyzer_bus_attributes()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: analyzer::Analyzer bus::Bus attributes dictionary.
 */
void
get_analyzer_bus_attributes(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			    const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto attrs = ctrl.analyzer_bus_attributes();
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(attrs));
	});
}

/** @namespace endpoint
 * ### Analyzer capabilities
 *
 * Get analyzer::Analyzer capabilities.
 *
 * **Method**: `GET`
 *
 * **Path**: `/analyzer_capabilities`
 *
 * **Function**: get_analyzer_capabilities()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: analyzer::Analyzer capabilities list.
 */
void
get_analyzer_capabilities(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
			  const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto supported = json::value(ctrl.analyzer_capabilities());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(supported));
	});
}

/** @namespace endpoint
 * ### Select analyzer
 *
 * Select analyzer::Analyzer.
 *
 * **Method**: `POST`
 *
 * **Path**: `/analyzer`
 *
 * **Function**: post_analyzer()
 *
 * **Arguments**:
 * - `analyzer`: analyzer::Analyzer name.
 * - `bus`: bus::Bus name.
 *
 * **Body mime**: `application/json`
 *
 * **Method body**: analyzer::Analyzer and bus::Bus attributes dictionaries in form
 * `{"analyzer_attributes": ..., "bus_attributes": ...}`.
 * analyzer::Analyzer and bus::Bus have the same structure as get_analyzer_attribute_choices() and
 * get_bus_attribute_choices() dictionaries.
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: analyzer::Analyzer and bus::Bus attributes. They could be changed by the driver constructor code.
 */
void
post_analyzer(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
	      const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("analyzer");
	CHECK_ARG("bus");
	auto analyzer_name = req->getQuery("analyzer");
	auto bus_name	   = req->getQuery("bus");
	CHECK_CONTENT_HEADER("application/json");
	std::unique_ptr<std::string> payload(new std::string(""));
	res->onData([res, payload = std::move(payload), analyzer_name, bus_name, &ctrl](auto chunk, auto isLast) {
		payload->append(chunk);
		if (isLast) {
			CATCH_ERROR({
				auto message	    = json::parse(*payload);
				auto analyzer_attrs = message.as_object()["analyzer_attributes"];
				auto bus_attrs	    = message.as_object()["bus_attributes"];
				ctrl.set_analyzer(json::string(analyzer_name), analyzer_attrs, json::string(bus_name),
						  bus_attrs);
			});
			res->writeStatus(uWS::HTTP_200_OK);
			res->end();
		}
	});
	res->onAborted([]() {});
}

/** @namespace endpoint
 * ### Analyzer name
 *
 * Get connected analyzer::Analyzer name.
 *
 * **Method**: `GET`
 *
 * **Path**: `/analyzer`
 *
 * **Function**: get_analyzer()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: analyzer::Analyzer name.
 */
void
get_analyzer(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto analyzer_name = ctrl.analyzer_name();
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(analyzer_name));
		res->end();
	});
}

/** @namespace endpoint
 * ### Analyzer bus name
 *
 * Get connected analyzer::Analyzer bus::Bus name.
 *
 * **Method**: `GET`
 *
 * **Path**: `/analyzer_bus`
 *
 * **Function**: get_analyzer_bus()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: analyzer::Analyzer bus::Bus name.
 */
void
get_analyzer_bus(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
		 const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto bus_name = ctrl.analyzer_bus_name();
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(bus_name));
		res->end();
	});
}

/** @namespace endpoint
 * ### Disconnect analyzer
 *
 * Disconnect analyzer::Analyzer.
 *
 * **Method**: `DELETE`
 *
 * **Path**: `/analyzer`
 *
 * **Function**: del_analyzer()
 */
void
del_analyzer(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		ctrl.analyzer_disconnect();
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Analyzer supported numbers of points
 *
 * Get analyzer::Analyzer supported numbers of points.
 *
 * **Method**: `GET`
 *
 * **Path**: `/nop_values`
 *
 * **Function**: get_nop_values()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: list of supported numbers of points.
 */
void
get_nop_values(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
	       const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto value = json::value_from(ctrl.number_of_points_values());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(value));
	});
}

/** @namespace endpoint
 * ### Get number of points
 *
 * Get analyzer::Analyzer number of points.
 *
 * **Method**: `GET`
 *
 * **Path**: `/nop`
 *
 * **Function**: get_nop()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: number of points.
 */
void
get_nop(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto value = json::value(ctrl.number_of_points());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(value));
	});
}

/** @namespace endpoint
 * ### Set number of points
 *
 * Set analyzer::Analyzer number of points.
 *
 * **Method**: `POST`
 *
 * **Path**: `/nop`
 *
 * **Function**: post_nop()
 *
 * **Arguments**:
 * - `value`: number of points.
 */
void
post_nop(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("value");
	auto value = req->getQuery("value");
	CATCH_ERROR({
		ctrl.number_of_points(boost::lexical_cast<std::uint32_t>(value));
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Get S parts
 *
 * Get analyzer::Analyzer S parts.
 *
 * **Method**: `GET`
 *
 * **Path**: `/s_parts`
 *
 * **Function**: get_s_parts()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: analyzer::Analyzer S parts.
 */
void
get_s_parts(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto value = json::value_from(ctrl.s_parts());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(value));
	});
}

/** @namespace endpoint
 * ### Get S part
 *
 * Get analyzer::Analyzer S part.
 *
 * **Method**: `GET`
 *
 * **Path**: `/s_part`
 *
 * **Function**: get_s_part()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: analyzer::Analyzer S part.
 */
void
get_s_part(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto value = json::value_from(ctrl.s_part());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(value));
	});
}

/** @namespace endpoint
 * ### Set S part
 *
 * Set analyzer::Analyzer S part.
 *
 * **Method**: `POST`
 *
 * **Path**: `/s_part`
 *
 * **Function**: post_s_part()
 *
 * **Arguments**:
 * - `from`: S from port.
 * - `to`: S to port.
 */
void
post_s_part(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("from");
	CHECK_ARG("to");
	auto from = req->getQuery("from");
	auto to	  = req->getQuery("to");
	CATCH_ERROR({
		ctrl.s_part(boost::lexical_cast<util::s_port_t>(from), boost::lexical_cast<util::s_port_t>(to));
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Get left frequency
 *
 * Get analyzer::Analyzer left frequency.
 *
 * **Method**: `GET`
 *
 * **Path**: `/f_left`
 *
 * **Function**: get_f_left()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: analyzer::Analyzer left frequency.
 */
void
get_f_left(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto value = json::value(ctrl.f_left());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(value));
	});
}

/** @namespace endpoint
 * ### Set left frequency
 *
 * Set analyzer::Analyzer left frequency.
 *
 * **Method**: `POST`
 *
 * **Path**: `/f_left`
 *
 * **Function**: post_f_left()
 *
 * **Arguments**:
 * - `value`: left frequency.
 */
void
post_f_left(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("value");
	auto value = req->getQuery("value");
	CATCH_ERROR({
		ctrl.f_left(boost::lexical_cast<util::f_t>(value));
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Get right frequency
 *
 * Get analyzer::Analyzer right frequency.
 *
 * **Method**: `GET`
 *
 * **Path**: `/f_right`
 *
 * **Function**: get_f_right()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: analyzer::Analyzer right frequency.
 */
void
get_f_right(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto value = json::value(ctrl.f_right());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(value));
	});
}

/** @namespace endpoint
 * ### Set right frequency
 *
 * Set analyzer::Analyzer right frequency.
 *
 * **Method**: `POST`
 *
 * **Path**: `/f_right`
 *
 * **Function**: post_f_right()
 *
 * **Arguments**:
 * - `value`: right frequency.
 */
void
post_f_right(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("value");
	auto value = req->getQuery("value");
	CATCH_ERROR({
		ctrl.f_right(boost::lexical_cast<util::f_t>(value));
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Get center frequency
 *
 * Get analyzer::Analyzer center frequency.
 *
 * **Method**: `GET`
 *
 * **Path**: `/f_center`
 *
 * **Function**: get_f_center()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: analyzer::Analyzer center frequency.
 */
void
get_f_center(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto value = json::value(ctrl.f_center());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(value));
	});
}

/** @namespace endpoint
 * ### Set center frequency
 *
 * Set analyzer::Analyzer center frequency.
 *
 * **Method**: `POST`
 *
 * **Path**: `/f_center`
 *
 * **Function**: post_f_center()
 *
 * **Arguments**:
 * - `value`: center frequency.
 */
void
post_f_center(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl,
	      const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("value");
	auto value = req->getQuery("value");
	CATCH_ERROR({
		ctrl.f_center(boost::lexical_cast<util::f_t>(value));
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Get frequency span
 *
 * Get analyzer::Analyzer frequency span.
 *
 * **Method**: `GET`
 *
 * **Path**: `/f_span`
 *
 * **Function**: get_f_span()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: analyzer::Analyzer frequency span.
 */
void
get_f_span(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto value = json::value(ctrl.f_span());
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(json::serialize(value));
	});
}

/** @namespace endpoint
 * ### Set frequency span
 *
 * Set analyzer::Analyzer frequency span.
 *
 * **Method**: `POST`
 *
 * **Path**: `/f_span`
 *
 * **Function**: post_f_span()
 *
 * **Arguments**:
 * - `value`: frequency span.
 */
void
post_f_span(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("value");
	auto value = req->getQuery("value");
	CATCH_ERROR({
		ctrl.f_span(boost::lexical_cast<util::f_t>(value));
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

/** @namespace endpoint
 * ### Get frequencies
 *
 * Get analyzer::Analyzer frequencies.
 *
 * **Method**: `GET`
 *
 * **Path**: `/f_all`
 *
 * **Function**: get_f_all()
 *
 * **Return mime**: `application/json`
 *
 * **Return value**: analyzer::Analyzer frequencies.
 */
void
get_f_all(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CATCH_ERROR({
		auto value   = ctrl.f_all();
		auto message = json::serialize(json::value({{"left", std::get<0>(value)},
							    {"right", std::get<1>(value)},
							    {"center", std::get<2>(value)},
							    {"span", std::get<3>(value)}}));
		res->writeStatus(uWS::HTTP_200_OK);
		res->writeHeader("Content-Type", "application/json");
		res->end(message);
	});
}

/** @namespace endpoint
 * ### Set frequencies
 *
 * Set analyzer::Analyzer frequencies.
 *
 * **Method**: `POST`
 *
 * **Path**: `/f_all`
 *
 * **Function**: post_f_all()
 *
 * **Arguments**:
 * - `left`: left frequency.
 * - `right`: right frequency.
 * - `center`: center frequency.
 * - `span`: frequency span.
 */
void
post_f_all(uWS::HttpResponse<true> *res, uWS::HttpRequest *req, controller::Controller &ctrl, const std::string &auth)
{
	BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
	CHECK_AUTHORIZED();
	CHECK_ARG("left");
	CHECK_ARG("right");
	CHECK_ARG("center");
	CHECK_ARG("span");
	auto left   = req->getQuery("left");
	auto right  = req->getQuery("right");
	auto center = req->getQuery("center");
	auto span   = req->getQuery("span");
	CATCH_ERROR({
		ctrl.f_all(boost::lexical_cast<util::f_t>(left), boost::lexical_cast<util::f_t>(right),
			   boost::lexical_cast<util::f_t>(center), boost::lexical_cast<util::f_t>(span));
		res->writeStatus(uWS::HTTP_200_OK);
		res->end();
	});
}

} // namespace endpoint
