#pragma once

#include <config.h>
#ifdef ANALYZER_AGILENT506X

/** @file agilent_506x.hxx
 * @brief analyzer::Agilent506X analyzer::Analyzer implementation.
 */

#include "analyzer.hxx"

#include <analyzer/event.hxx>
#include <bus/bus.hxx>
#include <bus/list.hxx>
#include <event_queue.hxx>
#include <tuple>
#include <types.hxx>

namespace analyzer
{

/**
 * @brief Agilent506X Analyzer class.
 */
class Agilent506X : public Analyzer
{
      private:
	bool	    measurement_in_progress = false; ///< Measurement in progress flag.
	util::msg_t data_buffer;		     ///< Data buffer.

	/**
	 * @brief Check measurement is in progress.
	 *
	 * @throws std::runtime_error Measurement is in progress.
	 */
	void check_measurement_progress(void);

	/**
	 * @brief Set status register value.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 * @param value New register value.
	 */
	void status_byte(std::uint8_t value);

	/**
	 * @brief Get status register value.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 * @return Register value.
	 */
	std::uint8_t status_byte(void);

	/**
	 * @brief Set ESR register value.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 * @param value New register value.
	 */
	void esr(std::uint8_t value);

	/**
	 * @brief Get ESR register value.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 * @return Register value.
	 */
	std::uint8_t esr(void);

	/**
	 * @brief Set OSCR register value.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 * @param value New register value.
	 */
	void oscr(std::uint16_t value);

	/**
	 * @brief Get OSCR register value.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 * @return Register value.
	 */
	std::uint16_t oscr(void);

	/**
	 * @brief Set QSER register value.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 * @param value New register value.
	 */
	void qser(std::uint16_t value);

	/**
	 * @brief Get QSER register value.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 * @return Register value.
	 */
	std::uint16_t qser(void);

	/**
	 * @brief Set averaging count.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 * @param value New averaging count value.
	 */
	void averaging(std::uint16_t value);

	/**
	 * @brief Reset averaging.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 */
	void reset_averaging(void);

	/**
	 * @brief Set curve smoothing.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 * @param value Curve smoothing value.
	 */
	void smoothing(double value);

	/**
	 * @brief Set power level.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 * @param value Power level value.
	 */
	void power(double value);

	/**
	 * @brief Enable/disable Analyzer beeping.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 * @param value Enable state.
	 */
	void beep(bool value);

	/**
	 * @brief Lock front panel controlling of the Analyzer.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 * @param enable New lock value.
	 */
	void input_lock(bool enable);

	/**
	 * @brief Set measurement integration bandwidth.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 * @param value New integration bandwidth value.
	 */
	void integration_bandwidth(std::uint16_t value);

	/**
	 * @brief Configure Analyzer channel for measurement.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 */
	void channel(void);

	/**
	 * @brief Configure Analyzer number format for measurement.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 */
	void number_form(void);

	/**
	 * @brief Configure Analyzer data transfer format for measurement.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 */
	void data_format(void);

	/**
	 * @brief Get number of points.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 * @return Number of points.
	 */
	util::nop_t _number_of_points(void);

	/**
	 * @brief Get left frequency.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 * @return Left frequency.
	 */
	util::f_t _f_left(void);

	/**
	 * @brief Get frequency span.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 * @return Frequency span.
	 */
	util::f_t _f_span(void);

	/**
	 * @brief Get S matrix part.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 * @return S matrix part.
	 */
	util::s_part_t _s_part(void);

	/**
	 * @brief Check ESR register interrupts.
	 *
	 * Check ESR register interrupts and push appropriate signals to the queue.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 */
	void check_esr(util::EventQueue<analyzer_event> &event_queue) noexcept;

	/**
	 * @brief Flush Analyzer error list.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 */
	void flush_errors(void) noexcept;

      public:
	static constexpr const char   *NAME = "Agilent506X"; ///< Analyzer name.
	static const util::name_list_t SUPPORTED_BUSSES;     ///< List of supported bus::Bus drivers.
	/**
	 * @brief Bus attribute choices.
	 *
	 * @return Bus attribute choices.
	 */
	static const util::attribute_t &attribute_choices(void) noexcept;
	/**
	 * @brief Patch bus::Bus attributes.
	 *
	 * Prior to passing attributes to bus::Bus constructor, attributes are passed to @ref patch_bus_attributes()
	 * function. This allows passing driver specific configuration to bus. For example, this call may set interrupt
	 * bitmask for the bus or select a specific baud rate value.
	 *
	 * @param[in,out] attributes bus::Bus attributes.
	 */
	static void patch_bus_attributes(util::attribute_t &attributes) noexcept;

	/**
	 * @brief Analyzer constructor.
	 *
	 * @param[in] attributes Analyzer attributes.
	 * @param[in] bus bus::Bus reference.
	 */
	Agilent506X(util::attribute_t &&attributes, std::shared_ptr<bus::Bus> bus);

	/**
	 * @brief Do not allow Analyzer copying.
	 */
	Agilent506X(Agilent506X &) = delete;

	/**
	 * @brief Do not allow Analyzer moving.
	 */
	Agilent506X(Agilent506X &&) = delete;

	/**
	 * @brief Analyzer destructor.
	 */
	~Agilent506X();

	const util::string_t &name(void) const noexcept;
	bool		      interrupted(void) noexcept;
	void		      check_interrupt(util::EventQueue<analyzer_event> &) noexcept;
	util::nop_vals_t      number_of_points_values(void) const noexcept;
	util::nop_t	      number_of_points(void);
	void		      number_of_points(util::nop_t);
	util::s_parts_t	      s_parts(void) const noexcept;
	void		      s_part(util::s_port_t, util::s_port_t);
	util::s_part_t	      s_part(void);
	util::f_t	      f_left(void);
	util::f_lim_t	      f_left(util::f_t);
	util::f_t	      f_right(void);
	util::f_lim_t	      f_right(util::f_t);
	util::f_t	      f_center(void);
	util::f_lim_t	      f_center(util::f_t);
	util::f_t	      f_span(void);
	util::f_lim_t	      f_span(util::f_t);
	util::f_lim_t	      f_all(util::f_t, util::f_t, util::f_t, util::f_t);
	util::f_lim_t	      f_all(void);
	void		      measure(void);
	bool		      measuring(void);
	void		      measurements(result::Scattering &result);
	void		      interrupt_measure(util::EventQueue<analyzer_event> &);
};

} // namespace analyzer

#endif // ANALYZER_AGILENT506X
