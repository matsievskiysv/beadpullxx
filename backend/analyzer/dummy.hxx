#pragma once

#include <config.h>
#ifdef ANALYZER_DUMMY

/** @file dummy.hxx
 * @brief analyzer::Dummy implementation.
 */

#include "analyzer.hxx"

#include <random>

namespace analyzer
{

/**
 * @brief Dummy Analyzer class.
 */
class Dummy : public Analyzer
{
      private:
	std::mt19937			 gen;			      ///< Random number generator algorithm.
	std::uniform_real_distribution<> dis;			      ///< Uniform distribution function.
	util::f_t			 f_left_value  = 0;	      ///< Left frequency.
	util::f_t			 f_right_value = 0;	      ///< Right frequency.
	util::nop_t			 nop_value     = 3;	      ///< Number of points.
	util::s_part_t			 s_part_value  = {1, 1};      ///< S matrix part.
	const static util::meas_array_t	 meas_re_points;	      ///< Analyzer measurement real part data.
	const static util::meas_array_t	 meas_im_points;	      ///< Analyzer measurement imaginary part data.
	bool				 interd		     = false; ///< Interrupted flag.
	bool				 measure_in_progress = false; ///< Measurement in progress flag.

	bool fail_on_constructor    = false; ///< Fail on Dummy::Dummy(). Set via attributes.
	bool fail_on_interrupt	    = false; ///< Fail on interrupted(). Set via attributes.
	bool fail_on_nop	    = false; ///< Fail on number_of_points(). Set via attributes.
	bool fail_on_s_part	    = false; ///< Fail on s_part(). Set via attributes.
	bool fail_on_f_left	    = false; ///< Fail on f_left(). Set via attributes.
	bool fail_on_f_right	    = false; ///< Fail on f_right(). Set via attributes.
	bool fail_on_f_center	    = false; ///< Fail on f_center(). Set via attributes.
	bool fail_on_f_span	    = false; ///< Fail on f_span(). Set via attributes.
	bool fail_on_measure	    = false; ///< Fail on measure(). Set via attributes.

	/**
	 * @brief Exchange dummy data with bus.
	 *
	 * Purpose of this function is to trigger bus::Dummy exceptions in motor::Dummy calls.
	 */
	void bus_send_rcv(void);

      public:
	/**
	 * @brief Dummy Analyzer constructor.
	 *
	 * @param[in] attributes Analyzer attributes.
	 * @param[in] bus bus::Bus reference.
	 */
	Dummy(util::attribute_t &&attributes, std::shared_ptr<bus::Bus> bus);

	/**
	 * @brief Do not allow Dummy Analyzer copying.
	 */
	Dummy(Dummy &) = delete;

	/**
	 * @brief Do not allow Dummy Analyzer moving.
	 */
	Dummy(Dummy &&) = delete;

	/**
	 * @brief Dummy Analyzer destructor.
	 */
	~Dummy();

	static constexpr const char   *NAME = "dummy";	 ///< Analyzer name.
	static const util::name_list_t SUPPORTED_BUSSES; ///< List of supported bus::Bus drivers.
	/**
	 * @brief Bus attribute choices.
	 *
	 * @return Bus attribute choices.
	 */
	static const util::attribute_t &attribute_choices(void) noexcept;

	const util::string_t &name(void) const noexcept;
	bool		      interrupted(void) noexcept;
	void		      check_interrupt(util::EventQueue<analyzer_event> &event) noexcept;
	util::nop_vals_t      number_of_points_values(void) const noexcept;
	util::nop_t	      number_of_points(void);
	void		      number_of_points(util::nop_t nop);
	util::s_parts_t	      s_parts(void) const noexcept;
	void		      s_part(util::s_port_t from, util::s_port_t to);
	util::s_part_t	      s_part(void);
	util::f_t	      f_left(void);
	util::f_lim_t	      f_left(util::f_t);
	util::f_t	      f_right(void);
	util::f_lim_t	      f_right(util::f_t);
	util::f_t	      f_center(void);
	util::f_lim_t	      f_center(util::f_t);
	util::f_t	      f_span(void);
	util::f_lim_t	      f_span(util::f_t);
	util::f_lim_t	      f_all(util::f_t, util::f_t, util::f_t, util::f_t);
	util::f_lim_t	      f_all(void);
	void		      measure(void);
	bool		      measuring(void);
	void		      measurements(result::Scattering &result);
};

} // namespace analyzer

#endif // ANALYZER_DUMMY
