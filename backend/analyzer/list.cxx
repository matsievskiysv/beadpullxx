/** @file list.cxx
 * @brief analyzer::Analyzer list.
 */

#include "list.hxx"

namespace analyzer
{

const util::name_list_t analyzer_list{
#ifdef ANALYZER_DUMMY
    util::string_t(Dummy::NAME),
#endif
#ifdef ANALYZER_LITEVNA
    util::string_t(LiteVNA::NAME),
#endif
#ifdef ANALYZER_AGILENT87XX
    util::string_t(Agilent87XX::NAME),
#endif
#ifdef ANALYZER_AGILENT506X
    util::string_t(Agilent506X::NAME),
#endif
};

const util::name_list_t &
get_supported_busses([[maybe_unused]] const util::string_t &analyzer)
{
#ifdef ANALYZER_DUMMY
	if (analyzer == Dummy::NAME)
		return Dummy::SUPPORTED_BUSSES;
#endif
// GCOVR_EXCL_START do not test hardware driver
#ifdef ANALYZER_LITEVNA
	if (analyzer == LiteVNA::NAME)
		return LiteVNA::SUPPORTED_BUSSES;
#endif
#ifdef ANALYZER_AGILENT87XX
	if (analyzer == Agilent87XX::NAME)
		return Agilent87XX::SUPPORTED_BUSSES;
#endif
#ifdef ANALYZER_AGILENT506X
	if (analyzer == Agilent506X::NAME)
		return Agilent506X::SUPPORTED_BUSSES;
#endif
// GCOVR_EXCL_STOP
	throw std::runtime_error("unknown analyzer");
}

const util::attribute_t &
get_attribute_choices([[maybe_unused]] const util::string_t &analyzer)
{
#ifdef ANALYZER_DUMMY
	if (analyzer == Dummy::NAME)
		return Dummy::attribute_choices();
#endif
// GCOVR_EXCL_START do not test hardware driver
#ifdef ANALYZER_LITEVNA
	if (analyzer == LiteVNA::NAME)
		return LiteVNA::attribute_choices();
#endif
#ifdef ANALYZER_AGILENT87XX
	if (analyzer == Agilent87XX::NAME)
		return Agilent87XX::attribute_choices();
#endif
#ifdef ANALYZER_AGILENT506X
	if (analyzer == Agilent506X::NAME)
		return Agilent506X::attribute_choices();
#endif
// GCOVR_EXCL_STOP
	throw std::runtime_error("unknown analyzer");
}

std::shared_ptr<Analyzer>
create_analyzer([[maybe_unused]] const util::string_t &analyzer, [[maybe_unused]] util::attribute_t &&attributes,
		[[maybe_unused]] std::shared_ptr<bus::Bus> bus)
{
#ifdef ANALYZER_DUMMY
	if (analyzer == Dummy::NAME)
		return std::shared_ptr<Analyzer>(new Dummy(std::move(attributes), bus));
#endif
// GCOVR_EXCL_START do not test hardware driver
#ifdef ANALYZER_LITEVNA
	if (analyzer == LiteVNA::NAME)
		return std::shared_ptr<Analyzer>(new LiteVNA(std::move(attributes), bus));
#endif
#ifdef ANALYZER_AGILENT87XX
	if (analyzer == Agilent87XX::NAME)
		return std::shared_ptr<Analyzer>(new Agilent87XX(std::move(attributes), bus));
#endif
#ifdef ANALYZER_AGILENT506X
	if (analyzer == Agilent506X::NAME)
		return std::shared_ptr<Analyzer>(new Agilent506X(std::move(attributes), bus));
#endif
// GCOVR_EXCL_STOP
	throw std::runtime_error("unknown analyzer");
}

void
patch_bus_attributes([[maybe_unused]] const util::string_t &analyzer, [[maybe_unused]] util::attribute_t &attributes)
{
#ifdef ANALYZER_DUMMY
	if (analyzer == Dummy::NAME) {
		Dummy::patch_bus_attributes(attributes);
		return;
	}
#endif
// GCOVR_EXCL_START do not test hardware driver
#ifdef ANALYZER_LITEVNA
	if (analyzer == LiteVNA::NAME) {
		LiteVNA::patch_bus_attributes(attributes);
		return;
	}
#endif
#ifdef ANALYZER_AGILENT87XX
	if (analyzer == Agilent87XX::NAME) {
		Agilent87XX::patch_bus_attributes(attributes);
		return;
	}
#endif
#ifdef ANALYZER_AGILENT506X
	if (analyzer == Agilent506X::NAME) {
		Agilent506X::patch_bus_attributes(attributes);
		return;
	}
#endif
// GCOVR_EXCL_STOP
	throw std::runtime_error("unknown analyzer");
}

} // namespace analyzer
