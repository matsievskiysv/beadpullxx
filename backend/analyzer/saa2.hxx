#pragma once

#include <config.h>
#ifdef ANALYZER_SAA2

/** @file saa2.hxx
 * @brief analyzer::SAA2 communication protocol functions.
 */

#include <types.hxx>

namespace analyzer
{

/**
 * @brief SAA2 command list.
 */
enum SAA2Cmd : std::uint8_t {
	NO_OP	 = 0x00, ///< No operation
	IDCT	 = 0x0d, ///< Device indication
	IDCT_RPL = 0x32, ///< Constant device indication reply
	RD1	 = 0x10, ///< Read 1 byte
	RD2	 = 0x11, ///< Read 2 byte
	RD4	 = 0x12, ///< Read 4 byte
	RD8	 = 0x13, ///< Read 8 byte
	RD_FIFO	 = 0x18, ///< Read FIFO
	WR1	 = 0x20, ///< Write 1 byte
	WR2	 = 0x21, ///< Write 2 byte
	WR4	 = 0x22, ///< Write 4 byte
	WR8	 = 0x23, ///< Write 8 byte
	WR_FIFO	 = 0x28, ///< Write FIFO
};

/**
 * @brief SAA2 communication protocol functions.
 */
class SAA2
{
      private:
	std::uint8_t fifo_chunk_size; ///< Size of chunk in FIFO.

      protected:
	std::uint16_t saa2_rcv_bytes_remains = 0; ///< Size of data remains to be read.

	/**
	 * @brief Send data to SAA2 device.
	 *
	 * @param address Read address.
	 */
	virtual void saa2_send(const util::msg_t &address) = 0;

	/**
	 * @brief Receive data from SAA2 device.
	 *
	 * @param message Message container to receive data into.
	 * @param count Byte count to read to \p message container.
	 */
	virtual void saa2_rcv(util::msg_t &message, std::uint8_t count) = 0;

	/**
	 * @brief Check device.
	 *
	 * @throws std::runtime_error Wrong SAA2Cmd::IDCT reply.
	 */
	void saa2_indicate(void);

	/**
	 * @brief Receive 1 byte from SAA2 device.
	 *
	 * @param address Read address.
	 * @return Data byte.
	 * @throws std::runtime_error Unexpected message size.
	 */
	std::uint8_t saa2_read(std::uint8_t address);

	/**
	 * @brief Receive 2 bytes from SAA2 device.
	 *
	 * @param address Read address.
	 * @return Data bytes.
	 * @throws std::runtime_error Unexpected message size.
	 */
	std::uint16_t saa2_read2(std::uint8_t address);

	/**
	 * @brief Receive 4 bytes from SAA2 device.
	 *
	 * @param address Read address.
	 * @return Data bytes.
	 * @throws std::runtime_error Unexpected message size.
	 */
	std::uint32_t saa2_read4(std::uint8_t address);

	/**
	 * @brief Receive 8 bytes from SAA2 device.
	 *
	 * @param address Read address.
	 * @return Data bytes.
	 * @throws std::runtime_error Unexpected message size.
	 */
	std::uint64_t saa2_read8(std::uint8_t address);

	/**
	 * @brief Receive data from SAA2 device FIFO.
	 *
	 * @param address Read address.
	 * @param count Read count.
	 * @throws std::runtime_error Unexpected message size.
	 */
	void saa2_read(std::uint8_t address, std::uint8_t count);

	/**
	 * @brief Send byte to SAA2 device.
	 *
	 * @param address Write address.
	 * @param data Data bytes.
	 */
	void saa2_write(std::uint8_t address, std::uint8_t data);

	/**
	 * @brief Send 2 bytes to SAA2 device.
	 *
	 * @param address Write address.
	 * @param data Data bytes.
	 */
	void saa2_write(std::uint8_t address, std::uint16_t data);

	/**
	 * @brief Send 4 bytes to SAA2 device.
	 *
	 * @param address Write address.
	 * @param data Data bytes.
	 */
	void saa2_write(std::uint8_t address, std::uint32_t data);

	/**
	 * @brief Send 8 bytes to SAA2 device.
	 *
	 * @param address Write address.
	 * @param data Data bytes.
	 */
	void saa2_write(std::uint8_t address, std::uint64_t data);

	/**
	 * @brief Send data to SAA2 device FIFO.
	 *
	 * @param address Write address.
	 * @param data Data bytes.
	 * @param count Data count.
	 */
	void saa2_write(std::uint8_t address, const util::msg_t &data, std::uint8_t count);

      public:
	/**
	 * @brief SAA2 communication controller.
	 *
	 * @param fifo_chunk_size Size of chunk for FIFO communication.
	 */
	SAA2(std::uint8_t fifo_chunk_size);
};

} // namespace analyzer

#endif // ANALYZER_SAA2
