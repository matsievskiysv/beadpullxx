#include <config.h>
#ifdef ANALYZER_LITEVNA

/** @file litevna.cxx
 * @brief analyzer::LiteVNA analyzer::Analyzer implementation.
 */

// GCOVR_EXCL_START do not test hardware driver

#include "litevna.hxx"

#include <boost/endian/buffers.hpp>
#include <boost/log/trivial.hpp>
#include <complex>
#include <span>

namespace analyzer
{

namespace endian = boost::endian;

/**
 * @brief LiteVNA register map.
 */
enum LiteVNAReg {
	SWEEP_START_HZ	       = 0x00, ///< Start frequency in Hz
	SWEEP_STEP_HZ	       = 0x10, ///< Frequency step in Hz
	SWEEP_POINTS	       = 0x20, ///< Number of frequency points
	VALUES_PER_FREQUENCY   = 0x22, ///< Number of points per frequency point
	DATA_MODE	       = 0x26, ///< Data transfer mode
	DATA_MODE_USB	       = 0,    ///< SAA2 control mode
	DATA_MODE_RAW	       = 1,    ///< Raw mode
	DATA_MODE_NORMAL       = 2,    ///< Interactive mode
	DATA_MODE_USB_CAL      = 3,    ///< SAA2 control mode with calibrated data
	VALUES_FIFO	       = 0x30, ///< SAA2 FIFO address
	AVERAGE		       = 0x40, ///< Data averaging
	POWER_LF	       = 0x41, ///< LF power level
	POWER_HF	       = 0x42, ///< HF power level
	POWER_HF_M4_DBM	       = 0,    ///< HF power level value
	POWER_HF_M1_DBM	       = 1,    ///< HF power level value
	POWER_HF_P2_DBM	       = 2,    ///< HF power level value
	POWER_HF_P5_DBM	       = 3,    ///< HF power level value
	TRACEMODE	       = 0x44, ///< Trace mode
	TRACEMODE_ALL	       = 0,    ///< S11 and S21 traces
	TRACEMODE_S11	       = 1,    ///< S11 trace
	TRACEMODE_S21	       = 2,    ///< S21 trace
	SERIAL_NUMBER	       = 0xd0, ///< Device serial number
	DEVICE_VARIANT	       = 0xf0, ///< Device variant
	DEVICE_VARIANT_LITEVNA = 0x02, ///< Device variant for LiteVNA
	PROTOCOL_VERSION       = 0xf1, ///< Protocol version
	PROTOCOL_VERSION_V1    = 1,    ///< Protocol version 1
	HARDWARE_REVISION      = 0xf2, ///< Hardware revision
	FIRMWARE_VERSION_MAJOR = 0xf3, ///< Firmware revision major
	FIRMWARE_VERSION_MINOR = 0xf4, ///< Firmware revision minor
	UNIX_TIME	       = 0x58, ///< Clock time
};

/**
 * @brief LiteVNA SAA2 data block.
 */
struct LiteVNAData {
	endian::little_int32_buf_t fwd0Re;    ///< Forward power, real part.
	endian::little_int32_buf_t fwd0Im;    ///< Forward power, imaginary part.
	endian::little_int32_buf_t rev0Re;    ///< Reflected power, real part.
	endian::little_int32_buf_t rev0Im;    ///< Reflected power, imaginary part.
	endian::little_int32_buf_t rev1Re;    ///< Transmitted power, real part.
	endian::little_int32_buf_t rev1Im;    ///< Transmitted power, imaginary part.
	endian::little_int16_buf_t freqIndex; ///< Frequency index.
	endian::little_int16_buf_t padding1;  ///< Data padding.
	endian::little_int32_buf_t padding2;  ///< Data padding.
};
static_assert(sizeof(LiteVNAData) == 32);

const util::name_list_t LiteVNA::SUPPORTED_BUSSES = {
#ifdef BUS_SERIAL
    util::string_t(bus::Serial::NAME),
#endif
};

static const util::attribute_t _attribute_choices = {
    {"template", "litevna"},
    {"power_low", {{"min", 1}, {"max", 3}, {"step", 1}, {"value", 1}}},
    {"power_high", {{"min", 1}, {"max", 3}, {"step", 1}, {"value", 3}}},
    {"averaging", {{"min", 1}, {"max", 80}, {"step", 1}, {"value", 1}}},
}; ///< Analyzer attribute choices.

const util::attribute_t &
LiteVNA::attribute_choices(void) noexcept
{
	return _attribute_choices;
}

void
LiteVNA::saa2_send(const util::msg_t &msg)
{
	bus->flush_input();
	bus->send(msg);
}

void
LiteVNA::saa2_rcv(util::msg_t &msg, std::uint8_t count)
{
	bus->rcv(msg, count);
}

void
LiteVNA::check_measurement_progress(void)
{
	if (measuring())
		throw std::runtime_error("measurement is in progress");
}

std::uint8_t
LiteVNA::cmd_device_variant(void)
{
	auto rv = saa2_read(LiteVNAReg::DEVICE_VARIANT);
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] device variant: " << rv;
	return rv;
}

std::uint8_t
LiteVNA::cmd_protocol_version(void)
{
	auto rv = saa2_read(LiteVNAReg::PROTOCOL_VERSION);
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] protocol version: " << rv;
	return rv;
}

std::uint8_t
LiteVNA::cmd_hardware_revision(void)
{
	auto rv = saa2_read(LiteVNAReg::HARDWARE_REVISION);
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] hardware revision: " << rv;
	return rv;
}

std::tuple<std::uint8_t, std::uint8_t>
LiteVNA::cmd_firmware_version(void)
{
	auto maj = saa2_read2(LiteVNAReg::FIRMWARE_VERSION_MAJOR);
	auto min = saa2_read2(LiteVNAReg::FIRMWARE_VERSION_MINOR);
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] firmware version: " << maj << "." << min;
	return std::make_tuple(maj, min);
}

std::uint8_t
LiteVNA::cmd_lo_power(void)
{
	auto rv = saa2_read(LiteVNAReg::POWER_LF);
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] low frequency power: " << rv;
	return rv;
}

void
LiteVNA::cmd_lo_power(std::uint8_t power)
{
	if ((power < 0x01) || (power > 0x03))
		throw std::runtime_error("low frequency power must be in range [1, 3]");
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] set low frequency power: " << power;
	saa2_write(LiteVNAReg::POWER_LF, power);
}

std::uint8_t
LiteVNA::cmd_hi_power(void)
{
	auto rv = saa2_read(LiteVNAReg::POWER_HF);
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] high frequency power: " << rv;
	return rv;
}

void
LiteVNA::cmd_hi_power(std::uint8_t power)
{
	if ((power < 0x01) || (power > 0x03))
		throw std::runtime_error("high frequency power must be in range [1, 3]");
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] set high frequency power: " << power;
	saa2_write(LiteVNAReg::POWER_LF, power);
}

std::uint8_t
LiteVNA::cmd_average(void)
{
	auto rv = saa2_read(LiteVNAReg::AVERAGE);
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] average value: " << rv;
	return rv;
}

void
LiteVNA::cmd_average(std::uint8_t value)
{
	if (value > 80)
		throw std::runtime_error("average value must be in range [1, 80]");
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] set high frequency power: " << value;
	saa2_write(LiteVNAReg::AVERAGE, value);
}

void
LiteVNA::f_range(util::f_t left, util::f_t right)
{
	auto nop = number_of_points();
	if ((left < 50000) || (left > 6300000000))
		throw std::runtime_error("left frequency must be in range [50kHz, 6.3GHz]");
	if ((right < 50000) || (right > 6300000000))
		throw std::runtime_error("right frequency must be in range [50kHz, 6.3GHz]");
	if (left >= right)
		throw std::runtime_error("right frequency must be greater then left frequency");
	auto lock = bus->get_lock();
	saa2_indicate();
	if (nop > 1) {
		auto step = ((right - left) / (nop - 1));
		if (step == 0)
			throw std::runtime_error("frequency range is too narrow for selected number of points");
		BOOST_LOG_TRIVIAL(trace) << "[LiteVNA] set frequency step: " << step;
		saa2_write(LiteVNAReg::SWEEP_STEP_HZ, step);
	}
	BOOST_LOG_TRIVIAL(trace) << "[LiteVNA] set left frequency: " << left;
	saa2_write(LiteVNAReg::SWEEP_START_HZ, left);
}

LiteVNA::LiteVNA(util::attribute_t &&attributes, std::shared_ptr<bus::Bus> bus)
    : Analyzer(std::move(attributes), bus), SAA2(sizeof(LiteVNAData))
{
	BOOST_LOG_TRIVIAL(trace) << "[LiteVNA] attributes " << attributes;
	data_buffer.reserve(ANALYZER_MAX_MEASUREMENT_KIB * 1024);
	auto lock = this->bus->get_lock();
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] handshake";
	saa2_indicate();
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] query device info";
	if (cmd_protocol_version() != LiteVNAReg::PROTOCOL_VERSION_V1) {
		BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] wrong protocol version";
		throw std::runtime_error("wrong protocol version");
	}
	if (cmd_device_variant() != LiteVNAReg::DEVICE_VARIANT_LITEVNA) {
		BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] wrong device code";
		throw std::runtime_error("wrong device code");
	}
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] set initial values";
	saa2_write(LiteVNAReg::SWEEP_START_HZ, std::uint64_t(1000000000));
	saa2_write(LiteVNAReg::SWEEP_STEP_HZ, std::uint64_t(10000000));
	saa2_write(LiteVNAReg::SWEEP_POINTS, std::uint16_t(11));
	saa2_write(LiteVNAReg::TRACEMODE, std::uint8_t(LiteVNAReg::TRACEMODE_S11));
	saa2_write(LiteVNAReg::DATA_MODE, std::uint8_t(LiteVNAReg::DATA_MODE_USB_CAL));
	saa2_write(LiteVNAReg::VALUES_FIFO, std::uint8_t(0));
	auto power_lo = attributes.as_object()["power_low"].as_int64();
	cmd_lo_power(power_lo);
	auto power_hi = attributes.as_object()["power_high"].as_int64();
	cmd_hi_power(power_hi);
	auto averaging = attributes.as_object()["averaging"].as_int64();
	cmd_average(averaging);
	this->attributes.as_object()["device_variant"]	  = cmd_device_variant();
	this->attributes.as_object()["protocol_version"]  = cmd_protocol_version();
	this->attributes.as_object()["hardware_revision"] = cmd_hardware_revision();
	auto [ver_maj, ver_min]				  = cmd_firmware_version();
	this->attributes.as_object()["firmware_version"]  = {ver_maj, ver_min};
	saa2_indicate();
	saa2_read(LiteVNAReg::VALUES_FIFO, 1);
	util::msg_t message;
	this->bus->rcv(message, saa2_rcv_bytes_remains);
	if (message.size() != saa2_rcv_bytes_remains) {
		BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] unexpected FIFO read size";
		throw std::runtime_error("unexpected FIFO read size");
	}
	saa2_rcv_bytes_remains = 0;
	saa2_indicate();
}

LiteVNA::~LiteVNA()
{
	auto lock = bus->get_lock();
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] leave USB mode";
	try {
		saa2_write(LiteVNAReg::DATA_MODE, std::uint8_t(LiteVNAReg::DATA_MODE_NORMAL));
	} catch (const std::exception &err) {
		BOOST_LOG_TRIVIAL(warning) << "[LiteVNA] destructor error: " << err.what();
	}
}

static const util::string_t _name = util::string_t(LiteVNA::NAME); ///< Analyzer name.

const util::string_t &
LiteVNA::name(void) const noexcept
{
	return _name;
}

bool
LiteVNA::interrupted(void) noexcept
{
	return is_interrupted || bus->interrupted();
}

void
LiteVNA::check_interrupt(util::EventQueue<analyzer_event> &event_queue) noexcept
{
	auto lock = bus->get_lock();
	if (is_interrupted) {
		total_rcv_bytes_remains = saa2_rcv_bytes_remains;
	}
	util::msg_t chunk;
	try {
		bus->check_interrupt(chunk, saa2_rcv_bytes_remains);
	} catch (const bus::Timeout &) {
	} catch (const std::exception &err) {
		BOOST_LOG_TRIVIAL(warning) << "[LiteVNA] exception during interrupt receive " << err.what();
		saa2_rcv_bytes_remains	= 0;
		total_rcv_bytes_remains = 0;
		event_queue.push(std::make_tuple(Event::ERROR, "exception during interrupt"));
		return;
	}
	if (chunk.size() > saa2_rcv_bytes_remains) {
		saa2_rcv_bytes_remains	= 0;
		total_rcv_bytes_remains = 0;
		BOOST_LOG_TRIVIAL(warning) << "[LiteVNA] received more bytes then expected: " << chunk.size() << " > "
					   << int(saa2_rcv_bytes_remains);
		event_queue.push(std::make_tuple(Event::ERROR, "received more bytes then expected"));
		return;
	}
	auto buffer_size = data_buffer.size();
	data_buffer.resize(buffer_size + chunk.size());
	std::copy(chunk.begin(), chunk.end(), data_buffer.begin() + buffer_size);
	saa2_rcv_bytes_remains -= chunk.size();
	total_rcv_bytes_remains -= chunk.size();
	BOOST_LOG_TRIVIAL(trace) << "[LiteVNA] received " << chunk.size() << " data bytes; "
				 << "chunk/total remains: " << int(saa2_rcv_bytes_remains) << "/"
				 << total_rcv_bytes_remains;
	if ((saa2_rcv_bytes_remains == 0) && (total_rcv_bytes_remains > 0)) {
		auto total_rcv_points_remains = total_rcv_bytes_remains / sizeof(LiteVNAData);
		auto points_read_size = total_rcv_points_remains / 0xff == 0 ? total_rcv_points_remains % 0xff : 0xff;
		if (points_read_size == 0) {
			BOOST_LOG_TRIVIAL(warning) << "[LiteVNA] requested to read zero data points";
		}
		saa2_indicate();
		saa2_read(LiteVNAReg::VALUES_FIFO, points_read_size);
	} else if ((saa2_rcv_bytes_remains == 0) && (total_rcv_bytes_remains == 0)) {
		if (is_interrupted)
			event_queue.push(std::make_tuple(Event::INTERRUPTED, "measure interrupted"));
		else
			event_queue.push(std::make_tuple(Event::MEASURE_COMPLETE, "measure complete"));
		is_interrupted = false;
	}
}

util::nop_vals_t
LiteVNA::number_of_points_values(void) const noexcept
{
	util::nop_vals_t vals = {3, 11, 51, 101, 1001};
	return vals;
}

util::nop_t
LiteVNA::number_of_points(void)
{
	check_measurement_progress();
	auto lock = bus->get_lock();
	saa2_indicate();
	auto rv = saa2_read2(LiteVNAReg::SWEEP_POINTS);
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] get number of points: " << rv;
	return rv;
}

void
LiteVNA::number_of_points(util::nop_t nop)
{
	check_measurement_progress();
	auto left  = f_left();
	auto right = f_right();
	{
		auto lock = bus->get_lock();
		saa2_indicate();
		if (nop > 1) {
			auto step = ((right - left) / (nop - 1));
			if (step == 0)
				throw std::runtime_error("frequency range is too narrow for selected number of points");
		}
		BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] set number of points: " << nop;
		saa2_write(LiteVNAReg::SWEEP_POINTS, std::uint16_t(nop));
	}
	f_range(left, right);
}

util::s_parts_t
LiteVNA::s_parts(void) const noexcept
{
	util::s_parts_t parts = {{1, 1}, {2, 1}};
	return parts;
}

void
LiteVNA::s_part(util::s_port_t from, util::s_port_t to)
{
	check_measurement_progress();
	auto lock = bus->get_lock();
	saa2_indicate();
	if ((from != 1) || ((to != 1) && (to != 2))) {
		BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] invalid port number";
		throw std::runtime_error("invalid port number");
	}
	std::uint8_t val;
	switch (to) {
	case 1:
		val = LiteVNAReg::TRACEMODE_S11;
		break;
	case 2:
		val = LiteVNAReg::TRACEMODE_S21;
		break;
	default:
		BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] invalid port number";
		throw std::runtime_error("invalid port number");
		break;
	}
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] set tracemode: " << int(val);
	saa2_write(LiteVNAReg::TRACEMODE, val);
}

util::s_part_t
LiteVNA::s_part(void)
{
	check_measurement_progress();
	auto lock = bus->get_lock();
	saa2_indicate();
	auto part = saa2_read(LiteVNAReg::TRACEMODE);
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] get tracemode: " << int(part);
	switch (part) {
	case LiteVNAReg::TRACEMODE_S11:
		return util::s_part_t(1, 1);
	case LiteVNAReg::TRACEMODE_S21:
		return util::s_part_t(2, 1);
	default:
		BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] invalid port number";
		throw std::runtime_error("invalid port number");
	}
}

util::f_t
LiteVNA::f_left(void)
{
	check_measurement_progress();
	auto lock = bus->get_lock();
	saa2_indicate();
	auto rv = saa2_read8(LiteVNAReg::SWEEP_START_HZ);
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] get left frequency: " << rv;
	return rv;
}

util::f_lim_t
LiteVNA::f_left(util::f_t left)
{
	check_measurement_progress();
	auto right = f_right();
	f_range(left, right);
	return f_all();
}

util::f_t
LiteVNA::f_right(void)
{
	check_measurement_progress();
	auto left = f_left();
	auto lock = bus->get_lock();
	saa2_indicate();
	auto step = saa2_read8(LiteVNAReg::SWEEP_STEP_HZ);
	auto nop  = saa2_read2(LiteVNAReg::SWEEP_POINTS);
	BOOST_LOG_TRIVIAL(trace) << "[LiteVNA] left frequency: " << left << "; step: " << step << "; nop: " << nop;
	auto right = left + (nop - 1) * step;
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] get right frequency: " << right;
	return right;
}

util::f_lim_t
LiteVNA::f_right(util::f_t right)
{
	check_measurement_progress();
	auto left = f_left();
	f_range(left, right);
	return f_all();
}

util::f_t
LiteVNA::f_center(void)
{
	check_measurement_progress();
	auto left   = f_left();
	auto span   = f_span();
	auto center = left + span / 2;
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] get center frequency: " << center;
	return center;
}

util::f_lim_t
LiteVNA::f_center(util::f_t center)
{
	check_measurement_progress();
	auto span  = f_span();
	auto left  = center - span / 2;
	auto right = center + span / 2;
	f_range(left, right);
	return f_all();
}

util::f_t
LiteVNA::f_span(void)
{
	check_measurement_progress();
	auto left  = f_left();
	auto right = f_right();
	auto span  = right - left;
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] get span: " << span;
	return span;
}

util::f_lim_t
LiteVNA::f_span(util::f_t span)
{
	check_measurement_progress();
	auto center = f_center();
	auto left   = center - span / 2;
	auto right  = center + span / 2;
	f_range(left, right);
	return f_all();
}

util::f_lim_t
LiteVNA::f_all(util::f_t left, util::f_t right, util::f_t center, util::f_t span)
{
	check_measurement_progress();
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] set all frequencies; left: " << left << "; right: " << right
				 << ";center: " << center << ";span: " << span;
	auto nop = number_of_points();
	if (nop <= 1) {
		auto lock = bus->get_lock();
		saa2_indicate();
		BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] set left frequency: " << center;
		saa2_write(LiteVNAReg::SWEEP_START_HZ, center);
	} else {
		f_range(left, right);
	}
	return f_all();
}

util::f_lim_t
LiteVNA::f_all(void)
{
	check_measurement_progress();
	auto lock = bus->get_lock();
	saa2_indicate();
	auto left   = saa2_read8(LiteVNAReg::SWEEP_START_HZ);
	auto step   = saa2_read8(LiteVNAReg::SWEEP_STEP_HZ);
	auto nop    = saa2_read2(LiteVNAReg::SWEEP_POINTS);
	auto right  = left + (nop - 1) * step;
	auto span   = right - left;
	auto center = left + span / 2;
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] get all frequencies; left: " << left << "; right: " << right
				 << ";center: " << center << ";span: " << span;
	return util::f_lim_t(left, right, center, span);
}

void
LiteVNA::interrupt_measure(util::EventQueue<analyzer_event> &)
{
	auto lock      = bus->get_lock();
	is_interrupted = true;
}

void
LiteVNA::measure(void)
{
	check_measurement_progress();
	auto lock = bus->get_lock();
	saa2_indicate();
	auto nop = saa2_read2(LiteVNAReg::SWEEP_POINTS);
	auto ppf = saa2_read2(LiteVNAReg::VALUES_PER_FREQUENCY);
	ppf	 = ppf == 0 ? 1 : ppf;
	if ((nop * ppf * sizeof(LiteVNAData)) > (ANALYZER_MAX_MEASUREMENT_KIB * 1024)) {
		BOOST_LOG_TRIVIAL(warning)
		    << "[LiteVNA] requested measurement size exceeds limit " STR(ANALYZER_MAX_MEASUREMENT_KIB) " KiB";
		throw std::runtime_error(
		    "requested measurement size exceeds limit " STR(ANALYZER_MAX_MEASUREMENT_KIB) " KiB");
	}
	BOOST_LOG_TRIVIAL(warning) << "[LiteVNA] requested measurement number of points: " << nop
				   << "; points per frequency: " << ppf;
	auto total_rcv_points_remains = nop * ppf;
	total_rcv_bytes_remains	      = total_rcv_points_remains * sizeof(LiteVNAData);
	data_buffer.resize(0);
	auto points_read_size = total_rcv_points_remains / 0xff == 0 ? total_rcv_points_remains % 0xff : 0xff;
	if (points_read_size == 0) {
		BOOST_LOG_TRIVIAL(warning) << "[LiteVNA] requested to read zero data points";
		throw std::runtime_error("requested to read zero data points");
	}
	saa2_indicate();
	BOOST_LOG_TRIVIAL(trace) << "[LiteVNA] flushing FIFO";
	saa2_write(LiteVNAReg::SWEEP_POINTS, nop); // NOTE: without this extra write FIFO does not get cleared
	saa2_write(LiteVNAReg::VALUES_FIFO, std::uint8_t(0));
	BOOST_LOG_TRIVIAL(trace) << "[LiteVNA] requesting to read points/total: " << int(points_read_size) << "/"
				 << total_rcv_bytes_remains;
	saa2_read(LiteVNAReg::VALUES_FIFO, points_read_size);
}

bool
LiteVNA::measuring(void)
{
	return (saa2_rcv_bytes_remains > 0) || (total_rcv_bytes_remains > 0);
}

void
LiteVNA::measurements(result::Scattering &result)
{
	auto res_lock = result.get_lock();
	auto lock     = bus->get_lock();
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] process data buffer";
	saa2_indicate();
	auto nop    = saa2_read2(LiteVNAReg::SWEEP_POINTS);
	auto left   = saa2_read8(LiteVNAReg::SWEEP_START_HZ);
	auto f_step = saa2_read8(LiteVNAReg::SWEEP_STEP_HZ);
	auto part   = saa2_read(LiteVNAReg::TRACEMODE);
	if ((data_buffer.size() / sizeof(LiteVNAData) != nop) || (data_buffer.size() % sizeof(LiteVNAData) != 0)) {
		BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] data buffer is not aligned";
		throw std::runtime_error("data buffer is not aligned");
	}
	std::span<LiteVNAData> points(reinterpret_cast<LiteVNAData *>(data_buffer.data()),
				      data_buffer.size() / sizeof(LiteVNAData));
	util::f_array_t	      &f_points	   = result.get_f_buffer();
	util::meas_array_t    &s_re_points = result.get_s_re_buffer();
	util::meas_array_t    &s_im_points = result.get_s_im_buffer();
	result.scattering_type		   = part == LiteVNAReg::TRACEMODE_S11 ? result::ScatteringType::Reflection
									       : result::ScatteringType::Transmission;

	f_points.resize(points.size());
	s_re_points.resize(points.size());
	s_im_points.resize(points.size());
	BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] size: " << data_buffer.size() << " " << points.size();
	bool bad_tracemode = false;
#pragma omp parallel for
	for (size_t i = 0; i < points.size(); ++i) {
		f_points[i] = left + f_step * points[i].freqIndex.value();
		// TODO: add checksum check //
		// uint8_t checksum = 0x46;
		// for (int i = 0; i < (fifo_size - 1); i++)
		// 	checksum = (checksum ^ ((checksum << 1) | 1u)) ^ data[i];
		// const bool checksum_ok = (checksum == fifo->checksum) ? true : false;
		auto ref_channel = std::complex<util::meas_t>(points[i].fwd0Re.value(), points[i].fwd0Im.value());
		auto rev0 =
		    std::complex<util::meas_t>(points[i].rev0Re.value(), points[i].rev0Im.value()) / ref_channel;
		auto rev1 =
		    std::complex<util::meas_t>(points[i].rev1Re.value(), points[i].rev1Im.value()) / ref_channel;
		switch (part) {
		case LiteVNAReg::TRACEMODE_S11:
			s_re_points[i] = rev0.real();
			s_im_points[i] = rev0.imag();
			break;
		case LiteVNAReg::TRACEMODE_S21:
			s_re_points[i] = rev1.real();
			s_im_points[i] = rev1.imag();
			break;
		default:
			bad_tracemode = true;
			break;
		}
	}
	// Do not throw exception in OMP context. Wait for loop to end.
	if (bad_tracemode) {
		BOOST_LOG_TRIVIAL(debug) << "[LiteVNA] unknown tracemode";
		throw std::runtime_error("unknown tracemode");
	}
	result.process();
}

} // namespace analyzer

// GCOVR_EXCL_STOP
#endif // ANALYZER_LITEVNA
