#include <config.h>
#ifdef ANALYZER_SAA2

/** @file saa2.cxx
 * @brief analyzer::SAA2 communication protocol functions.
 */

// GCOVR_EXCL_START do not test hardware driver

#include "saa2.hxx"

#include <boost/log/trivial.hpp>

namespace analyzer
{

SAA2::SAA2(std::uint8_t fifo_chunk_size) : fifo_chunk_size(fifo_chunk_size)
{
}

void
SAA2::saa2_indicate(void)
{
	util::msg_t message = {SAA2Cmd::IDCT};
	BOOST_LOG_TRIVIAL(trace) << "[SAA2] send: " << std::hex << std::showbase << std::nouppercase << message;
	saa2_send(message);
	saa2_rcv(message, 1);
	BOOST_LOG_TRIVIAL(trace) << "[SAA2] rcv: " << std::hex << std::showbase << std::nouppercase << message;
	if ((message.size() != 1) || message[0] != SAA2Cmd::IDCT_RPL) {
		BOOST_LOG_TRIVIAL(info) << "[SAA2] unexpected indicate message";
		throw std::runtime_error("unexpected indicate message");
	}
}

std::uint8_t
SAA2::saa2_read(std::uint8_t address)
{
	util::msg_t message = {SAA2Cmd::RD1, address};
	BOOST_LOG_TRIVIAL(trace) << "[SAA2] send: " << std::hex << std::showbase << std::nouppercase << message;
	saa2_send(message);
	saa2_rcv(message, 1);
	BOOST_LOG_TRIVIAL(trace) << "[SAA2] rcv: " << std::hex << std::showbase << std::nouppercase << message;
	if (message.size() != 1) {
		BOOST_LOG_TRIVIAL(trace) << "[SAA2] message size";
		throw std::runtime_error("unexpected message size");
	}
	return message[0];
}

std::uint16_t
SAA2::saa2_read2(std::uint8_t address)
{
	util::msg_t message = {SAA2Cmd::RD2, address};
	BOOST_LOG_TRIVIAL(trace) << "[SAA2] send: " << std::hex << std::showbase << std::nouppercase << message;
	saa2_send(message);
	saa2_rcv(message, 2);
	BOOST_LOG_TRIVIAL(trace) << "[SAA2] rcv: " << std::hex << std::showbase << std::nouppercase << message;
	if (message.size() != 2) {
		BOOST_LOG_TRIVIAL(trace) << "[SAA2] message size";
		throw std::runtime_error("unexpected message size");
	}
	return static_cast<std::uint16_t>(message[0]) | (static_cast<std::uint16_t>(message[1]) << 8);
}

std::uint32_t
SAA2::saa2_read4(std::uint8_t address)
{
	util::msg_t message = {SAA2Cmd::RD4, address};
	BOOST_LOG_TRIVIAL(trace) << "[SAA2] send: " << std::hex << std::showbase << std::nouppercase << message;
	saa2_send(message);
	saa2_rcv(message, 4);
	BOOST_LOG_TRIVIAL(trace) << "[SAA2] rcv: " << std::hex << std::showbase << std::nouppercase << message;
	if (message.size() != 4) {
		BOOST_LOG_TRIVIAL(trace) << "[SAA2] message size";
		throw std::runtime_error("unexpected message size");
	}
	return static_cast<std::uint32_t>(message[0]) | (static_cast<std::uint32_t>(message[1]) << 8) |
	       (static_cast<std::uint32_t>(message[2]) << 16) | (static_cast<std::uint32_t>(message[3]) << 24);
}

std::uint64_t
SAA2::saa2_read8(std::uint8_t address)
{
	util::msg_t message = {SAA2Cmd::RD8, address};
	BOOST_LOG_TRIVIAL(trace) << "[SAA2] send: " << std::hex << std::showbase << std::nouppercase << message;
	saa2_send(message);
	saa2_rcv(message, 8);
	BOOST_LOG_TRIVIAL(trace) << "[SAA2] rcv: " << std::hex << std::showbase << std::nouppercase << message;
	if (message.size() != 8) {
		BOOST_LOG_TRIVIAL(trace) << "[SAA2] message size";
		throw std::runtime_error("unexpected message size");
	}
	return static_cast<std::uint64_t>(message[0]) | (static_cast<std::uint64_t>(message[1]) << 8) |
	       (static_cast<std::uint64_t>(message[2]) << 16) | (static_cast<std::uint64_t>(message[3]) << 24) |
	       (static_cast<std::uint64_t>(message[4]) << 32) | (static_cast<std::uint64_t>(message[5]) << 40) |
	       (static_cast<std::uint64_t>(message[6]) << 48) | (static_cast<std::uint64_t>(message[7]) << 56);
}

void
SAA2::saa2_read(std::uint8_t address, std::uint8_t count)
{
	saa2_rcv_bytes_remains	  = count * fifo_chunk_size;
	const util::msg_t message = {SAA2Cmd::RD_FIFO, address, count};
	BOOST_LOG_TRIVIAL(trace) << "[SAA2] send: " << std::hex << std::showbase << std::nouppercase << message;
	saa2_send(message);
}

void
SAA2::saa2_write(std::uint8_t address, std::uint8_t data)
{
	const util::msg_t message = {SAA2Cmd::WR1, address, data};
	BOOST_LOG_TRIVIAL(trace) << "[SAA2] send: " << std::hex << std::showbase << std::nouppercase << message;
	saa2_send(message);
}

void
SAA2::saa2_write(std::uint8_t address, std::uint16_t data)
{
	const util::msg_t message = {SAA2Cmd::WR2, address, static_cast<std::uint8_t>(data & 0xff),
				     static_cast<std::uint8_t>((data >> 8) & 0xff)};
	BOOST_LOG_TRIVIAL(trace) << "[SAA2] send: " << std::hex << std::showbase << std::nouppercase << message;
	saa2_send(message);
}

void
SAA2::saa2_write(std::uint8_t address, std::uint32_t data)
{
	const util::msg_t message = {SAA2Cmd::WR4,
				     address,
				     static_cast<std::uint8_t>(data & 0xff),
				     static_cast<std::uint8_t>((data >> 8) & 0xff),
				     static_cast<std::uint8_t>((data >> 16) & 0xff),
				     static_cast<std::uint8_t>((data >> 24) & 0xff)};
	BOOST_LOG_TRIVIAL(trace) << "[SAA2] send: " << std::hex << std::showbase << std::nouppercase << message;
	saa2_send(message);
}

void
SAA2::saa2_write(std::uint8_t address, std::uint64_t data)
{
	const util::msg_t message = {SAA2Cmd::WR8,
				     address,
				     static_cast<std::uint8_t>(data & 0xff),
				     static_cast<std::uint8_t>((data >> 8) & 0xff),
				     static_cast<std::uint8_t>((data >> 16) & 0xff),
				     static_cast<std::uint8_t>((data >> 24) & 0xff),
				     static_cast<std::uint8_t>((data >> 32) & 0xff),
				     static_cast<std::uint8_t>((data >> 40) & 0xff),
				     static_cast<std::uint8_t>((data >> 48) & 0xff),
				     static_cast<std::uint8_t>((data >> 56) & 0xff)};
	BOOST_LOG_TRIVIAL(trace) << "[SAA2] send: " << std::hex << std::showbase << std::nouppercase << message;
	saa2_send(message);
}

void
SAA2::saa2_write(std::uint8_t address, const util::msg_t &msg, std::uint8_t count)
{
	const util::msg_t message = {SAA2Cmd::WR_FIFO, address, count};
	BOOST_LOG_TRIVIAL(trace) << "[SAA2] send: " << std::hex << std::showbase << std::nouppercase << message << msg;
	saa2_send(message);
	saa2_send(msg);
}

} // namespace analyzer

// GCOVR_EXCL_STOP
#endif // ANALYZER_SAA2
