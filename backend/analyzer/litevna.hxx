#pragma once

#include <config.h>
#ifdef ANALYZER_LITEVNA

/** @file litevna.hxx
 * @brief analyzer::LiteVNA analyzer::Analyzer implementation.
 */

#ifndef ANALYZER_SAA2
#error ANALYZER_SAA2 is needed for ANALYZER_LITEVNA
#endif

#include "analyzer.hxx"
#include "saa2.hxx"

#include <analyzer/event.hxx>
#include <bus/bus.hxx>
#include <bus/list.hxx>
#include <event_queue.hxx>
#include <tuple>
#include <types.hxx>

namespace analyzer
{

/**
 * @brief LiteVNA Analyzer class. Uses SAA2 communication protocol.
 */
class LiteVNA : public Analyzer, public SAA2
{
      protected:
	void saa2_send(const util::msg_t &);
	void saa2_rcv(util::msg_t &, std::uint8_t);

      private:
	util::msg_t   data_buffer;		       ///< Data buffer
	std::uint32_t total_rcv_bytes_remains = 0;     ///< Byte count left in current FIFO transaction.
	bool	      is_interrupted	      = false; ///< Measure interrupt flag.

	/**
	 * @brief Check device availability.
	 *
	 * @throws std::runtime_error Measurement is in progress.
	 */
	void check_measurement_progress(void);

	/**
	 * @brief Read device variant.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 *
	 * @return Device variant.
	 */
	std::uint8_t cmd_device_variant(void);

	/**
	 * @brief Read device protocol version.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 *
	 * @return Device protocol version.
	 */
	std::uint8_t cmd_protocol_version(void);

	/**
	 * @brief Read device hardware revision.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 *
	 * @return Device hardware revision.
	 */
	std::uint8_t cmd_hardware_revision(void);

	/**
	 * @brief Read device firmware version.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 *
	 * @return Device firmware version pair.
	 */
	std::tuple<std::uint8_t, std::uint8_t> cmd_firmware_version(void);

	/**
	 * @brief Get LO frequency power.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 *
	 * @return LO frequency power level code.
	 */
	std::uint8_t cmd_lo_power(void);

	/**
	 * @brief Set LO frequency power.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 *
	 * @param power LO frequency power level code.
	 */
	void cmd_lo_power(std::uint8_t power);

	/**
	 * @brief Get HI frequency power.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 *
	 * @return HI frequency power level code.
	 */
	std::uint8_t cmd_hi_power(void);

	/**
	 * @brief Set HI frequency power.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 *
	 * @param power HI frequency power level code.
	 */
	void cmd_hi_power(std::uint8_t power);

	/**
	 * @brief Get averaging value.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 *
	 * @return Averaging value.
	 */
	std::uint8_t cmd_average(void);

	/**
	 * @brief Set averaging value.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 *
	 * @param value Averaging value.
	 */
	void cmd_average(std::uint8_t value);

	/**
	 * @brief Set frequency range.
	 *
	 * @warning Caller must obtain bus::Bus::mutex prior to this call via bus::Bus::get_lock.
	 *
	 * @param left Left frequency point.
	 * @param right Right frequency point.
	 * @throws std::runtime_error invalid frequency range.
	 */
	void f_range(util::f_t left, util::f_t right);

      public:
	static constexpr const char   *NAME = "LiteVNA"; ///< Analyzer name.
	static const util::name_list_t SUPPORTED_BUSSES; ///< List of supported bus::Bus drivers.
	/**
	 * @brief Bus attribute choices.
	 *
	 * @return Bus attribute choices.
	 */
	static const util::attribute_t &attribute_choices(void) noexcept;

	/**
	 * @brief LiteVNA Analyzer constructor.
	 *
	 * @param[in] attributes Analyzer attributes.
	 * @param[in] bus bus::Bus reference.
	 */
	LiteVNA(util::attribute_t &&attributes, std::shared_ptr<bus::Bus> bus);

	/**
	 * @brief Do not allow LiteVNA Analyzer copying.
	 */
	LiteVNA(LiteVNA &) = delete;

	/**
	 * @brief Do not allow LiteVNA Analyzer moving.
	 */
	LiteVNA(LiteVNA &&) = delete;

	/**
	 * @brief LiteVNA Analyzer destructor.
	 */
	~LiteVNA();

	const util::string_t &name(void) const noexcept;
	bool		      interrupted(void) noexcept;
	void		      check_interrupt(util::EventQueue<analyzer_event> &event) noexcept;
	util::nop_vals_t      number_of_points_values(void) const noexcept;
	util::nop_t	      number_of_points(void);
	void		      number_of_points(util::nop_t);
	util::s_parts_t	      s_parts(void) const noexcept;
	void		      s_part(util::s_port_t, util::s_port_t);
	util::s_part_t	      s_part(void);
	util::f_t	      f_left(void);
	util::f_lim_t	      f_left(util::f_t);
	util::f_t	      f_right(void);
	util::f_lim_t	      f_right(util::f_t);
	util::f_t	      f_center(void);
	util::f_lim_t	      f_center(util::f_t);
	util::f_t	      f_span(void);
	util::f_lim_t	      f_span(util::f_t);
	util::f_lim_t	      f_all(util::f_t, util::f_t, util::f_t, util::f_t);
	util::f_lim_t	      f_all(void);
	void		      measure(void);
	bool		      measuring(void);
	void		      measurements(result::Scattering &result);
	void		      interrupt_measure(util::EventQueue<analyzer_event> &);
};

} // namespace analyzer

#endif // ANALYZER_LITEVNA
