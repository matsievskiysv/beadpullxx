/** @file analyzer.cxx
 * @brief Base analyzer::Analyzer default implementations.
 */

#include "analyzer.hxx"

#include <algorithm>
#include <boost/json/value_from.hpp>
#include <cmath>
#include <complex>

namespace json = boost::json;

namespace analyzer
{

const util::name_list_t Analyzer::SUPPORTED_BUSSES = {};

static const util::attribute_t _attribute_choices = {{"template", nullptr}}; ///< Analyzer attribute choices.

const util::attribute_t &
Analyzer::attribute_choices(void) noexcept
{
	return _attribute_choices;
}

void
Analyzer::patch_bus_attributes(util::attribute_t &) noexcept
{
}

Analyzer::Analyzer(util::attribute_t &&attributes, std::shared_ptr<bus::Bus> bus)
    : bus(std::move(bus)), attributes(attributes)
{
}

Analyzer::~Analyzer()
{
}

static const util::string_t _name = util::string_t(Analyzer::NAME); ///< Analyzer name.

const util::string_t &
Analyzer::name(void) const noexcept
{
	return _name;
}

const util::string_t
Analyzer::bus_name(void) const
{
	return bus->name();
}

util::attribute_t &
Analyzer::bus_attributes(void) const
{
	return bus->attributes;
};

void
Analyzer::interrupt_measure(util::EventQueue<analyzer_event> &)
{
}

} // namespace analyzer
