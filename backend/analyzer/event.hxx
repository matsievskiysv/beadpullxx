/** @file event.hxx
 * @brief analyzer::Analyzer events.
 */

#pragma once

#include <types.hxx>

namespace analyzer
{

/**
 * @brief analyzer::Analyzer event type.
 */
enum class Event {
	MEASURE_COMPLETE, ///< analyzer::Analyzer measurement complete.
	ERROR,		  ///< analyzer::Analyzer error.
	INTERRUPTED,	  ///< analyzer::Analyzer interrupted.
};

using analyzer_event = std::tuple<Event, util::string_t>; ///< analyzer::Analyzer event.

} // namespace analyzer
