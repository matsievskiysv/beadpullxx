#include <config.h>
#ifdef ANALYZER_AGILENT506X

/** @file agilent_506x.cxx
 * @brief analyzer::Agilent506X analyzer::Analyzer implementation.
 */

// GCOVR_EXCL_START do not test hardware driver

#include "agilent_506x.hxx"

#include <boost/algorithm/string/trim_all.hpp>
#include <boost/endian/buffers.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/log/trivial.hpp>
#include <span>

namespace analyzer
{

namespace endian = boost::endian;

static const size_t DATA_CHUNK_SIZE = 1 << 10; ///< Data chunk transfer size.

/**
 * @brief Representation of the Agilent506X measurement point.
 */
struct Agilent506XData {
	endian::big_float64_buf_t re; ///< Real part.
	endian::big_float64_buf_t im; ///< Imaginary part.
};
static_assert(sizeof(Agilent506XData) == 16);

/**
 * @brief Agilent506X status register byte.
 */
enum StatusByte {
	CHECK_ERROR_EVENT_QUEUE = (1 << 2), ///< Set to “1” if the error/event queue contains data; reset to “0” when
					    ///< all the data has been retrieved.
	CHECK_QSRS =
	    (1 << 3), ///< Set to “1” when one of the enabled bits in the status event status register is set to “1.”
	MAV = (1 << 4), ///< Set to “1” when the output queue contains data; reset to “0” when all the data has been
			///< retrieved.
	SESRS =
	    (1 << 5), ///< Set to “1” when one of the enabled bits in the status event status register is set to “1.”
	RQS = (1 << 6), ///< Set to “1” when any of the status byte register bits enabled by the service request enable
			///< register is set to “1”; reset to “0” when all the data has been retrieved through serial
			///< polling.
	OSRS = (1 << 7), ///< Set to “1” when one of the enabled bits in the operational status register is set to “1.”
};

/**
 * @brief Agilent506X event status register byte.
 */
enum EventStatusRegister {
	OPC	    = (1 << 0), ///< A command for which OPC has been enabled has completed operation.
	QUERY_ERROR = (1 << 2), ///< The analyzer has been addressed to talk but there is nothing in the output
				///< queue to transmit.
	IDE = (1 << 3), ///< Set to “1” when an error has occurred and the error is not a command, query, or execution
			///< error.
	EXECUTION_ERROR = (1 << 4), ///< A command was received that could not be executed.
	SYNTAX_ERROR	= (1 << 5), ///< The incoming GPIB commands contained a syntax error. The syntax error can
				    ///< only be cleared by a device clear or an instrument preset.
	POWER_ON = (1 << 7),	    ///< A power-on sequence has occurred since the last read of the register.
};

/**
 * @brief Agilent506X operation status condition register byte.
 */
enum OperationStatusConditionRegister {
	MEASUREMENT	  = (1 << 4),  ///< Set to “1” during measurement
	WAIT_FOR_TRIGGER  = (1 << 5),  ///< Set to “1” while the instrument is waiting for a trigger
	VBA_MACRO_RUNNING = (1 << 14), ///< Set to “1” while a VBA macro is running.
};

/**
 * @brief Agilent506X questionable status condition register byte.
 */
enum QuestionableStatusConditionRegister {
	BANDWIDTH_TEST_FAIL = (1 << 8), ///< Set to “1” while one of the enabled bits in the questionable bandwidth
					///< limit status event register is set to “1.”
	RIPPLE_TEST_FAIL = (1 << 9), ///< Set to “1” while one of the enabled bits in the questionable ripple limit
				     ///< status event register is set to “1.”
	LIMIT_TEST_FAIL = (1 << 10), ///< Set to “1” while one of the enabled bits in the questionable limit status
				     ///< event register is set to “1.”
};

const util::name_list_t Agilent506X::SUPPORTED_BUSSES = {
#ifdef BUS_GPIB
    util::string_t(bus::GPIB::NAME),
#endif
};

/**
 * @brief Test char is whitespace or non-printable.
 *
 * @param ch Char to be tested.
 * @return Char is whitespace or non-printable.
 */
static bool
is_whitespacish(const char ch)
{
	return ch <= ' ' || ch > '~';
}

/**
 * @brief Analyzer attribute value ranges.
 */
static const util::attribute_t _attribute_choices = {
    {"template", "agilent506x"},
    {"ifbw", {{"choices", {10, 30, 100, 300, 1000, 3000, 10000, 30000}}, {"value", 3000}}},
    {"power", {{"min", -5}, {"max", 10}, {"step", 0.1}, {"value", 0}}},
    {"averaging", {{"min", 0}, {"max", 999}, {"step", 1}, {"value", 0}}},
    {"smoothing", {{"min", 0}, {"max", 25}, {"step", 0.05}, {"value", 0}}},
    {"beep", {{"value", true}}},
};

const util::attribute_t &
Agilent506X::attribute_choices(void) noexcept
{
	return _attribute_choices;
}

void
Agilent506X::patch_bus_attributes(util::attribute_t &attributes) noexcept
{
	attributes.as_object()["status_byte_mask"] = StatusByte::SESRS;
	attributes.as_object()["send_eoi"]	   = true;
	attributes.as_object()["eos_reos"]	   = false;
	attributes.as_object()["eos_xeos"]	   = false;
	attributes.as_object()["eos_bin"]	   = false;
}

void
Agilent506X::check_measurement_progress(void)
{
	if (measurement_in_progress)
		throw std::runtime_error("measurement is in progress");
}

void
Agilent506X::status_byte(std::uint8_t value)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] set config status byte enable: " << std::hex << std::showbase
				 << std::nouppercase << value;
	auto message = util::str2msg(boost::str(boost::format(";*SRE %1%\n") % int(value)));
	bus->send(message);
}

std::uint8_t
Agilent506X::status_byte(void)
{
	BOOST_LOG_TRIVIAL(trace) << "[Agilent506X] status byte query";
	auto message = util::str2msg("*STB?\n");
	bus->send(message);
	bus->rcv(message, 64);
	auto status_string = std::string(message.begin(), message.end());
	boost::algorithm::trim_all_if(status_string, is_whitespacish);
	auto status = int(boost::lexical_cast<std::uint16_t>(status_string));
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] status byte: " << std::hex << std::showbase << std::nouppercase
				 << int(status);
	return status;
}

void
Agilent506X::esr(std::uint8_t value)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] set event status enable: " << std::hex << std::showbase
				 << std::nouppercase << value;
	auto message = util::str2msg(boost::str(boost::format(";*ESE %1%\n") % int(value)));
	bus->send(message);
}

std::uint8_t
Agilent506X::esr(void)
{
	BOOST_LOG_TRIVIAL(trace) << "[Agilent506X] event register query";
	auto message = util::str2msg(";*ESR?\n");
	bus->send(message);
	bus->rcv(message, 64);
	auto status_string = std::string(message.begin(), message.end());
	boost::algorithm::trim_all_if(status_string, is_whitespacish);
	auto status = boost::lexical_cast<std::uint16_t>(status_string);
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] event register: " << std::hex << std::showbase << std::nouppercase
				 << int(status);
	return status;
}

void
Agilent506X::oscr(std::uint16_t value)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] set Operation Status Enable Register enable: " << std::hex
				 << std::showbase << std::nouppercase << value;
	auto message = util::str2msg(boost::str(boost::format(";:STAT:OPER:ENAB %1%\n") % value));
	bus->send(message);
}

std::uint16_t
Agilent506X::oscr(void)
{
	BOOST_LOG_TRIVIAL(trace) << "[Agilent506X] Operation Status Enable Register query";
	auto message = util::str2msg(";:STAT:OPER?\n");
	bus->send(message);
	bus->rcv(message, 64);
	auto status_string = std::string(message.begin(), message.end());
	boost::algorithm::trim_all_if(status_string, is_whitespacish);
	auto status = int(boost::lexical_cast<std::uint16_t>(status_string));
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] Operation Status Enable Register: " << std::hex << std::showbase
				 << std::nouppercase << int(status);
	return status;
}

void
Agilent506X::qser(std::uint16_t value)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] set Questionable Status Event Register enable: " << std::hex
				 << std::showbase << std::nouppercase << value;
	auto message = util::str2msg(boost::str(boost::format(";:STAT:QUES:ENAB %1%\n") % value));
	bus->send(message);
}

std::uint16_t
Agilent506X::qser(void)
{
	BOOST_LOG_TRIVIAL(trace) << "[Agilent506X] Questionable Status Event Register query";
	auto message = util::str2msg(";:STAT:QUES?\n");
	bus->send(message);
	bus->rcv(message, 64);
	auto status_string = std::string(message.begin(), message.end());
	boost::algorithm::trim_all_if(status_string, is_whitespacish);
	auto status = int(boost::lexical_cast<std::uint16_t>(status_string));
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] Operation Status Enable Register: " << std::hex << std::showbase
				 << std::nouppercase << int(status);
	return status;
}

void
Agilent506X::averaging(std::uint16_t value)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] averaging: " << value;
	{
		auto message =
		    util::str2msg(boost::str(boost::format(";:SENS1:AVER %1%\n") % (value > 0 ? "ON" : "OFF")));
		bus->send(message);
	}
	if (value > 0) {
		auto message = util::str2msg(boost::str(boost::format(";:SENS1:AVER:COUN %1%\n") % value));
		bus->send(message);
	}
}

void
Agilent506X::reset_averaging(void)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] reset averaging";
	auto message = util::str2msg(";:SENS1:AVER:CLE\n");
	bus->send(message);
}

void
Agilent506X::smoothing(double value)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] smoothing: " << value;
	{
		auto message =
		    util::str2msg(boost::str(boost::format(";:CALC1:SMO:STAT %1%\n") % (value >= 0.05 ? "ON" : "OFF")));
		bus->send(message);
	}
	if (value >= 0.05) {
		auto message = util::str2msg(boost::str(boost::format(";:CALC1:SMO:APER %1$.3f\n") % value));
		bus->send(message);
	}
}

void
Agilent506X::power(double value)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] power: " << value;
	auto message = util::str2msg(boost::str(boost::format(";:SOUR1:POW %1$.02f\n") % value));
	bus->send(message);
}

void
Agilent506X::beep(bool value)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] beep: " << value;
	auto message = util::str2msg(boost::str(
	    boost::format(";:SYST:BEEP:COMP:STAT OFF\n;:SYST:BEEP:WARN:STAT %1%\n") % (value ? "ON" : "OFF")));
	bus->send(message);
}

void
Agilent506X::input_lock(bool enable)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] release";
	auto message = util::str2msg(
	    boost::str(boost::format(";:SYST:KLOC:KBD %1%\n;:SYST:KLOC:MOUS %1%\n") % (enable ? "ON" : "OFF")));
	bus->send(message);
}

void
Agilent506X::integration_bandwidth(std::uint16_t value)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] integration bandwidth: " << value;
	auto message = util::str2msg(boost::str(boost::format(":SENS1:BWID:RES %1%\n") % value));
	bus->send(message);
}

void
Agilent506X::number_form(void)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] data form: double";
	auto message = util::str2msg(";:FORM:DATA REAL\n");
	bus->send(message);
}

void
Agilent506X::channel(void)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] set channel";
	auto message = util::str2msg(";:DISP:WIND1:ACT\n;:CALC1:PAR1:SEL\n;:OUTP:STAT ON\n;"
				     ":INIT1:CONT OFF\n;:TRIG:SOUR BUS\n");
	bus->send(message);
}

void
Agilent506X::data_format(void)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] Smith chart mode";
	auto message = util::str2msg(";:SENS1:SWE:TYPE LIN\n;:CALC1:SEL:FORM SCOM\n");
	bus->send(message);
}

util::nop_t
Agilent506X::_number_of_points(void)
{
	BOOST_LOG_TRIVIAL(trace) << "[Agilent506X] number of points query";
	auto message = util::str2msg(":SENS1:SWE:POIN?\n");
	bus->send(message);
	bus->rcv(message, 64);
	auto nop_string = std::string(message.begin(), message.end());
	boost::algorithm::trim_all_if(nop_string, is_whitespacish);
	auto nop = int(boost::lexical_cast<util::meas_t>(nop_string));
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] number of points: " << nop;
	return nop;
}

util::f_t
Agilent506X::_f_left(void)
{
	BOOST_LOG_TRIVIAL(trace) << "[Agilent506X] left frequency query";
	auto message = util::str2msg(";:SENS1:FREQ:STAR?\n");
	bus->send(message);
	bus->rcv(message, 64);
	auto f_string = std::string(message.begin(), message.end());
	boost::algorithm::trim_all_if(f_string, is_whitespacish);
	auto f = util::f_t(boost::lexical_cast<util::meas_t>(f_string));
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] left frequency: " << f;
	return f;
}

util::f_t
Agilent506X::_f_span(void)
{
	BOOST_LOG_TRIVIAL(trace) << "[Agilent506X] frequency span query";
	auto message = util::str2msg(";:SENS1:FREQ:SPAN?\n");
	bus->send(message);
	bus->rcv(message, 64);
	auto f_string = std::string(message.begin(), message.end());
	boost::algorithm::trim_all_if(f_string, is_whitespacish);
	auto f = util::f_t(boost::lexical_cast<util::meas_t>(f_string));
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] frequency span: " << f;
	return f;
}

util::s_part_t
Agilent506X::_s_part(void)
{
	BOOST_LOG_TRIVIAL(trace) << "[Agilent506X] s part query";
	auto message = util::str2msg(";:CALC1:PAR1:DEF?\n");
	bus->send(message);
	bus->rcv(message, 64);
	if ((message.size() < 3) || (message[0] != 'S')) {
		BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] cannot get measurement part";
		throw std::runtime_error("cannot get measurement part");
	}
	return util::s_part_t(message[1] - '0', message[2] - '0');
}

void
Agilent506X::check_esr(util::EventQueue<analyzer_event> &event_queue) noexcept
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] check ESR";
	std::uint8_t val;
	try {
		val = esr();
	} catch (const std::exception &err) {
		BOOST_LOG_TRIVIAL(warning) << "[Agilent506X] error during ESR check: " << err.what();
		return;
	}
	if (val & (EventStatusRegister::EXECUTION_ERROR | EventStatusRegister::IDE)) {
		BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] execution error";
		event_queue.push(std::make_tuple(Event::ERROR, "execution error"));
	}
	if (val & EventStatusRegister::POWER_ON)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] power on";
	if (val & EventStatusRegister::QUERY_ERROR)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] query error";
	if (val & EventStatusRegister::SYNTAX_ERROR)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] syntax error";
	if (val & EventStatusRegister::OPC) {
		BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] OPC command complete";
		try {
			auto nop       = _number_of_points();
			auto data_size = nop * 2 * sizeof(double);
			auto message   = util::str2msg(";:CALC1:SEL:DATA:FDAT?\n");
			bus->send(message);
			BOOST_LOG_TRIVIAL(trace) << "[Agilent506X] read size: " << data_size;
			bus->rcv(message, 8);
			if ((message[0] != '#') || (message[1] != '6')) {
				BOOST_LOG_TRIVIAL(warning) << "[Agilent506X] wrong data header";
			} else {
				auto size_string = std::string(message.begin() + 2, message.end());
				auto size	 = boost::lexical_cast<std::size_t>(size_string);
				BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] reported data size: " << size
							 << "; expected data size: " << data_size;
				if (size != data_size) {
					BOOST_LOG_TRIVIAL(warning) << "[Agilent506X] unexpected data size";
					event_queue.push(std::make_tuple(Event::ERROR, "unexpected data size"));
				} else {
					std::size_t offset = 0;
					while (data_size > 0) {
						util::msg_t chunk;
						auto	    chunk_size = data_size % DATA_CHUNK_SIZE;
						chunk_size	       = chunk_size == 0 ? DATA_CHUNK_SIZE : chunk_size;
						chunk.reserve(chunk_size);
						bus->rcv(chunk, chunk_size);
						std::copy(chunk.begin(), chunk.end(), data_buffer.begin() + offset);
						offset += chunk_size;
						data_size -= chunk_size;
					}
					event_queue.push(std::make_tuple(Event::MEASURE_COMPLETE, "measure complete"));
				}
			}
		} catch (const std::exception &err) {
			BOOST_LOG_TRIVIAL(warning) << "[Agilent506X] error during data read: " << err.what();
			return;
		}
		measurement_in_progress = false;
	}
}

void
Agilent506X::flush_errors(void) noexcept
{
	bool error_flag = true;
	while (error_flag) {
		auto message = util::str2msg(";:SYST:ERR?\n");
		try {
			bus->send(message);
			bus->rcv(message, 64);
		} catch (const std::exception &err) {
			BOOST_LOG_TRIVIAL(warning) << "[Agilent506X] error during error flush: " << err.what();
			return;
		}
		if (message.size() > 0) {
			std::string error(message.begin(), message.end());
			boost::algorithm::trim_all_if(error, is_whitespacish);
			BOOST_LOG_TRIVIAL(info) << "[Agilent506X] error: " << error;
		}
		try {
			bus->check_interrupt(message, 1);
			error_flag = message[1] & StatusByte::CHECK_ERROR_EVENT_QUEUE;
		} catch (const std::exception &err) {
			BOOST_LOG_TRIVIAL(warning) << "[Agilent506X] interrupt check error: " << err.what();
		}
	}
}

Agilent506X::Agilent506X(util::attribute_t &&attributes, std::shared_ptr<bus::Bus> bus)
    : Analyzer(std::move(attributes), bus)
{
	BOOST_LOG_TRIVIAL(trace) << "[Agilent506X] attributes " << attributes;
	data_buffer.reserve(ANALYZER_MAX_MEASUREMENT_KIB * 1024);
	auto lock = this->bus->get_lock();
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] query identity";
	{
		auto message = util::str2msg(";*IDN?\n");
		this->bus->send(message);
		this->bus->rcv(message, 64);
		if (message.size() == 0) {
			BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] cannot get device identity";
			throw std::runtime_error("cannot get device identity");
		}
		auto identity = std::string(message.begin(), message.end());
		boost::algorithm::trim_all_if(identity, is_whitespacish);
		this->attributes.as_object()["identity"] = identity;
		BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] identity: " << identity;
		if ((identity.find("E5061") == std::string::npos) && (identity.find("E5062") == std::string::npos)) {
			BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] unknown device identity";
			throw std::runtime_error("unknown device identity");
		}
	}
	channel();
	input_lock(true);
	averaging(attributes.as_object()["averaging"].as_int64());
	if (attributes.as_object()["smoothing"].is_int64())
		smoothing(attributes.as_object()["smoothing"].as_int64());
	else
		smoothing(attributes.as_object()["smoothing"].as_double());
	if (attributes.as_object()["power"].is_int64())
		smoothing(attributes.as_object()["power"].as_int64());
	else
		smoothing(attributes.as_object()["power"].as_double());
	beep(attributes.as_object()["beep"].as_bool());
	integration_bandwidth(attributes.as_object()["ifbw"].as_int64());
	number_form();
	data_format();
	status_byte(StatusByte::SESRS);
	esr(EventStatusRegister::OPC);
	oscr(0);
	qser(0);
	flush_errors();
}

Agilent506X::~Agilent506X()
{
	try {
		input_lock(false);
	} catch (const std::exception &err) {
		BOOST_LOG_TRIVIAL(warning) << "[Agilent506X] release device";
	}
	BOOST_LOG_TRIVIAL(trace) << "[Agilent506X] free analyzer";
}

static const util::string_t _name = util::string_t(Agilent506X::NAME); ///< Analyzer name.

const util::string_t &
Agilent506X::name(void) const noexcept
{
	return _name;
}

bool
Agilent506X::interrupted(void) noexcept
{
	auto lock = bus->get_lock();
	return bus->interrupted();
}

void
Agilent506X::check_interrupt(util::EventQueue<analyzer_event> &event_queue) noexcept
{
	auto lock = bus->get_lock();
	BOOST_LOG_TRIVIAL(trace) << "[Agilent506X] check interrupt";
	util::msg_t message;
	try {
		bus->check_interrupt(message, 1);
	} catch (const std::exception &err) {
		BOOST_LOG_TRIVIAL(warning) << "[Agilent506X] interrupt check error: " << err.what();
	}
	if (message[0] & StatusByte::CHECK_ERROR_EVENT_QUEUE) {
		flush_errors();
	}
	if (message[0] & StatusByte::SESRS) {
		check_esr(event_queue);
	}
}

util::nop_vals_t
Agilent506X::number_of_points_values(void) const noexcept
{
	util::nop_vals_t vals = {3, 11, 21, 26, 51, 101, 201, 401, 801, 1601};
	return vals;
}

util::nop_t
Agilent506X::number_of_points(void)
{
	check_measurement_progress();
	auto lock = bus->get_lock();
	return _number_of_points();
}

void
Agilent506X::number_of_points(util::nop_t nop)
{
	check_measurement_progress();
	auto lock = bus->get_lock();
	BOOST_LOG_TRIVIAL(trace) << "[Agilent506X] set number of points: " << nop;
	auto message = util::str2msg(boost::str(boost::format(";:SENS1:SWE:POIN %1%\n") % nop));
	bus->send(message);
}

util::s_parts_t
Agilent506X::s_parts(void) const noexcept
{
	util::s_parts_t parts = {{1, 1}, {2, 1}};
	return parts;
}

void
Agilent506X::s_part(util::s_port_t from, util::s_port_t to)
{
	check_measurement_progress();
	if ((from != 1) || ((to != 1) && (to != 2))) {
		BOOST_LOG_TRIVIAL(debug) << "[Agilent506x] invalid port number";
		throw std::runtime_error("invalid port number");
	}
	auto lock    = bus->get_lock();
	auto message = util::str2msg(boost::str(boost::format(";:CALC1:PAR1:DEF S%1%%2%\n") % to % from));
	bus->send(message);
}

util::s_part_t
Agilent506X::s_part(void)
{
	check_measurement_progress();
	auto lock = bus->get_lock();
	return _s_part();
}

util::f_t
Agilent506X::f_left(void)
{
	check_measurement_progress();
	auto lock = bus->get_lock();
	return _f_left();
}

util::f_lim_t
Agilent506X::f_left(util::f_t left)
{
	{
		check_measurement_progress();
		auto lock = bus->get_lock();
		BOOST_LOG_TRIVIAL(trace) << "[Agilent506X] set left frequency: " << left;
		auto message = util::str2msg(boost::str(boost::format(";:SENS1:FREQ:STAR %1%\n") % left));
		bus->send(message);
	}
	return f_all();
}

util::f_t
Agilent506X::f_right(void)
{
	check_measurement_progress();
	auto lock = bus->get_lock();
	BOOST_LOG_TRIVIAL(trace) << "[Agilent506X] right frequency query";
	auto message = util::str2msg(";:SENS1:FREQ:STOP?\n");
	bus->send(message);
	bus->rcv(message, 64);
	auto f_string = std::string(message.begin(), message.end());
	boost::algorithm::trim_all_if(f_string, is_whitespacish);
	auto f = util::f_t(boost::lexical_cast<util::meas_t>(f_string));
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] right frequency: " << f;
	return f;
}

util::f_lim_t
Agilent506X::f_right(util::f_t right)
{
	{
		check_measurement_progress();
		auto lock = bus->get_lock();
		BOOST_LOG_TRIVIAL(trace) << "[Agilent506X] set right frequency: " << right;
		auto message = util::str2msg(boost::str(boost::format(";:SENS1:FREQ:STOP %1%\n") % right));
		bus->send(message);
	}
	return f_all();
}

util::f_t
Agilent506X::f_center(void)
{
	check_measurement_progress();
	auto lock = bus->get_lock();
	BOOST_LOG_TRIVIAL(trace) << "[Agilent506X] center frequency query";
	auto message = util::str2msg(";:SENS1:FREQ:CENT?\n");
	bus->send(message);
	bus->rcv(message, 64);
	auto f_string = std::string(message.begin(), message.end());
	boost::algorithm::trim_all_if(f_string, is_whitespacish);
	auto f = util::f_t(boost::lexical_cast<util::meas_t>(f_string));
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] center frequency: " << f;
	return f;
}

util::f_lim_t
Agilent506X::f_center(util::f_t center)
{
	{
		check_measurement_progress();
		auto lock = bus->get_lock();
		BOOST_LOG_TRIVIAL(trace) << "[Agilent506X] set center frequency: " << center;
		auto message = util::str2msg(boost::str(boost::format(";:SENS1:FREQ:CENT %1%\n") % center));
		bus->send(message);
	}
	return f_all();
}

util::f_t
Agilent506X::f_span(void)
{
	check_measurement_progress();
	auto lock = bus->get_lock();
	return _f_span();
}

util::f_lim_t
Agilent506X::f_span(util::f_t span)
{
	{
		check_measurement_progress();
		auto lock = bus->get_lock();
		BOOST_LOG_TRIVIAL(trace) << "[Agilent506X] set frequency span: " << span;
		auto message = util::str2msg(boost::str(boost::format(";:SENS1:FREQ:SPAN %1%\n") % span));
		bus->send(message);
	}
	return f_all();
}

util::f_lim_t
Agilent506X::f_all(util::f_t left, util::f_t right, util::f_t, util::f_t)
{
	f_left(left);
	f_right(right);
	return f_all();
}

util::f_lim_t
Agilent506X::f_all(void)
{
	auto f_l = f_left();
	auto f_r = f_right();
	auto f_c = f_center();
	auto f_s = f_span();
	return util::f_lim_t(f_l, f_r, f_c, f_s);
}

void
Agilent506X::interrupt_measure(util::EventQueue<analyzer_event> &event_queue)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] interrupt measure";
	auto message = util::str2msg(";*CLS\n");
	this->bus->send(message);
	event_queue.push(std::make_tuple(Event::INTERRUPTED, "interrupted"));
	measurement_in_progress = false;
}

void
Agilent506X::measure(void)
{
	check_measurement_progress();
	auto nop = number_of_points();
	if ((nop * sizeof(double) * 2) > (ANALYZER_MAX_MEASUREMENT_KIB * 1024)) {
		BOOST_LOG_TRIVIAL(warning) << "[Agileng87XX] requested measurement size exceeds limit " STR(
		    ANALYZER_MAX_MEASUREMENT_KIB) " KiB";
		throw std::runtime_error(
		    "requested measurement size exceeds limit " STR(ANALYZER_MAX_MEASUREMENT_KIB) " KiB");
	}
	auto lock = bus->get_lock();
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] start measure";
	auto message = util::str2msg(";:INIT1\n;:TRIG:SING\n;*OPC\n");
	this->bus->send(message);
	measurement_in_progress = true;
}

bool
Agilent506X::measuring(void)
{
	return measurement_in_progress;
}

void
Agilent506X::measurements(result::Scattering &result)
{
	auto res_lock = result.get_lock();
	auto lock     = bus->get_lock();
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] process data buffer";
	auto nop    = _number_of_points();
	auto left   = _f_left();
	auto f_step = _f_span() / (nop - 1);
	auto part   = _s_part();
	BOOST_LOG_TRIVIAL(trace) << "[Agilent506X] nop: " << nop << " left: " << left << " f_step: " << f_step
				 << " size: " << data_buffer.size();
	std::span<Agilent506XData> points(reinterpret_cast<Agilent506XData *>(data_buffer.data()), nop);
	util::f_array_t		  &f_points    = result.get_f_buffer();
	util::meas_array_t	  &s_re_points = result.get_s_re_buffer();
	util::meas_array_t	  &s_im_points = result.get_s_im_buffer();
	result.scattering_type = std::get<0>(part) == std::get<1>(part) ? result::ScatteringType::Reflection
									: result::ScatteringType::Transmission;
	f_points.resize(points.size());
	s_re_points.resize(points.size());
	s_im_points.resize(points.size());
	BOOST_LOG_TRIVIAL(debug) << "[Agilent506X] size: " << points.size();
#pragma omp parallel for
	for (size_t i = 0; i < points.size(); ++i) {
		f_points[i]    = left + f_step * i;
		s_re_points[i] = points[i].re.value();
		s_im_points[i] = points[i].im.value();
	}
	result.process();
}

} // namespace analyzer

// GCOVR_EXCL_STOP
#endif // ANALYZER_AGILENT506X
