#include <config.h>
#ifdef ANALYZER_AGILENT87XX

/** @file agilent_87xx.cxx
 * @brief analyzer::Agilent87xx analyzer::Analyzer implementation.
 */

// GCOVR_EXCL_START do not test hardware driver

#include "agilent_87xx.hxx"

#include <boost/algorithm/string/trim_all.hpp>
#include <boost/endian/buffers.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/log/trivial.hpp>
#include <span>

namespace analyzer
{

namespace endian = boost::endian;

static const size_t DATA_CHUNK_SIZE = 1 << 10; ///< Data chunk transfer size.

/**
 * @brief Representation of the Agilent87XX measurement point.
 */
struct Agilent87XXData {
	endian::big_float64_buf_t re; ///< Real part.
	endian::big_float64_buf_t im; ///< Imaginary part.
};
static_assert(sizeof(Agilent87XXData) == 16);

/**
 * @brief Agilent87XX status register byte.
 */
enum StatusByte {
	REVERSE_GET = (1 << 0), ///< Not available.
	FORWARD_GET = (1 << 1), ///< Not available.
	CHECK_ESRB  = (1 << 2), ///< One of the enabled bits in event status register B has been set.
	CHECK_ERROR = (1 << 3), ///< An error has occurred and the message has been placed in the error queue, but has
				///< not been read yet.
	MESSAGE_READY	= (1 << 4), ///< A command has prepared information to be output, but it has not been read yet.
	CHECK_ESR	= (1 << 5), ///< One of the enabled bits in the event-status register has been set.
	REQUEST_SERVICE = (1 << 6), ///< One of the enabled status-byte bits is causing an SRQ.
	PRESET		= (1 << 7), ///< An instrument preset has been executed.
};

/**
 * @brief Agilent87XX event status register byte.
 */
enum EventStatusRegister {
	OPC		= (1 << 0), ///< A command for which OPC has been enabled has completed operation.
	REQUEST_CONTROL = (1 << 1), ///< The analyzer has been commanded to perform an operation that requires
				    ///< control of a peripheral, and needs control of GPIB. Requires pass-control
				    ///< mode.
	QUERY_ERROR = (1 << 2),	    ///< The analyzer has been addressed to talk but there is nothing in the output
				    ///< queue to transmit.
	SEQUENCE_BIT	= (1 << 3), ///< A sequence has executed the assert SRQ command.
	EXECUTION_ERROR = (1 << 4), ///< A command was received that could not be executed.
	SYNTAX_ERROR	= (1 << 5), ///< The incoming GPIB commands contained a syntax error. The syntax error can
				    ///< only be cleared by a device clear or an instrument preset.
	USER_REQUEST = (1 << 6),    ///< The operator has pressed a front-panel key or turned the RPG.
	POWER_ON     = (1 << 7),    ///< A power-on sequence has occurred since the last read of the register.
};

/**
 * @brief Agilent87XX event status register B byte.
 */
enum EventStatusRegisterB {
	ACT_COMPLETE = (1 << 0), ///< A single sweep, group, or calibration step has been completed since the
				 ///< last read of the register.
	SERVICE_WAIT_DONE =
	    (1 << 1), ///< An internal service routine has completed operation, or is waiting for an operator response.
	DATA_ENTRY_COMPLETE = (1 << 2), ///< A terminator key has been pressed or a value entered over GPIB since the
					///< last read of the register.
	LIMIT_FAILED_CH2 = (1 << 3),	///< Limit test failed on Channel 2.
	LIMIT_FAILED_CH1 = (1 << 4),	///< Limit test failed on Channel 1.
	SEARCH_FAILED_CH2 =
	    (1 << 5), ///< A marker search was executed on Channel 2, but the target value was not found.
	SEARCH_FAILED_CH1 =
	    (1 << 6),		     ///< A marker search was executed on Channel 1, but the target value was not found.
	COPY_COMPLETE	 = (1 << 7), ///< A copy has been completed since the last read of the register.
	LIMIT_FAILED_CH4 = (1 << 8), ///< Limit test failed on Channel 4.
	LIMIT_FAILED_CH3 = (1 << 9), ///< Limit test failed on Channel 3.
	SEARCH_FAILED_CH4 =
	    (1 << 10), ///< A marker search was executed on Channel 4, but the target value was not found.
	SEARCH_FAILED_CH3 =
	    (1 << 11), ///< A marker search was executed on Channel 3, but the target value was not found.
};

const util::name_list_t Agilent87XX::SUPPORTED_BUSSES = {
#ifdef BUS_GPIB
    util::string_t(bus::GPIB::NAME),
#endif
};

/**
 * @brief Test char is whitespace or non-printable.
 *
 * @param ch Char to be tested.
 * @return Char is whitespace or non-printable.
 */
static bool
is_whitespacish(const char ch)
{
	return ch <= ' ' || ch > '~';
}

const util::attribute_t _attribute_choices = {
    {"template", "agilent87xx"},
    {"ifbw", {{"choices", {10, 30, 100, 300, 1000, 3000, 3700, 6000}}, {"value", 3700}}},
    {"power", {{"min", -20}, {"max", 5}, {"step", 1}, {"value", 0}}},
    {"averaging", {{"min", 0}, {"max", 1000}, {"step", 1}, {"value", 0}}},
    {"smoothing", {{"min", 0}, {"max", 20}, {"step", 0.05}, {"value", 0}}},
    {"beep", {{"value", true}}},
    {"display", {{"value", true}}},
};

const util::attribute_t &
Agilent87XX::attribute_choices(void) noexcept
{
	return _attribute_choices;
}

void
Agilent87XX::patch_bus_attributes(util::attribute_t &attributes) noexcept
{
	attributes.as_object()["status_byte_mask"] = StatusByte::CHECK_ESR;
	attributes.as_object()["send_eoi"]	   = true;
	attributes.as_object()["eos_reos"]	   = false;
	attributes.as_object()["eos_xeos"]	   = false;
	attributes.as_object()["eos_bin"]	   = false;
}

void
Agilent87XX::check_measurement_progress(void)
{
	if (measurement_in_progress)
		throw std::runtime_error("measurement is in progress");
}

void
Agilent87XX::status_byte(std::uint8_t value)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] set config status byte enable: " << std::hex << std::showbase
				 << std::nouppercase << value;
	std::string str = boost::str(boost::format(";SRE%1%") % int(value));
	util::msg_t message(str.begin(), str.end());
	bus->send(message);
}

std::uint8_t
Agilent87XX::status_byte(void)
{
	BOOST_LOG_TRIVIAL(trace) << "[Agilent87XX] status byte query";
	std::string str = "*STB?";
	util::msg_t message(str.begin(), str.end());
	bus->send(message);
	bus->rcv(message, 64);
	auto status_string = std::string(message.begin(), message.end());
	boost::algorithm::trim_all_if(status_string, is_whitespacish);
	auto status = int(boost::lexical_cast<std::uint16_t>(status_string));
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] status byte: " << std::hex << std::showbase << std::nouppercase
				 << int(status);
	return status;
}

void
Agilent87XX::esr(std::uint8_t value)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] set event status enable: " << std::hex << std::showbase
				 << std::nouppercase << value;
	std::string str = boost::str(boost::format(";ESE%1%") % int(value));
	util::msg_t message(str.begin(), str.end());
	bus->send(message);
}

std::uint8_t
Agilent87XX::esr(void)
{
	BOOST_LOG_TRIVIAL(trace) << "[Agilent87XX] event register query";
	std::string str = ";*ESR?";
	util::msg_t message(str.begin(), str.end());
	bus->send(message);
	bus->rcv(message, 64);
	auto status_string = std::string(message.begin(), message.end());
	boost::algorithm::trim_all_if(status_string, is_whitespacish);
	auto status = boost::lexical_cast<std::uint16_t>(status_string);
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] event register: " << std::hex << std::showbase << std::nouppercase
				 << int(status);
	return status;
}

void
Agilent87XX::esrb(std::uint16_t value)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] set event status B enable: " << std::hex << std::showbase
				 << std::nouppercase << value;
	std::string str = boost::str(boost::format(";ESNB%1%") % value);
	util::msg_t message(str.begin(), str.end());
	bus->send(message);
}

std::uint16_t
Agilent87XX::esrb(void)
{
	BOOST_LOG_TRIVIAL(trace) << "[Agilent87XX] event register B query";
	std::string str = ";*ESB?";
	util::msg_t message(str.begin(), str.end());
	bus->send(message);
	bus->rcv(message, 64);
	auto status_string = std::string(message.begin(), message.end());
	boost::algorithm::trim_all_if(status_string, is_whitespacish);
	auto status = int(boost::lexical_cast<std::uint16_t>(status_string));
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] event register B: " << std::hex << std::showbase << std::nouppercase
				 << int(status);
	return status;
}

void
Agilent87XX::coax(void)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] select coax";
	std::string str = ";ADPTCOAX";
	util::msg_t message(str.begin(), str.end());
	bus->send(message);
}

void
Agilent87XX::channel(std::uint8_t ch)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] select channel: " << int(ch);
	std::string str = boost::str(boost::format(";CHAN%1%") % int(ch));
	util::msg_t message(str.begin(), str.end());
	bus->send(message);
}

void
Agilent87XX::averaging(std::uint16_t value)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] averaging: " << value;
	{
		std::string str = boost::str(boost::format(";AVERO%1%") % (value > 0 ? "ON" : "OFF"));
		util::msg_t message(str.begin(), str.end());
		bus->send(message);
	}
	if (value > 0) {
		std::string str = boost::str(boost::format(";AVERFACT%1%") % value);
		util::msg_t message(str.begin(), str.end());
		bus->send(message);
	}
}

void
Agilent87XX::reset_averaging(void)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] reset averaging";
	auto message = util::str2msg(";AVERREST");
	bus->send(message);
}

void
Agilent87XX::blank_display(bool value)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] blank display: " << value;
	auto message = util::str2msg(boost::str(boost::format(";BLAD%1%") % (value ? "ON" : "OFF")));
	bus->send(message);
}

void
Agilent87XX::smoothing(double value)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] smoothing: " << value;
	{
		auto message = util::str2msg(boost::str(boost::format(";SMOOO%1%") % (value >= 0.05 ? "ON" : "OFF")));
		bus->send(message);
	}
	if (value >= 0.05) {
		auto message = util::str2msg(boost::str(boost::format(";SMOOAPER%1$.3f") % value));
		bus->send(message);
	}
}

void
Agilent87XX::power(std::int8_t value)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] power: " << int(value);
	auto message = util::str2msg(boost::str(boost::format(";POWE%1%") % int(value)));
	bus->send(message);
}

void
Agilent87XX::beep(bool value)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] beep: " << value;
	auto message =
	    util::str2msg(boost::str(boost::format(";BEEPDONE%1%;BEEPWARN%1%;BEEPFAIL%1%") % (value ? "ON" : "OFF")));
	bus->send(message);
}

void
Agilent87XX::release(void)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] release";
	auto message = util::str2msg(";KEY42");
	bus->send(message);
}

void
Agilent87XX::integration_bandwidth(std::uint16_t value)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] integration bandwidth: " << value;
	auto message = util::str2msg(boost::str(boost::format(";IFBW%1%") % value));
	bus->send(message);
}

void
Agilent87XX::number_form(void)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] data form: double";
	auto message = util::str2msg(";FORM3");
	bus->send(message);
}

void
Agilent87XX::hold_mode(bool enable)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] hold mode: " << enable;
	auto message = util::str2msg(boost::str(boost::format(";%1%") % (enable ? "HOLD" : "CONT")));
	bus->send(message);
}

void
Agilent87XX::data_format(void)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] Smith chart mode";
	auto message = util::str2msg(";SMIC");
	bus->send(message);
}

void
Agilent87XX::conversion(void)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] conversion off";
	auto message = util::str2msg(";CONVOFF");
	bus->send(message);
}

util::nop_t
Agilent87XX::_number_of_points(void)
{
	BOOST_LOG_TRIVIAL(trace) << "[Agilent87XX] number of points query";
	auto message = util::str2msg(";POIN?");
	bus->send(message);
	bus->rcv(message, 64);
	auto nop_string = std::string(message.begin(), message.end());
	boost::algorithm::trim_all_if(nop_string, is_whitespacish);
	auto nop = int(boost::lexical_cast<util::meas_t>(nop_string));
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] number of points: " << nop;
	return nop;
}

util::f_t
Agilent87XX::_f_left(void)
{
	BOOST_LOG_TRIVIAL(trace) << "[Agilent87XX] left frequency query";
	auto message = util::str2msg(";STAR?");
	bus->send(message);
	bus->rcv(message, 64);
	auto f_string = std::string(message.begin(), message.end());
	boost::algorithm::trim_all_if(f_string, is_whitespacish);
	auto f = util::f_t(boost::lexical_cast<util::meas_t>(f_string));
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] left frequency: " << f;
	return f;
}

util::f_t
Agilent87XX::_f_span(void)
{
	BOOST_LOG_TRIVIAL(trace) << "[Agilent87XX] frequency span query";
	auto message = util::str2msg(";SPAN?");
	bus->send(message);
	bus->rcv(message, 64);
	auto f_string = std::string(message.begin(), message.end());
	boost::algorithm::trim_all_if(f_string, is_whitespacish);
	auto f = util::f_t(boost::lexical_cast<util::meas_t>(f_string));
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] frequency span: " << f;
	return f;
}

util::s_part_t
Agilent87XX::_s_part(void)
{
	bool refl, trans;
	{
		auto message = util::str2msg(";TRAP?");
		bus->send(message);
		bus->rcv(message, 64);
		if (message.size() == 0) {
			BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] cannot get measurement part";
			throw std::runtime_error("cannot get measurement part");
		}
		switch (message[0]) {
		case '0':
			trans = false;
			break;
		case '1':
			trans = true;
			break;
		default:
			BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] unexpected measurement part response";
			throw std::runtime_error("unexpected measurement part response");
		}
	}
	{
		auto message = util::str2msg(";RFLP?");
		bus->send(message);
		bus->rcv(message, 64);
		if (message.size() == 0) {
			BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] cannot get measurement part";
			throw std::runtime_error("cannot get measurement part");
		}
		switch (message[0]) {
		case '0':
			refl = false;
			break;
		case '1':
			refl = true;
			break;
		default:
			BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] unexpected measurement part response";
			throw std::runtime_error("unexpected measurement part response");
		}
	}
	if (trans == refl) {
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] inconsistent measurement part";
		throw std::runtime_error("inconsistent measurement part");
	}
	return util::s_part_t(refl ? 1 : 2, 1);
}

void
Agilent87XX::check_esrb(util::EventQueue<analyzer_event> &) noexcept
{
	auto lock = bus->get_lock();
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] check ESRB";
	std::int16_t val;
	try {
		val = esrb();
	} catch (const std::exception &err) {
		BOOST_LOG_TRIVIAL(warning) << "[Agilent87XX] error during ESRB check: " << err.what();
		return;
	}
	if (val & EventStatusRegisterB::ACT_COMPLETE)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] action complete";
	if (val & EventStatusRegisterB::COPY_COMPLETE)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] copy complete";
	if (val & EventStatusRegisterB::DATA_ENTRY_COMPLETE)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] data entry complete";
	if (val & EventStatusRegisterB::LIMIT_FAILED_CH1)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] limit failed on channel 1";
	if (val & EventStatusRegisterB::LIMIT_FAILED_CH2)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] limit failed on channel 2";
	if (val & EventStatusRegisterB::LIMIT_FAILED_CH3)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] limit failed on channel 3";
	if (val & EventStatusRegisterB::LIMIT_FAILED_CH4)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] limit failed on channel 4";
	if (val & EventStatusRegisterB::SEARCH_FAILED_CH1)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] search failed on channel 1";
	if (val & EventStatusRegisterB::SEARCH_FAILED_CH2)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] search failed on channel 2";
	if (val & EventStatusRegisterB::SEARCH_FAILED_CH3)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] search failed on channel 3";
	if (val & EventStatusRegisterB::SEARCH_FAILED_CH4)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] search failed on channel 4";
	if (val & EventStatusRegisterB::SERVICE_WAIT_DONE)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] service action wait";
}

void
Agilent87XX::check_esr(util::EventQueue<analyzer_event> &event_queue) noexcept
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] check ESR";
	std::uint8_t val;
	try {
		val = esr();
	} catch (const std::exception &err) {
		BOOST_LOG_TRIVIAL(warning) << "[Agilent87XX] error during ESR check: " << err.what();
		return;
	}
	if (val & EventStatusRegister::OPC) {
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] OPC command complete";
		try {
			auto nop       = _number_of_points();
			auto data_size = nop * 2 * sizeof(double);
			auto message   = util::str2msg(";OUTPFORM");
			bus->send(message);
			BOOST_LOG_TRIVIAL(trace) << "[Agilent87XX] read size: " << data_size;
			bus->rcv(message, 4);
			if ((message[0] != '#') || (message[1] != 'A')) {
				BOOST_LOG_TRIVIAL(warning) << "[Agilent87XX] wrong data header";
			} else {
				std::uint16_t size = message[2] << 8 | message[3];
				BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] reported data size: " << size
							 << "; expected data size: " << data_size;
				if (size != data_size) {
					BOOST_LOG_TRIVIAL(warning) << "[Agilent87XX] unexpected data size";
					event_queue.push(std::make_tuple(Event::ERROR, "unexpected data size"));
				} else {
					std::size_t offset = 0;
					while (data_size > 0) {
						util::msg_t chunk;
						auto	    chunk_size = data_size % DATA_CHUNK_SIZE;
						chunk_size	       = chunk_size == 0 ? DATA_CHUNK_SIZE : chunk_size;
						chunk.reserve(chunk_size);
						bus->rcv(chunk, chunk_size);
						std::copy(chunk.begin(), chunk.end(), data_buffer.begin() + offset);
						offset += chunk_size;
						data_size -= chunk_size;
					}
					event_queue.push(std::make_tuple(Event::MEASURE_COMPLETE, "measure complete"));
				}
			}
		} catch (const std::exception &err) {
			BOOST_LOG_TRIVIAL(warning) << "[Agilent87XX] error during data read: " << err.what();
			return;
		}
		measurement_in_progress = false;
	}
	if (val & EventStatusRegister::EXECUTION_ERROR) {
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] execution error";
		event_queue.push(std::make_tuple(Event::ERROR, "execution error"));
	}
	if (val & EventStatusRegister::POWER_ON)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] power on";
	if (val & EventStatusRegister::QUERY_ERROR)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] query error";
	if (val & EventStatusRegister::REQUEST_CONTROL)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] request control";
	if (val & EventStatusRegister::SEQUENCE_BIT)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] sequence bit";
	if (val & EventStatusRegister::SYNTAX_ERROR)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] syntax error";
	if (val & EventStatusRegister::USER_REQUEST)
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] user request";
}

void
Agilent87XX::flush_errors(void) noexcept
{
	bool error_flag = true;
	while (error_flag) {
		auto message = util::str2msg(";OUTPERRO");
		try {
			bus->send(message);
			bus->rcv(message, 64);
		} catch (const std::exception &err) {
			BOOST_LOG_TRIVIAL(warning) << "[Agilent87XX] error during error flush: " << err.what();
			return;
		}
		if (message.size() > 0) {
			std::string error(message.begin(), message.end());
			boost::algorithm::trim_all_if(error, is_whitespacish);
			BOOST_LOG_TRIVIAL(info) << "[Agilent87XX] error: " << error;
		}
		try {
			bus->check_interrupt(message, 1);
			error_flag = message[1] & StatusByte::CHECK_ERROR;
		} catch (const std::exception &err) {
			BOOST_LOG_TRIVIAL(warning) << "[Agilent87XX] interrupt check error: " << err.what();
		}
	}
}

Agilent87XX::Agilent87XX(util::attribute_t &&attributes, std::shared_ptr<bus::Bus> bus)
    : Analyzer(std::move(attributes), bus)
{
	BOOST_LOG_TRIVIAL(trace) << "[Agilent87XX] attributes " << attributes;
	auto lock = this->bus->get_lock();
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] query identity";
	{
		auto message = util::str2msg(";IDN?");
		this->bus->send(message);
		this->bus->rcv(message, 64);
		if (message.size() == 0) {
			BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] cannot get device identity";
			throw std::runtime_error("cannot get device identity");
		}
		auto identity = std::string(message.begin(), message.end());
		boost::algorithm::trim_all_if(identity, is_whitespacish);
		this->attributes.as_object()["identity"] = identity;
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] identity: " << identity;
		if ((identity.find("8719ET") == std::string::npos) && (identity.find("8720ET") == std::string::npos) &&
		    (identity.find("8722ET") == std::string::npos) && (identity.find("8753ET") == std::string::npos)) {
			BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] unknown device identity";
			throw std::runtime_error("unknown device identity");
		}
	}
	{
		auto message = util::str2msg(";OUTPSERN");
		this->bus->send(message);
		this->bus->rcv(message, 64);
		auto serial = std::string(message.begin(), message.end());
		boost::algorithm::trim_all_if(serial, is_whitespacish);
		this->attributes.as_object()["serial"] = serial;
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] serial: " << serial;
	}
	channel(1);
	coax();
	conversion();
	averaging(attributes.as_object()["averaging"].as_int64());
	blank_display(!attributes.as_object()["display"].as_bool());
	if (attributes.as_object()["smoothing"].is_int64())
		smoothing(attributes.as_object()["smoothing"].as_int64());
	else
		smoothing(attributes.as_object()["smoothing"].as_double());
	power(attributes.as_object()["power"].as_int64());
	beep(attributes.as_object()["beep"].as_bool());
	integration_bandwidth(attributes.as_object()["ifbw"].as_int64());
	number_form();
	data_format();
	hold_mode(true);
	status_byte(StatusByte::CHECK_ESR | StatusByte::MESSAGE_READY);
	esr(EventStatusRegister::OPC);
	esrb(0);
	flush_errors();
}

Agilent87XX::~Agilent87XX()
{
	try {
		hold_mode(false);
		blank_display(false);
		release();
	} catch (const std::exception &err) {
		BOOST_LOG_TRIVIAL(warning) << "[Agilent87XX] release device";
	}
	BOOST_LOG_TRIVIAL(trace) << "[Agilent87XX] free analyzer";
}

static const util::string_t _name = util::string_t(Agilent87XX::NAME); ///< Analyzer name.

const util::string_t &
Agilent87XX::name(void) const noexcept
{
	return _name;
}

bool
Agilent87XX::interrupted(void) noexcept
{
	auto lock = bus->get_lock();
	return bus->interrupted();
}

void
Agilent87XX::check_interrupt(util::EventQueue<analyzer_event> &event_queue) noexcept
{
	auto lock = bus->get_lock();
	BOOST_LOG_TRIVIAL(trace) << "[Agilent87XX] check interrupt";
	util::msg_t message;
	try {
		bus->check_interrupt(message, 1);
	} catch (const std::exception &err) {
		BOOST_LOG_TRIVIAL(warning) << "[Agilent87XX] interrupt check error: " << err.what();
	}
	if (message[0] & StatusByte::REVERSE_GET) {
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] unexpected status bit set";
	}
	if (message[0] & StatusByte::FORWARD_GET) {
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] unexpected status bit set";
	}
	if (message[0] & StatusByte::CHECK_ESRB) {
		check_esrb(event_queue);
	}
	if (message[0] & StatusByte::CHECK_ERROR) {
		flush_errors();
	}
	if (message[0] & StatusByte::CHECK_ESR) {
		check_esr(event_queue);
	}
	if (message[0] & StatusByte::PRESET) {
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] preset status bit set";
	}
}

util::nop_vals_t
Agilent87XX::number_of_points_values(void) const noexcept
{
	util::nop_vals_t vals = {3, 11, 21, 26, 51, 101, 201, 401, 801, 1601};
	return vals;
}

util::nop_t
Agilent87XX::number_of_points(void)
{
	check_measurement_progress();
	auto lock = bus->get_lock();
	return _number_of_points();
}

void
Agilent87XX::number_of_points(util::nop_t nop)
{
	check_measurement_progress();
	auto lock = bus->get_lock();
	BOOST_LOG_TRIVIAL(trace) << "[Agilent87XX] set number of points: " << nop;
	auto message = util::str2msg(boost::str(boost::format(";POIN%1%") % nop));
	bus->send(message);
}

util::s_parts_t
Agilent87XX::s_parts(void) const noexcept
{
	util::s_parts_t parts = {{1, 1}, {2, 1}};
	return parts;
}

void
Agilent87XX::s_part(util::s_port_t from, util::s_port_t to)
{
	check_measurement_progress();
	if ((from != 1) || ((to != 1) && (to != 2))) {
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87xx] invalid port number";
		throw std::runtime_error("invalid port number");
	}
	auto lock = bus->get_lock();
	switch (to) {
	case 1: {
		auto message = util::str2msg(";RFLP");
		bus->send(message);
		break;
	}
	case 2: {
		auto message = util::str2msg(";TRAP");
		bus->send(message);
		break;
	}
	default:
		BOOST_LOG_TRIVIAL(debug) << "[Agilent87xx] invalid port number";
		throw std::runtime_error("invalid port number");
		break;
	}
}

util::s_part_t
Agilent87XX::s_part(void)
{
	check_measurement_progress();
	auto lock = bus->get_lock();
	return _s_part();
}

util::f_t
Agilent87XX::f_left(void)
{
	check_measurement_progress();
	auto lock = bus->get_lock();
	return _f_left();
}

util::f_lim_t
Agilent87XX::f_left(util::f_t left)
{
	{
		check_measurement_progress();
		auto lock = bus->get_lock();
		BOOST_LOG_TRIVIAL(trace) << "[Agilent87XX] set left frequency: " << left;
		auto message = util::str2msg(boost::str(boost::format(";STAR%1%HZ") % left));
		bus->send(message);
	}
	return f_all();
}

util::f_t
Agilent87XX::f_right(void)
{
	check_measurement_progress();
	auto lock = bus->get_lock();
	BOOST_LOG_TRIVIAL(trace) << "[Agilent87XX] right frequency query";
	auto message = util::str2msg(";STOP?");
	bus->send(message);
	bus->rcv(message, 64);
	auto f_string = std::string(message.begin(), message.end());
	boost::algorithm::trim_all_if(f_string, is_whitespacish);
	auto f = util::f_t(boost::lexical_cast<util::meas_t>(f_string));
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] right frequency: " << f;
	return f;
}

util::f_lim_t
Agilent87XX::f_right(util::f_t right)
{
	{
		check_measurement_progress();
		auto lock = bus->get_lock();
		BOOST_LOG_TRIVIAL(trace) << "[Agilent87XX] set right frequency: " << right;
		auto message = util::str2msg(boost::str(boost::format(";STOP%1%HZ") % right));
		bus->send(message);
	}
	return f_all();
}

util::f_t
Agilent87XX::f_center(void)
{
	check_measurement_progress();
	auto lock = bus->get_lock();
	BOOST_LOG_TRIVIAL(trace) << "[Agilent87XX] center frequency query";
	auto message = util::str2msg(";CENT?");
	bus->send(message);
	bus->rcv(message, 64);
	auto f_string = std::string(message.begin(), message.end());
	boost::algorithm::trim_all_if(f_string, is_whitespacish);
	auto f = util::f_t(boost::lexical_cast<util::meas_t>(f_string));
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] center frequency: " << f;
	return f;
}

util::f_lim_t
Agilent87XX::f_center(util::f_t center)
{
	{
		check_measurement_progress();
		auto lock = bus->get_lock();
		BOOST_LOG_TRIVIAL(trace) << "[Agilent87XX] set center frequency: " << center;
		auto message = util::str2msg(boost::str(boost::format(";CENT%1%HZ") % center));
		bus->send(message);
	}
	return f_all();
}

util::f_t
Agilent87XX::f_span(void)
{
	check_measurement_progress();
	auto lock = bus->get_lock();
	return _f_span();
}

util::f_lim_t
Agilent87XX::f_span(util::f_t span)
{
	{
		check_measurement_progress();
		auto lock = bus->get_lock();
		BOOST_LOG_TRIVIAL(trace) << "[Agilent87XX] set frequency span: " << span;
		auto message = util::str2msg(boost::str(boost::format(";SPAN%1%HZ") % span));
		bus->send(message);
	}
	return f_all();
}

util::f_lim_t
Agilent87XX::f_all(util::f_t left, util::f_t right, util::f_t, util::f_t)
{
	f_left(left);
	f_right(right);
	return f_all();
}

util::f_lim_t
Agilent87XX::f_all(void)
{
	auto f_l = f_left();
	auto f_r = f_right();
	auto f_c = f_center();
	auto f_s = f_span();
	return util::f_lim_t(f_l, f_r, f_c, f_s);
}

void
Agilent87XX::interrupt_measure(util::EventQueue<analyzer_event> &event_queue)
{
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] interrupt measure";
	auto message = util::str2msg(";*CLS");
	this->bus->send(message);
	event_queue.push(std::make_tuple(Event::INTERRUPTED, "interrupted"));
	measurement_in_progress = false;
}

void
Agilent87XX::measure(void)
{
	check_measurement_progress();
	auto nop = number_of_points();
	if ((nop * sizeof(double) * 2) > (ANALYZER_MAX_MEASUREMENT_KIB * 1024)) {
		BOOST_LOG_TRIVIAL(warning) << "[Agileng87XX] requested measurement size exceeds limit " STR(
		    ANALYZER_MAX_MEASUREMENT_KIB) " KiB";
		throw std::runtime_error(
		    "requested measurement size exceeds limit " STR(ANALYZER_MAX_MEASUREMENT_KIB) " KiB");
	}
	auto lock = bus->get_lock();
	data_buffer.resize(nop * sizeof(Agilent87XXData));
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] start measure";
	auto message = util::str2msg(";OPC;SING");
	this->bus->send(message);
	measurement_in_progress = true;
}

bool
Agilent87XX::measuring(void)
{
	return measurement_in_progress;
}

void
Agilent87XX::measurements(result::Scattering &result)
{
	auto res_lock = result.get_lock();
	auto lock     = bus->get_lock();
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] process data buffer";
	auto nop    = _number_of_points();
	auto left   = _f_left();
	auto f_step = _f_span() / (nop - 1);
	auto part   = _s_part();
	BOOST_LOG_TRIVIAL(trace) << "[Agilent87XX] nop: " << nop << " left: " << left << " f_step: " << f_step
				 << " size: " << data_buffer.size();
	std::span<Agilent87XXData> points(reinterpret_cast<Agilent87XXData *>(data_buffer.data()), nop);
	util::f_array_t		  &f_points    = result.get_f_buffer();
	util::meas_array_t	  &s_re_points = result.get_s_re_buffer();
	util::meas_array_t	  &s_im_points = result.get_s_im_buffer();
	result.scattering_type = std::get<0>(part) == std::get<1>(part) ? result::ScatteringType::Reflection
									: result::ScatteringType::Transmission;
	f_points.resize(points.size());
	s_re_points.resize(points.size());
	s_im_points.resize(points.size());
	BOOST_LOG_TRIVIAL(debug) << "[Agilent87XX] size: " << points.size();
#pragma omp parallel for
	for (size_t i = 0; i < points.size(); ++i) {
		f_points[i]    = left + f_step * i;
		s_re_points[i] = points[i].re.value();
		s_im_points[i] = points[i].im.value();
	}
	result.process();
}

} // namespace analyzer

// GCOVR_EXCL_STOP
#endif // ANALYZER_AGILENT87XX
