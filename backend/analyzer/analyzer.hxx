/** @file analyzer.hxx
 * @brief Base analyzer::Analyzer declarations.
 */

#pragma once

#include <analyzer/event.hxx>
#include <bus/bus.hxx>
#include <bus/list.hxx>
#include <event_queue.hxx>
#include <result/scattering.hxx>
#include <types.hxx>

/** @namespace analyzer
 * @brief analyzer::Analyzer driver namespace.
 */
namespace analyzer
{

/**
 * @brief Analyzer class.
 */
class Analyzer
{
      protected:
	std::shared_ptr<bus::Bus> bus = nullptr; ///< Reference to bus::Bus used by the Analyzer.

      public:
	static constexpr const char   *NAME = "analyzer"; ///< Analyzer name.
	static const util::name_list_t SUPPORTED_BUSSES;  ///< List of supported bus::Bus drivers.
	util::attribute_t	       attributes;	  ///< Selected Analyzer attributes.
	/**
	 * @brief Bus attribute choices.
	 *
	 * @return Bus attribute choices.
	 */
	static const util::attribute_t &attribute_choices(void) noexcept;
	/**
	 * @brief Patch bus::Bus attributes.
	 *
	 * Prior to passing attributes to bus::Bus constructor, attributes are passed to @ref patch_bus_attributes()
	 * function. This allows passing driver specific configuration to bus. For example, this call may set interrupt
	 * bitmask for the bus or select a specific baud rate value.
	 *
	 * @param[in,out] attributes bus::Bus attributes.
	 */
	static void patch_bus_attributes(util::attribute_t &attributes) noexcept;

	/**
	 * @brief Analyzer constructor.
	 *
	 * @param[in] attributes Analyzer attributes.
	 * @param[in] bus bus::Bus reference.
	 */
	Analyzer(util::attribute_t &&attributes, std::shared_ptr<bus::Bus> bus);

	/**
	 * @brief Do not allow Analyzer copying.
	 */
	Analyzer(Analyzer &) = delete;

	/**
	 * @brief Do not allow Analyzer moving.
	 */
	Analyzer(Analyzer &&) = delete;

	/**
	 * @brief Analyzer destructor.
	 */
	virtual ~Analyzer();

	/**
	 * @brief Analyzer name.
	 *
	 * @return Analyzer name.
	 */
	virtual const util::string_t &name(void) const noexcept;

	/**
	 * @brief bus::Bus name.
	 *
	 * @return bus::Bus name.
	 */
	virtual const util::string_t bus_name(void) const;

	/**
	 * @brief bus::Bus attributes.
	 *
	 * @return bus::Bus attributes.
	 */
	util::attribute_t &bus_attributes(void) const;

	/**
	 * @brief Check if bus::Bus triggered interrupt.
	 *
	 * @return Interrupt triggered.
	 */
	virtual bool interrupted(void) noexcept = 0;

	/**
	 * @brief Check bus::Bus interrupt message.
	 *
	 * Driver is expected to push @ref analyzer_event to \p events queue.
	 *
	 * @param[out] events Event queue.
	 */
	virtual void check_interrupt(util::EventQueue<analyzer_event> &events) noexcept = 0;

	/**
	 * @brief List supported number of measurement points.
	 *
	 * @return List number of points.
	 */
	virtual util::nop_vals_t number_of_points_values(void) const noexcept = 0;

	/**
	 * @brief Selected number of measurement points.
	 *
	 * @return Selected number of points.
	 */
	virtual util::nop_t number_of_points(void) = 0;

	/**
	 * @brief Select number of measurement points.
	 *
	 * @param[in] nop Select number of points.
	 */
	virtual void number_of_points(util::nop_t nop) = 0;

	/**
	 * @brief List of supported port connections.
	 *
	 * @return List supported port connections.
	 */
	virtual util::s_parts_t s_parts(void) const noexcept = 0;

	/**
	 * @brief Select port connection.
	 *
	 * @param[in] from Select port connection.
	 * @param[in] to Select port connection.
	 */
	virtual void s_part(util::s_port_t from, util::s_port_t to) = 0;

	/**
	 * @brief Selected port connection.
	 *
	 * @return Selected port connection.
	 */
	virtual util::s_part_t s_part(void) = 0;

	/**
	 * @brief Selected left frequency.
	 *
	 * @return Left frequency.
	 */
	virtual util::f_t f_left(void) = 0;

	/**
	 * @brief Selected left frequency.
	 *
	 * @param[in] left Left frequency.
	 * @return Frequency bounds.
	 */
	virtual util::f_lim_t f_left(util::f_t left) = 0;

	/**
	 * @brief Selected right frequency.
	 *
	 * @return Right frequency.
	 */
	virtual util::f_t f_right(void) = 0;

	/**
	 * @brief Selected right frequency.
	 *
	 * @param[in] right Right frequency.
	 * @return Frequency bounds.
	 */
	virtual util::f_lim_t f_right(util::f_t right) = 0;

	/**
	 * @brief Selected center frequency.
	 *
	 * @return Center frequency.
	 */
	virtual util::f_t f_center(void) = 0;

	/**
	 * @brief Selected center frequency.
	 *
	 * @param[in] center Center frequency.
	 * @return Frequency bounds.
	 */
	virtual util::f_lim_t f_center(util::f_t center) = 0;

	/**
	 * @brief Selected span frequency.
	 *
	 * @return Span frequency.
	 */
	virtual util::f_t f_span(void) = 0;

	/**
	 * @brief Selected span frequency.
	 *
	 * @param[in] span Span frequency.
	 * @return Frequency bounds.
	 */
	virtual util::f_lim_t f_span(util::f_t span) = 0;

	/**
	 * @brief Select bound frequency.
	 *
	 * @param[in] left Left frequency.
	 * @param[in] right Right frequency.
	 * @param[in] center Center frequency.
	 * @param[in] span Span frequency.
	 * @return Frequency bounds.
	 */
	virtual util::f_lim_t f_all(util::f_t left, util::f_t right, util::f_t center, util::f_t span) = 0;

	/**
	 * @brief Selected bound frequency.
	 *
	 * @return Frequency bounds.
	 */
	virtual util::f_lim_t f_all(void) = 0;

	/**
	 * @brief Do measurement.
	 */
	virtual void measure(void) = 0;

	/**
	 * @brief Measurement in progress.
	 *
	 * @return Status.
	 */
	virtual bool measuring(void) = 0;

	/**
	 * @brief Retrieve measurement data.
	 *
	 * @param[out] result Scattering data container.
	 */
	virtual void measurements(result::Scattering &result) = 0;

	/**
	 * @brief Interrupt measurement.
	 *
	 * @param[out] event_queue Scattering data container.
	 */
	virtual void interrupt_measure(util::EventQueue<analyzer_event> &event_queue);
};

} // namespace analyzer
