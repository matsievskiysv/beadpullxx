/** @file list.hxx
 * @brief analyzer::Analyzer list.
 */

#pragma once

#include "analyzer.hxx"

#include <config.h>
#include <types.hxx>
#ifdef ANALYZER_DUMMY
#include "dummy.hxx"
#endif
#ifdef ANALYZER_LITEVNA
#include "litevna.hxx"
#endif
#ifdef ANALYZER_AGILENT87XX
#include "agilent_87xx.hxx"
#endif
#ifdef ANALYZER_AGILENT506X
#include "agilent_506x.hxx"
#endif

namespace analyzer
{

extern const util::name_list_t analyzer_list; ///< List of compiled Analyzer drivers.

/**
 * @brief Supported bus::Bus drivers by Analyzer.
 *
 * @param[in] analyzer Analyzer name.
 * @return bus::Bus drivers list.
 */
const util::name_list_t &get_supported_busses(const util::string_t &analyzer);

/**
 * @brief Attribute choices for the Analyzer.
 *
 * @param[in] analyzer Analyzer name.
 * @return Analyzer attributes.
 */
const util::attribute_t &get_attribute_choices(const util::string_t &analyzer);

/**
 * @brief Create Analyzer.
 *
 * @param[in] analyzer Analyzer name.
 * @param[in] attributes Analyzer attributes.
 * @param[in] bus bus::Bus driver.
 * @return Analyzer pointer.
 * @throws std::runtime_error Any constructor errors.
 */
std::shared_ptr<Analyzer> create_analyzer(const util::string_t &analyzer, util::attribute_t &&attributes,
					  std::shared_ptr<bus::Bus> bus);

/**
 * @brief Patch \p attributes.
 *
 * @param[in] analyzer Analyzer name.
 * @param[in,out] attributes Analyzer attributes.
 * @throws std::runtime_error Any constructor errors.
 */
void patch_bus_attributes(const util::string_t &analyzer, util::attribute_t &attributes);

} // namespace analyzer
