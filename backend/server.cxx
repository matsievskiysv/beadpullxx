/** @file server.cxx
 * @brief Server related definitions.
 */

#include "server.hxx"

#include <boost/filesystem.hpp>
#include <boost/json.hpp>
#include <config.h>
#include <endpoints/analyzer.hxx>
#include <endpoints/endpoints.hxx>
#include <endpoints/measure.hxx>
#include <endpoints/motor.hxx>
#include <fstream>
#include <tuple>

namespace json = boost::json;
namespace fs   = boost::filesystem;

const char USAGE[] =
    R"(BeadPull++.
Web user interface to the bead-pull test bench.
It uses stepper motor and vector analyzer to measure accelerating structure field distribution.

    Usage:
      beadpullxx --config=<config> <root_dir>
      beadpullxx (-h | --help)
      beadpullxx (-v | --version)

    Options:
      -h --help          Show this screen.
      -v --version       Show version.
      --config=<config>  YAML configuration file.
      <root_dir>         Directory with static server files.
)";

const std::map<const std::string, const std::string> mime_types{
    {".css", "text/css"},	 {".html", "text/html"},	{".ico", "image/vnd.microsoft.icon"},
    {".js", "text/javascript"},	 {".json", "application/json"}, {".map", "application/json"},
    {".pdf", "application/pdf"}, {".svg", "image/svg+xml"},	{".woff", "application/x-font-woff"}};

const std::map<const std::string, const logging::trivial::severity_level> log_levels{
    {"trace", boost::log::trivial::trace}, {"debug", boost::log::trivial::debug},
    {"info", boost::log::trivial::info},   {"warning", boost::log::trivial::warning},
    {"error", boost::log::trivial::error}, {"fatal", boost::log::trivial::fatal}};

std::string
create_simple_page(const char *title, const char *message)
{
	const std::string str1 = "<!doctype html><html><head><title>";
	const std::string str2 = "</title><meta charset=\"utf-8\"/></head><body><h1>";
	const std::string str3 = "</h1></body>";
	const std::string str4 = "</html>";
	return str1 + title + str2 + title + str3 + message + str4;
}

bool
send_chunked(uWS::HttpResponse<true> *res, std::shared_ptr<util::Chunked> chunked)
{
	while (true) {
		auto [changed, end]	= chunked->compress_chunk();
		auto		  chunk = chunked->get_chunk();
		const std::string buffer_view(chunk.begin(), chunk.end());
		if (end) {
			BOOST_LOG_TRIVIAL(trace) << "Sending final chunk of size " << chunk.size();
			auto [ok, done] = res->tryEnd(buffer_view);
			if (!ok) {
				BOOST_LOG_TRIVIAL(debug) << "Pausing sending due to backpressure";
				return false;
			}
			if (done)
				return true;
			return false;
		} else {
			BOOST_LOG_TRIVIAL(trace) << "Sending chunk of size " << chunk.size();
			if (!res->write(buffer_view)) {
				BOOST_LOG_TRIVIAL(debug) << "Pausing sending due to backpressure";
				return true;
			}
		}
	}
}

void
serve_file(const boost::filesystem::path &root_dir_path, uWS::HttpResponse<true> *res, uWS::HttpRequest *req)
{
	try {
		fs::path file_path = root_dir_path / fs::path(req->getUrl().begin(), req->getUrl().end());
		if (!fs::is_regular_file(file_path)) {
			BOOST_LOG_TRIVIAL(debug) << "File not found";
			res->cork([&]() {
				res->writeStatus("404 Not Found");
				res->writeHeader("Content-Type", "text/html");
				res->end(create_simple_page("Not found", "Page not found"));
			});
			return;
		}
		if (mime_types.count(file_path.extension().string()) == 0) {
			BOOST_LOG_TRIVIAL(debug) << "Unsupported mime type";
			res->cork([&]() {
				res->writeStatus("403 Forbidden");
				res->writeHeader("Content-Type", "text/html");
				res->end(create_simple_page("Forbidden", "File type is not supported"));
			});
			return;
		}
		auto file_stream  = util::Chunked::stream_t(new std::ifstream(file_path));
		bool compress	  = req->getHeader("accept-encoding").find("deflate") != std::string::npos;
		auto chunked_file = std::shared_ptr<util::Chunked>(
		    new util::Chunked(std::move(file_stream), SEND_CHUNK_SIZE, compress));
		res->cork([&]() {
			BOOST_LOG_TRIVIAL(debug) << "Serving " << file_path.extension() << " file";
			res->writeStatus(uWS::HTTP_200_OK);
			res->writeHeader("Content-Type", mime_types.find(file_path.extension().string())->second);
			if (compress)
				res->writeHeader("Content-Encoding", "deflate");
		});
		res->onWritable([=](auto) { return send_chunked(res, chunked_file); });
		res->onAborted([]() { BOOST_LOG_TRIVIAL(debug) << "Connection aborted"; });
		send_chunked(res, chunked_file);
	} catch (const std::exception &err) {
		BOOST_LOG_TRIVIAL(error) << err.what();
		res->cork([&]() {
			res->writeStatus("500 Internal Server Error");
			res->writeHeader("Content-Type", "text/html");
			res->end(create_simple_page("Server error", err.what()));
		});
		return;
	}
}

uWS::App
redirect_app(const YAML::Node &config)
{
	return uWS::App()
	    .addServerName(config["server"]["name"].as<std::string>())
	    .any("/*",
		 [&](auto *res, auto *req) {
			 std::string url  = std::string{req->getHeader("host")};
			 std::string host = url.substr(0, url.find(":"));
			 if (config["server"]["proxy"]["https"].as<int>() != 443) {
				 host += ":";
				 host += config["server"]["proxy"]["https"].as<std::string>();
			 }
			 res->writeStatus("301 Moved Permanently");
			 res->writeHeader("Location", "https://" + host + "/index.html");
			 res->end();
		 })
	    .listen(config["server"]["port"]["http"].as<int>(), [&](auto *listen_socket) {
		    if (listen_socket) {
			    BOOST_LOG_TRIVIAL(info) << "Listening on HTTP port: " << config["server"]["port"]["http"];
		    } else {
			    BOOST_LOG_TRIVIAL(error)
				<< "Cannot bind to HTTP port: " << config["server"]["port"]["http"];
			    std::terminate();
		    }
	    });
}

uWS::SSLApp
main_app(const YAML::Node &config)
{
	uWS::SSLApp app = uWS::SSLApp({.key_file_name  = config["server"]["ssl"]["key"].as<std::string>().c_str(),
				       .cert_file_name = config["server"]["ssl"]["cert"].as<std::string>().c_str()});
	app.addServerName(config["server"]["name"].as<std::string>());

	return app;
}

/**
 * @brief Register server endpoint.
 *
 * @note Should be used in context of configure_main_app().
 * @param method HTTP method.
 * @param uri Endpoint address.
 */
#define ENDPOINT(method, uri)                                                                                          \
	do {                                                                                                           \
		app.method("/api/" #uri,                                                                               \
			   [&](auto *res, auto *req) { endpoint::method##_##uri(res, req, ctrl, auth); });             \
	} while (0)

void
configure_main_app(uWS::SSLApp &app, controller::Controller &ctrl, const YAML::Node &config,
		   const boost::filesystem::path &root_dir_path, const std::string &auth)
{
	struct PerSocketData {
	};

	app.get("/", [](auto *res, auto *req) {
		BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
		res->writeStatus("301 Moved Permanently");
		res->writeHeader("Location", "/index.html");
		res->end();
	});
	app.get("/libs/*", [&](auto *res, auto *req) {
		BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] lib " << req->getUrl();
		serve_file(root_dir_path, res, req);
	});
	app.get("/static/*", [&](auto *res, auto *req) {
		BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] static " << req->getUrl();
		serve_file(root_dir_path, res, req);
	});
	app.get("/*", [&](auto *res, auto *req) {
		BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
		CHECK_AUTHORIZED();
		serve_file(root_dir_path, res, req);
	});

	ENDPOINT(get, bus_attribute_choices);

	// analyzer
	ENDPOINT(get, analyzer_list);
	ENDPOINT(get, analyzer_bus_list);
	ENDPOINT(get, analyzer_attribute_choices);
	ENDPOINT(get, analyzer_attributes);
	ENDPOINT(get, analyzer_bus_attributes);
	ENDPOINT(get, analyzer_capabilities);
	ENDPOINT(post, analyzer);
	ENDPOINT(get, analyzer);
	ENDPOINT(del, analyzer);
	ENDPOINT(get, analyzer_bus);
	ENDPOINT(get, nop_values);
	ENDPOINT(get, nop);
	ENDPOINT(post, nop);
	ENDPOINT(get, s_parts);
	ENDPOINT(get, s_part);
	ENDPOINT(post, s_part);
	ENDPOINT(get, f_left);
	ENDPOINT(post, f_left);
	ENDPOINT(get, f_right);
	ENDPOINT(post, f_right);
	ENDPOINT(get, f_center);
	ENDPOINT(post, f_center);
	ENDPOINT(get, f_span);
	ENDPOINT(post, f_span);
	ENDPOINT(get, f_all);
	ENDPOINT(post, f_all);

	// motor
	ENDPOINT(get, motor_list);
	ENDPOINT(get, motor_bus_list);
	ENDPOINT(get, motor_attribute_choices);
	ENDPOINT(get, motor_attributes);
	ENDPOINT(get, motor_bus_attributes);
	ENDPOINT(get, motor_capabilities);
	ENDPOINT(post, motor);
	ENDPOINT(get, motor);
	ENDPOINT(get, motor_bus);
	ENDPOINT(del, motor);
	ENDPOINT(get, speed);
	ENDPOINT(post, speed);
	ENDPOINT(get, return_speed);
	ENDPOINT(post, return_speed);
	ENDPOINT(get, acceleration);
	ENDPOINT(post, acceleration);
	ENDPOINT(get, deceleration);
	ENDPOINT(post, deceleration);
	ENDPOINT(get, steppers);
	ENDPOINT(post, steppers);
	ENDPOINT(get, position);
	ENDPOINT(post, move_to);
	ENDPOINT(post, move_by);
	ENDPOINT(post, set_origin);
	ENDPOINT(post, stop);
	ENDPOINT(get, reverse);
	ENDPOINT(post, reverse);
	ENDPOINT(get, position_multiplier);
	ENDPOINT(post, position_multiplier);
	ENDPOINT(get, speed_multiplier);
	ENDPOINT(post, speed_multiplier);
	ENDPOINT(get, acceleration_multiplier);
	ENDPOINT(post, acceleration_multiplier);

	// measure
	ENDPOINT(post, interrupt_measure_scattering);
	ENDPOINT(post, measure_scattering);
	ENDPOINT(get, measuring_scattering);
	ENDPOINT(get, scattering_measurements);
	ENDPOINT(get, measuring_field);
	ENDPOINT(post, measure_field);
	ENDPOINT(post, interrupt_measure_field);
	ENDPOINT(get, field_measurements);

	app.any("/*", [](auto *res, auto *req) {
		BOOST_LOG_TRIVIAL(info) << "[" << req->getMethod() << "] " << req->getUrl();
		res->writeStatus("405 Method Not Allowed");
		res->end();
	});

	app.ws<PerSocketData>(
	    "/*", {.compression	     = uWS::SHARED_COMPRESSOR,
		   .maxPayloadLength = 16 * 1024,
		   .idleTimeout	     = 10,
		   .maxBackpressure  = 1 * 1024 * 1024,
		   .upgrade =
		       [](auto *res, auto *req, auto *context) {
			       BOOST_LOG_TRIVIAL(debug) << "[ws] " << req->getUrl();
			       res->onAborted([=]() { BOOST_LOG_TRIVIAL(debug) << "[ws] aborted " << req->getUrl(); });
			       res->template upgrade<PerSocketData>(
				   {}, req->getHeader("sec-websocket-key"), req->getHeader("sec-websocket-protocol"),
				   req->getHeader("sec-websocket-extensions"), context);
		       },
		   .open = [](auto *) { BOOST_LOG_TRIVIAL(debug) << "[ws] open"; },
		   .message =
		       [](auto *ws, std::string_view message, uWS::OpCode) {
			       BOOST_LOG_TRIVIAL(debug) << "[ws] " << message;
			       try {
				       auto message_parsed = json::parse(message);
				       auto topics	   = message_parsed.as_object()["subscribe"].as_array();
				       for (auto topic : topics) {
					       auto topic_name = topic.as_string();
					       BOOST_LOG_TRIVIAL(info) << "[ws] subscribing to " << topic_name;
					       ws->subscribe(topic_name);
				       }
			       } catch (std::exception &err) {
				       BOOST_LOG_TRIVIAL(warning) << "[ws] " << err.what();
			       }
		       },
		   .subscription =
		       [](auto *, auto topic, auto new_cnt, auto old_cnt) {
			       BOOST_LOG_TRIVIAL(debug)
				   << "[ws] " << topic << " subscriber " << (new_cnt > old_cnt ? "added" : "removed");
		       },
		   .close = [](auto *, int, std::string_view) { BOOST_LOG_TRIVIAL(debug) << "[ws] closed"; }});

	app.listen(config["server"]["port"]["https"].as<int>(), [&](auto *listen_socket) {
		if (listen_socket) {
			BOOST_LOG_TRIVIAL(info) << "Listening on HTTPS port: " << config["server"]["port"]["https"];
		} else {
			BOOST_LOG_TRIVIAL(error) << "Cannot bind to HTTPS port: " << config["server"]["port"]["https"];
			std::terminate();
		}
	});
}
