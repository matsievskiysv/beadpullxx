/** @file motor.hxx
 * @brief Base @ref motor::Motor and related classes declarations.
 */

#pragma once

#include <bus/bus.hxx>
#include <bus/list.hxx>
#include <event_queue.hxx>
#include <motor/event.hxx>
#include <types.hxx>

/** @namespace motor
 * @brief motor::Motor driver namespace.
 */
namespace motor
{

/**
 * @brief Motor class.
 */
class Motor
{
      protected:
	std::shared_ptr<bus::Bus> bus = nullptr; ///< Reference to bus::Bus used by the Motor.

      public:
	static constexpr const char   *NAME = "motor";	 ///< Motor name.
	static const util::name_list_t SUPPORTED_BUSSES; ///< List of supported bus::Bus drivers.
	util::attribute_t	       attributes;	 ///< Selected Motor attributes.
	/**
	 * @brief Bus attribute choices.
	 *
	 * @return Bus attribute choices.
	 */
	static const util::attribute_t &attribute_choices(void) noexcept;
	/**
	 * @brief Patch bus::Bus attributes.
	 *
	 * Prior to passing attributes to bus::Bus constructor, attributes are passed to \ref patch_bus_attributes()
	 * function. This allows passing driver specific configuration to bus. For example, this call may set interrupt
	 * bitmask for the bus or select a specific baud rate value.
	 *
	 * @param[in,out] attributes bus::Bus attributes.
	 */
	static void patch_bus_attributes(util::attribute_t &attributes) noexcept;

	double position_multiplier     = 1.0; ///< Motor position multiplier.
	double speed_multiplier	       = 1.0; ///< Motor speed multiplier.
	double acceleration_multiplier = 1.0; ///< Motor acceleration multiplier.

	/**
	 * @brief Motor constructor.
	 *
	 * @param[in] attributes Motor attributes.
	 * @param[in] bus bus::Bus reference.
	 */
	Motor(util::attribute_t &&attributes, std::shared_ptr<bus::Bus> bus);

	/**
	 * @brief Do not allow Motor copying.
	 */
	Motor(Motor &) = delete;

	/**
	 * @brief Do not allow Motor moving.
	 */
	Motor(Motor &&) = delete;

	/**
	 * @brief Motor destructor.
	 */
	virtual ~Motor();

	/**
	 * @brief Motor name.
	 *
	 * @return Motor name.
	 */
	virtual const util::string_t &name(void) const noexcept;

	/**
	 * @brief bus::Bus name.
	 *
	 * @return bus::Bus name.
	 */
	virtual const util::string_t &bus_name(void) const noexcept;

	/**
	 * @brief bus::Bus attributes.
	 *
	 * @return bus::Bus attributes.
	 */
	util::attribute_t &bus_attributes(void) noexcept;

	/**
	 * @brief Check if bus::Bus triggered interrupt.
	 *
	 * @return Interrupt triggered.
	 */
	virtual bool interrupted(void) noexcept = 0;

	/**
	 * @brief Check bus::Bus interrupt message.
	 *
	 * Driver is expected to push \ref motor_event to \p events queue.
	 *
	 * @param[out] events Event queue.
	 */
	virtual void check_interrupt(util::EventQueue<motor_event> &events) noexcept = 0;

	/**
	 * @brief Set origin to current position.
	 */
	virtual void set_origin(void) = 0;

	/**
	 * @brief Move body to \p new_position.
	 *
	 * @throws std::runtime_error Steppers powered off.
	 * @param[in] new_position New motor::Motor position.
	 * @param[in] return_speed Use return speed value for faster return.
	 */
	virtual void move_to(util::pos_t new_position, bool return_speed = false) = 0;

	/**
	 * @brief Move body by the \p position_increment.
	 *
	 * @throws std::runtime_error Steppers powered off.
	 * @param[in] position_increment New motor::Motor position.
	 * @param[in] return_speed Use return speed value for faster return.
	 */
	virtual void move_by(util::pos_t position_increment, bool return_speed = false) = 0;

	/**
	 * @brief Get current position.
	 *
	 * @return motor::Motor position.
	 */
	virtual util::pos_t position(void) = 0;

	/**
	 * @brief Stop motor::Motor immediately.
	 *
	 * @param[out] events Event queue.
	 */
	virtual void stop(util::EventQueue<motor_event> &events) = 0;

	/**
	 * @brief Set reverse Motor flag.
	 *
	 * @param[in] val Reverse flag value.
	 */
	virtual void reverse(bool val) = 0;

	/**
	 * @brief Get reverse Motor flag.
	 *
	 * @return Reverse flag value.
	 */
	virtual bool reverse(void) = 0;

	/**
	 * @brief Check if driver stepper shutdown supported.
	 *
	 * @return Stepper shutdown supported.
	 */
	virtual bool steppers_shutdown_supported(void) const noexcept;

	/**
	 * @brief Set steppers power state.
	 *
	 * @throws runtime_error If stepper shutdown not supported.
	 * @param[in] enable Stepper shutdown state.
	 */
	virtual void steppers(bool enable);

	/**
	 * @brief Get steppers power state.
	 *
	 * @throws runtime_error If stepper shutdown not supported.
	 * @return Stepper shutdown state.
	 */
	virtual bool steppers(void);

	/**
	 * @brief Check if driver speed control supported.
	 *
	 * @return Speed control supported.
	 */
	virtual bool speed_supported(void) const noexcept;

	/**
	 * @brief Set Motor speed.
	 *
	 * @throws runtime_error If speed control not supported.
	 * @param[in] new_speed New speed value.
	 */
	virtual void speed(util::speed_t new_speed);

	/**
	 * @brief Get Motor speed.
	 *
	 * @throws runtime_error If speed control not supported.
	 * @return Speed value.
	 */
	virtual util::speed_t speed(void);

	/**
	 * @brief Set Motor return speed.
	 *
	 * @throws runtime_error If speed control not supported.
	 * @param[in] new_speed New speed value.
	 */
	virtual void return_speed(util::speed_t new_speed);

	/**
	 * @brief Get Motor return speed.
	 *
	 * @throws runtime_error If speed control not supported.
	 * @return Speed value.
	 */
	virtual util::speed_t return_speed(void);

	/**
	 * @brief Check if driver acceleration control supported.
	 *
	 * @return Acceleration control supported.
	 */
	virtual bool acceleration_supported(void) const noexcept;

	/**
	 * @brief Set Motor acceleration.
	 *
	 * @throws runtime_error If acceleration control not supported.
	 * @param[in] new_acceleration New acceleration value.
	 */
	virtual void acceleration(util::acceleration_t new_acceleration);

	/**
	 * @brief Get Motor acceleration.
	 *
	 * @throws runtime_error If speed control not supported.
	 * @return Acceleration value.
	 */
	virtual util::acceleration_t acceleration(void);

	/**
	 * @brief Check if driver deceleration control supported.
	 *
	 * @return Deceleration control supported.
	 */
	virtual bool deceleration_supported(void) const noexcept;

	/**
	 * @brief Set Motor deceleration.
	 *
	 * @throws runtime_error If acceleration control not supported.
	 * @param[in] new_deceleration New deceleration value.
	 */
	virtual void deceleration(util::acceleration_t new_deceleration);

	/**
	 * @brief Get Motor deceleration.
	 *
	 * @throws runtime_error If speed control not supported.
	 * @return Deceleration value.
	 */
	virtual util::acceleration_t deceleration(void);
};

} // namespace motor
