#include <config.h>
#ifdef MOTOR_UIROBOT

/** @file uirobot.cxx
 * @brief motor::UIRobot implementation.
 */

// GCOVR_EXCL_START do not test hardware driver

#include "uirobot.hxx"

#include <boost/format.hpp>
#include <boost/log/trivial.hpp>

namespace json = boost::json;

/**
 * @brief Convert 3 byte message to word.
 *
 * @param b1 Byte one.
 * @param b2 Byte two.
 * @param b3 Byte three.
 * @return message word.
 */
static std::uint16_t
bytes2word(std::uint8_t b1, std::uint8_t b2, std::uint8_t b3)
{
	return ((std::uint16_t(b1) & 0x3) << 14) | ((std::uint16_t(b2) & 0x7f) << 7) | (std::uint16_t(b3) & 0x7f);
}

/**
 * @brief Convert 5 byte message to word.
 *
 * @param b1 Byte one.
 * @param b2 Byte two.
 * @param b3 Byte three.
 * @param b4 Byte three.
 * @param b5 Byte three.
 * @return message word.
 */
static std::uint32_t
bytes2word(std::uint8_t b1, std::uint8_t b2, std::uint8_t b3, std::uint8_t b4, std::uint8_t b5)
{
	return ((std::uint16_t(b1) & 0xf) << 28) | ((std::uint16_t(b2) & 0x7f) << 21) |
	       ((std::uint16_t(b3) & 0x7f) << 14) | ((std::uint16_t(b4) & 0x7f) << 7) | (std::uint16_t(b5) & 0x7f);
}

namespace motor
{

const util::name_list_t UIRobot::SUPPORTED_BUSSES = {
#ifdef BUS_SERIAL
    util::string_t(bus::Serial::NAME),
#endif
};

static util::attribute_t _attribute_choices = {
    {"template", "uirobot"},
    {"new_baudrate", {{"choices", {9600, 19200, 57600}}, {"value", 57600}}},
    {"reconfigure", {{"value", false}}},
    {"current", {{"min", 0.0}, {"max", 8.0}, {"step", 0.1}, {"value", 1.0}}},
    {"auto_current", {{"min", 0}, {"max", 99}, {"step", 1}, {"value", 0}}},
    {"microstepping", {{"choices", {1, 2, 4, 8, 16}}, {"value", 1}}},
    {"max_start", {{"min", 0}, {"max", 65000000}, {"step", 1}, {"value", 0}}},
    {"max_stop", {{"min", 0}, {"max", 65000000}, {"step", 1}, {"value", 0}}},
#if 0
	    // NOTE: not supported for firmware version 800 //
	    {"backlash", {{"min", 0}, {"max", 65535}, {"step", 1}, {"value", 0}}},
#endif
}; ///< Motor attribute choices.

util::attribute_t &
UIRobot::attribute_choices(void)
{
	return _attribute_choices;
}

UIRobot::UIRobot(util::attribute_t &&attributes, std::shared_ptr<bus::Bus> bus) : Motor(std::move(attributes), bus)
{
	BOOST_LOG_TRIVIAL(trace) << "[UIRobot] attributes " << attributes;
	auto lock = this->bus->get_lock();
	this->bus->flush_input();
	BOOST_LOG_TRIVIAL(debug) << "[UIRobot] query model";
	auto message = util::str2msg("MDL;");
	this->bus->send(message);
	this->bus->flush_output();
	this->bus->rcv(message, 11);
	// NOTE: according to the manual message[4] == 0x02 //
	if (!((message.size() == 11) && (message[0] == 0xcc) && (message[1] == 0x00) && (message[2] == 0xde) &&
	      (message[3] == 0x18) && (message[10] == 0xff))) {
		BOOST_LOG_TRIVIAL(debug) << "[UIRobot] model query failed";
		throw std::runtime_error("model query failed");
	}
	// NOTE: according to the manual maxcurrent = message[5] / 10.0 //
	auto max_current = message[5];
	BOOST_LOG_TRIVIAL(debug) << "[UIRobot] max current: " << max_current;
	this->attributes.as_object()["max_current"] = max_current;
	// NOTE: this code was tested with version 800 //
	fw_version = bytes2word(message[7], message[8], message[9]);
	BOOST_LOG_TRIVIAL(debug) << "[UIRobot] firmware version: " << fw_version;
	this->attributes.as_object()["fw_version"] = fw_version;
	bool q_enc				   = message[6] & (1 << 6);
	BOOST_LOG_TRIVIAL(debug) << "[UIRobot] quadrature encoding: " << q_enc;
	this->attributes.as_object()["q_enc"] = q_enc;
	bool closed_loop		      = message[6] & (1 << 5);
	BOOST_LOG_TRIVIAL(debug) << "[UIRobot] closed loop: " << closed_loop;
	this->attributes.as_object()["closed_loop"] = closed_loop;
	bool adv_motion				    = message[6] & (1 << 4);
	BOOST_LOG_TRIVIAL(debug) << "[UIRobot] advanced motion: " << adv_motion;
	this->attributes.as_object()["adv_motion"] = adv_motion;
	json::array sensors;
	for (int i = 0; i < 4; ++i) {
		bool val = message[6] & (1 << i);
		sensors.push_back(val);
	}
	BOOST_LOG_TRIVIAL(debug) << "[UIRobot] sensors: " << sensors;
	this->attributes.as_object()["sensors"] = sensors;
	if (attributes.as_object()["reconfigure"].as_bool()) {
		auto baud = attributes.as_object()["new_baudrate"].as_int64();
		BOOST_LOG_TRIVIAL(debug) << "[UIRobot] configure baudrate: " << baud;
		auto message = util::str2msg(boost::str(boost::format("BDR%1%;") % baud));
		this->bus->send(message);
		this->bus->flush_output();
		this->bus->rcv(message, 4);
		if ((message.size() == 4) && (message[0] == 0xaa) && (message[2] == 0xbd) && (message[3] == 0xff)) {
			BOOST_LOG_TRIVIAL(info) << "[UIRobot] reconfigured baudrate: " << baud;
			throw std::runtime_error("reconnect with baudrate " + std::to_string(baud));
		} else {
			BOOST_LOG_TRIVIAL(warning) << "[UIRobot] baudrate reconfigure failed";
			throw std::runtime_error("baudrate reconfigure failed");
		}
	}
	{
		std::uint16_t cmd = (1 << 10)	/* Advanced Motion Control Mode */
				    | (1 << 4); /* Displacement Control (STP/POS/QEC) Completion RTCN */
		auto message = util::str2msg(boost::str(boost::format("MCF%1%;") % cmd));
		this->bus->send(message);
		this->bus->flush_output();
		this->bus->rcv(message, 7);
		if (!((message.size() == 7) && (message[0] == 0xaa) && (message[1] == 0x00) && (message[2] == 0xb0) &&
		      (message[6] == 0xff))) {
			BOOST_LOG_TRIVIAL(warning) << "[UIRobot] master register configuration failed";
			throw std::runtime_error("master register configuration failed");
		}
	}
	{
		auto microsteps = attributes.as_object()["microstepping"].as_int64();
		BOOST_LOG_TRIVIAL(debug) << "[UIRobot] configure micro steps: " << microsteps;
		auto message = util::str2msg(boost::str(boost::format("MCS%1%;") % microsteps));
		this->bus->send(message);
		this->bus->flush_output();
		this->bus->rcv(message, 13);
		if (!((message.size() == 13) && (message[0] == 0xaa) && (message[1] == 0x00) &&
		      ((message[2] & 0xf) == microsteps - 1) && (message[12] == 0xff))) {
			BOOST_LOG_TRIVIAL(warning) << "[UIRobot] micro step configuration failed";
			throw std::runtime_error("micro step configuration failed");
		}
	}
	{
		auto current = attributes.as_object()["auto_current"].as_int64();
		BOOST_LOG_TRIVIAL(debug) << "[UIRobot] auto current reduction: " << int(current);
		auto message = util::str2msg(boost::str(boost::format("ACR%1%;") % current));
		this->bus->send(message);
		this->bus->flush_output();
		this->bus->rcv(message, 13);
		if (!((message.size() == 13) && (message[0] == 0xaa) && (message[1] == 0x00) &&
		      (!!(message[2] & (1 << 6)) == (current != 0)) && (message[12] == 0xff))) {
			BOOST_LOG_TRIVIAL(warning) << "[UIRobot] auto current reduction configuration failed";
			throw std::runtime_error("auto current reduction configuration failed");
		}
	}
	{
		std::uint16_t current = 0;
		if (attributes.as_object()["current"].is_double()) {
			current = std::uint16_t(attributes.as_object()["current"].as_double()) * 10;
		} else {
			current = attributes.as_object()["current"].as_int64() * 10;
		}
		BOOST_LOG_TRIVIAL(debug) << "[UIRobot] current limit: " << int(current);
		auto message = util::str2msg(boost::str(boost::format("CUR%1%;") % current));
		this->bus->send(message);
		this->bus->flush_output();
		this->bus->rcv(message, 13);
		if (!((message.size() == 13) && (message[0] == 0xaa) && (message[1] == 0x00) &&
		      ((message[3] & 0x3f) == current) && (message[12] == 0xff))) {
			BOOST_LOG_TRIVIAL(warning) << "[UIRobot] current limit configuration failed";
			throw std::runtime_error("current limit configuration failed");
		}
	}
	{
		auto speed = attributes.as_object()["max_start"].as_int64();
		BOOST_LOG_TRIVIAL(debug) << "[UIRobot] start speed: " << speed;
		auto message = util::str2msg(boost::str(boost::format("MMS%1%;") % speed));
		this->bus->send(message);
		this->bus->flush_output();
		this->bus->rcv(message, 7);
		if (!((message.size() == 7) && (message[0] == 0xaa) && (message[1] == 0x00) && (message[2] == 0xb3) &&
		      (bytes2word(message[3], message[4], message[5]) == speed) && (message[6] == 0xff))) {
			BOOST_LOG_TRIVIAL(warning) << "[UIRobot] start speed configuration failed";
			throw std::runtime_error("start speed configuration failed");
		}
	}
	{
		auto speed = attributes.as_object()["max_stop"].as_int64();
		BOOST_LOG_TRIVIAL(debug) << "[UIRobot] stop speed: " << speed;
		auto message = util::str2msg(boost::str(boost::format("MMD%1%;") % speed));
		this->bus->send(message);
		this->bus->flush_output();
		this->bus->rcv(message, 7);
		if (!((message.size() == 7) && (message[0] == 0xaa) && (message[1] == 0x00) && (message[2] == 0xb4) &&
		      (bytes2word(message[3], message[4], message[5]) == speed) && (message[6] == 0xff))) {
			BOOST_LOG_TRIVIAL(warning) << "[UIRobot] stop speed configuration failed";
			throw std::runtime_error("stop speed configuration failed");
		}
	}
	BOOST_LOG_TRIVIAL(debug) << "[UIRobot] set initial acceleration";

#if 0
		// NOTE: not supported for firmware version 800 //
		{
			auto	    backlash   = attributes.as_object()["backlash"].as_int64();
			BOOST_LOG_TRIVIAL(debug) << "[UIRobot] backlash: " << backlash;
			auto message = util::str2msg(boost::str(boost::format("BLC%1%;") % backlash));
			this->bus->send(message);
			this->bus->flush_output();
			this->bus->rcv(message, 7);
			if (!((message.size() == 7) && (message[0] == 0xaa) && (message[1] == 0x00) &&
			      (message[2] == 0xde) && (bytes2word(message[3], message[4], message[5]) == backlash) &&
			      (message[6] == 0xff))) {
				BOOST_LOG_TRIVIAL(warning) << "[UIRobot] backlash configuration failed";
				throw std::runtime_error("backlash configuration failed");
			}
		}
#endif
}

UIRobot::~UIRobot()
{
	try {
		steppers(false);
	} catch (const std::exception &err) {
		BOOST_LOG_TRIVIAL(warning) << "[UIRobot] cannot disable steppers";
	}
	BOOST_LOG_TRIVIAL(debug) << "[UIRobot] free motor";
}

static const util::string_t _name = util::string_t(UIRobot::NAME); ///< Motor name.

const util::string_t &
UIRobot::name(void) const noexcept
{
	return _name;
}

bool
UIRobot::interrupted(void) noexcept
{
	auto lock = bus->get_lock();
	return bus->interrupted();
}

void
UIRobot::check_interrupt(util::EventQueue<motor_event> &event_queue) noexcept
{
	auto	    lock = bus->get_lock();
	util::msg_t message;
	try {
		bus->check_interrupt(message, 4);
	} catch (const bus::Timeout &) {
	} catch (const std::exception &err) {
		BOOST_LOG_TRIVIAL(warning) << "[UIRobot] exception during interrupt receive " << err.what();
		return;
	}
	bus->flush_input();
	if ((message.size() == 4) && (message[0] == 0xcc) && (message[1] == 0x0) && (message[3] == 0xff)) {
		switch (message[2]) {
		case 0xa0:
		case 0xa1:
			BOOST_LOG_TRIVIAL(warning) << "[UIRobot] event motor outside limits";
			event_queue.push(std::make_tuple(Event::ERROR, "motor outside limits"));
			break;
		case 0xa2:
		case 0xa3:
			BOOST_LOG_TRIVIAL(warning) << "[UIRobot] event message ignored";
			break;
		case 0xa8:
		case 0xa9:
			BOOST_LOG_TRIVIAL(debug) << "[UIRobot] event position update";
			event_queue.push(std::make_tuple(Event::STOPPED, "position update"));
			break;
		default:
			BOOST_LOG_TRIVIAL(warning) << "[UIRobot] unknown event";
		}
	} else {
		BOOST_LOG_TRIVIAL(warning) << "[UIRobot] unexpected event message";
	}
}

void
UIRobot::set_origin(void)
{
	auto lock = bus->get_lock();
	bus->flush_input();
	auto message = util::str2msg("ORG0;");
	BOOST_LOG_TRIVIAL(debug) << "[UIRobot] set origin";
	bus->send(message);
	bus->flush_output();
	bus->rcv(message, 9);
	if (!((message.size() == 9) && (message[0] == 0xcc) && (message[1] == 0x00) && (message[2] == 0xb0) &&
	      (message[3] == 0x00) && (message[4] == 0x00) && (message[5] == 0x00) && (message[6] == 0x00) &&
	      (message[7] == 0x00) && (message[8] == 0xff))) {
		BOOST_LOG_TRIVIAL(warning) << "[UIRobot] origin set failed";
		throw std::runtime_error("origin set failed");
	}
};

void
UIRobot::move_to(util::pos_t new_position, bool return_speed)
{
	if (!steppers())
		throw std::runtime_error("steppers disabled");
	auto lock = bus->get_lock();
	bus->flush_input();
	auto	    speed = static_cast<int>(((return_speed ? return_speed_value : speed_value)) * speed_multiplier);
	auto	    pos	  = static_cast<int>(new_position * position_multiplier * (reverse_value ? -1 : 1));
	auto message = util::str2msg(boost::str(boost::format("{SPD%1%;POS%2%;};") % speed % pos));
	BOOST_LOG_TRIVIAL(trace) << "[UIRobot] move to: " << new_position << "(" << pos << ") with speed "
				 << (return_speed ? return_speed_value : speed_value) << "(" << speed << ")";
	bus->send(message);
	bus->flush_output();
	bus->rcv(message, 13);
	if (!((message.size() == 13) && (message[0] == 0xaa) && (message[1] == 0x00) && (message[12] == 0xff))) {
		BOOST_LOG_TRIVIAL(warning) << "[UIRobot] move to position failed";
		throw std::runtime_error("move to position failed");
	}
}

void
UIRobot::move_by(util::pos_t position_increment, bool return_speed)
{
	if (!steppers())
		throw std::runtime_error("steppers disabled");
	auto lock = bus->get_lock();
	bus->flush_input();
	auto	    speed = static_cast<int>(((return_speed ? return_speed_value : speed_value)) * speed_multiplier);
	auto	    pos	  = static_cast<int>(position_increment * position_multiplier * (reverse_value ? -1 : 1));
	auto message = util::str2msg(boost::str(boost::format("{SPD%1%;STP%2%;};") % speed % pos));
	BOOST_LOG_TRIVIAL(trace) << "[UIRobot] move by: " << position_increment << "(" << pos << ") with speed "
				 << speed_value << "(" << speed << ")";
	bus->send(message);
	bus->flush_output();
	bus->rcv(message, 13);
	if (!((message.size() == 13) && (message[0] == 0xaa) && (message[1] == 0x00) && (message[12] == 0xff))) {
		BOOST_LOG_TRIVIAL(warning) << "[UIRobot] move to position failed";
		throw std::runtime_error("move to position failed");
	}
}

util::pos_t
UIRobot::position(void)
{
	auto lock = bus->get_lock();
	bus->flush_input();
	auto message = util::str2msg("POS;");
	bus->send(message);
	bus->flush_output();
	bus->rcv(message, 9);
	if (!((message.size() == 9) && (message[0] == 0xcc) && (message[1] == 0x00) && (message[2] == 0xb0) &&
	      (message[8] == 0xff))) {
		BOOST_LOG_TRIVIAL(warning) << "[UIRobot] position query failed";
		throw std::runtime_error("position query failed");
	}
	auto pos_internal = util::pos_t(bytes2word(message[3], message[4], message[5], message[6], message[7]));
	auto pos	  = util::pos_t((reverse_value ? -1 : 1) * pos_internal / position_multiplier);
	BOOST_LOG_TRIVIAL(trace) << "[UIRobot] position: " << pos << "(" << pos_internal << ")";
	return pos;
}

void
UIRobot::stop(util::EventQueue<motor_event> &)
{
	if (!steppers())
		throw std::runtime_error("steppers disabled");
	auto lock = bus->get_lock();
	bus->flush_input();
	auto message = util::str2msg("SPD0;");
	BOOST_LOG_TRIVIAL(debug) << "[UIRobot] stop";
	bus->send(message);
	bus->flush_output();
	bus->rcv(message, 7);
	if (!((message.size() == 7) && (message[0] == 0xaa) && (message[1] == 0x00) && (message[2] == 0xb5) &&
	      (message[3] == 0x0) && (message[4] == 0x0) && (message[5] == 0x0) && (message[6] == 0xff))) {
		BOOST_LOG_TRIVIAL(warning) << "[UIRobot] stop failed";
		throw std::runtime_error("stop failed");
	}
}

void
UIRobot::reverse(bool val)
{
	reverse_value = val;
}

bool
UIRobot::reverse(void)
{
	return reverse_value;
}

bool
UIRobot::steppers_shutdown_supported(void) const noexcept
{
	return true;
}

void
UIRobot::steppers(bool enable)
{
	auto lock = bus->get_lock();
	bus->flush_input();
	auto message = util::str2msg(boost::str(boost::format("%1%;") % (enable ? "ENA" : "OFF")));
	BOOST_LOG_TRIVIAL(trace) << "[UIRobot] steppers: " << enable;
	bus->send(message);
	bus->flush_output();
	bus->rcv(message, 13);
	if (!((message.size() == 13) && (message[0] == 0xaa) && (message[1] == 0x00) &&
	      (!!(message[2] & (1 << 5)) == enable) && (message[12] == 0xff))) {
		BOOST_LOG_TRIVIAL(warning) << "[UIRobot] steppers configuration failed";
		throw std::runtime_error("steppers configuration failed");
	}
}

bool
UIRobot::steppers(void)
{
	auto lock = bus->get_lock();
	bus->flush_input();
	auto message = util::str2msg("FBK;");
	bus->send(message);
	bus->flush_output();
	bus->rcv(message, 13);
	if (!((message.size() == 13) && (message[0] == 0xcc) && (message[1] == 0x00) && (message[12] == 0xff))) {
		BOOST_LOG_TRIVIAL(warning) << "[UIRobot] steppers query failed";
		throw std::runtime_error("steppers query failed");
	}
	bool value = message[2] & (1 << 5);
	BOOST_LOG_TRIVIAL(trace) << "[UIRobot] steppers status: " << value;
	return value;
}

bool
UIRobot::speed_supported(void) const noexcept
{
	return true;
}

void
UIRobot::speed(util::speed_t new_speed)
{
	speed_value = new_speed;
}

util::speed_t
UIRobot::speed(void)
{
	return speed_value;
}

void
UIRobot::return_speed(util::speed_t new_speed)
{
	return_speed_value = new_speed;
}

util::speed_t
UIRobot::return_speed(void)
{
	return return_speed_value;
}

bool
UIRobot::acceleration_supported(void) const noexcept
{
	return true;
}

void
UIRobot::acceleration(util::acceleration_t new_acceleration)
{
	auto lock = bus->get_lock();
	auto acc  = static_cast<util::acceleration_t>(new_acceleration * acceleration_multiplier);
	bus->flush_input();
	auto message = util::str2msg(boost::str(boost::format("MAC%1%;") % acc));
	BOOST_LOG_TRIVIAL(trace) << "[UIRobot] acceleration: " << new_acceleration << "(" << acc << ")";
	bus->send(message);
	bus->flush_output();
	bus->rcv(message, 10);
	if (!((message.size() == 10) && (message[0] == 0xaa) && (message[1] == 0x00) && (message[2] == 0xb1) &&
	      (message[3] == 0x0) && (bytes2word(message[4], message[5], message[6], message[7], message[8]) == acc) &&
	      (message[9] == 0xff))) {
		BOOST_LOG_TRIVIAL(warning) << "[UIRobot] acceleration set failed";
		throw std::runtime_error("acceleration set failed");
	}
}

util::acceleration_t
UIRobot::acceleration(void)
{
	auto lock = bus->get_lock();
	bus->flush_input();
	auto message = util::str2msg("MAC;");
	bus->send(message);
	bus->flush_output();
	bus->rcv(message, 10);
	if (!((message.size() == 10) && (message[0] == 0xaa) && (message[1] == 0x00) && (message[2] == 0xb1) &&
	      (message[3] == 0x0) && (message[9] == 0xff))) {
		BOOST_LOG_TRIVIAL(warning) << "[UIRobot] acceleration query failed";
		throw std::runtime_error("acceleration query failed");
	}
	auto acc_internal =
	    util::acceleration_t(bytes2word(message[4], message[5], message[6], message[7], message[8]));
	auto acc = util::acceleration_t(acc_internal / acceleration_multiplier);
	BOOST_LOG_TRIVIAL(trace) << "[UIRobot] acceleration: " << acc << "(" << acc_internal << ")";
	return acc;
}

bool
UIRobot::deceleration_supported(void) const noexcept
{
	return true;
}

void
UIRobot::deceleration(util::acceleration_t new_deceleration)
{
	auto lock = bus->get_lock();
	auto acc  = static_cast<util::acceleration_t>(new_deceleration * acceleration_multiplier);
	bus->flush_input();
	auto message = util::str2msg(boost::str(boost::format("MDE%1%;") % acc));
	BOOST_LOG_TRIVIAL(trace) << "[UIRobot] acceleration: " << new_deceleration << "(" << acc << ")";
	bus->send(message);
	bus->flush_output();
	bus->rcv(message, 10);
	if (!((message.size() == 10) && (message[0] == 0xaa) && (message[1] == 0x00) && (message[2] == 0xb2) &&
	      (message[3] == 0x0) && (bytes2word(message[4], message[5], message[6], message[7], message[8]) == acc) &&
	      (message[9] == 0xff))) {
		BOOST_LOG_TRIVIAL(warning) << "[UIRobot] deceleration set failed";
		throw std::runtime_error("deceleration set failed");
	}
}

util::acceleration_t
UIRobot::deceleration(void)
{
	auto lock = bus->get_lock();
	bus->flush_input();
	auto message = util::str2msg("MDE;");
	bus->send(message);
	bus->flush_output();
	bus->rcv(message, 10);
	if (!((message.size() == 10) && (message[0] == 0xaa) && (message[1] == 0x00) && (message[2] == 0xb2) &&
	      (message[3] == 0x0) && (message[9] == 0xff))) {
		BOOST_LOG_TRIVIAL(warning) << "[UIRobot] deceleration query failed";
		throw std::runtime_error("deceleration query failed");
	}
	auto acc_internal =
	    util::acceleration_t(bytes2word(message[4], message[5], message[6], message[7], message[8]));
	auto acc = util::acceleration_t(acc_internal / acceleration_multiplier);
	BOOST_LOG_TRIVIAL(trace) << "[UIRobot] deceleration: " << acc << "(" << acc_internal << ")";
	return acc;
}

} // namespace motor

// GCOVR_EXCL_STOP
#endif // MOTOR_UIROBOT
