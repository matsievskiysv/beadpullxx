/** @file event.hxx
 * @brief @ref motor::Motor events.
 */

#pragma once

#include <types.hxx>

namespace motor
{

/**
 * @brief motor::Motor event type.
 */
enum class Event {
	ERROR,	 ///< motor::Motor error.
	STOPPED, ///< motor::Motor stopped.
};

using motor_event = std::tuple<Event, util::string_t>; ///< motor::Motor event.

} // namespace motor
