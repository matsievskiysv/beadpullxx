#include <config.h>
#ifdef MOTOR_DUMMY

/** @file dummy.cxx
 * @brief @ref motor::Dummy @ref motor::Motor implementation for testing purposes.
 */

#include "dummy.hxx"

#include <boost/log/trivial.hpp>
#include <thread>
#include <tuple>

namespace motor
{

const util::name_list_t Dummy::SUPPORTED_BUSSES = {
#ifdef BUS_DUMMY
    util::string_t(bus::Dummy::NAME),
#endif
};

util::attribute_t _attribute_choices = {
    {"template", util::string_t(Dummy::NAME)},
    {"current_limit", {{"min", 5}, {"max", 10}, {"step", 1}, {"value", 6}}},
    {"fail_on_constructor", {{"value", false}}},
    {"fail_on_interrupt", {{"value", false}}},
    {"fail_on_set_origin", {{"value", false}}},
    {"fail_on_move_to", {{"value", false}}},
    {"fail_on_move_by", {{"value", false}}},
    {"fail_on_position", {{"value", false}}},
    {"fail_on_stop", {{"value", false}}},
    {"fail_on_reverse", {{"value", false}}},
    {"fail_on_steppers", {{"value", false}}},
    {"fail_on_speed", {{"value", false}}},
    {"fail_on_return_speed", {{"value", false}}},
    {"fail_on_acceleration", {{"value", false}}},
    {"fail_on_deceleration", {{"value", false}}},
    {"g", {{"min", 9.79}, {"max", 9.81}, {"step", 0.001}, {"value", 9.81}}}}; ///< Motor attribute choices.

void
Dummy::bus_send_rcv(void)
{
	auto	    lock    = bus->get_lock();
	util::msg_t message = {0};
	bus->send(message);
	bus->rcv(message, 10);
}

const util::attribute_t &
Dummy::attribute_choices(void) noexcept
{
	return _attribute_choices;
}

Dummy::Dummy(util::attribute_t &&attributes, std::shared_ptr<bus::Bus> bus) : Motor(std::move(attributes), bus)
{
	attributes.as_object()["current_limit"].as_int64();
	attributes.as_object()["g"].as_double();
	fail_on_constructor  = JSON_DEFAULT(attributes.as_object(), "fail_on_constructor", bool, false);
	fail_on_interrupt    = JSON_DEFAULT(attributes.as_object(), "fail_on_interrupt", bool, false);
	fail_on_set_origin   = JSON_DEFAULT(attributes.as_object(), "fail_on_set_origin", bool, false);
	fail_on_move_to	     = JSON_DEFAULT(attributes.as_object(), "fail_on_move_to", bool, false);
	fail_on_move_by	     = JSON_DEFAULT(attributes.as_object(), "fail_on_move_by", bool, false);
	fail_on_position     = JSON_DEFAULT(attributes.as_object(), "fail_on_position", bool, false);
	fail_on_stop	     = JSON_DEFAULT(attributes.as_object(), "fail_on_stop", bool, false);
	fail_on_reverse	     = JSON_DEFAULT(attributes.as_object(), "fail_on_reverse", bool, false);
	fail_on_steppers     = JSON_DEFAULT(attributes.as_object(), "fail_on_steppers", bool, false);
	fail_on_speed	     = JSON_DEFAULT(attributes.as_object(), "fail_on_speed", bool, false);
	fail_on_return_speed = JSON_DEFAULT(attributes.as_object(), "fail_on_return_speed", bool, false);
	fail_on_acceleration = JSON_DEFAULT(attributes.as_object(), "fail_on_acceleration", bool, false);
	fail_on_deceleration = JSON_DEFAULT(attributes.as_object(), "fail_on_deceleration", bool, false);

	BOOST_LOG_TRIVIAL(trace) << "[MotorDummy] fail_on_constructor " << fail_on_constructor;
	BOOST_LOG_TRIVIAL(trace) << "[MotorDummy] fail_on_interrupt " << fail_on_interrupt;
	BOOST_LOG_TRIVIAL(trace) << "[MotorDummy] fail_on_set_origin " << fail_on_set_origin;
	BOOST_LOG_TRIVIAL(trace) << "[MotorDummy] fail_on_move_to " << fail_on_move_to;
	BOOST_LOG_TRIVIAL(trace) << "[MotorDummy] fail_on_move_by " << fail_on_move_by;
	BOOST_LOG_TRIVIAL(trace) << "[MotorDummy] fail_on_position " << fail_on_position;
	BOOST_LOG_TRIVIAL(trace) << "[MotorDummy] fail_on_stop " << fail_on_stop;
	BOOST_LOG_TRIVIAL(trace) << "[MotorDummy] fail_on_reverse " << fail_on_reverse;
	BOOST_LOG_TRIVIAL(trace) << "[MotorDummy] fail_on_steppers " << fail_on_steppers;
	BOOST_LOG_TRIVIAL(trace) << "[MotorDummy] fail_on_speed " << fail_on_speed;
	BOOST_LOG_TRIVIAL(trace) << "[MotorDummy] fail_on_return_speed " << fail_on_return_speed;
	BOOST_LOG_TRIVIAL(trace) << "[MotorDummy] fail_on_acceleration " << fail_on_acceleration;
	BOOST_LOG_TRIVIAL(trace) << "[MotorDummy] fail_on_deceleration " << fail_on_deceleration;

	if (fail_on_constructor)
		throw std::runtime_error("fail on demand");

	BOOST_LOG_TRIVIAL(debug) << "[MotorDummy] create motor";
}

Dummy::~Dummy()
{
	BOOST_LOG_TRIVIAL(debug) << "[MotorDummy] free motor";
}

static const util::string_t _name = util::string_t(Dummy::NAME); ///< Motor attribute choices.

const util::string_t &
Dummy::name(void) const noexcept
{
	return _name;
}

bool
Dummy::interrupted(void) noexcept
{
	auto lock = bus->get_lock();
	bus->interrupted();
	return intrd;
}

void
Dummy::check_interrupt(util::EventQueue<motor_event> &events) noexcept
{
	try {
		util::msg_t message = {0};
		bus->check_interrupt(message, 10);
	} catch (const std::runtime_error &err) {
		events.push(std::make_tuple(Event::ERROR, err.what()));
	}

	if (fail_on_interrupt) {
		events.push(std::make_tuple(Event::ERROR, "failed on demand"));
	}
	if (intrd) {
		events.push(std::make_tuple(Event::STOPPED, "arrived"));
		intrd = false;
	}
}

void
Dummy::set_origin(void)
{
	bus_send_rcv();
	if (fail_on_set_origin)
		throw std::runtime_error("fail on demand");
	pos = 0;
};

void
Dummy::move_to(util::pos_t new_position, bool)
{
	bus_send_rcv();
	if (!steppers_value)
		throw std::runtime_error("steppers disabled");
	if (fail_on_move_to)
		throw std::runtime_error("fail on demand");
	pos   = new_position;
	intrd = true;
}

void
Dummy::move_by(util::pos_t position_increment, bool)
{
	bus_send_rcv();
	if (!steppers_value)
		throw std::runtime_error("steppers disabled");
	if (fail_on_move_by)
		throw std::runtime_error("fail on demand");
	pos += position_increment;
	intrd = true;
}

util::pos_t
Dummy::position(void)
{
	bus_send_rcv();
	if (fail_on_position)
		throw std::runtime_error("fail on demand");
	return pos;
}

void
Dummy::stop(util::EventQueue<motor_event> &events)
{
	bus_send_rcv();
	if (fail_on_stop)
		throw std::runtime_error("fail on demand");
	events.push(std::make_tuple(Event::STOPPED, "stop"));
}

void
Dummy::reverse(bool)
{
	bus_send_rcv();
	if (fail_on_reverse)
		throw std::runtime_error("fail on demand");
}

bool
Dummy::reverse(void)
{
	bus_send_rcv();
	return false;
}

bool
Dummy::steppers_shutdown_supported(void) const noexcept
{
	return true;
}

void
Dummy::steppers(bool enable)
{
	bus_send_rcv();
	if (fail_on_steppers)
		throw std::runtime_error("fail on demand");
	steppers_value = enable;
}

bool
Dummy::steppers(void)
{
	bus_send_rcv();
	return steppers_value;
}

bool
Dummy::speed_supported(void) const noexcept
{
	return true;
}

void
Dummy::speed(util::speed_t new_speed)
{
	bus_send_rcv();
	if (fail_on_speed)
		throw std::runtime_error("fail on demand");
	speed_value = new_speed;
}

util::speed_t
Dummy::speed(void)
{
	bus_send_rcv();
	return speed_value;
}

void
Dummy::return_speed(util::speed_t new_speed)
{
	bus_send_rcv();
	if (fail_on_return_speed)
		throw std::runtime_error("fail on demand");
	return_speed_value = new_speed;
}

util::speed_t
Dummy::return_speed(void)
{
	bus_send_rcv();
	return return_speed_value;
}

bool
Dummy::acceleration_supported(void) const noexcept
{
	return true;
}

void
Dummy::acceleration(util::acceleration_t new_acceleration)
{
	bus_send_rcv();
	if (fail_on_acceleration)
		throw std::runtime_error("fail on demand");
	acceleration_value = new_acceleration;
}

util::acceleration_t
Dummy::acceleration(void)
{
	bus_send_rcv();
	return acceleration_value;
}

bool
Dummy::deceleration_supported(void) const noexcept
{
	return true;
}

void
Dummy::deceleration(util::acceleration_t new_deceleration)
{
	bus_send_rcv();
	if (fail_on_deceleration)
		throw std::runtime_error("fail on demand");
	deceleration_value = new_deceleration;
}

util::acceleration_t
Dummy::deceleration(void)
{
	bus_send_rcv();
	return deceleration_value;
}

} // namespace motor

#endif // MOTOR_DUMMY
