/** @file motor.cxx
 * @brief Base @ref motor::Motor and related classes implementation.
 */

#include "motor.hxx"

#include <algorithm>

namespace motor
{

const util::name_list_t Motor::SUPPORTED_BUSSES = {};

static util::attribute_t _attribute_choices = {{"template", nullptr}};

const util::attribute_t &
Motor::attribute_choices(void) noexcept
{
	return _attribute_choices;
}

void
Motor::patch_bus_attributes(util::attribute_t &) noexcept
{
}

Motor::Motor(util::attribute_t &&attributes, std::shared_ptr<bus::Bus> bus) : bus(bus), attributes(attributes)
{
}

Motor::~Motor()
{
}

const static util::string_t _name = util::string_t(Motor::NAME); ///< Motor name.

const util::string_t &
Motor::name(void) const noexcept
{
	return _name;
}

const util::string_t &
Motor::bus_name(void) const noexcept
{
	return bus->name();
}

util::attribute_t &
Motor::bus_attributes(void) noexcept
{
	return bus->attributes;
};

bool
Motor::steppers_shutdown_supported(void) const noexcept
{
	return false;
};

void
Motor::steppers(bool)
{
	throw std::runtime_error("stepper shutdown not supported");
}

bool
Motor::steppers(void)
{
	throw std::runtime_error("stepper shutdown not supported");
}

bool
Motor::speed_supported(void) const noexcept
{
	return false;
}

void
Motor::speed(util::speed_t)
{
	throw std::runtime_error("speed not supported");
}

util::speed_t
Motor::speed(void)
{
	throw std::runtime_error("speed not supported");
}

void
Motor::return_speed(util::speed_t)
{
	throw std::runtime_error("speed not supported");
}

util::speed_t
Motor::return_speed(void)
{
	throw std::runtime_error("speed not supported");
}

bool
Motor::acceleration_supported(void) const noexcept
{
	return false;
}

void
Motor::acceleration(util::acceleration_t)
{
	throw std::runtime_error("acceleration not supported");
}

util::acceleration_t
Motor::acceleration(void)
{
	throw std::runtime_error("acceleration not supported");
}

bool
Motor::deceleration_supported(void) const noexcept
{
	return false;
}

void
Motor::deceleration(util::acceleration_t)
{
	throw std::runtime_error("deceleration not supported");
}

util::acceleration_t
Motor::deceleration(void)
{
	throw std::runtime_error("deceleration not supported");
}

} // namespace motor
