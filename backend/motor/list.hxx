/** @file list.hxx
 * @brief @ref motor::Motor list.
 */

#pragma once

#include "motor.hxx"

#include <config.h>
#include <types.hxx>
#ifdef MOTOR_DUMMY
#include "dummy.hxx"
#endif
#ifdef MOTOR_UIROBOT
#include "uirobot.hxx"
#endif

namespace motor
{

extern const util::name_list_t motor_list; ///< List of compiled motor::Motor drivers.

/**
 * @brief Supported bus::Bus drivers by motor::Motor.
 *
 * @param[in] motor motor::Motor name.
 * @return bus::Bus drivers list.
 */
const util::name_list_t &get_supported_busses(const util::string_t &motor);

/**
 * @brief Attribute choices for the motor::Motor.
 *
 * @param[in] motor motor::Motor name.
 * @return motor::Motor attributes.
 */
const util::attribute_t &get_attribute_choices(const util::string_t &motor);

/**
 * @brief Create motor::Motor.
 *
 * @param[in] motor motor::Motor name.
 * @param[in] attributes motor::Motor attributes.
 * @param[in] bus bus::Bus driver.
 * @return motor::Motor pointer.
 * @throws std::runtime_error Any constructor errors.
 */
std::shared_ptr<Motor> create_motor(const util::string_t &motor, util::attribute_t &&attributes,
				    std::shared_ptr<bus::Bus> bus);

/**
 * @brief Patch \p attributes.
 *
 * @param[in] motor motor::Motor name.
 * @param[in,out] attributes motor::Motor attributes.
 * @throws std::runtime_error Any constructor errors.
 */
void patch_bus_attributes(const util::string_t &motor, util::attribute_t &attributes);

} // namespace motor
