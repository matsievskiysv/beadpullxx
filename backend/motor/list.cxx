/** @file list.cxx
 * @brief @ref motor::Motor list.
 */

#include "list.hxx"

namespace motor
{

const util::name_list_t motor_list{
#ifdef MOTOR_DUMMY
    util::string_t(Dummy::NAME),
#endif
#ifdef MOTOR_UIROBOT
    util::string_t(UIRobot::NAME),
#endif
};

const util::name_list_t &
get_supported_busses([[maybe_unused]] const util::string_t &motor)
{
#ifdef MOTOR_DUMMY
	if (motor == Dummy::NAME)
		return Dummy::SUPPORTED_BUSSES;
#endif
// GCOVR_EXCL_START do not test hardware driver
#ifdef MOTOR_UIROBOT
	if (motor == UIRobot::NAME)
		return UIRobot::SUPPORTED_BUSSES;
#endif
// GCOVR_EXCL_STOP
	throw std::runtime_error("unknown motor");
}

const util::attribute_t &
get_attribute_choices([[maybe_unused]] const util::string_t &motor)
{
#ifdef MOTOR_DUMMY
	if (motor == Dummy::NAME)
		return Dummy::attribute_choices();
#endif
// GCOVR_EXCL_START do not test hardware driver
#ifdef MOTOR_UIROBOT
	if (motor == UIRobot::NAME)
		return UIRobot::attribute_choices();
#endif
// GCOVR_EXCL_STOP
	throw std::runtime_error("unknown motor");
}

std::shared_ptr<Motor>
create_motor([[maybe_unused]] const util::string_t &motor, [[maybe_unused]] util::attribute_t &&attributes,
	     [[maybe_unused]] std::shared_ptr<bus::Bus> bus)
{
#ifdef MOTOR_DUMMY
	if (motor == Dummy::NAME)
		return std::shared_ptr<Motor>(new Dummy(std::move(attributes), bus));
#endif
// GCOVR_EXCL_START do not test hardware driver
#ifdef MOTOR_UIROBOT
	if (motor == UIRobot::NAME)
		return std::shared_ptr<Motor>(new UIRobot(std::move(attributes), bus));
#endif
// GCOVR_EXCL_STOP
	throw std::runtime_error("unknown motor");
}

void
patch_bus_attributes([[maybe_unused]] const util::string_t &motor, [[maybe_unused]] util::attribute_t &attributes)
{
#ifdef MOTOR_DUMMY
	if (motor == Dummy::NAME) {
		Dummy::patch_bus_attributes(attributes);
		return;
	}
#endif
// GCOVR_EXCL_START do not test hardware driver
#ifdef MOTOR_UIROBOT
	if (motor == UIRobot::NAME) {
		UIRobot::patch_bus_attributes(attributes);
		return;
	}
#endif
// GCOVR_EXCL_STOP
	throw std::runtime_error("unknown motor");
}

} // namespace motor
