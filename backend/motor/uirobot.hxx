/** @file uirobot.hxx
 * @brief motor::UIRobot implementation.
 */

#pragma once

#include <config.h>
#ifdef MOTOR_UIROBOT

#include "motor.hxx"

namespace motor
{

/**
 * @brief UIRobot class.
 */
class UIRobot : public Motor
{
      private:
	std::uint16_t fw_version;		  ///< Firmware version.
	bool	      reverse_value	 = false; ///< Reverse flag.
	util::speed_t speed_value	 = 1000;  ///< Forward speed value.
	util::speed_t return_speed_value = 1000;  ///< Retern speed value.

      public:
	/**
	 * @brief UIRobot constructor.
	 *
	 * @param[in] attributes motor::Motor attributes.
	 * @param[in] bus bus::Bus reference.
	 */
	UIRobot(util::attribute_t &&attributes, std::shared_ptr<bus::Bus> bus);

	/**
	 * @brief UIRobot destructor.
	 */
	~UIRobot();

	static constexpr const char   *NAME = "UIRobot"; ///< Motor name.
	static const util::name_list_t SUPPORTED_BUSSES; ///< List of supported bus::Bus.

	/**
	 * @brief Bus attribute choices.
	 *
	 * @return Bus attribute choices.
	 */
	static util::attribute_t &attribute_choices(void);

	const util::string_t &name(void) const noexcept;
	bool		      interrupted(void) noexcept;
	void		      check_interrupt(util::EventQueue<motor_event> &events) noexcept;
	void		      set_origin(void);
	void		      move_to(util::pos_t new_position, bool return_speed);
	void		      move_by(util::pos_t position_increment, bool return_speed);
	util::pos_t	      position(void);
	void		      stop(util::EventQueue<motor_event> &events);
	void		      reverse(bool val);
	bool		      reverse(void);
	bool		      steppers_shutdown_supported(void) const noexcept;
	void		      steppers(bool enable);
	bool		      steppers(void);
	bool		      speed_supported(void) const noexcept;
	void		      speed(util::speed_t new_speed);
	util::speed_t	      speed(void);
	void		      return_speed(util::speed_t new_speed);
	util::speed_t	      return_speed(void);
	bool		      acceleration_supported(void) const noexcept;
	void		      acceleration(util::acceleration_t new_acceleration);
	util::acceleration_t  acceleration(void);
	bool		      deceleration_supported(void) const noexcept;
	void		      deceleration(util::acceleration_t new_deceleration);
	util::acceleration_t  deceleration(void);
};

} // namespace motor

#endif // MOTOR_UIROBOT
