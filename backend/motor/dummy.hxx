#pragma once

#include <config.h>
#ifdef MOTOR_DUMMY

/** @file dummy.hxx
 * @brief @ref motor::Dummy @ref motor::Motor implementation for testing purposes.
 */

#include "motor.hxx"

namespace motor
{

/**
 * @brief Dummy Motor class.
 */
class Dummy : public Motor
{
      private:
	std::chrono::time_point<std::chrono::steady_clock> time;		       ///< Time.
	util::pos_t					   pos;			       ///< Current position.
	bool						   intrd	      = false; ///< Interrupted flag.
	bool						   steppers_value     = false; ///< Steppers enabled flag.
	util::speed_t					   speed_value	      = 0;     ///< Speed value.
	util::speed_t					   return_speed_value = 0;     ///< Return speed value.
	util::acceleration_t				   acceleration_value = 0;     ///< Acceleration value.
	util::acceleration_t				   deceleration_value = 0;     ///< Deceleration value.

	bool fail_on_constructor    = false; ///< Fail on Dummy::Dummy(). Set via attributes.
	bool fail_on_interrupt	    = false; ///< Fail on check_interrupt(). Set via attributes.
	bool fail_on_set_origin	    = false; ///< Fail on set_origin(). Set via attributes.
	bool fail_on_move_to	    = false; ///< Fail on move_to(). Set via attributes.
	bool fail_on_move_by	    = false; ///< Fail on move_by(). Set via attributes.
	bool fail_on_position	    = false; ///< Fail on position(). Set via attributes.
	bool fail_on_stop	    = false; ///< Fail on stop(). Set via attributes.
	bool fail_on_reverse	    = false; ///< Fail on reverse(). Set via attributes.
	bool fail_on_steppers	    = false; ///< Fail on steppers(). Set via attributes.
	bool fail_on_speed	    = false; ///< Fail on speed(). Set via attributes.
	bool fail_on_return_speed   = false; ///< Fail on return_speed(). Set via attributes.
	bool fail_on_acceleration   = false; ///< Fail on acceleration(). Set via attributes.
	bool fail_on_deceleration   = false; ///< Fail on deceleration(). Set via attributes.

	/**
	 * @brief Exchange dummy data with bus.
	 *
	 * Purpose of this function is to trigger bus::Dummy exceptions in motor::Dummy calls.
	 */
	void bus_send_rcv(void);

      public:
	/**
	 * @brief Dummy constructor.
	 *
	 * @param[in] attributes Dummy attributes.
	 * @param[in] bus bus::Bus reference.
	 */
	Dummy(util::attribute_t &&attributes, std::shared_ptr<bus::Bus> bus);

	/**
	 * @brief Motor destructor.
	 */
	~Dummy();

	static constexpr const char   *NAME = "dummy";	 ///< Motor name.
	static const util::name_list_t SUPPORTED_BUSSES; ///< List of supported bus::Bus.
	/**
	 * @brief Bus attribute choices.
	 *
	 * @return Bus attribute choices.
	 */
	static const util::attribute_t &attribute_choices(void) noexcept;

	const util::string_t &name(void) const noexcept;
	bool		      interrupted(void) noexcept;
	void		      check_interrupt(util::EventQueue<motor_event> &events) noexcept;
	void		      set_origin(void);
	void		      move_to(util::pos_t new_position, bool return_speed = false);
	void		      move_by(util::pos_t position_increment, bool return_speed = false);
	util::pos_t	      position(void);
	void		      stop(util::EventQueue<motor_event> &events);
	void		      reverse(bool val);
	bool		      reverse(void);
	bool		      steppers_shutdown_supported(void) const noexcept;
	void		      steppers(bool enable);
	bool		      steppers(void);
	bool		      speed_supported(void) const noexcept;
	void		      speed(util::speed_t new_speed);
	util::speed_t	      speed(void);
	void		      return_speed(util::speed_t new_speed);
	util::speed_t	      return_speed(void);
	bool		      acceleration_supported(void) const noexcept;
	void		      acceleration(util::acceleration_t new_acceleration);
	util::acceleration_t  acceleration(void);
	bool		      deceleration_supported(void) const noexcept;
	void		      deceleration(util::acceleration_t new_deceleration);
	util::acceleration_t  deceleration(void);
};

} // namespace motor

#endif // MOTOR_DUMMY
