/** @file server.hxx
 * @brief Server related definitions.
 */

#pragma once

#include <App.h>
#include <boost/filesystem.hpp>
#include <boost/log/trivial.hpp>
#include <chunked.hxx>
#include <controller/controller.hxx>
#include <map>
#include <string>
#include <yaml-cpp/yaml.h>

/**
 * @brief Check simple authorization header.
 *
 * Check simple authorization header. Return error if not authorized.
 *
 * @note To be used in endpoint function context.
 */
#define CHECK_AUTHORIZED()                                                                                             \
	do {                                                                                                           \
		if (req->getHeader("authorization") != auth) {                                                         \
			BOOST_LOG_TRIVIAL(debug) << "Authorization fail";                                              \
			res->cork([&]() {                                                                              \
				res->writeStatus("401 Unauthorized");                                                  \
				res->writeHeader("WWW-Authenticate", "Basic");                                         \
				res->writeHeader("Content-Type", "text/html");                                         \
				res->end(create_simple_page("Not authorized", "Access not authorized"));               \
			});                                                                                            \
			return;                                                                                        \
		}                                                                                                      \
	} while (false)

/**
 * @brief Catch execution error.
 *
 * Catch execution exceptions and return error messages in HTTP response.
 *
 * @note To be used in endpoint function context.
 * @param body Code body.
 */
#define CATCH_ERROR(body)                                                                                              \
	do {                                                                                                           \
		try {                                                                                                  \
			body                                                                                           \
		} catch (std::exception & err) {                                                                       \
			res->writeStatus("400 Bad Request");                                                           \
			res->writeHeader("Content-Type", "application/json");                                          \
			res->end(json::serialize(json::string(err.what())));                                           \
		}                                                                                                      \
	} while (false)

/**
 * @brief Check HTTP argument list argument.
 *
 * Check HTTP argument presence and return error messages in HTTP response if not present.
 *
 * @note To be used in endpoint function context.
 * @param arg Argument to check.
 */
#define CHECK_ARG(arg)                                                                                                 \
	do {                                                                                                           \
		if (req->getQuery(arg) == "") {                                                                        \
			res->writeStatus("400 Bad Request");                                                           \
			res->writeHeader("Content-Type", "application/json");                                          \
			res->end(json::serialize(json::string("`" arg "` argument not supplied")));                    \
			return;                                                                                        \
		}                                                                                                      \
	} while (false);

/**
 * @brief Check HTTP argument value.
 *
 * @note To be used in endpoint function context.
 * @param name Name used in error report message.
 * @param test Test to execute. Return error, if evaluates to true.
 */
#define CHECK_VAL(name, test)                                                                                          \
	do {                                                                                                           \
		if (test) {                                                                                            \
			res->writeStatus("400 Bad Request");                                                           \
			res->writeHeader("Content-Type", "application/json");                                          \
			res->end(json::serialize(json::string("`" #name "` argument value error")));                   \
			return;                                                                                        \
		}                                                                                                      \
	} while (false);

/**
 * @brief Check content-type header value.
 *
 * Check `content-type` header value. Return error if does not match the expected value.
 *
 * @note To be used in endpoint function context.
 * @param type Expected `context-type` value.
 */
#define CHECK_CONTENT_HEADER(type)                                                                                     \
	do {                                                                                                           \
		if (req->getHeader("content-type") != type) {                                                          \
			res->writeStatus("400 Bad Request");                                                           \
			res->writeHeader("Content-Type", "text/html");                                                 \
			res->end(create_simple_page("Query error", "Wrong content type. Expecting `" type "`"));       \
			return;                                                                                        \
		}                                                                                                      \
	} while (false);

namespace fs	  = boost::filesystem;
namespace logging = boost::log;

extern const char USAGE[]; ///< Program help message.

extern const std::map<const std::string, const std::string> mime_types; ///< Mime type to file extension map.

extern const std::map<const std::string, const logging::trivial::severity_level> log_levels; ///< Log level name map.

/**
 * @brief Create simple HTML page.
 *
 * @param title Page title.
 * @param message Page message.
 * @return HTML page.
 */
std::string create_simple_page(const char *title, const char *message);

/**
 * @brief Send chunked data.
 *
 * @note To be used in endpoint function context.
 * @param res HTTP response.
 * @param chunked Chunked data stream.
 * @retval true Success.
 * @retval false Error.
 */
bool send_chunked(uWS::HttpResponse<true> *res, std::shared_ptr<util::Chunked> chunked);

/**
 * @brief Send filesystem file.
 *
 * @param root_dir_path Path to server root directory.
 * @param res HTTP response.
 * @param req HTTP request.
 */
void serve_file(const boost::filesystem::path &root_dir_path, uWS::HttpResponse<true> *res, uWS::HttpRequest *req);

/**
 * @brief HTTP to HTTPS redirect server constructor.
 *
 * @param config Server configuration.
 * @return Application.
 */
uWS::App redirect_app(const YAML::Node &config);

/**
 * @brief Main server constructor.
 *
 * @param config Server configuration.
 * @return Application.
 */
uWS::SSLApp main_app(const YAML::Node &config);

/**
 * @brief Configure main server.
 *
 * @param app Main server application.
 * @param ctrl Driver controller.
 * @param config Server configuration.
 * @param root_dir_path Path to server root directory.
 * @param auth Simple authentication string.
 */
void configure_main_app(uWS::SSLApp &app, controller::Controller &ctrl, const YAML::Node &config,
			const boost::filesystem::path &root_dir_path, const std::string &auth);
