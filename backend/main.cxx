/** @file main.cxx
 * @brief Program entry point.
 */

#include <App.h>
#include <base64.hxx>
#include <boost/filesystem.hpp>
#include <boost/json.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <chunked.hxx>
#include <config.h>
#include <controller/controller.hxx>
#include <csignal>
#include <docopt.h>
#include <functional>
#include <iostream>
#include <server.hxx>
#include <sstream>
#include <string>
#include <thread>
#include <tuple>
#include <vector>
#include <yaml-cpp/yaml.h>

namespace fs	  = boost::filesystem;
namespace logging = boost::log;

/**
 * @brief Signal shutdown handler.
 *
 * This handler catches signals and gracefully shuts down program threads.
 */
std::function<void(int)> shutdown_handler = [](int) {
	// placeholder
};

/**
 * @brief Signal handler wrapper.
 *
 * @param signal Signal value.
 */
void
signal_handler(int signal)
{
	shutdown_handler(signal);
}

/**
 * @brief Program entry point.
 *
 * @param argc Angument count.
 * @param argv Angument list.
 * @retval 0 Success.
 * @return Program status.
 */
int
main(int argc, char *argv[])
{
	logging::add_common_attributes();
	logging::add_console_log(
	    std::cout,
	    logging::keywords::format =
		(logging::expressions::stream
		 << "["
		 << logging::expressions::format_date_time<boost::posix_time::ptime>("TimeStamp", "%Y-%m-%dT%H:%M:%S")
		 << "]"
		 << ": <" << logging::trivial::severity << "> " << logging::expressions::smessage));
	logging::core::get()->set_filter(logging::trivial::severity >= logging::trivial::info);

	std::map<std::string, docopt::value> args = docopt::docopt(
	    USAGE, {argv + 1, argv + argc}, true, "BeadPull++ " BEADPULLXX_VERSION_MAJOR "." BEADPULLXX_VERSION_MINOR);

	for (auto arg : args) {
		BOOST_LOG_TRIVIAL(trace) << "Program argument " << arg.first << "=" << arg.second;
	}

	fs::path config_path(args["--config"].asString());
	fs::path root_dir_path(args["<root_dir>"].asString());

	if (!fs::is_regular_file(config_path)) {
		BOOST_LOG_TRIVIAL(fatal) << args["--config"] << " is not a file";
		return 1;
	}
	if (!fs::is_directory(root_dir_path)) {
		BOOST_LOG_TRIVIAL(fatal) << args["<root_dir>"] << " is not a directory";
		return 1;
	}

	YAML::Node config = YAML::LoadFile(config_path.generic_string());

	const std::string auth =
	    "Basic " + util::base64_encode(config["server"]["cred"]["login"].as<std::string>() + ":" +
					   config["server"]["cred"]["password"].as<std::string>());

	logging::core::get()->set_filter(logging::trivial::severity >=
					 log_levels.find(config["server"]["logging"].as<std::string>())->second);

	BOOST_LOG_TRIVIAL(debug) << "Server logging level: " << config["server"]["logging"];
	BOOST_LOG_TRIVIAL(debug) << "Server credentials: " << config["server"]["cred"]["login"] << ":"
				 << config["server"]["cred"]["password"];
	BOOST_LOG_TRIVIAL(debug) << "Server certificates: " << config["server"]["ssl"]["cert"] << ":"
				 << config["server"]["ssl"]["key"];

	if (!fs::is_regular_file(config["server"]["ssl"]["cert"].as<std::string>())) {
		BOOST_LOG_TRIVIAL(fatal) << config["server"]["ssl"]["cert"] << " is not a file";
		return 1;
	}
	if (!fs::is_regular_file(config["server"]["ssl"]["key"].as<std::string>())) {
		BOOST_LOG_TRIVIAL(fatal) << config["server"]["ssl"]["key"] << " is not a file";
		return 1;
	}

	auto redirect = redirect_app(config);
	auto app      = main_app(config);

	auto		       loop = uWS::Loop::get();
	controller::Controller ctrl(
	    controller::Timeout(std::chrono::milliseconds(config["timeout"]["motor"]["communication"].as<int>()),
				std::chrono::milliseconds(config["timeout"]["motor"]["step"].as<int>()),
				std::chrono::milliseconds(config["timeout"]["analyzer"]["communication"].as<int>()),
				std::chrono::milliseconds(config["timeout"]["analyzer"]["measure"].as<int>()),
				std::chrono::milliseconds(config["pre_measure_delay"].as<int>())),
	    [&](auto topic, auto message) {
		    BOOST_LOG_TRIVIAL(info) << "ws"
					    << "[" << topic << "]: " << json::serialize(message);
		    loop->defer([=, &app]() {
			    BOOST_LOG_TRIVIAL(info) << "ws"
						    << "[" << topic << "]: " << json::serialize(message);
			    app.publish(topic, json::serialize(message), uWS::OpCode::TEXT);
		    });
	    });

	configure_main_app(app, ctrl, config, root_dir_path, auth);

	std::jthread controller_loop([&]() {
		ctrl.event_loop();
		BOOST_LOG_TRIVIAL(info) << "Stopped event loop";
	});

	shutdown_handler = [&](int) {
		BOOST_LOG_TRIVIAL(info) << "Stopping application";
		ctrl.close();
		redirect.close();
		app.close();
	};
	std::signal(SIGINT, signal_handler);

	app.run();
	BOOST_LOG_TRIVIAL(info) << "Stopped application";

	return 0;
}
