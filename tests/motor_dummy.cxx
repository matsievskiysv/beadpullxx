#include <algorithm>
#include <bus/dummy.hxx>
#include <bus/list.hxx>
#include <cassert>
#include <config.h>
#include <event_queue.hxx>
#include <motor/dummy.hxx>
#include <motor/event.hxx>
#include <motor/list.hxx>
#include <vector>

using namespace motor;

int
main(void)
{
#ifdef MOTOR_DUMMY

#define THROWS(expr, result)                                                                                           \
	do {                                                                                                           \
		bool throwed_exception = false;                                                                        \
		try {                                                                                                  \
			expr;                                                                                          \
		} catch (const std::exception &) {                                                                     \
			throwed_exception = true;                                                                      \
		}                                                                                                      \
		assert(throwed_exception == result);                                                                   \
	} while (0)

	util::EventQueue<motor::motor_event> efd(10, nullptr);

	assert(get_supported_busses("dummy").size() > 0);
	THROWS(get_attribute_choices("dummy"), false);
	auto choices = get_attribute_choices("dummy");
	assert(choices.as_object()["template"] == "dummy");
	assert(choices.as_object()["current_limit"].is_object());
	assert(choices.as_object()["current_limit"].as_object()["min"].is_number());
	assert(choices.as_object()["current_limit"].as_object()["min"].as_int64() == 5);
	assert(choices.as_object()["current_limit"].as_object()["max"].is_number());
	assert(choices.as_object()["current_limit"].as_object()["max"].as_int64() == 10);
	assert(choices.as_object()["current_limit"].as_object()["step"].is_number());
	assert(choices.as_object()["current_limit"].as_object()["step"].as_int64() == 1);
	assert(choices.as_object()["current_limit"].as_object()["value"].is_number());
	assert(choices.as_object()["current_limit"].as_object()["value"].as_int64() == 6);
	assert(choices.as_object()["g"].is_object());
	assert(choices.as_object()["g"].as_object()["min"].is_number());
	assert(choices.as_object()["g"].as_object()["min"].as_double() == 9.79);
	assert(choices.as_object()["g"].as_object()["max"].is_number());
	assert(choices.as_object()["g"].as_object()["max"].as_double() == 9.81);
	assert(choices.as_object()["g"].as_object()["step"].is_number());
	assert(choices.as_object()["g"].as_object()["step"].as_double() == 0.001);
	assert(choices.as_object()["g"].as_object()["value"].is_number());
	assert(choices.as_object()["g"].as_object()["value"].as_double() == 9.81);

	assert(std::find(get_supported_busses("dummy").begin(), get_supported_busses("dummy").end(), "dummy") !=
	       get_supported_busses("dummy").end());
	auto bus =
	    bus::create_bus("dummy", std::chrono::milliseconds(200), nullptr,
			    {{"device", "/dev/ttyUSB0"}, {"baudrate", 9600}, {"delay", 0.5}});
	auto motor = create_motor(
	    "dummy", {{"name", "my_motor"}, {"current_limit", 10}, {"g", 9.81}},
	    std::move(bus));

	assert(motor->attributes.is_object());
	assert(motor->attributes.as_object()["name"] == "my_motor");
	assert(motor->attributes.as_object()["current_limit"] == 10);
	assert(motor->attributes.as_object()["g"] == 9.81);
	assert(motor->bus_attributes().is_object());
	assert(motor->bus_attributes().as_object()["device"] == "/dev/ttyUSB0");
	assert(motor->bus_attributes().as_object()["baudrate"] == 9600);
	assert(motor->bus_attributes().as_object()["delay"] == 0.5);
	THROWS(motor->set_origin(), false);
	THROWS(motor->move_to(5000), true);
	THROWS(motor->move_by(4000), true);
	THROWS(motor->steppers(true), false);
	assert(motor->steppers());
	THROWS(motor->move_to(5000), false);
	assert(motor->position() == 5000);
	THROWS(motor->move_to(-3000), false);
	assert(motor->position() == -3000);
	THROWS(motor->move_by(5000), false);
	assert(motor->position() == 2000);
	THROWS(motor->move_by(-10000), false);
	assert(motor->position() == -8000);
	THROWS(motor->move_to(-5000), false);
	THROWS(motor->position(), false);
	THROWS(motor->stop(efd), false);
	assert(motor->steppers_shutdown_supported());
	THROWS(motor->steppers(false), false);
	assert(!motor->steppers());
	THROWS(motor->steppers(true), false);
	assert(motor->steppers());
	assert(motor->speed_supported());
	THROWS(motor->speed(10), false);
	assert(motor->speed() == 10);
	THROWS(motor->return_speed(15), false);
	assert(motor->return_speed() == 15);
	assert(motor->acceleration_supported());
	THROWS(motor->acceleration(11), false);
	assert(motor->acceleration() == 11);
	assert(motor->deceleration_supported());
	THROWS(motor->deceleration(9), false);
	assert(motor->deceleration() == 9);
	THROWS(motor->bus_name(), false);
	assert(motor->bus_name() == "dummy");
	THROWS(motor->bus_attributes(), false);
	THROWS(motor->interrupted(), false);
#endif
	return 0;
}
