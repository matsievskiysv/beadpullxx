#include <algorithm>
#include <base64.hxx>
#include <cassert>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <string>
#include <tuple>
#include <vector>

using namespace util;
using namespace std;

string
randomStrGen(size_t length)
{
	auto randchar = []() -> char {
		const char   charset[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		const size_t max_index = (sizeof(charset) - 1);
		return charset[rand() % max_index];
	};
	string result(length, 0);
	generate_n(result.begin(), length, randchar);

	return result;
}

int
main(void)
{
	srand(0);
	vector<tuple<string, string, function<string(string)>>> pairs = {
	    {"hello", "aGVsbG8=", base64_encode},
	    {"hello world", "aGVsbG8gd29ybGQ=", base64_encode},
	    {"user:password", "dXNlcjpwYXNzd29yZA==", base64_encode},
	    {"aGVsbG8=", "hello", base64_decode},
	    {"aGVsbG8gd29ybGQ=", "hello world", base64_decode},
	    {"dXNlcjpwYXNzd29yZA==", "user:password", base64_decode},
	};
	for (auto [str1, str2, func] : pairs) {
		cout << "check " << str1 << " : " << str2 << endl;
		assert(func(str1) == str2);
	}
	for (size_t i = 0; i < 100; i++) {
		for (size_t len = 5; len < 50; len++) {
			auto str = randomStrGen(len);
			cout << "check " << str << endl;
			assert(base64_decode(base64_encode(str)) == str);
		}
	}
	return 0;
}
