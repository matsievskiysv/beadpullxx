#include <algorithm>
#include <bus/list.hxx>
#include <cassert>
#include <config.h>
#include <types.hxx>

using namespace bus;
namespace json = boost::json;

int
main(void)
{
#ifdef BUS_DUMMY

#define THROWS(expr, result)                                                                                           \
	do {                                                                                                           \
		bool throwed_exception = false;                                                                        \
		try {                                                                                                  \
			expr;                                                                                          \
		} catch (const std::exception &) {                                                                     \
			throwed_exception = true;                                                                      \
		}                                                                                                      \
		assert(throwed_exception == result);                                                                   \
	} while (0)

	assert(bus_list.size() > 0);
	assert(std::find(bus_list.begin(), bus_list.end(), "dummy") != bus_list.end());
	THROWS(bus::get_attribute_choices("dummy"), false);
	auto choices = bus::get_attribute_choices("dummy");
	assert(choices.as_object()["template"] == "dummy");
	assert(choices.as_object()["device"].is_object());
	assert(choices.as_object()["device"].as_object()["choices"].is_array());
	assert(choices.as_object()["device"].as_object()["choices"].as_array().size() == 3);
	assert(choices.as_object()["device"].as_object()["choices"].as_array().at(0) == "/dev/ttyUSB0");
	assert(choices.as_object()["device"].as_object()["choices"].as_array().at(1) == "/dev/ttyUSB1");
	assert(choices.as_object()["device"].as_object()["choices"].as_array().at(2) == "/dev/ttyUSB2");
	assert(choices.as_object()["device"].as_object()["value"] == "/dev/ttyUSB0");
	assert(choices.as_object()["baudrate"].as_object()["choices"].is_array());
	assert(choices.as_object()["baudrate"].as_object()["choices"].as_array().size() == 3);
	assert(choices.as_object()["baudrate"].as_object()["choices"].as_array().at(0) == 9600);
	assert(choices.as_object()["baudrate"].as_object()["choices"].as_array().at(1) == 57600);
	assert(choices.as_object()["baudrate"].as_object()["choices"].as_array().at(2) == 115200);
	assert(choices.as_object()["baudrate"].as_object()["value"] == 57600);
	assert(choices.as_object()["delay"].is_object());
	assert(choices.as_object()["delay"].as_object()["min"].is_number());
	assert(choices.as_object()["delay"].as_object()["min"].as_double() == 0.1);
	assert(choices.as_object()["delay"].as_object()["max"].is_number());
	assert(choices.as_object()["delay"].as_object()["max"].as_double() == 0.5);

	THROWS(create_bus(bus_list[0], std::chrono::milliseconds(200), nullptr,
			  {{"baudrate", 19200},
			   {"delay", 0.5},
			   {"empty", nullptr}}),
	       true);
	auto bus = create_bus(bus_list[0], std::chrono::milliseconds(200), nullptr,
			      {{"device", "/dev/ttyUSB0"},
			       {"baudrate", 19200},
			       {"delay", 0.5},
			       {"empty", nullptr}});
	assert(bus->name() == "dummy");
	util::msg_t buffer = {};
	THROWS(
	    {
		    bus->send(buffer);
		    bus->rcv(buffer, buffer.size());
	    },
	    false);
	assert(bus->attributes.is_object());
	assert(bus->attributes.as_object()["device"].as_string() == "/dev/ttyUSB0");
	assert(bus->attributes.as_object()["baudrate"].as_int64() == 19200);
	assert(bus->attributes.as_object()["delay"].as_double() == 0.5);
	assert(bus->attributes.as_object()["empty"].is_null());
	assert(!bus->interrupted());
	assert(bus->attributes.as_object()["nonexistent"].is_null());
	assert(bus->attributes.as_object()["nonexistent"] == nullptr);
	THROWS(bus->attributes.as_object()["nonexistent"].as_int64(), true);
#endif
	return 0;
}
