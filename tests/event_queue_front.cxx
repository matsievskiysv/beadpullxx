#include <cassert>
#include <event_queue.hxx>
#include <semaphore>

using namespace util;

int
main(void)
{
	EventQueue<int> efd(100, nullptr);
	efd.push(1);
	efd.push(3);
	efd.push(5);
	efd.push(10, true);
	efd.push(15, true);
	assert(efd.has_event());
	assert(efd.consume() == 15);
	assert(efd.has_event());
	assert(efd.consume() == 10);
	assert(efd.has_event());
	assert(efd.consume() == 1);
	assert(efd.has_event());
	assert(efd.consume() == 3);
	assert(efd.has_event());
	assert(efd.consume() == 5);
	assert(!efd.has_event());
	return 0;
}
