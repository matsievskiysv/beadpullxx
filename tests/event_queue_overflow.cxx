#include <cassert>
#include <event_queue.hxx>
#include <semaphore>

using namespace util;

int
main(void)
{
#define THROWS(expr, result)                                                                                           \
	do {                                                                                                           \
		bool throwed_exception = false;                                                                        \
		try {                                                                                                  \
			expr;                                                                                          \
		} catch (const std::exception &) {                                                                     \
			throwed_exception = true;                                                                      \
		}                                                                                                      \
		assert(throwed_exception == result);                                                                   \
	} while (0)

	EventQueue<int> efd(3, nullptr);
	assert(!efd.push(1));
	assert(!efd.push(3));
	assert(!efd.push(5));
	assert(efd.push(10));
	assert(efd.consume() == 3);
	assert(efd.consume() == 5);
	assert(efd.consume() == 10);
	THROWS(efd.consume(), true);
	return 0;
}
