#include <analyzer/dummy.hxx>
#include <analyzer/list.hxx>
#include <bus/dummy.hxx>
#include <bus/list.hxx>
#include <cassert>
#include <config.h>
#include <iostream>
#include <vector>

using namespace analyzer;

int
main(void)
{
#ifdef ANALYZER_DUMMY

#define THROWS(expr, result)                                                                                           \
	do {                                                                                                           \
		bool throwed_exception = false;                                                                        \
		try {                                                                                                  \
			expr;                                                                                          \
		} catch (const std::exception &) {                                                                     \
			throwed_exception = true;                                                                      \
		}                                                                                                      \
		assert(throwed_exception == result);                                                                   \
	} while (0)

	assert(get_supported_busses("dummy").size() > 0);
	assert(get_supported_busses("dummy")[0] == "dummy");
	auto choices = get_attribute_choices("dummy");
	assert(choices.as_object()["smoothing"].is_object());
	assert(choices.as_object()["smoothing"].as_object()["value"].is_bool());
	assert(choices.as_object()["smoothing"].as_object()["value"].as_bool());

	assert(std::find(get_supported_busses("dummy").begin(), get_supported_busses("dummy").end(), "dummy") !=
	       get_supported_busses("dummy").end());
	auto bus =
	    bus::create_bus(get_supported_busses("dummy")[0], std::chrono::milliseconds(200), nullptr,
			    {{"device", "/dev/ttyUSB0"}, {"baudrate", 9600}, {"flowcontrol", false}, {"delay", 0.5}});
#if 0
	auto analyzer = create_analyzer(Dummy::NAME, {{"name", "my_analyzer"}, {"power", 20}, {"smoothing", false}},
					std::move(bus));

	assert(analyzer->attributes.is_object());
	assert(analyzer->attributes.as_object()["name"] == "my_analyzer");
	assert(analyzer->attributes.as_object()["power"] == 20);
	assert(analyzer->attributes.as_object()["smoothing"] == false);
	assert(analyzer->bus_attributes().is_object());
	assert(analyzer->bus_attributes().as_object()["device"] == "/dev/ttyUSB0");
	assert(analyzer->bus_attributes().as_object()["baudrate"] == 9600);
	assert(analyzer->bus_attributes().as_object()["flowcontrol"] == false);
	assert(analyzer->bus_attributes().as_object()["delay"] == 0.5);
	THROWS(analyzer->bus_name(), false);
	THROWS(analyzer->bus_attributes(), false);
	THROWS(analyzer->interrupted(), false);
	assert(analyzer->number_of_points_values() == util::nop_vals_t({3, 11, 51, 101, 501, 1001}));
	assert(analyzer->number_of_points() == 3);
	THROWS(analyzer->number_of_points(11), false);
	assert(analyzer->number_of_points() == 11);
	assert(analyzer->s_parts().size() > 1);
	THROWS(analyzer->s_part(2, 1), false);
	assert(analyzer->s_part() == util::s_part_t({2, 1}));
	THROWS(analyzer->f_right(2000), false);
	assert(analyzer->f_right() == 2000);
	THROWS(analyzer->f_left(1000), false);
	assert(analyzer->f_left() == 1000);
	assert(analyzer->f_center() == 1500);
	assert(analyzer->f_span() == 1000);
	THROWS(analyzer->f_center(3500), false);
	assert(analyzer->f_center() == 3500);
	assert(analyzer->f_left() == 3000);
	assert(analyzer->f_right() == 4000);
	assert(analyzer->f_span() == 1000);
	THROWS(analyzer->f_span(1000), false);
	assert(analyzer->f_span() == 1000);
	assert(analyzer->f_center() == 3500);
	assert(analyzer->f_left() == 3000);
	assert(analyzer->f_right() == 4000);
	THROWS(analyzer->measure(), false);
	THROWS(analyzer->measurements(), false);
	assert(analyzer->measurements().is_object());
	assert(analyzer->measurements().as_object()["f"].is_array());
	assert(analyzer->measurements().as_object()["s_re"].is_array());
	assert(analyzer->measurements().as_object()["s_im"].is_array());
	assert(analyzer->measurements().as_object()["s_mag"].is_array());
	assert(analyzer->measurements().as_object()["s_mag_log"].is_array());
	assert(analyzer->measurements().as_object()["phase"].is_array());
	assert(analyzer->measurements().as_object()["zin_re"].is_array());
	assert(analyzer->measurements().as_object()["zin_im"].is_array());
#endif
#endif
	return 0;
}
