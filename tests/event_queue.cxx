#include <array>
#include <cassert>
#include <chrono>
#include <event_queue.hxx>
#include <iostream>
#include <thread>
#include <types.hxx>

using namespace util;
using namespace std::chrono_literals;

#define NUM_A 36
#define NUM_B 42
#define NUM_C 55

int
main(void)
{
	auto			       master_semaphore = util::notifier_t(new std::binary_semaphore(0));
	EventQueue<int>		       efd_a(100, master_semaphore);
	EventQueue<std::string>	       efd_b(100, master_semaphore);
	EventQueue<std::array<int, 3>> efd_c(100, master_semaphore);
	std::array<int, 3>	       arr{0, 0, 0};
	std::jthread		       a([&efd_a]() {
		  for (int i = 0; i < NUM_A; ++i) {
			  std::cout << "Queue A: push " << i << std::endl;
			  efd_a.push(std::move(i));
			  std::this_thread::sleep_for(3ms);
		  }
	  });
	std::jthread		       b([&efd_b]() {
		  for (int i = 0; i < NUM_B; ++i) {
			  std::cout << "Queue B: push " << i << std::endl;
			  efd_b.push(std::to_string(i));
			  std::this_thread::sleep_for(4ms);
		  }
	  });
	std::jthread		       c([&efd_c]() {
		  for (int i = 0; i < NUM_C; ++i) {
			  auto arr = std::array<int, 3>{0 + i, 1 + i, 2 + i};
			  std::cout << "Queue C: push [" << arr[0] << ", " << arr[1] << ", " << arr[2] << "]"
				    << std::endl;
			  efd_c.push(std::move(arr));
			  std::this_thread::sleep_for(5ms);
		  }
	  });
	while (master_semaphore->try_acquire_for(100ms)) {
		while (efd_a.has_event()) {
			auto value = efd_a.consume();
			std::cout << "Queue A: pop " << value << std::endl;
			arr[0]++;
		}
		while (efd_b.has_event()) {
			auto value = efd_b.consume();
			std::cout << "Queue B: pop " << value << std::endl;
			arr[1]++;
		}
		while (efd_c.has_event()) {
			auto value = efd_c.consume();
			std::cout << "Queue C: pop [" << value[0] << ", " << value[1] << ", " << value[2] << "]"
				  << std::endl;
			arr[2]++;
		}
	}
	std::cout << "Event count: [" << arr[0] << ", " << arr[1] << ", " << arr[2] << "]" << std::endl;
	assert(arr[0] == NUM_A);
	assert(arr[1] == NUM_B);
	assert(arr[2] == NUM_C);
	return 0;
}
