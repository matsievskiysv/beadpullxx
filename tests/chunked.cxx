#include <iostream>
#include <cassert>
#include <chunked.hxx>
#include <fstream>
#include <vector>

extern "C" {
#include <zlib.h>
}

#ifndef SOURCE_DIR
#error missing SOURCE_DIR definition
#endif

using namespace std;
using namespace util;

int
main(void)
{
	auto chunked_file_uncompressed = ifstream(SOURCE_DIR "/chunked_file", ios::ate);
	assert(chunked_file_uncompressed.is_open());
	auto chunked_file_uncompressed_size = chunked_file_uncompressed.tellg();
	for (int size_power = 2; size_power <= 15; size_power++) {
		int size = 2 << size_power;
		cout << "chunk size 2^" << size_power << endl;
		{
			Chunked uncompressed(Chunked::stream_t(new ifstream(SOURCE_DIR "/chunked_file")), size, false);
			assert(chunked_file_uncompressed.is_open());
			chunked_file_uncompressed.seekg(0);
			bool ended = false;
			do {
				ended		   = get<1>(uncompressed.compress_chunk());
				auto	     chunk = uncompressed.get_chunk();
				vector<char> ref_chunk(chunk.size());
				chunked_file_uncompressed.read(ref_chunk.data(), chunk.size());
				for (size_t i = 0; i < chunk.size(); i++)
					assert(chunk.at(i) == ref_chunk.at(i));
			} while (!ended);
			chunked_file_uncompressed.peek();
			assert(chunked_file_uncompressed.eof());
		}
		{
			Chunked compressed(Chunked::stream_t(new ifstream(SOURCE_DIR "/chunked_file")), size, true);
			assert(chunked_file_uncompressed.is_open());
			vector<uint8_t> compressed_data;
			compressed_data.reserve(chunked_file_uncompressed_size);
			vector<uint8_t> uncompressed_data(chunked_file_uncompressed_size);
			size_t		uncompressed_data_size = chunked_file_uncompressed_size;
			bool		ended		       = false;
			do {
				ended	   = get<1>(compressed.compress_chunk());
				auto chunk = compressed.get_chunk();
				compressed_data.insert(compressed_data.end(), chunk.begin(), chunk.end());
			} while (!ended);
			auto rv = uncompress(uncompressed_data.data(), &uncompressed_data_size, compressed_data.data(),
					     compressed_data.size());
			assert(rv == Z_OK);
			uncompressed_data.resize(uncompressed_data_size);
			chunked_file_uncompressed.seekg(0);
			for (const auto &ch1 : uncompressed_data) {
				char ch2;
				chunked_file_uncompressed.get(ch2);
				assert(ch1 == ch2);
			}
			chunked_file_uncompressed.peek();
			assert(chunked_file_uncompressed.eof());
		}
	}
	return 0;
}
