#include <array>
#include <cassert>
#include <chrono>
#include <event_queue.hxx>
#include <iostream>
#include <string>
#include <thread>
#include <tuple>
#include <types.hxx>

using namespace util;
using namespace std::chrono_literals;

int
main(void)
{
	EventQueue<std::tuple<int, std::string>> efd(100, nullptr);
	efd.push(std::make_tuple(2, "two"));
	efd.push(std::make_tuple(1, "one"), true);
	efd.push(std::make_tuple(3, "three"));
	assert(efd.has_event());
	{
		auto [num, str] = efd.consume();
		assert(num == 1);
		assert(str == "one");
	}
	{
		auto [num, str] = efd.consume();
		assert(num == 2);
		assert(str == "two");
	}
	{
		auto [num, str] = efd.consume();
		assert(num == 3);
		assert(str == "three");
	}
	return 0;
}
