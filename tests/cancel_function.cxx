#include <cassert>
#include <chrono>
#include <condition_variable>
#include <future>
#include <memory>
#include <thread>

class Inner
{
      public:
	Inner(bool fail)
	{
		if (fail)
			throw std::runtime_error("error");
		std::this_thread::sleep_for(std::chrono::milliseconds(150));
	};
	~Inner(){};
};

class Wrapper
{
      private:
	std::shared_ptr<Inner> obj = nullptr;
	bool		       fail;

      public:
	Wrapper(bool fail) : fail(fail){};
	~Wrapper(){};

	void
	cancellable_function(std::chrono::duration<uint32_t, std::micro> timeout)
	{
		auto							    fail = this->fail;
		std::packaged_task<std::shared_ptr<Inner>(std::stop_token)> task([fail](std::stop_token token) {
			std::shared_ptr<Inner> new_obj(new Inner(fail));
			if (token.stop_requested())
				throw std::runtime_error("aborted");
			return new_obj;
		});

		auto	     result = task.get_future();
		std::jthread thr(std::move(task));

		if (result.wait_for(timeout) == std::future_status::ready) {
			obj = result.get();
		} else {
			thr.request_stop();
			thr.detach();
			throw std::runtime_error("timeout");
		}
	}
};

int
main(void)
{
	{
		auto cls = std::shared_ptr<Wrapper>(new Wrapper(true));
		try {
			cls->cancellable_function(std::chrono::milliseconds(200));
			assert(false);
		} catch (const std::exception &err) {
			assert(true);
			assert(err.what() == std::string("error"));
		}

		// wait for detached thread to finish
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
	}
	{
		auto cls = std::shared_ptr<Wrapper>(new Wrapper(false));
		try {
			cls->cancellable_function(std::chrono::milliseconds(100));
			assert(false);
		} catch (const std::exception &err) {
			assert(true);
			assert(err.what() == std::string("timeout"));
		}

		// wait for detached thread to finish
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
	}
	return 0;
}
