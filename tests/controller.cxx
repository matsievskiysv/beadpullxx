#include <algorithm>
#include <cassert>
#include <config.h>
#include <controller/controller.hxx>
#include <iostream>
#include <tuple>

using namespace controller;

static boost::json::value topic	  = nullptr;
static boost::json::value message = nullptr;

void
send_message_callback(const util::string_t &_topic, const util::attribute_t &_message)
{
	topic	= _topic;
	message = _message;
}

int
main(void)
{
#ifdef BUS_DUMMY

#define THROWS(expr, result)                                                                                           \
	do {                                                                                                           \
		bool throwed_exception = false;                                                                        \
		try {                                                                                                  \
			expr;                                                                                          \
		} catch (const std::exception &err) {                                                                  \
			throwed_exception = true;                                                                      \
		}                                                                                                      \
		assert(throwed_exception == result);                                                                   \
	} while (0)

#if 0
	Controller ctrl(std::chrono::milliseconds(200), std::chrono::milliseconds(200), send_message_callback);
	auto	   motor_list = ctrl.motor_list();
	assert(std::find(motor_list.begin(), motor_list.end(), "dummy") != motor_list.end());
	auto motor_bus_list = ctrl.motor_bus_list("dummy");
	assert(motor_bus_list.size() > 0);
	assert(std::find(motor_bus_list.begin(), motor_bus_list.end(), "dummy") != motor_bus_list.end());
	assert(!ctrl.motor_connected());
	THROWS(ctrl.motor_name(), true);
	THROWS(ctrl.motor_capabilities(), true);
	assert(topic == nullptr);
	assert(message == nullptr);
	THROWS(ctrl.set_motor(
		   "dummy", {{"name", "my_motor"}, {"current_limit", "10"}, {"ai_acceleration", false}, {"g", 9.81}},
		   "dummy", {{"device", "/dev/ttyUSB0"}, {"baudrate", 9600}, {"flowcontrol", false}, {"delay", 0.5}}),
	       true);
	assert(topic == nullptr);
	assert(message == nullptr);
	THROWS(ctrl.set_motor("dummy",
			      {{"name", "my_motor"}, {"current_limit", 10}, {"ai_acceleration", false}, {"g", 9.81}},
			      "dummy", {{"device", 10}, {"baudrate", 9600}, {"flowcontrol", false}, {"delay", 0.5}}),
	       true);
	assert(topic == nullptr);
	assert(message == nullptr);
	THROWS(ctrl.speed(50), true);
	THROWS(ctrl.return_speed(50), true);
	THROWS(ctrl.acceleration(50), true);
	THROWS(ctrl.deceleration(50), true);
	THROWS(ctrl.steppers(false), true);
	THROWS(ctrl.move_to(12), true);
	THROWS(ctrl.position(), true);
	THROWS(ctrl.move_by(33), true);
	THROWS(ctrl.set_origin(), true);
	THROWS(ctrl.stop(), true);
	THROWS(ctrl.set_motor(
		   "dummy", {{"name", "my_motor"}, {"current_limit", 10}, {"ai_acceleration", false}, {"g", 9.81}},
		   "dummy", {{"device", "/dev/ttyUSB0"}, {"baudrate", 9600}, {"flowcontrol", false}, {"delay", 0.5}}),
	       false);
	assert(topic == "motor");
	assert(message == boost::json::value({{"event", "connect"}, {"data", {{"motor", "dummy"}, {"bus", "dummy"}}}}));
	assert(ctrl.motor_connected());
	THROWS(ctrl.motor_name(), false);
	assert(ctrl.motor_name() == "dummy");
	THROWS(ctrl.motor_bus_name(), false);
	assert(ctrl.motor_bus_name() == "dummy");
	THROWS(ctrl.motor_capabilities(), false);
	assert(ctrl.motor_capabilities() == boost::json::value({{"steppers", true},
								{"speed", true},
								{"return_speed", true},
								{"acceleration", true},
								{"deceleration", true}}));
	THROWS(ctrl.speed(50), false);
	assert(ctrl.speed() == 50);
	assert(topic == "motor");
	assert(message == boost::json::value({{"event", "update"}, {"target", "speed"}, {"value", 50}}));
	THROWS(ctrl.return_speed(40), false);
	assert(ctrl.return_speed() == 40);
	assert(topic == "motor");
	assert(message == boost::json::value({{"event", "update"}, {"target", "return_speed"}, {"value", 40}}));
	THROWS(ctrl.acceleration(30), false);
	assert(ctrl.acceleration() == 30);
	assert(topic == "motor");
	assert(message == boost::json::value({{"event", "update"}, {"target", "acceleration"}, {"value", 30}}));
	THROWS(ctrl.deceleration(20), false);
	assert(ctrl.deceleration() == 20);
	assert(topic == "motor");
	assert(message == boost::json::value({{"event", "update"}, {"target", "deceleration"}, {"value", 20}}));
	THROWS(ctrl.steppers(false), false);
	assert(!ctrl.steppers());
	assert(message == boost::json::value({{"event", "update"}, {"target", "steppers"}, {"value", false}}));
	THROWS(ctrl.move_to(12), true);
	THROWS(ctrl.move_by(33), true);
	THROWS(ctrl.steppers(true), false);
	assert(ctrl.steppers());
	assert(message == boost::json::value({{"event", "update"}, {"target", "steppers"}, {"value", true}}));
	THROWS(ctrl.move_to(100), false);
	assert(ctrl.position() == 100);
	THROWS(ctrl.move_to(-100), false);
	assert(ctrl.position() == -100);
	THROWS(ctrl.move_by(10), false);
	assert(ctrl.position() == -90);
	THROWS(ctrl.move_by(-20), false);
	assert(ctrl.position() == -110);
	THROWS(ctrl.set_origin(), false);
	assert(ctrl.position() == 0);
	THROWS(ctrl.stop(), false);
	ctrl.motor_disconnect();
	assert(!ctrl.motor_connected());
	assert(topic == "motor");
	assert(message == boost::json::value({{"event", "disconnect"}}));

	auto analyzer_list = ctrl.analyzer_list();
	assert(std::find(analyzer_list.begin(), analyzer_list.end(), "dummy") != analyzer_list.end());
	auto analyzer_bus_list = ctrl.analyzer_bus_list("dummy");
	assert(std::find(analyzer_bus_list.begin(), analyzer_bus_list.end(), "dummy") != motor_bus_list.end());
	assert(!ctrl.analyzer_connected());
	THROWS(ctrl.analyzer_name(), true);
	THROWS(
	    ctrl.set_analyzer("dummy", {{"name", "my_analyzer"}, {"power", "20"}, {"smoothing", false}}, "dummy",
			      {{"device", "/dev/ttyUSB0"}, {"baudrate", 9600}, {"flowcontrol", false}, {"delay", 0.5}}),
	    true);
	THROWS(ctrl.set_analyzer("dummy", {{"name", "my_analyzer"}, {"power", 20}, {"smoothing", true}}, "dummy",
				 {{"device", 10}, {"baudrate", 9600}, {"flowcontrol", false}, {"delay", 0.5}}),
	       true);
	THROWS(
	    ctrl.set_analyzer("dummy", {{"name", "my_analyzer"}, {"power", 20}, {"smoothing", false}}, "dummy",
			      {{"device", "/dev/ttyUSB0"}, {"baudrate", 9600}, {"flowcontrol", false}, {"delay", 0.5}}),
	    false);
	assert(ctrl.analyzer_connected());
	THROWS(ctrl.analyzer_name(), false);
	assert(ctrl.analyzer_name() == "dummy");
	THROWS(ctrl.analyzer_bus_name(), false);
	assert(ctrl.analyzer_bus_name() == "dummy");
	assert(topic == "analyzer");
	assert(message ==
	       boost::json::value({{"event", "connect"}, {"data", {{"analyzer", "dummy"}, {"bus", "dummy"}}}}));
	// assert(ctrl.analyzer_capabilities() == boost::json::value({{"integration_band", true},
	// 							   {"smoothing", true},
	// 							   {"averaging", true},
	// 							   {"markers", true},
	// 							   {"quality_measure", true}}));
	ctrl.analyzer_disconnect();
	assert(!ctrl.analyzer_connected());
	assert(topic == "analyzer");
	assert(message == boost::json::value({{"event", "disconnect"}}));

	assert(ctrl.motor_attribute_choices("dummy").is_object());
	assert(ctrl.analyzer_attribute_choices("dummy").is_object());
	assert(ctrl.bus_attribute_choices("dummy").is_object());
#endif
#endif
	return 0;
}
