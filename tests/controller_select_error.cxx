#include <algorithm>
#include <cassert>
#include <config.h>
#include <controller/controller.hxx>
#include <iostream>
#include <motor/list.hxx>
#include <thread>

using namespace controller;

void
send_message_callback(const util::string_t &, const util::attribute_t &)
{
}

int
main(void)
{
#ifdef MOTOR_DUMMY
#if 0

#define THROWS(expr, result)                                                                                           \
	do {                                                                                                           \
		bool throwed_exception = false;                                                                        \
		try {                                                                                                  \
			expr;                                                                                          \
		} catch (const std::exception &) {                                                                     \
			throwed_exception = true;                                                                      \
		}                                                                                                      \
		assert(throwed_exception == result);                                                                   \
	} while (0)

	Controller ctrl(std::chrono::milliseconds(300), std::chrono::milliseconds(300), send_message_callback);
	auto	   motor_list = ctrl.motor_list();
	assert(std::find(motor_list.begin(), motor_list.end(), "failing_constructor") != motor_list.end());
	assert(!ctrl.motor_connected());
	try {
		ctrl.set_motor("failing_constructor", {}, "dummy", {});
	} catch (const std::exception &err) {
		assert(err.what() == std::string("not an object"));
	}
	try {
		ctrl.set_motor("failing_constructor", {}, "dummy",
			       {{"device", "/dev/ttyUSB0"},
				{"baudrate", 19200},
				{"flowcontrol", false},
				{"delay", 0.5},
				{"empty", nullptr},
				{"fail_on_constructor", true},
				{"fail_on_send", true},
				{"fail_on_rcv", true}});
	} catch (const std::exception &err) {
		std::cout << err.what() << std::endl;
		assert(err.what() == std::string("motor connection error"));
	}
	assert(!ctrl.motor_connected());
	assert(std::find(motor_list.begin(), motor_list.end(), "failing_timeout") != motor_list.end());
	try {
		ctrl.set_motor(
		    "failing_timeout", {}, "dummy",
		    {{"device", "/dev/ttyUSB0"}, {"baudrate", 9600}, {"flowcontrol", false}, {"delay", 0.5}});
	} catch (const std::exception &err) {
		assert(err.what() == std::string("motor connection timeout"));
	}
	assert(!ctrl.motor_connected());
	auto motor_bus_list = ctrl.motor_bus_list("failing");
	assert(std::find(motor_bus_list.begin(), motor_bus_list.end(), "failing_constructor") != motor_list.end());
	try {
		ctrl.set_motor("failing", {}, "failing_constructor", {});
	} catch (const std::exception &err) {
		assert(err.what() == std::string("bus connection error"));
	}
	assert(!ctrl.motor_connected());
	assert(std::find(motor_bus_list.begin(), motor_bus_list.end(), "failing_timeout") != motor_list.end());
	try {
		ctrl.set_motor("failing", {}, "failing_timeout", {});
	} catch (const std::exception &err) {
		assert(err.what() == std::string("motor connection timeout"));
	}
	assert(!ctrl.motor_connected());

	// wait for thread to finish
	std::this_thread::sleep_for(std::chrono::milliseconds(400));
#endif
#endif
	return 0;
}
