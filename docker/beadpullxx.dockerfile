from debian:stable

workdir /opt/beadpullxx

arg DEBIAN_FRONTEND=noninteractive

run apt-get update \
    && apt-get install -y \
    build-essential \
    cmake \
    libasio-dev \
    libboost-filesystem1.81-dev \
    libboost-json1.81-dev \
    libboost-log1.81-dev \
    libdocopt-dev \
    libserial-dev \
    libssl-dev \
    libyaml-cpp-dev \
    npm \
    pkg-config \
    zlib1g-dev \
    && apt-get autoclean \
    && rm -rf /var/lib/apt/lists/*

copy . /opt/beadpullxx
run cd /opt/beadpullxx \
    && npm install \
    && npm run build

expose 8080
expose 8443
entrypoint npm run start
